<?php
    
    Class Business_value{
        
        private $conn;
		public $mid;
		public $right=array();
		public $left=array();
		public $data=array();
          
        function __construct(){
          	 $this->CI = get_instance();
		}
		
		function user($prm){
		    $this->mid=$prm;
		    $this->directUser();
		}	
				
        public function directUser()
			{  
			    $this->data=array();
			    // for 1:1
				$result=array();
				
				$query=$this->CI->db->query("SELECT * FROM `users` where `user_posid` = '$this->mid'");
				
				foreach($query->result() as $row)
				 {
					if(strtolower($row->user_position)=='l')
							{
								$result['l'][]=$row->user_id;
							}
					 else if(strtolower($row->user_position)=='r'){
								$result['r'][]=$row->user_id;   
							}
				}
				
				if(empty($result['r'][0])){
				    $result['r'][0]='';
				}
				
				if(empty($result['l'][0])){
	                $result['l'][0]='';			    
				}
				
				return  (object) array('right'=>$result['r'][0],'left'=>$result['l'][0]);
			}
				
		public function findAmount($userIds)
			{
				$query=$this->CI->db->query("SELECT SUM(`amount`) as `amount` FROM `purchase_package` WHERE user_id IN('$userIds') and status='success'");
				
				$result=$query->row();
				
		        if($result->amount==null)
					{
					  return '0';
					}
		        else
					{
					  return $result->amount;
					}
	        }
	        
    	function array_flatten($array=null)
		  	{
				$result = array();
					 if (!is_array($array)) 
					 {
					   return false;
					 }
					 foreach ($array as $key => $value)
					 {
						if(is_array($value))
						{
							$result = array_merge($result, $this->array_flatten($value));
						} 
						else 
						{
							if($value != '')
							{
								$result[] = $value;    
							}
						}
					}
				 return $result;
			 }
				   
        public function allmember1($mid)
    		{
                $result=array();
                
                $query=$this->CI->db->query("SELECT * FROM `users` WHERE `user_posid` in ('$mid')");
                
                foreach($query->result() as $row)
                {
                	$result[]=$row->user_id;
                }
                
                array_push($this->data,$result);
                
                foreach($result as $key=>$value)
                {
                	$this->allmember1($result[$key]);
                }
                
                return $this->data;
                
        	} 
                	
        public function businessvalue()
            {
                //$this->data=array();
                $idleft=array($this->directUser()->left);
                if(count($idleft)!= 0)
                {
                	$leftarray=$this->array_flatten($this->allmember1($this->directUser()->left));
                    $arrayleft=array_merge($leftarray,$idleft);
                   // print_r($arrayleft);
                	$leftamount=$this->findAmount(implode("','",$arrayleft));
                }
                else
                {
                  	$leftamount='0'; 
                }
                
                //$this->data=array();
                
                $idright=array($this->directUser()->right);
                
                if(!empty($idright))
                {
                    $rightarray=$this->array_flatten($this->allmember1($this->directUser()->right));
                    $arrayright=array_merge($rightarray,$idright);
                    //print_r($arrayright);
                    $rightamount=$this->findAmount(implode("','",$arrayright));
                }
                else
                {
                    $rightamount='0';
                }
                
                echo  '<li class="clear_all"><p class = "lef"> Left BV </p> <p class="lef extra_add">'. $leftamount.'</p> </li>';
                echo  '<li class="clear_all"><p class = "lef"> Right BV </p> <p class="lef extra_add">'. $rightamount.'</p> </li>';  
            }
            
        public function printpackage($mid){
                $query=$this->CI->db->query("SELECT * FROM `purchase_package` where user_id='".$mid."' and status='success' order by id asc");
                
                if($query->num_rows()>0){
                    echo '<li class="clear_all"><p class = "lef"> Active Since </p> <p class="lef extra_add">'.date('Y-m-d',strtotime($query->row()->date)).' </p> </li>'; 
                }
    		    else{
    		        echo  '<li class="clear_all"><p class = "lef"> Active Since </p> <p class="lef extra_add">0000-00-00 </p> </li>';   
    		    }
            }
            
        public function printBelowMember($mid,$member=null){
                
                $this->mid=$mid;
                
                $idleft=array($this->directUser()->left);
                
                if(count($idleft)!= 0)
                {
                	$leftarray=$this->array_flatten($this->allmember1($this->directUser()->left));
                    $arrayleft=array_unique(array_merge($leftarray,$idleft));
                }
                else
                {
                  	$arrayleft=array(); 
                }
                
                //$this->data=array();
                
                $idright=array($this->directUser()->right);
                
                if(!empty($idright))
                {
                    $rightarray=$this->array_flatten($this->allmember1($this->directUser()->right));
                    $arrayright=array_unique(array_merge($rightarray,$idright));
                }
                else
                {
                    $arrayright=array();
                }
                
                /***********/
                
                if(!empty($member)){
                    
                    if(empty($arrayleft[0])){
                        $leftusers=array();
                    }
                    else{
                        $leftusers=array();
                        $leftusers_row=$this->activeMembers($arrayleft,1);
                        foreach($leftusers_row as $leftrow){
                            $leftusers[]=$leftrow->user_id;
                        }
                    }
                    
                    if(empty($arrayright[0])){
                        $rightusers=array();
                    }
                    else{
                        $rightusers=array();
                        $rightusers_row=$this->activeMembers($arrayright,1);
                        foreach($rightusers_row as $rightrow){
                            $rightusers[]=$rightrow->user_id;
                        }
                    }
                    
                    return (object) array('L'=>$leftusers,'R'=>$rightusers);
                    
                }
                
                /**********/
                
                if(empty($arrayleft[0])){
                    $leftusers=0;
                    $unpaidleft=0;
                }
                else{
                    $leftusers=$this->activeMembers($arrayleft);
                    $unpaidleft=count($arrayleft)-$this->activeMembers($arrayleft);
                }
                
                if(empty($arrayright[0])){
                    $rightusers=0;
                    $unpaidright=0;
                }
                else{
                    $rightusers=$this->activeMembers($arrayright);
                    $unpaidright=count($arrayright)-$this->activeMembers($arrayright);
                }
                
                echo  '<li class="clear_all"><p class = "lef"> Active User </p> <p class="lef extra_add"> L-'.$leftusers.' | R-'.$rightusers.' </p> </li>';
                echo  '<li class="clear_all"><p class = "lef"> InActive User</p> <p class="lef extra_add">L-'.$unpaidleft.' | R-'.$unpaidright.'</p> </li>';
        }
        
        
        public function activeMembers($user_id,$userArray=null){
            $user_id=implode("','",$user_id);
            $query = CI()->db->query("SELECT distinct(`user_id`) FROM  `purchase_package` WHERE `purchase_package`.status='success' and user_id in ('$user_id')");
            
            if(!empty($userArray)){
                return $query->result();
            }
            else{
                return $query->num_rows();
            }
        }
        
    }