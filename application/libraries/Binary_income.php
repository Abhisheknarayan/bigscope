<?php
    
    Class Binary_income{
        
        private $user;
        
        private $left;
        
        private $right;
        
        private $matchingAmount;
        
        private $incomeType;
        
        private $bvincomeType;
        
        private $leftBV;
        
        private $rightBV;
        
        function __construct(){
            $this->CI = get_instance();
            $this->CI->load->library('business_value');
        }
        
        function findLatestUser($user){
            $this->user=$user;
            $data=$this->CI->business_value->printBelowMember($this->user,1);
            $this->right=$data->R;
            $this->left=$data->L;
        }
        
        function matchingAmount($users){ // income users
        
            if($this->incomeType=='2:1'){
                $leftone=$this->CI->business_value->findAmount($users[0]);
                $lefttwo=$this->CI->business_value->findAmount($users[1]);
                $rightone=$this->CI->business_value->findAmount($users[2]);

                //$matchingAmount=($leftone+$lefttwo)/2;
                
                //$matchingAmount=min(array($leftone,$lefttwo,$rightone));
                
                $leftBV=$leftone+$lefttwo;
                $rightBV=$rightone;
            }
            else if($this->incomeType=='1:2'){
                $leftone=$this->CI->business_value->findAmount($users[0]);
                $rightone=$this->CI->business_value->findAmount($users[1]);
                $righttwo=$this->CI->business_value->findAmount($users[2]);
                
                //$matchingAmount=($rightone+$righttwo)/2;
                
                //$matchingAmount=min(array($leftone,$rightone,$righttwo));
                
                $leftBV=$leftone;
                $rightBV=$rightone+$righttwo;
            }     
        
            $this->leftBV=$leftBV+$this->carryForwordedAmount()->left;
            $this->rightBV=$rightBV+$this->carryForwordedAmount()->right;
            
            $bv=array($this->leftBV,$this->rightBV);
            $max=max($bv);
            
            if(isset($max)){
                $maxBV=array_keys($bv,$max);
                
                if($maxBV[0]==1 && count($maxBV)==1){
                        $this->bvincomeType='1:2';
                    }
                else if($maxBV[0]==0){
                        $this->bvincomeType='2:1';
                    }
            }
            
            if($this->bvincomeType=='1:2'){
                
                if(($this->rightBV/2)>=$this->leftBV){
                    $matchingAmount=$this->leftBV;
                }
                else if(($this->rightBV/2)<$this->leftBV){
                    $matchingAmount=($this->rightBV/2);
                }
                
            }
            else if($this->bvincomeType=='2:1'){
                
                if(($this->leftBV/2)>=$this->rightBV){
                    $matchingAmount=$this->rightBV;
                }
                else if(($this->leftBV/2)<$this->rightBV){
                    $matchingAmount=($this->leftBV/2);
                }
            }
            
            if(!empty($matchingAmount) || $matchingAmount>0){
                $this->matchingAmount=$matchingAmount;
                return $matchingAmount;
            }
            else{
                return false;
            }
        }
        
        function cappingAmount(){
            $selfAmount=my_package_amount($this->user);
            $query=$this->CI->db->query("SELECT * FROM `binary_capping` WHERE '$selfAmount' BETWEEN `amount` and `max_amount` order by id desc");
            
            if($query->num_rows()>0){
                return $query->row()->capping_amount;
            }
            else{
                return false;
            }
        }
        
        function carryForward(){
		
				$leftCarry=0;
				$rightCarry=0;
				
				// $leftAmount=$this->CI->business_value->findAmount(implode("','",$this->left));
				
				// if($this->matchingAmount >= $leftAmount){
				// 	$leftCarry=0;
				// }
				// else{
				// 	foreach($this->left as $us_er){
				// 	    echo 'left';
				// 	    $val=$this->CI->business_value->findAmount($us_er);
				// 		$leftCarry+=$val-$this->matchingAmount;
				// 	}	
				// }
				
				// if($this->matchingAmount >= $this->CI->business_value->findAmount(implode("','",$this->right))){
				// 	$rightCarry=0;
				// }
				// else{
				// 	foreach($this->right as $us_er){
				// 	    echo 'right';
				// 	    echo $val=$this->CI->business_value->findAmount($us_er);
				// 		$rightCarry+=$val-$this->matchingAmount;
				// 	}	
				// }
			
			$leftAmount=$this->leftBV;	
			$rightAmount=$this->rightBV;
			
			if($this->bvincomeType=='2:1'){
			    $leftCarry=$leftAmount-($this->matchingAmount*2);
			    $rightCarry=$rightAmount-$this->matchingAmount;
			}
			else if($this->bvincomeType=='1:2'){
			    $leftCarry=$leftAmount-$this->matchingAmount;
			    $rightCarry=$rightAmount-($this->matchingAmount*2);
			}
				
			return (object) array('left'=>$leftCarry,'right'=>$rightCarry);
		}
        
        function carryForwordedAmount(){
			$sql=$this->CI->db->query("SELECT SUM(`leftmatching`) as `left`,SUM(`rightmatching`) as `right` FROM `my_income1` WHERE `usid`='$this->user' and status='0'");
			if($sql->num_rows()>0){
				$row=$sql->row();
				if(empty($row->left)){
				    $row->left=0;
				}
				if(empty($row->right)){
				    $row->right=0;
				}
				return $row;
			}
			else{
				return (object) array('left'=>'0','right'=>'0');
			}
		}
        
        function incomeAmount(){
			if($this->matchingAmount<=$this->cappingAmount()){
				return $this->matchingAmount;
			}
			else{
				return $this->cappingAmount();
			}
		}
        
        function generateIncome($amt){
            $binaryPercent=binary_income_percent();
            $incomeAmount=(($this->incomeAmount()*$binaryPercent)/100);
            $perAmount=$this->incomeAmount();
            $carry=$this->carryForward();
            print_r($carry);
            $carry_left=$carry->left;
            $carry_right=$carry->right;
            $this->CI->db->query("UPDATE `my_income1` SET `status`='1' WHERE `usid`='$this->user'");
            $this->CI->db->query("INSERT INTO `my_income1` (`usid`, `matching_amt`, `capping_amt`,`amount`, `leftmatching`, `rightmatching`, `percentage`, `tm`, `income_type`) VALUES('$this->user','$amt','$perAmount', '$incomeAmount', '$carry_left', '$carry_right','$binaryPercent','".date('Y-m-d H:i:s')."', '$this->bvincomeType')");
        }
        
        
        
        function generateBinary(){
            $date=date('Y-m-d H:i:s');
            $this->userExist();
            if($this->incometype()){
                if($this->incomeType=='2:1'){
                    $user=array($this->left[0],$this->left[1],$this->right[0]);
                    $users=implode(',',$user);
                    $this->CI->db->query("INSERT INTO `income_status` (`member_id`, `name`, `posid`,`income_type`,`date_`) VALUES ('$this->user', '$users', 'L','$this->incomeType','$date')");
                    $amt=$this->matchingAmount($user);
                }
                else if($this->incomeType=='1:2'){
                    $user=array($this->left[0],$this->right[0],$this->right[1]);
                    $users=implode(',',$user);
                    $this->CI->db->query("INSERT INTO `income_status` (`member_id`, `name`, `posid`,`income_type`,`date_`) VALUES ('$this->user', '$users', 'R','$this->incomeType','$date')");
                    $amt=$this->matchingAmount($user);
                }  
                $this->generateIncome($amt);
            }
        }
        
        function userExist(){
            $data=$this->CI->db->query("SELECT GROUP_CONCAT(`name`) as `members` FROM `income_status` where member_id='$this->user'")->row();
            $existData=explode(',',$data->members);
            $this->left=array_values(array_diff($this->left,$existData));
            $this->right=array_values(array_diff($this->right,$existData));
        }
        
        function incometype(){
            if(count($this->left)>=2 && count($this->right)>=1){
                $this->left=array($this->left[0],$this->left[1]);
                $this->right=array($this->right[0]);
                return $this->incomeType='2:1';
            }
            else if(count($this->left)==1 && count($this->right)>=2){
                $this->left=array($this->left[0]);
                $this->right=array($this->right[0],$this->right[1]);
                return $this->incomeType='1:2';
            }
            return false;
        }
    }