<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="Bigscopecurrency Landing Page HTML5 Template">
    <meta name="keywords" content="Bigscopecurrency, bitcoin, bitcoin landing, blockchain, crypto trading ">
    <meta name="author" content="EcologyTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Coupon Mall</title>
    <!-- <link rel="shortcut icon" href="<?= base_url('resource/front/') ?>images/favicon.png" type="image/x-icon"> -->
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/bootstrap.min.css">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>fonts/flaticon/flaticon.css">
    <!-- popup-->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/owl.theme.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/jquery.countdown.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/animate.css">
    <!-- preloader css-->    
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/preloader.css">
    <!-- main style-->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/cryptonio.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/responsive.css">
    
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
                            
          <style type="text/css">
              .ptb-10{padding: 10px 0px 10px 0px}
              .text-left{text-align: left!important;}

              .list li{list-style-type: circle;}

                .border-divide{border-bottom: solid #3c3c3c 1px;
    padding: 22px;}
          </style>                  
                            
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="body_06">

<div class="particle">
<canvas id="canvas" style="width:100%"></canvas>
</div>
<!-- Preloader -->
<!-- <div id="loader-wrapper"> -->
    <!-- <div id="loader"></div> -->
    <!-- <div class="loader-section section-left"></div> -->
    <!-- <div class="loader-section section-right"></div> -->
<!-- </div> -->
<header id="header-06" class="header bg5">





    <!-- START NAVBAR -->
    <nav class="navbar navbar-expand-md navbar-light bg-faded cripto_nav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <a class="navbar-brand"  data-scroll  href="#"><img src="<?= base_url('resource/front/') ?>images/logo.png" alt="logo" style=".navbar-brand img {
    width: 49%;
}"></a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll href="home#header-06" class="nav-link active">Home</a></li>
                    <li class="nav-item"><a data-scroll href="#welcome_cryptonic_06" class="nav-link">About</a></li>
                    <li class="nav-item"><a data-scroll href="#token_sale_06" class="nav-link">BCO Value</a></li>
                    <li class="nav-item"><a data-scroll href="#roadmap_06" class="nav-link">Business Process</a></li>
                                 
                    <li class="nav-item"><a data-scroll href="#faq-area-06" class="nav-link">Faq</a></li>
                    <li class="nav-item"><a data-scroll href="#contact_06" class="nav-link">Contact</a></li>
					 <li class="nav-item"><a target="_blank" data-scroll href="<?= base_url("home/login") ?>" class="nav-link">Log in</a></li>
                    <li class="nav-item"><a target="_blank" data-scroll href="<?= base_url("home/register") ?>" class="nav-link ">Sign up</a></li>
                </ul>
            </div>
            
        </div>
    </nav><!-- END NAVBAR -->

    
    <span class="shape3 header-shape"><img src="<?= base_url('resource/front/') ?>images/about-bg.png" alt=""></span>  
    
    <div id="particles1-js" class="particles"></div>  
</header> <!-- End Header -->


<section id="welcome_cryptonic_06">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                    <h2 class=" wow fadeInUp" data-animate="fadeInUp" data-delay=".1s">PRIVACY POLICY</h2>

                </div>
            </div>
        </div>      
        <div class="row ">     
            <div class="col-sm-12 col-md-7 col-lg-7 mx-auto">  

                <p class=" wow fadeInUp" data-animate="fadeInUp" data-delay="0.3s">"The privacy policy description has the information how Mycouponmall stores, processes and utilizes the personal and public data of website visitors. It is recommended for every visitor to go through our privacy policy norms carefully before visiting our website, and using our services."</p>     
                    
                    <P class="about-text">The privacy guidelines mentioned in this privacy policy description are applicable to data that is accessible and processed by website visitors.</P>
                    
                    <a href="JavaScript:Void(0)" class="btn btn-default btn-default-style wow fadeInUp about-text-show" data-animate="fadeInUp" data-delay="0.4s">Read More</a>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5 mx-auto">
                <div class="">
                    <!-- about_cryptonic-content -->
                    <img src="<?= base_url('resource/front/') ?>images/privacy-policy.png" alt=""  class="wow ZoomInUp" data-animate="ZoomInUp" data-delay=".3s">
                    
                </div>
            </div>

        </div>

<div class="ptb-10"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="sub-title">
             <h2 class="text-left wow fadeInUp" data-animate="fadeInUp" data-delay=".1s">Major Points to discuss</h2>
            </div>
            
            <p class="text-left">Anyone who visits mycouponmall, collecting details of user like ISP, operating system, ISP, etc., is helpful for us to identify the visitors. At the same time, when visitors register with mycouponmall, they need to enter common information such as user name, phone number, mail contact, etc. We also ask users' name and contact details at the time when he/she requests a query, or call back from us for support.

We also use cookies for internal purposes in order to collect user habits and interests so as to offer better service experience to end users.
</p>

            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="sub-title">
             <h2 class="text-left wow fadeInUp" data-animate="fadeInUp" data-delay=".1s">Using your information</h2>
            </div>
            
            <p class="text-left">Mycouponmall website uses your personal information to fulfil business goals & make easy availability of services. We collect information for following objectives:</p>

            <ul  class="text-left list">
                <li>Fulfillment of quotes</li>
                <li>Sales follow ups</li>
                <li>Business follow-ups</li>
                <li>Help desk for services</li>

            </ul>
            <p class="text-left">Mycouponmall never shares, sells or exchanges the personal information with any third party until and unless you agree. </p>

            </div>

        </div>

          <div class="row">
            <div class="col-md-12">
                <div class="sub-title">
             <h2 class="text-left wow fadeInUp" data-animate="fadeInUp" data-delay=".1s">Protecting your  Information</h2>
            </div>

            <ul class="text-left list">
                <li>Mycouponmall uses highly advanced security tools and guidelines for storing and processing  your data from unauthorized access.</li>
                <li>A user is free to modify, change or correct personal information by contacting us and discussing it for modification.</li>
                <li>If any user wants to disconnect you can simply drop us the information through mail </li>
                <li>We will store your information till valid date of registration</li>            </ul>
            <div class="ptb-10"></div>
            <h4 class="text-primary text-left">Legal and Regulatory obligations</h4>
           <p class="text-left">We are bound to disclose your personal information to government or legal authorities in case of some legal obligations. Still, we will notify you before proceeding for such request. </p>


           <div class="ptb-10"></div>
            <h4 class="text-primary text-left">Update or modification of Privacy statement</h4>
           <p class="text-left">We may change our privacy policy anytime depending upon our requirements, changing legal procedures or update in cyber rules. We will notify you about the changes through mail. We still recommend you to keep checking for our privacy policy changes.</p>

            </div>

        </div>






    </div>
    <span class="shape1 header-shape"><img src="<?= base_url('resource/front/') ?>images/docs-bg.png" alt=""></span>
    
    <span class="bubble2 header-shape"><img width="40%" src="<?= base_url('resource/front/') ?>images/white-round-sm.svg" alt=""></span> 
    <div id="particles2-js" class="particles"></div>  

 <div class="border-divide"></div>
</section> <!-- End welcome_cryptonic -->



<footer id="footer-06" class="footer">
    <div class="container">
        <div class="row footer-btm-wrapper">
            <div class="col-md-12 col-lg-12">
                <div class="footer_items">
                    <div class="footer-single-col footer_single_list_1">
                        <h3 class="subtitle_1">&nbsp;<img src="<?= base_url('resource/front/') ?>images/logo.png" alt=""></h3>
                        <p>
                           My Coupon Mall Known for financial services advisor for a decade.we brings a smart structured & organised
                           plans for leader and investors.This is a process to enhance the market 
                           & make a magnificient changes to world.which works on marketing strategy for professionals who 
                           looking for world calss plan for individual benifits.</p>
                        <ul class="social-links list-unstyled ml-auto">
                            <li class="nav-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>

                    <div class="footer-single-col footer_single_list_2">
                        <h3 class="subtitle_1">Company</h3> 
                        <ul class="list-unstyled">
                            	<li><a href="<?=base_url()?>">Home</a></li>
							<li><a href="#welcome_cryptonic_06">About us</a></li>
							<li><a href="#roadmap_06">Business Packages</a></li>
                        </ul>
                        
                           
                        
                    </div>

                    <div class="footer-single-col footer_single_list_3">
                        <h3 class="subtitle_1">Contact Us</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">If You Have Any Query, Please Contact Us On Given Email Id.</a></li>
                            <li><a href="#">Info@mycouponmall.co</a></li>
                       
                        </ul>
                    </div>


                    
                </div>                  
            </div>
        </div>       
    </div>  
	
	
		
	
	
	
	
	
    <div class="copyright">
        <p> Copyright &copy; 2020, Created  by <span>MyCouponMall</span></p>            
    </div>   
    <span class="shape1 header-shape"><img src="images/shape/home_6/footer_shape_1.png" alt=""></span>
    <span class="shape2 header-shape"><img src="images/shape/home_6/footer_shape_2.png" alt=""></span>
    <span class="shape3 header-shape"><img src="images/shape/home_6/footer_shape_3.png" alt=""></span>           
</footer><!-- ./ End Footer Area-->

    <!-- JavaScript Files -->
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery-3.2.1.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/popper.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/bootstrap.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/owl.carousel.min.js"></script>    
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.sticky.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/isotope.pkgd.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.downCount2.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.countdown.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/particles.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/app.js"></script>      
    <script src="<?= base_url('resource/front/') ?>js/assets/smooth-scroll.js"></script> 
    <script src="<?= base_url('resource/front/') ?>js/assets/wow.min.js"></script>    
    <script src="<?= base_url('resource/front/') ?>js/assets/wow.min.js"></script>   	
    <script src="<?= base_url('resource/front/') ?>js/custom.js"></script>  
	
	<script>
$(document).ready(function(){
  $(".about-text-show").click(function(){
    $(".about-text").fadeToggle("fast")
  });
});
</script>

	
	
	<script>
	"use strict";

var canvas = document.getElementById('canvas'),
  ctx = canvas.getContext('2d'),
  w = canvas.width = window.innerWidth,
  h = canvas.height = window.innerHeight,
    
  hue = 217,
  stars = [],
  count = 0,
  maxStars = 1400;

// Thanks @jackrugile for the performance tip! https://codepen.io/jackrugile/pen/BjBGoM
// Cache gradient
var canvas2 = document.createElement('canvas'),
    ctx2 = canvas2.getContext('2d');
    canvas2.width = 100;
    canvas2.height = 100;
var half = canvas2.width/2,
    gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
    gradient2.addColorStop(0.025, '#fff');
    gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
    gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
    gradient2.addColorStop(1, 'transparent');

    ctx2.fillStyle = gradient2;
    ctx2.beginPath();
    ctx2.arc(half, half, half, 0, Math.PI * 2);
    ctx2.fill();

// End cache

function random(min, max) {
  if (arguments.length < 2) {
    max = min;
    min = 0;
  }
  
  if (min > max) {
    var hold = max;
    max = min;
    min = hold;
  }

  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function maxOrbit(x,y) {
  var max = Math.max(x,y),
      diameter = Math.round(Math.sqrt(max*max + max*max));
  return diameter/2;
}

var Star = function() {

  this.orbitRadius = random(maxOrbit(w,h));
  this.radius = random(60, this.orbitRadius) / 12;
  this.orbitX = w / 2;
  this.orbitY = h / 2;
  this.timePassed = random(0, maxStars);
  this.speed = random(this.orbitRadius) / 50000;
  this.alpha = random(2, 10) / 10;

  count++;
  stars[count] = this;
}

Star.prototype.draw = function() {
  var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
      y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
      twinkle = random(10);

  if (twinkle === 1 && this.alpha > 0) {
    this.alpha -= 0.05;
  } else if (twinkle === 2 && this.alpha < 1) {
    this.alpha += 0.05;
  }

  ctx.globalAlpha = this.alpha;
    ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
  this.timePassed += this.speed;
}

for (var i = 0; i < maxStars; i++) {
  new Star();
}

function animation() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 0.8;
    ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 1)';
    ctx.fillRect(0, 0, w, h)
  
  ctx.globalCompositeOperation = 'lighter';
  for (var i = 1, l = stars.length; i < l; i++) {
    stars[i].draw();
  };  
  
  window.requestAnimationFrame(animation);
}

animation();
	</script>
	
	
	
	<script 
src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false">
</script>
 <script> window.cookieconsent.initialise({ "palette": { "popup": { "background": "#000" }, "button": { "background": "#f1d600" } }, "theme": "classic" }); 
</script>
	
	
</body>

</html>
