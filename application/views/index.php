<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="Bigscopecurrency Landing Page HTML5 Template">
    <meta name="keywords" content="Bigscopecurrency, bitcoin, bitcoin landing, blockchain, crypto trading ">
    <meta name="author" content="EcologyTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Coupon Mall</title>
    <!-- <link rel="shortcut icon" href="<?= base_url('resource/front/') ?>images/favicon.png" type="image/x-icon"> -->
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/bootstrap.min.css">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>fonts/flaticon/flaticon.css">
    <!-- popup-->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/owl.theme.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/jquery.countdown.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/animate.css">
    <!-- preloader css-->    
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/assets/preloader.css">
    <!-- main style-->
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/cryptonio.css">
    <link rel="stylesheet" href="<?= base_url('resource/front/') ?>css/responsive.css">
    
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
                            
                            
                            
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="body_06">

<div class="particle">
<canvas id="canvas" style="width:100%"></canvas>
</div>
<!-- Preloader -->
<!-- <div id="loader-wrapper"> -->
    <!-- <div id="loader"></div> -->
    <!-- <div class="loader-section section-left"></div> -->
    <!-- <div class="loader-section section-right"></div> -->
<!-- </div> -->
<header id="header-06" class="header bg5">





    <!-- START NAVBAR -->
    <nav class="navbar navbar-expand-md navbar-light bg-faded cripto_nav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <a class="navbar-brand"  data-scroll  href="#"><img src="<?= base_url('resource/front/') ?>images/logo.png" alt="logo" style=".navbar-brand img {
    width: 49%;
}"></a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll href="#header-06" class="nav-link active">Home</a></li>
                    <li class="nav-item"><a data-scroll href="#welcome_cryptonic_06" class="nav-link">About</a></li>
                    <li class="nav-item"><a data-scroll href="#token_sale_06" class="nav-link">BCO Value</a></li>
                    <li class="nav-item"><a data-scroll href="#roadmap_06" class="nav-link">Business Process</a></li>
                                 
                    <li class="nav-item"><a data-scroll href="#faq-area-06" class="nav-link">Faq</a></li>
                    <li class="nav-item"><a data-scroll href="#contact_06" class="nav-link">Contact</a></li>
					 <li class="nav-item"><a target="_blank" data-scroll href="<?= base_url("home/login") ?>" class="nav-link">Log in</a></li>
                    <li class="nav-item"><a target="_blank" data-scroll href="<?= base_url("home/register") ?>" class="nav-link ">Sign up</a></li>
                </ul>
            </div>
            
        </div>
    </nav><!-- END NAVBAR -->
    <div class="container ">
        <div class="row">                
            <div class="col-12 col-md-12 col-lg-6">
                <div class="intro-text">
                    <h1><span style="    color: #00e8e2;">Our Exertions- </span> Your trust, Your Benefit</h1>
                    <h4></h4>
                    <p class="title-text-add">Upgrade the strategic investment with us
					</p>
                    <?php echo $this->session->flashdata('enquiry');?>
                    
                </div>
            </div>
            <div class="col-sm-6 col-md-12 col-lg-6 img-wrapper">
                
				
			
			
			
			

				
				
                    <div class="bounce_wrapper">
                        
                       
						 	<div class="circle-ripple"></div>
                           <img src="<?= base_url('resource/front/') ?>images/getting-started.png" alt="">
                         
                    </div>
                
            </div>               
        </div>  
    </div>
    <span class="shape1 header-shape"><img src="<?= base_url('resource/front/') ?>images/shape/home_6/header6_shape_1.png" alt=""></span>
    <span class="shape2 header-shape"><img src="<?= base_url('resource/front/') ?>images/shape/home_6/header6_shape_2.png" alt=""></span>
    <span class="shape3 header-shape"><img src="<?= base_url('resource/front/') ?>images/about-bg.png" alt=""></span>  
    <span class="shape4 header-shape "><img src="<?= base_url('resource/front/') ?>images/shape/home_6/header6_shape_4.png" alt=""></span>   
    <span class="shape5 header-shape"><img src="<?= base_url('resource/front/') ?>images/shape/home_6/header6_shape_5.png" alt=""></span>     
    <span class="shape6 header-shape"><img src="<?= base_url('resource/front/') ?>images/shape/home_6/header6_shape_6.png" alt=""></span>     

    <span class="bubble1 header-shape"><img src="<?= base_url('resource/front/') ?>images/particals_icon/fixed1.png" alt=""></span>   
    <span class="bubble2 header-shape"><img src="<?= base_url('resource/front/') ?>images/particals_icon/fixed1.png" alt=""></span>     
    <span class="bubble3 header-shape"><img src="<?= base_url('resource/front/') ?>images/particals_icon/fixed2.png" alt=""></span>
    <span class="bubble4 header-shape"><img src="<?= base_url('resource/front/') ?>images/particals_icon/fixed4.png" alt=""></span>  

    <div id="particles1-js" class="particles"></div>  
</header> <!-- End Header -->


<section id="welcome_cryptonic_06">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                    <h2 class=" wow fadeInUp" data-animate="fadeInUp" data-delay=".1s">Welcome to the My Coupon Mall</h2>

                    <p>Benefit of joining membership program with us</p>
                </div>
            </div>
        </div>      
        <div class="row ">     
            <div class="col-sm-12 col-md-10 col-lg-10 mx-auto">
                <div class="">
                    <!-- about_cryptonic-content -->
                    <img src="<?= base_url('resource/front/') ?>images/welcome-6.png" alt=""  class="wow ZoomInUp" data-animate="ZoomInUp" data-delay=".3s">
                    <p class=" wow fadeInUp" data-animate="fadeInUp" data-delay="0.3s">Mycouponmall membership program is open for drivers, passengers and other people. Registration here is very simple, and the platform is open for everyone to become our member by investing a small pre decided amount.

                    
                    
                    
                    </p>
                    
                    
                    <P class="about-text">This is a process to enhance the market & make a magnificient changes to world.which works on marketing strategy for professionals who looking for world calss plan for individual benifits.</P>
                    
                    <a href="JavaScript:Void(0)" class="btn btn-default btn-default-style wow fadeInUp about-text-show" data-animate="fadeInUp" data-delay="0.4s">Read More</a>
                </div>
            </div>           
        </div>         
    </div>
    <span class="shape1 header-shape"><img src="<?= base_url('resource/front/') ?>images/docs-bg.png" alt=""></span>
    <span class="bubble1 header-shape"><img width="40%" src="<?= base_url('resource/front/') ?>images/white-round-sm.svg" alt=""></span>   
    <span class="bubble2 header-shape"><img width="40%" src="<?= base_url('resource/front/') ?>images/white-round-sm.svg" alt=""></span> 
    <div id="particles2-js" class="particles"></div>  
</section> <!-- End welcome_cryptonic -->

<!-- <section id="about_cryptonic_06"> -->
    <!-- <div class="container"> -->
        <!-- <div class="row">         -->
            <!-- <div class="col-sm-12 col-md-7 col-lg-5 padding-none"> -->
                <!-- <div class="about_cryptonic-content"> -->
                    <!-- <h2 class=" wow fadeInUp" data-animate="fadeInUp" data-delay="0.2s">Bigscope is The Best for your ICO</h2> -->
                    <!-- <p class=" wow fadeInUp" data-animate="fadeInUp" data-delay="0.3s">Artificial intelligence based on neural networks, built using the newest algorithms for self-learning, analysis and comparison of neurons in which will be self-corrected, based on the history of success and failure, taking into account the correlation of the objects of analysis.</p> -->

                    <!-- <p class=" wow fadeInUp" data-animate="fadeInUp" data-delay="0.4s">Due to the use of large computing power and artificial based on the neural network, the NRM assistant will instantly analyze user data and offer solutions for their further use.</p> -->
                    <!-- <a href="#" class="btn btn-default btn-default-style wow fadeInUp" data-animate="fadeInUp" data-delay="0.5s">Download Whitepaper</a> -->
                <!-- </div> -->
            <!-- </div> -->
            <!-- <div class="col-sm-12 col-md-5 col-lg-7"> -->
                <!-- <div class="about-img"> -->
                    <!-- <div class="img-wrapper"> -->
                         <!-- <img src="images/index-about.png" alt=""  class="wow fadeInUp" data-animate="fadeInUp" data-delay=".3s"> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div>             -->
        <!-- </div>     -->
    <!-- </div> -->
    <!-- <span class="shape1 header-shape"><img src="images/shape/home_6/about-light-1.png" alt=""></span> -->
    <!-- <span class="shape2 header-shape"><img src="images/shape/home_6/about-light-2.png" alt=""></span> -->
    <!-- <span class="bubble1 header-shape"><img src="images/particals_icon/fixed1.png" alt=""></span>    -->
    <!-- <span class="bubble2 header-shape"><img src="images/particals_icon/fixed2.png" alt=""></span>      -->
    <!-- <span class="bubble3 header-shape"><img src="images/particals_icon/fixed3.png" alt=""></span> -->
    <!-- <div id="particles3-js" class="particles"></div> -->
<!-- </section> <!-- End about_cryptonic --> 


<section id="best_feature_06">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                 
                    <h2 class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">Features</h2>
                    <!-- <h2>Best Features</h2> -->
                    <!--<p>Due to the use of large computing power is assistant will instantly analyze user data offer solutions for their further use orem Ipsum has been the industry's st</p>-->
                </div>
            </div>
        </div>        
        <div class="row feature_items d-flex justify-content-between">        
           



		 
	
	
	
		  <!-- <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12"> -->
    <!-- <div class="hovereffect"> -->
        <!-- <img class="img-responsive" src="images/bg2.jpg" alt=""> -->
            <!-- <div class="overlay"> -->
               <!-- <div class="pricing--heading text--center"> -->
                        <!-- <div class="pricing--icon"> -->
                          
						  <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        <!-- </div> -->
                        <!-- <h4>Starter Plan </h4> -->
                        <!-- <p><span class="currency"></span>Bigscope 180</p> -->
                        <!-- <div class="pricing--desc"> -->
						  <!-- <div class="start_style"> -->
                         <!-- <ul><li>  Invest - 100 USD</li> -->
<!-- <li> Daily Return - 1%</li> -->
<!-- <li> Return Days - 180</li> -->
<!-- <li> Return - 180 USD</li> -->
<!-- </ul> -->
 <!-- </div> -->
   <!-- <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="http://bigscope.co/bigscope/home/login"> Join Now</a> -->
                        <!-- </div> -->
                      
                    <!-- </div> -->
            <!-- </div> -->
			<!-- <div class="waveWrapper waveAnimation"> -->
  <!-- <div class="waveWrapperInner bgTop"> -->
    <!-- <div class="wave waveTop" style="background-image: url('././images/shape/home_6/header6_shape_6.png')"></div> -->
  <!-- </div> -->
  <!-- <div class="waveWrapperInner bgMiddle"> -->
    <!-- <div class="wave waveMiddle" style="background-image: url('././images/shape/home_6/2.png')"></div> -->
  <!-- </div> -->
  <!-- <div class="waveWrapperInner bgBottom"> -->
    <!-- <div class="wave waveBottom" style="background-image: url('././images/shape/home_6/3.png')"></div> -->
  <!-- </div> -->
<!-- </div> -->
    <!-- </div> -->
<!-- </div> -->









<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						 <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Easy to use
						 </li>
						 
						 <li> Mycouponmall membership program is built upon a very simple and easy to use software interface. Anyone with basics of internet technology can register here and avail the benefits.

						 </li>
</ul>
 </div>
                        </div>
                       
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>



<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						  <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Trust of Security
						 </li>
						 
						 <li>  Mycouponmall brings ahead the quickest, trusted, secure and reliable solution to let this system work smoothly. We use secure online payment environments, and our wallets are secured with strong encryption technology.
						 </li>
</ul>
 </div>
                        </div>
                      
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>




<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						   <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Earnig Opportunity
						 </li>
						 
						 <li> Our exertions are always to lay down a platform where everyone associated with us can avail the benefits. To make this process become successful, we have provision of earning opportunity via binary income, direct income and referral income.
						 </li>
</ul>
 </div>
                        </div>
                       
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>




<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						  <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Quick to Response
						 </li>
						 
						 <li>  The complete mycouponmall platform is designed based on advanced and highly secure web development technology.
						 </li>
</ul>
 </div>
                        </div>
                      
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>



<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
							  <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Help and Support
						 </li>
						 
						 <li> Mycouponmall delivers an online support facility from where anyone can know about plans, features, objectives and benefits in detail.

						 </li>
</ul>
 </div>
                        </div>
                      
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            
            
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						  	  <!-- <img class="icon_size" src="http://bigscope.co/bigscope/resource/frontend/icon_logo.png"> -->
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                       
                        <div class="pricing--desc">
						  <div class="start_style">
						  
                         <ul>
						  <li class="first-li"> Help and Support
						 </li>
						 
						 <li>  Provides an online help tool to study more about Bigscope MLM Software along with 24*7 support. Contact Us to know more about Help and Support
						 </li>
</ul>
 </div>
                        </div>
                      
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>

		 
        </div>  
		
		
		
		
    </div>
    <span class="shape1 header-shape"><img src="<?= base_url('resource/front/') ?>images/shape/home_6/feature-1.png" alt=""></span>
    <div id="particles4-js" class="particles"></div>  
	
	
	
	
</section> <!-- End best_feature -->

<section id="token_sale_06">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                   
                    <h2 class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">BCO Value</h2>
                </div>
            </div>
        </div>        
        

        <div class="row pricing_bottom">        
            <div class="col-sm-12 col-md-12 col-lg-12">  
                <div class="pricing_list_wrapper">
                    <ul class="list-unstyled item_list_1  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                        
 <li>
 <h3>Pre-Sale starts</h3> <span>1 BCO = $ 1</span>
 
 </li>
 
                       
                     </ul>   
                     
                      <ul class="list-unstyled item_list_1  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                        
 <li class="text-center">
     
     <img src="<?= base_url('resource/front/images/logo.png') ?>" alt="" style="
    width: 40%;
    margin: 0 auto;
">

<p> My Coupon Mall is launching <span class="text-effect-add">10 Billon BCO</span> </p>


     
 </li>
 
                       
                     </ul>   
                     
                     
                      <ul class="list-unstyled item_list_1  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                        
 <li><a href="<?base_url()?>/home/register" class="btn btn-default add-btn-table wow fadeInUp" data-animate="fadeInUp" data-delay="0.4s" style="visibility: visible; animation-name: fadeInUp;">Buy now</a></li>
 
                       
                     </ul>   
                                        
                </div>
            </div>
        </div>  
        
        
        
   
        
        
        
        
        
        
    </div>
    <span class="shape1 header-shape"><img src="<?= base_url('resource/front/') ?>images/docs-bg.png" alt=""></span> 
    <span class="shape2 header-shape"><img src="<?= base_url('resource/front/') ?>images/about-bg.png" alt=""></span>  
   
    <div id="particles5-js" class="particles"></div>  
</section> <!-- End token_sale -->











<section id="table" class="table-add-style">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                    <!-- <h6 class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Token Details</h6> -->
                    <h2 class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">Membership Plans:</h2>
                    <p>At present we have three major membership plans that are open for any user.</p>
                </div>
				
				
				
				 <table class="table table-dark table-hover">
    <thead>
      <tr>
        <th>Package name</th>
        <th>Purchasing Amount</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td>Primary membership Plan</td>
       
        <td>$500</td>
				 <td><a href="<?base_url()?>/home/register" class="btn btn-default add-btn-table wow fadeInUp" data-animate="fadeInUp" data-delay="0.4s" style="visibility: visible; animation-name: fadeInUp;">Buy now</a></td>
      </tr>
      <tr>
        <td>
Gold Membership Plan
</td>
       
        <td>$1000</td>
		 <td><a href="<?base_url()?>/home/register" class="btn btn-default add-btn-table wow fadeInUp" data-animate="fadeInUp" data-delay="0.4s" style="visibility: visible; animation-name: fadeInUp;">Buy now</a></td>
      </tr>
      <tr>
        <td>
Diamond Membership Plan
</td>
       
        <td>$2000</td>
		 <td><a href="<?base_url()?>/home/register" class="btn btn-default add-btn-table wow fadeInUp" data-animate="fadeInUp" data-delay="0.4s" style="visibility: visible; animation-name: fadeInUp;">Buy now</a></td>
      </tr>
      
	  
    </tbody>
  </table>
				
				
			
				
				
            </div>
        </div> 
                
    </div> 
    <div id="particles6-js" class="particles"></div>
</section> <!-- End token_distribution -->




<section  id="token_sale_add">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                    <!-- <h6 class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Token Details</h6> -->
                     <h2 class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">Types of Earning Opportunities:</h2>
                    <p>Binary Income</p>
                </div>
				
				
				
				<div class="row pricing_items">        
            <div class="col-sm-12 col-md-6 col-lg-4 single-wrapper wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                <div class="pricing_single">
                    <div class="offer_price">
                        <h4>DAILY RETURN <span class="hover_text">DAILY RETURN</span>
                        <span class="shape_1"></span></h4>
                    </div>
                    <div class="offer_details">
                        <!--<span>15 Days</span>-->
                       <h3 >MyCouponMall provides a binary income opportunity depending on 1:2 or 2:1 plan at the first level.</h3>
                        <!--<p>Sofecap $2M</p>-->
                    </div>
                    
                        
                </div>
            </div>
            
            
            <div class="col-sm-12 col-md-6 col-lg-4 single-wrapper wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                <div class="pricing_single">
                    <div class="offer_price">
                        <h4>DIRECT INCOME <span class="hover_text">DIRECT INCOME</span>
                        <span class="shape_1"></span></h4>
                    </div>
                    <div class="offer_details">
                        <!--<span>15 Days</span>-->
                       <h3 >Mycouponmall gives existing user an opportunity to earn 10% of direct income on the registration of new user joining the membership program through referral code.</h3>
                        <!--<p>Sofecap $2M</p>-->
                    </div>
                    
                        
                </div>
            </div>
            
            <div class="col-sm-12 col-md-6 col-lg-4 single-wrapper wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                <div class="pricing_single">
                    <div class="offer_price">
                        <h4>BINARY INCOME <span class="hover_text">BINARY INCOME</span>
                        <span class="shape_1"></span></h4>
                    </div>
                    <div class="offer_details">
                        <!--<span>15 Days</span>-->
                       <h3 >MyCouponMall provides a binary income opportunity depending on 1:2 or 2:1 plan at the first level.</h3>
                        <!--<p>Sofecap $2M</p>-->
                    </div>
                    
                        
                </div>
            </div>
            
        </div>    
				
				
				
				
				
            </div>
        </div> 
                
    </div> 
    <div id="particles6-js" class="particles"></div>
</section> <!-- End token_distribution -->














<section id="roadmap_06">
    <div class="container">
            
        <div class="row">
            
            <div class="col-sm-12">
                <div class="sub-title">
                     <h2 class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Business Process</h2> 
                     <!--<p class=" wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">Due to the use of large computing power and artificial instantly analyze user 
                    data and offer solutions for their further use.</p> -->
                 </div>
				
				
				
				
            </div>
            
                    
            <div class="location_04">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="location_wrapper">
                        
                        
                        
                         <li><img src="<?= base_url('resource/front/') ?>images/BusinessProcess.png" alt="" class="img-fluid"></li>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <span class="shape2 header-shape"><img src="images/shape/home_6/road_map_2.png" alt=""></span>
    <span class="shape3 header-shape"><img src="images/shape/home_6/road_map_3.png" alt=""></span>   
    <div id="particles7-js" class="particles"></div>
</section> <!-- End roadmap -->




<section id="faq-area-06" class="faq-area">
    <div class="container">
        <div class="row">        
            <div class="col-sm-12">
                <div class="sub-title">
                    <h2 class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">Frequently Asked Questions</h2>
                    <!--<p class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">Artificial based on the neural network, the NRM assistant will instantly analyze user data use.</p>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-5">
                <div class="faq-img  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                    <img src="<?= base_url('resource/front/') ?>images/token-img.png" alt="" class="img-fluid">
                </div>
            </div>            
            <div class="col-sm-12 col-md-12 col-lg-7">
                <div class="faq-wrapper">
                        <!--Accordion wrapper-->
                    <div class="panel-group accordion-wrapper" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel accordion-single accordion-01  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                     What is Mycouponmall.com?

                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-body  accordion-content">
                                    <p>Mycouponmall is an open driver membership program that not only provides them membership, but also provides them with an earning opportunity. The membership program is not only open for drivers, but also for passengers and other members too.
</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel accordion-single accordion-02  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">
                            <div class="panel-heading active" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    How can I purchase goods from mycouponmall?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse  in show" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body  accordion-content">
                                    <p>To purchase goods from mycouponmall, you should have money in wallet. You will have to convert the money amount in coupons, and with coupon you can purchase goods.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel accordion-single accordion-03  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".4s">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Is MycouponMall secure?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="card-body  accordion-content">
                                    <p>Yes. Mycouponmall uses trusted payment integration, and uses strong data encryption technology to keep your money secure.</p>
                                </div>
                            </div>
                        </div>                        
                        <div class="panel accordion-single accordion-03  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                            <div class="panel-heading" role="tab" id="headingfour">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                        What is the membership fee at mycouponmall?

                                    </a>
                                </h4>
                            </div>
                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                              <div class="card-body  accordion-content">
                                    <p>The membership fee depends on which plan you choose. You can choose any of the plan from primary( INR 500/-), Gold ( INR 1000/-), Diamond ( INR 2000/-)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="particles10-js" class="particles"></div>
</section><!-- End faq-area -->



















<section id="contact_06">
    <div class="container">       
        <div class="row">           
            
            <div class="col-md-6 offset-md-3">
                <div class="contact_form">
                    <?php echo form_open_multipart('home/send_enquiry')?>
                        <div class="row">
                            
                            <div class="sub-title">
                        <h2 class="wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.2s; animation-name: fadeInUp;">Get in touch with us</h2>
                    </div>
                            
                            
                            <div class="col-12 col-sm-12  form-group  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.2s; animation-name: fadeInUp;">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required="">
                            </div>
                            <div class="col-12 col-sm-12  form-group  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.3s; animation-name: fadeInUp;">
                                <input type="email" class="form-control person-email" id="email" name="email" placeholder="E-mail Address" required="">
                            </div>
                <div class="col-12 col-sm-12 form-group  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.4s; animation-name: fadeInUp;">
                <textarea class="form-control" id="comment" name="comment" placeholder="Message" minlength="100", maxlength="1500" required=""></textarea>
                            </div>                              
                            <div class="col-12 col-sm-12 form-group">
                    <div class="submit-btn  wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeInUp;">
                                     <input type="submit" value="Send Message">
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>                     
            </div>                  
        </div>                   
    </div>
    <div id="particles11-js" class="particles"></div>
</section>



















<footer id="footer-06" class="footer">
    <div class="container">
        <div class="row footer-btm-wrapper">
            <div class="col-md-12 col-lg-12">
                <div class="footer_items">
                    
                    <div class="footer-single-col footer_single_list_1">
                       <!--  <h3 class="subtitle_1">&nbsp;<img src="<?= base_url('resource/front/') ?>images/logo.png" alt=""></h3> -->
                           <h3 class="subtitle_1">About MyCouponMall</h3>
                        <p>
                          Mycouponmall is a kind of small investment plan that provides you significant returns and excellent benefits. Anyone can join our membership programs and avail returns.  As a membership benefit you get three different ways to earn money-Binary income, direct income and referral income. Having decades of experience, and great exposure in the financial planning sector, we bring forth structured, organized plans for investors.</p>
                        <ul class="social-links list-unstyled ml-auto">
                            <li class="nav-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="nav-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>

                    <div class="footer-single-col footer_single_list_2">
                        <h3 class="subtitle_1">Company</h3> 
                        <ul class="list-unstyled">
                            	<li><a href="<?=base_url()?>">Home</a></li>
							<li><a href="#welcome_cryptonic_06">About us</a></li>
							<li><a href="#roadmap_06">Business Packages</a></li>
                        </ul>
                        
                           
                        
                    </div>

                    <div class="footer-single-col footer_single_list_3">
                        <h3 class="subtitle_1">Contact Us</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">If You Have Any Query, Please Contact Us On Given Email Id.</a></li>
                            <li><a href="#">Info@mycouponmall.co</a></li>
                       
                        </ul>
                    </div>


                    
                </div>                  
            </div>
        </div>       
    </div>  
	
	
		
	
	
	
	
	
    <div class="copyright">
        <p> Copyright &copy; 2020, Created  by <span>   MyCouponMall</span> &nbsp;&nbsp;&nbsp;&nbsp;  <span class="text-right"><a target="_blank" href="<?= base_url("home/privacy_policy") ?>"><i class="fa fa-lock"></i> Privacy Policy</a></span></p>            
    </div>   
    <span class="shape1 header-shape"><img src="images/shape/home_6/footer_shape_1.png" alt=""></span>
    <span class="shape2 header-shape"><img src="images/shape/home_6/footer_shape_2.png" alt=""></span>
    <span class="shape3 header-shape"><img src="images/shape/home_6/footer_shape_3.png" alt=""></span>           
</footer><!-- ./ End Footer Area-->

    <!-- JavaScript Files -->
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery-3.2.1.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/popper.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/bootstrap.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/owl.carousel.min.js"></script>    
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.sticky.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/isotope.pkgd.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.downCount2.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/jquery.countdown.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/particles.js"></script>
    <script src="<?= base_url('resource/front/') ?>js/assets/app.js"></script>      
    <script src="<?= base_url('resource/front/') ?>js/assets/smooth-scroll.js"></script> 
    <script src="<?= base_url('resource/front/') ?>js/assets/wow.min.js"></script>    
    <script src="<?= base_url('resource/front/') ?>js/assets/wow.min.js"></script>   	
    <script src="<?= base_url('resource/front/') ?>js/custom.js"></script>  
	
	<script>
$(document).ready(function(){
  $(".about-text-show").click(function(){
    $(".about-text").fadeToggle("fast")
  });
});
</script>

	
	
	<script>
	"use strict";

var canvas = document.getElementById('canvas'),
  ctx = canvas.getContext('2d'),
  w = canvas.width = window.innerWidth,
  h = canvas.height = window.innerHeight,
    
  hue = 217,
  stars = [],
  count = 0,
  maxStars = 1400;

// Thanks @jackrugile for the performance tip! https://codepen.io/jackrugile/pen/BjBGoM
// Cache gradient
var canvas2 = document.createElement('canvas'),
    ctx2 = canvas2.getContext('2d');
    canvas2.width = 100;
    canvas2.height = 100;
var half = canvas2.width/2,
    gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
    gradient2.addColorStop(0.025, '#fff');
    gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
    gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
    gradient2.addColorStop(1, 'transparent');

    ctx2.fillStyle = gradient2;
    ctx2.beginPath();
    ctx2.arc(half, half, half, 0, Math.PI * 2);
    ctx2.fill();

// End cache

function random(min, max) {
  if (arguments.length < 2) {
    max = min;
    min = 0;
  }
  
  if (min > max) {
    var hold = max;
    max = min;
    min = hold;
  }

  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function maxOrbit(x,y) {
  var max = Math.max(x,y),
      diameter = Math.round(Math.sqrt(max*max + max*max));
  return diameter/2;
}

var Star = function() {

  this.orbitRadius = random(maxOrbit(w,h));
  this.radius = random(60, this.orbitRadius) / 12;
  this.orbitX = w / 2;
  this.orbitY = h / 2;
  this.timePassed = random(0, maxStars);
  this.speed = random(this.orbitRadius) / 50000;
  this.alpha = random(2, 10) / 10;

  count++;
  stars[count] = this;
}

Star.prototype.draw = function() {
  var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
      y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
      twinkle = random(10);

  if (twinkle === 1 && this.alpha > 0) {
    this.alpha -= 0.05;
  } else if (twinkle === 2 && this.alpha < 1) {
    this.alpha += 0.05;
  }

  ctx.globalAlpha = this.alpha;
    ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
  this.timePassed += this.speed;
}

for (var i = 0; i < maxStars; i++) {
  new Star();
}

function animation() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 0.8;
    ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 1)';
    ctx.fillRect(0, 0, w, h)
  
  ctx.globalCompositeOperation = 'lighter';
  for (var i = 1, l = stars.length; i < l; i++) {
    stars[i].draw();
  };  
  
  window.requestAnimationFrame(animation);
}

animation();
	</script>
	
	
	
	<script 
src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false">
</script>
 <script> window.cookieconsent.initialise({ "palette": { "popup": { "background": "#000" }, "button": { "background": "#f1d600" } }, "theme": "classic" }); 
</script>
	
	
</body>

</html>
