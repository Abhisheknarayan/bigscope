

<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Token Transaction History</h4>
                        <ol class="breadcrumb">
                            
                        </ol>
                    </div>
                </div>
                    
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Token Transaction History</div>
                             <div class="card-body">
                                  <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>User ID</th>
                                                <th>No of tokens</th>
                                                <th>Amount</th>
                                                <th>Transaction Key</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Purchased From</th>
                                               <th>Transaction Type</th>
                                            </tr>
                                        </thead>
                                         <?php $i=1;
                                         foreach ($data->result() as $row) : ?>
                                        <tbody> 
                                        <tr>
                                <td><?php echo $i++;?></td>
                                 <td><?php echo $row->to_wallet_address;?></td>
                                 <td><?php echo $row->amount;?></td>
                                 <td><?php echo $row->value;?></td>
                                  <td><?php echo $row->transaction_key;?></td>
                               
                                <td><?php echo $row->date_;?></td> 
                                 
                                <td><?php echo $row->status;?></td>
                               
                              
                                 <td><?php echo $row->from_wallet_address;?></td>
                                 <td><?php  if ($row->transaction_type==1) {

                                   echo "Sales";
                                 }
                                 else{
                                  echo "Purchase";
                                 }

                                 ?></td>

                                
                               
                                
                                
                                          </tr>
                                          
                                           <?php  endforeach; ?>
                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>User ID</th>
                                                <th>No of tokens</th>
                                                <th>Amount</th>
                                                <th>Transaction Key</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Purchased From</th>
                                               <th>Transaction Type</th>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
        
        
 


