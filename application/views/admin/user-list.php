 <!-- Start wrapper-->
 <style>
     mark {
  background: orange;
  color: black;
}
 </style>
    <div id="wrapper">
        <div class="clearfix"></div>
        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">
                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">User List</h4>
                        <ol class="breadcrumb"></ol>
                    </div>
                </div>
                <!-- End Breadcrumb-->
                
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table"></i> User List
                            </div>
                            
                            <div class="card-body">
                                
                                 <?= $this->session->flashdata('update_user'); ?>
                                 
                                <div class="table-responsive actual_data">
                                    
                                    
                                    
                                   <div class="row">   
                             <!--   <div class="col-lg-3 offset-lg-6">    -->
                             <!--   <div class="position-relative has-icon-right">-->
                        	    <!--<label for="timesheetinput1"> Date from:</label>-->
                        	    <!--<input type="date" id="amount" class="form-control form-control-rounded" name="from" placeholder="Enter the date ">-->
                             <!--	</div>-->
                             <!--	</div>-->
                             	
                             <!--	 <div class="col-lg-3">  -->
                            	<!--<div class="position-relative has-icon-right">-->
                        	    <!--<label for="timesheetinput1"> Date to:</label>-->
                        	    <!--<input type="date" id="amount" class="form-control form-control-rounded" name="to" placeholder="Enter the date ">-->
                            	<!--</div>-->
                                
                             <!--    </div>  -->
                                    
                                    <table id="example" class="table table-bordered">
                                        
                                        <thead>
                                            
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>UserName</th>
                                                <th>Sponsor</th>
                                                <th>Email </th>
                                                <th>Mobile</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach ($user->result() as $row) : ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i++;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->user_id;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->first_name;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->sponser_id;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->user_email;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->user_mobile;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->user_status;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->joining_date;?>
                                                </td>
                                                <td>
                                                    <a href="<?= base_url('admin/view_user_profile/'.$row->user_id.'')?>"> <i class="fa fa-edit" title="Edit"></i></a>
                                                </td>
                                                

                                            </tr>
                                            <?php  endforeach; ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>UserName</th>
                                                <th>Sponsor</th>
                                                <th>Email </th>
                                                <th>Mobile</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Action</th>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="table-responsive filter_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <script>-->
<!--                    $(function() {-->
<!--  $("input").on("input.highlight", function() {-->
    
<!--    var searchTerm = $(this).val();-->
    
<!--    $("#example").unmark().mark(searchTerm);-->
<!--  }).trigger("input.highlight").focus();-->
<!--});-->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<!--<script src="https://cdn.jsdelivr.net/mark.js/7.0.0/jquery.mark.min.js"></script>-->
<!--<input type="text" value="test">-->
<!--<div id="context">-->
<!--  Lorem ipsum dolor test sit amet-->
<!--</div>-->
<!--                </script>-->
                <!-- End Row-->

            

       
        

 
