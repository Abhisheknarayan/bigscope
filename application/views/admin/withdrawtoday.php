
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
 
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Withdraw Request</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Withdraw Request</div>
            <div class="card-body">

              <h5><i class="fa fa-search"></i> Search By</h5>
                  <div class="row mt-3">
                    <div class="col-md-1 text-center">
                      <label>User Id</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <input type="text" name="" class="form-control" placeholder="User Id" id='withdraw_userid'>
                    </div>

                    <div class="col-md-1 text-center">
                      <label>Status</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <select class="form-control" id='withdraw_planid'>
                        <option value="">--Select Status--</option>
                        <option value="2">New</option>
                        <option value="1">Accept</option>
                        <option value="0">Decline</option>

                      </select>
                    </div>

                    <div class="col-md-1 text-center">
                      <label>From</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="From" id="withdraw_date_from">
                    </div>

                    <div class="col-md-1 text-center">
                      <label>To</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="To" id="withdraw_date_to">
                    </div>
                  </div>

              <div class="table-responsive actual_data">
              <table id="example" class="table table-bordered">
                      <thead>
                        <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                               <th>BTC Address</th>
                               <th>Requested Amount</th>
                               <th>Status</th>
                               <th>Date</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                      <td>
                                      <input type="checkbox" id="status" data-toggle="toggle" data-on="Accept" data-off="Decline" >
                                       <input type="checkbox" id="status1" data-toggle="toggle" data-on="Cancel" data-off="Cancel" >
                                      <input type="hidden" id="user_id" value="">
                                      </td>
                                 
                                    <td></td>
                                          </tr>

                                          <script type="text/javascript">
                                            $("#status<?php echo $i; ?>").change(function(){
                                              var user=$("#user_id<?php echo $i; ?>").val();
                                              if($(this).prop("checked") == true){
                                                  //alert("Checkbox is checked.");
                                                  $.ajax({
                                                    type:'post',
                                                    url:'ajax.php',
                                                    data:'user='+user+'&action=1',
                                                    success:function(data){
                                                      if(data=='1'){
                                                        history.go();
                                                      }
                                                      else{
                                                        alert("Please Try Again");
                                                      }
                                                    }
                                                  });
                                              }
                                              else if($(this).prop("checked") == false){
                                                  //alert("Checkbox is unchecked.");
                                                  $.ajax({
                                                    type:'post',
                                                    url:'ajax.php',
                                                    data:'user='+user+'&action=0',
                                                    success:function(data){
                                                      if(data=='1'){
                                                        history.go();
                                                      }
                                                      else{
                                                        alert("Please Try Again");
                                                      }
                                                    }
                                                  });
                                              }
                                            });
                                          </script>
                                          <?php
                                          $i++;
                                          
                                           ?>
                        </tbody>
                        <tfoot>
                        <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                               <th>BTC Address</th>
                               <th>Requested Amount</th>
                               <th>Status</th>
                               <th>Date</th>
              
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="table-responsive filter_data">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
         
        

     