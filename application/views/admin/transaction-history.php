<body>
<!-- Start wrapper-->
 <div id="wrapper">
<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Transaction History</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Transaction History</div>
            <div class="card-body">
                              
                              

              <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                       <thead>
                            <tr>
                                <th>No</th>
                                
                                <th>Order id</th>
                                <th>User ID</th>
                                <th>Package</th>
                                <th>Purchase Amount($)</th>
                                <th>Pay By</th>
                                <th>Purchase Date</th>
                                <th>status</th>
                            
                            </tr>
                           
                        </thead>
                        <tbody>
                            <?php $i=1;
                             foreach ($p_history->result() as $row) : ?>
                                <tr>
                                    <td><?php echo $i++;?></td>
                                
                                    <td><?php echo $row->order_id;?></td>
                                    <td><?php echo $row->user_id;?></td>
                                    <td><?php echo $row->name ;  ?></td>
                                    <td><?php echo $row->amount;?></td>
                                    <td><?php echo ucfirst(str_replace('-',' ',$row->type));?></td>
                                    
                                    <td><?php echo $row->date;?></td>
                                    <td><?php echo $row->status;?></td>
                                
                                </tr>
                            <?php  endforeach; ?>
                        </tbody>
                    </table>
                 </div>

                 <!-- <div class="table-responsive filter_data">-->
             
                 <!--</div>-->

            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
   $(document).ready(function() {
    $('#example').DataTable( {  
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],   
        dom: 'Bfrtip',        
        buttons: [            
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'
            
        ]
    } );
} );
</script>
     