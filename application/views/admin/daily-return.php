<?php
//print_r($data);
?>

<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Daily Return Income</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>
                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Daily Return Income</div>
                            <div class="card-body">
                                <h5><i class="fa fa-search"></i> Search By</h5>
                                <div class="row mt-3">
                                    <div class="col-md-1 text-center">
                                        <label>User Id</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="text" name="" class="form-control" placeholder="User Id" id='roi_userid'>
                                    </div>

                                    <div class="col-md-1 text-center">
                                        <label>Plan</label>
                                    </div>
                                    <!--<div class="col-md-2 form-group no-padding">-->

                                    <!--    <select class="form-control" id='roi_planid'>-->
                                    <!--        <option value="">--Select Plan--</option>-->
                                    <!--        <option value="1">Venus180</option>-->
                                    <!--        <option value="2">Venus270</option>-->
                                    <!--        <option value="3">Venus360</option>-->
                                    <!--      </select>-->
                                    <!--</div>-->

                                    <div class="col-md-1 text-center">
                                        <label>From</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="date" name="" class="form-control" placeholder="From" id="roi_date_from">
                                    </div>

                                    <div class="col-md-1 text-center">
                                        <label>To</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="date" name="" class="form-control" placeholder="To" id="roi_date_to">
                                    </div>
                                </div>

                                <div class="table-responsive actual_data">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Plan Name</th>
                                                <th>Package Amount($)</th>
                                                <th>Percent(%)</th>
                                                <th>Daily Return Income($)</th>
                                                <th>Date</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <?php 
                                            $i=1;
                                            foreach ($data as $row){
                                            ?>
                                                 <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $row->user_id;?></td>
                                                   <td><?php echo $row->name;?></td>
                                                   <td ><?php echo $row->amount;?></td> 
                                                   <td><?php echo $row->percentage;?></td>
                                                   <td><?php echo $row->roi_amount;?></td>
                                                   <td ><?php echo $row->roi_date;?></td> 
                                                  
                                                  </tr>
                                            <?php 
                                            
                                            $i++;
                                            }; 
                                            ?>
                                        </tbody>                                        
                                            <tfoot>
                                                <tr>
                                                    <th>SNo</th>
                                                    <th>User Id</th>
                                                    <th>Plan Name</th>
                                                    <th>Package Amount($)</th>
                                                    <th>Percent(%)</th>
                                                    <th>Daily Return Income($)</th>
                                                    <th>Date</th>
                                                </tr>
                                            </tfoot>
                                    </table>
                                </div>
                                <div class="table-responsive filter_data">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
        </script>