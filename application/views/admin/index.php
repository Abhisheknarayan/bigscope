
<style>
.circle-wrapper {
                position: relative;
    width: 170px;
    top: -40px;
    left: -48px;
    height: 142px;
    padding: 110px 0px 0px 0px;
}


.circle-zero:before {
    content: "";
    background: rgba(0, 0, 0, 0.07);
    position: absolute;
    width: 168px;
    height: 2px;
    left: -60%;
    top: 50%;
    transform: rotate(45deg);
    z-index: -2;
}

.circle-zero {
    background: #1c1e21;
    border-radius: 50%;
    box-shadow: 0px 3px 7px 0.7px rgba(0, 0, 0, .5);
    box-sizing: content-box;
    height: 68px;
    width: 68px;
    position: absolute;
    top: 35%;
    left: 35%;
    text-align: center;
}


.circle-one:before {
  content: "";
  background: rgba(21, 167, 235, .7);
  border-radius: 50%;
  position: absolute;
  width: 4px;
  height: 4px;
  right: -56%;
  top: 45%;
  animation: circle-move 10s ease infinite;
}

.circle-one {
    border: 7px solid;
    border-color: rgb(28, 30, 33) rgb(132, 171, 52) transparent transparent;
    border-radius: 50%;
    box-sizing: content-box;
    height: 90px;
    width: 92px;
    position: absolute;
    top: 27%;
    left: 25%;
    z-index: -1;
    animation: circle-move 10s ease infinite;
} 

.circle-two {
    background: rgba(0,0,0,.017);
    border: 15px solid;
    border-color: rgb(243, 162, 77) transparent transparent;
    border-radius: 50%;
    box-shadow: 15px 0 25px -20px rgba(0, 0, 0, 0.65);
    box-sizing: content-box;
    height: 90px;
    width: 90px;
    position: absolute;
    top: 25%;
    left: 21%;
    z-index: -2;
    animation: circle-move 14s ease infinite;
}

.circle-three {
  background: rgba(0,0,0,.012);
  border: 2px solid;
  border-color: #CE93D8 transparent;
  border-radius: 50%;
  box-sizing: content-box;
  height: 168px;
  width: 168px;
  position: absolute;
  z-index: -3;
  top: 15%;
  left: 15%;
  animation: circle-move 15s ease infinite;
}

.circle-shadow {
  background: rgba(0, 0, 0, 0.2);
  border-radius: 50%;
  filter: blur(2px);
  height: 20px;
  width: 160px;
  position: absolute;
  top: 100%;
  left: 18%;
}


@keyframes circle-move {
  0% {
    transform: rotate(0deg);
  }
  70% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(0deg);
  }
}






   .circle-zero h1 {
    position: relative;
    top: 0px;
    font-size: 22px;
    color: #ffffff;
}
    
     .circle-zero p {
    position: relative;
    top: -25px;
    font-size: 14px;
    color: white;
}
  
 .min-height-add:hover .add-animatio_a  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 


.min-height-add:hover .add-animatio_b  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 



.min-height-add:hover .add-animatio_c  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 

.min-height-add:hover .add-animatio_d  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 

.min-height-add:hover .add-animatio  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 

.min-height-add:hover .gradient-bloody  {
   
    position: relative;
    top: -50px;
    left: 40px;
} 

  .extra_wrap .card-img-top 
{
    width: 20%;
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
    margin: 0 auto;
    padding: 10px 0px 0px 0px;
}
   
   
 .Binsry_bg{    background-image: url(./././assets/images/gallery/12.jpg);}  
  
  .Binsry_text {
    padding: 0px 10px 20px 10px;
    text-align: center;
    font-size: 1.2rem;
    /* float: left; */
    /* font-family: cursive; */
    /* display: inline-block; */
    color: #f77e03;
}




.sparkley {

  background: lighten( $buttonBackground , 10% );
 display: inline-block;
    border: none;
    padding: 16px 36px;
    font-weight: normal;
        border-bottom: 11px solid #87ad37;
    background: #151515;
    color: white;
    margin-bottom: 21px;
    margin: 0 auto;
    margin-bottom: 10px;
    border-radius: 3px;
    transition: all 0.25s ease;
    box-shadow: 0 38px 32px -23px black;
  
  &:hover {
   
    background: $buttonBackground;
    color: transparentize( $buttonColor,0.8 );
    
  }
  
}

.prize3{ margin: 58px 0px 0px 0px;}



header a, a:visited {
    text-decoration:none;
    color:#fcfcfc;
}

@keyframes move-twink-back {
    from {background-position:0 0;}
    to {background-position:-10000px 5000px;}
}
@-webkit-keyframes move-twink-back {
    from {background-position:0 0;}
    to {background-position:-10000px 5000px;}
}
@-moz-keyframes move-twink-back {
    from {background-position:0 0;}
    to {background-position:-10000px 5000px;}
}
@-ms-keyframes move-twink-back {
    from {background-position:0 0;}
    to {background-position:-10000px 5000px;}
}

@keyframes move-clouds-back {
    from {background-position:0 0;}
    to {background-position:10000px 0;}
}
@-webkit-keyframes move-clouds-back {
    from {background-position:0 0;}
    to {background-position:10000px 0;}
}
@-moz-keyframes move-clouds-back {
    from {background-position:0 0;}
    to {background-position:10000px 0;}
}
@-ms-keyframes move-clouds-back {
    from {background-position: 0;}
    to {background-position:10000px 0;}
}

.stars, .twinkling, .clouds {
  position:absolute;
  top:0;
  left:0;
  right:0;
  bottom:0;
  width:100%;
  height:100%;
  display:block;
}

.stars {
  background:#000 url(<?=base_url('resource/admin')?>/images/stars.png) repeat top center;
  z-index:0;
}

.twinkling{

  z-index:1;

  -moz-animation:move-twink-back 200s linear infinite;
  -ms-animation:move-twink-back 200s linear infinite;
  -o-animation:move-twink-back 200s linear infinite;
  -webkit-animation:move-twink-back 200s linear infinite;
  animation:move-twink-back 200s linear infinite;
}

.clouds{
    background:transparent url(http://www.script-tutorials.com/demos/360/images/clouds3.png) repeat top center;
    z-index:3;

  -moz-animation:move-clouds-back 200s linear infinite;
  -ms-animation:move-clouds-back 200s linear infinite;
  -o-animation:move-clouds-back 200s linear infinite;
  -webkit-animation:move-clouds-back 200s linear infinite;
  animation:move-clouds-back 200s linear infinite;
}


.text_highlightani{    font-size: 22px;
    font-weight: bold;}
    
    
    
  .hight_light_text_two {
    font-size: 26px;
    font-size: 26px;
    font-weight: bolder;
    color: black;
    padding: 0px 4px;
    background: linear-gradient(to right, #87ad37 20%, #929292 40%, #f5881f 60%, #d89328 80%);
    background-size: 200% auto;
    background-clip: text;
    text-fill-color: transparent;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    animation: shine 1s linear infinite;
}



@keyframes shine {
    to {
      background-position: 200% center;
    }
  }
  
    
    
    
    
   #canvas {
    position: absolute;
    z-index: 1;
} 
    
    .overflow{ overflow:hidden;}
     .min_height_set{ min-height: 492px;}
    
    .prize1{    margin: 44px 0px;}
	
	
	

	     

   </style>

<body>

<!-- Start wrapper-->


 <div id="wrapper">
 <div class="demo"></div>	
  <!--Start sidebar-wrapper-->
  

<div class="clearfix"></div>
	
  <div class="content-wrapper" style="min-height: 840px;" >
    <div class="container-fluid">
 
      <!--Start Dashboard Content-->
	  
 
      <div class="row mt-3">
        <div class="col-12 col-lg-6 col-xl-3 ">
          <div class="card  animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-info">$ <?= total_daily_return() ?></h4>
                <span  class="text-white">Daily Return Income</span>
                <span class="dashboard3-chart-5"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="fa fa-dollar text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-danger border-left-sm animated bounceInDown equal-height">
            <div class="card-body ">
              <div class="media">
               <div class="media-body text-left">
                <h4 class="text-danger">$ <?= total_direct_income() ?></h4>
                <span class="text-white"> Total Direct income</span>
                <span class="dashboard3-chart-2"></span>
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                <i class="icon-wallet text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
       
        
     
         <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-warning border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-warning">$ <?= total_binary_income() ?></h4>
                <span class="text-white">Total Binary Income</span>
                <span class="dashboard3-chart-4"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                <i class="icon-pie-chart text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-info border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                <h4 class="text-info">$ <?= total_income() ?></h4>
                <span class="text-white">Total Income</span>
                <span class="dashboard3-chart-5"></span>
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="icon-wallet text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
         <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-warning border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-warning"><?= total_user()?></h4>
                <span class="text-white">Total Bigscope Users</span><br>
                <span class="dashboard3-chart-4"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                <i class="fa fa-users text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
         <div class="col-12 col-lg-6 col-xl-3">
            <div class="card border-danger border-left-sm animated bounceInDown equal-height">
              <div class="card-body">
                <div class="media">
                <div class="media-body text-left">
                  <h4 class="text-danger"> <?= total_paid_user()?></h4>
                  <span class="text-white">Total paid Users</span><br>
                  <span class="dashboard3-chart-2"></span>
                </div>
                <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                  <i class="fa fa-users text-white"></i></div>
              </div>
              </div>
            </div>
        </div>
    
         <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-info border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-info"><?= total_unpaid()?></h4>
                <span class="text-white">Total unpaid Users</span>
                <span class="dashboard3-chart-5"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="icon-user text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
         <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-danger border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-danger"> <?= todayJoining()?></h4>
                <span class="text-white">Today joining</span>
                <span class="dashboard3-chart-2"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                <i class="icon-user text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
		 <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-success border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                 
                <h4 class="text-success">$  <?= my_package_amount()?></h4>
                <span  class="text-white">Total joining amount</span>
                <span class="dashboard3-chart-3"></span>
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                <i class="fa fa-dollar text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        
		
		 <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-success border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                
                <h4 class="text-success"></h4>
                
                 <div class="bcoavailable"><span  class="text-white">Number of BCO available-</span><span><?= availableToken() ?></span> </div>
                <div class="current-price"><span  class="text-white">BCO current price</span><span>$ <?= token_price() ?></span> </div>
                
                
               <!-- <span class="dashboard3-chart-3"></span> -->
              </div>
              
              
               <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                <i class="fa fa-dollar text-white"></i></div>
                
                
                
            </div>
            </div>
          </div>
        </div>
        
        
        
        
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-danger border-left-sm animated bounceInDown equal-height">
            <div class="card-body min_height_plus">
                <div class="media">
                  <div class="media-body text-left">
                    <h4 class="text-warning">$ <?= total_purchase_coin()->coin ?></h4>
                    <span class="text-white">Sold <?= COIN ?></span>
                    <span class="dashboard3-chart-4"><canvas width="118" height="25" style="display: inline-block; width: 118px; height: 25px; vertical-align: top;"></canvas></span>
                  </div>
                  
                   <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                    <i class="fa fa-dollar text-white"></i></div>
                </div>
            </div>
          </div>
        </div>
        
		 <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-success border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                
                <h6 class=" text_white">Value in $ <?= total_withdraw_request()->amount?></h6>
                <h6 class=" text_white">Value in BTC <?= total_withdraw_request()->btcAmount?></h6>
                <b><span class="text_black highlight_text">Total Withdraw Request </span></b>                
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                <i class="icon-user text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
		  
      </div>
      
      

      <!-- weeke -->
   <!--    jai -->

   <!-- jaie -->
         <div class="row">
        <div class="col-12 col-lg-6 col-xl-3 ">
          <div class="card border-info border-left-sm animated bounceInDown equal-height">
            <div class="card-body min_height_plus">
              <div class="media">
              <div class="media-body text-left">
               
                <h4 class="text-info">$ <?= total_withdraw()->amount?></h4>
                <span  class="text-white">Total Withdrawl amount</span>
                <span class="dashboard3-chart-5"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="fa fa-dollar text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        
        <div class="col-12 col-lg-6 col-xl-3 ">
          <div class="card border-info border-left-sm animated bounceInDown equal-height">
            <div class="card-body min_height_plus">
              <div class="media">
              <div class="media-body text-left">
               
                <h4 class="text-info">$ <?= today_withdraw()->amount ;?></h4>
                <span  class="text-white">Today Withdrawl amount</span>
                <span class="dashboard3-chart-5"></span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="fa fa-dollar text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="card border-success border-left-sm animated bounceInDown equal-height">
              <div class="card-body">
                <div class="media">
                 <div class="media-body text-left">
                   
                  <h4 class="text-success"> <?= total_support_ticket() ?></h4>
                  <span  class="text-white">Total Support Ticket</span><br>
                  <span class="dashboard3-chart-3"></span>
                </div>
                 <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                  <i class="fa fa-ticket"></i></div>
              </div>
              </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="card border-success border-left-sm animated bounceInDown equal-height">
              <div class="card-body">
                <div class="media">
                 <div class="media-body text-left">
                   
                  <h4 class="text-success"> <?= today_support_ticket() ?></h4>
                  <span  class="text-white">Today Support Ticket</span><br>
                  <span class="dashboard3-chart-3"></span>
                </div>
                 <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                  <i class="fa fa-ticket"></i></div>
              </div>
              </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-success border-left-sm animated bounceInDown equal-height">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                 
                <h4 class="text-success">$ <?= todayJoiningAmount()?></h4>
                <span  class="text-white">Today joining amount</span>
                <span class="dashboard3-chart-3"></span>
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                <i class="fa fa-dollar text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
         
		<!-- <div class="col-6 col-lg-6 col-xl-4  hidden-xs">
          
              
              <div class="card">
          
            <div class="card-body">
               <canvas id="AreaChart" height="90"></canvas>
            </div>
          </div>
            </div> -->
			
		 <!-- <div class="col-6 col-lg-6 col-xl-4">
              <div class="card">
              <div class="card-header">Package Reports</div>
              <div class="card-body">
               <div id="c3-pie-chart"></div>
              </div>
            </div>
          </div>
		  -->
		 
      </div>
	  
	  
	  
	  
    <div class="row mt-3" style="display:none;">
      
        <div class="col-12 col-lg-12 col-xl-12 col-sm-12">
          
              
                 <div class="row ">
              
              <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                 
                  <div class="card" style="">
                         <div class="card-header">Current Week Binsry Bank</div>
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                <div class="card border-success border-left-sm animated bounceInDown ">
                                 
                                  <div class="card-body">
                                    <div class="row">
                                      
                                        <div class="col-lg-12 col-xl-12">
                                           <p class="Binsry_text">Binsry Countdown Timer</p> 
                                             </div>
                                      
                                     <div class=" col-lg-12 col-xl-12">
                                        
                                        
                                           <div class="row"> 
                                        <div class="col-sm-3">
 <div class="circle-wrapper">
    <div class="circle-zero"><h1 id="days"></h1><p>Days</p></div>
    <div class="circle-one"></div>
    <div class="circle-two"></div>
  
  </div>
    </div>
    
  
  <div class="col-sm-3">
 <div class="circle-wrapper">
    <div class="circle-zero"><h1 id="hours"></h1><p>Hours</p></div>
    <div class="circle-one"></div>
    <div class="circle-two"></div>
   
  </div>
    </div>
  
  
  <div class="col-sm-3">
 <div class="circle-wrapper">
    <div class="circle-zero"><h1 id="minutes"></h1><p>Min</p></div>
    <div class="circle-one"></div>
    <div class="circle-two"></div>
 
  </div>
    </div>
  
  <div class="col-sm-3">
 <div class="circle-wrapper">
    <div class="circle-zero"><h1 id="seconds"></h1><p>Sec</p></div>
    <div class="circle-one"></div>
    <div class="circle-two"></div>
  
  </div>
    </div>
                                        
         </div>                               
                                        
                                    </div>
                                               </div>
                                  </div>
                                </div>
                              </div>

                                    
                                        <div class="col-12 col-lg-12 col-xl-12 col-sm-12 overflow min_height_set">
                 <canvas id="canvas"></canvas>
                      <div class="card" style="">
                        
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                <div class="card border-success border-left-sm animated bounceInDown ">
                                 
                                  <div class="card-body">
                                    <div class="row">
                                      <div class="col-lg-12 col-xl-12 col-sm-12">
                                        <div class="media">
<!--                                         <div class="media-body text-left">-->
<!--                                                                                       <span class="blink_text  hight_light_text_two">Weekly Turnover</span>-->

                                          <!--<h4 class=" blink card-text fo_si_40 " id="total_amt">$-->
                                          
                              <!--</h4>-->
                                          
<!--                                        </div>-->
                                      </div>
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                       <div class="media">
                                         <div class="media-body text-left">
                                                                                       <span class="blink_text"></span>

                                          <h4 class=" blink card-text fo_si_40" id="total_amt"></h4>
                                          
                                        </div>
                                      </div>
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      <div class="col-lg-12 col-xl-12 col-sm-12">
                                        <div class="media">
                                         <div class="media-body text-center">
                                          <span class="sparkley last">Weekly Win Prize</span>
										  
                                          <h4 class=" blink card-text fo_si_40 " id="total_amt">$</h4>
                                          
                                        </div>
                                      </div>
                                     </div>
                            
                                      
                                      
                                      
                                       <div class="col-lg-12 col-xl-12 col-sm-12">
                                        <div class="media">
                                         <div class="media-body text-center">
                                        
										  
                                          <img src="assets/images/prize1.gif" class="prize1">
                                          
                                        </div>
                                      </div>
                                     </div>
                                  
                  </div>
                  </div>
                      </div>
    								  </div>
                      </div>
                      </div>
    						  </div>
                  </div>
    					</div>
           </div>
                    </div>
             </div>
               </div>
  <div class="col-12 col-lg-6 col-xl-6 col-sm-12">
                 
                      <div class="card" style="">
                         <div class="card-header">Daily Cash-in Game</div>
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                <div class="card border-success border-left-sm animated bounceInDown ">
                                 
                                  <div class="card-body">
                                    <div class="row">
                                      
                                        <div class="col-lg-12 col-xl-12">
                                           <p class="Binsry_text">Daily Cash-in Game Countdown Timer</p> 
                                             </div>
                                        <div class=" col-lg-12 col-xl-12">
                                        <div class="row"> 
                                        <div class="col-sm-3">
                                         <div class="circle-wrapper">
                                            <div class="circle-zero"><h1 id="daysd">NaN</h1><p>Days</p></div>
                                            <div class="circle-one"></div>
                                            <div class="circle-two"></div>
                                          
                                          </div>
                                            </div>
                                            
                                          
                                          <div class="col-sm-3">
                                         <div class="circle-wrapper">
                                            <div class="circle-zero"><h1 id="hoursd">NaN</h1><p>Hours</p></div>
                                            <div class="circle-one"></div>
                                            <div class="circle-two"></div>
                                           
                                          </div>
                                            </div>
                                          
                                          
                                          <div class="col-sm-3">
                                         <div class="circle-wrapper">
                                            <div class="circle-zero"><h1 id="minutesd">NaN</h1><p>Min</p></div>
                                            <div class="circle-one"></div>
                                            <div class="circle-two"></div>
                                         
                                          </div>
                                            </div>
                                          
                                          <div class="col-sm-3">
                                         <div class="circle-wrapper">
                                            <div class="circle-zero"><h1 id="secondsd">NaN</h1><p>Sec</p></div>
                                            <div class="circle-one"></div>
                                            <div class="circle-two"></div>
                                          
                                          </div>
                                            </div>
                                        
         </div>                               
                                        
                                    </div>
                                                                      </div>
                                  </div>
                                </div>
                              </div>

                               
                                            
            <div class="col-12 col-lg-12 col-xl-12 col-sm-12 overflow min_height_set">
                  
                      <div class="card" style="">
                      
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12 col-xl-12">
                                <div class="card border-success border-left-sm animated bounceInDown ">
                                 
                                  <div class="card-body">
                                    <div class="row">
                                      <div class="col-lg-6 col-xl-6 col-sm-6">
                                        <div class="media">
                                         <!--<div class="media-body text-left">-->
                                         <!--      <span class="blink_text hight_light_text_two">Daily Turnover</span>-->
                                                                                         
                                             
                                          
                                          <!--<h4 class=" blink card-text fo_si_40 " id="total_amt">$-->
                           </h4>
                                          
                              <!--          </div>-->
                                      </div>
                                      
                                      
                                      
                                      <div class="col-lg-12 col-xl-12 col-sm-12">
                                        <div class="media">
                                         <div class="media-body text-left">
                                          <span class="blink_text hight_light_text_two">Daily Win Prize</span>
                                          <h4 class=" blink card-text fo_si_40" id="total_amt">$</h4>
                                          
                                        </div>
                                      </div>
                                     </div>
                            
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                     </div>
                                   
                                    
                                      <div class="col-lg-6 col-xl-6 col-sm-6">
                                            <div class="final_winner">
                                                
                                                
                                                <div class="final_winner_wrap">
	<div class=" center">
		<div class="ripple1"></div>
		<div class="ripple2"></div>
		<div class="ripple3"></div>
	</div>
</div>
                                                
                                                
                                        <div class="media sparkley last" >
                                         <div class="media-body text-left Winning">
                                            <div class="stars"></div>
<div class="twinkling"></div> 
                                             
                                          <span class="blink_text "> <p class="">Winning Lucky No</p>   <h6 class=" blink card-text fo_si_40 text-info"></h6></span>
                                          <h4 class=" blink card-text fo_si_40 " id="total_amt">
                                           <!--<img src="win.php?win=<?php echo  $data_row['winlucky']; ?>" class="img-responsive " style="width:150px;height:150px;">-->
                                        </h4>
                                          
                                        </div>
                                      </div>
                                       </div>
                                     </div>
                                     
                                      <div class="col-lg-12 col-xl-12 col-sm-12 text-center">
                                          <img src="assets/images/prize3.gif" class="prize3">
                                     </div>
                                     
									 </div>
                                  </div>
								  </div>
                            </div>
                          </div>
						  </div>
                        </div>
					</div>
          
           </div>
                              </div>
                           
                          </div>
            </div>
            
                                    
         
          
          
          
          
        </div>
    </div>
						
      </div>
	  
      <div class="row mt-3">
        <!-- <div class="col-12 col-lg-6 col-xl-6"> -->
          
           
        <div class="row">
         
       
      
       
       
        <!-- total -->
    
    
    <!-- </div> -->
 
    
       
     
      <!--End Row-->
		  
		  
      <!--<div class="row mt-3">-->
      <!--  <div class="col-12 col-lg-12 col-xl-12">-->
          
              
      <!--        <div class="card">-->
          
      <!--      <div class="card-body">-->
      <!--         <canvas id="AreaChart" height="110"></canvas>-->
      <!--      </div>-->
      <!--    </div>-->
              
           
          
      <!--  </div>-->
		
        
		
      <!--</div>End Row-->

     <!--  <div class="row">
        <div class="col-12">
          <img src="assets/images/Binsry.png">
        </div>
      </div> -->
     
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
   
<!-- <script type="text/javascript">
  setInterval("my_function();",1000); 
 
    function my_function(){
        $("#Binsry_week").attr('src','counter.php?week=1');
        $("#Binsry_month").attr('src','counter.php?month=1');
        $("#Binsry_Binsry").attr('src','counter.php?Binsry=10');
        $("#Binsry_mBinsry").attr('src','counter.php?Binsry=5');
    }


    var c3PieChart = c3.generate({
    bindto: '#c3-pie-chart',
    data: {
      // iris data from R
      columns: [
        ["bigscope180", <?php echo $p180; ?>],
        ["bigscope270", <?php echo $p270; ?>],
        ["bigscope360", <?php echo $p360; ?>],
      ],
      type: 'pie',
      onclick: function(d, i) {
        console.log("onclick", d, i);
      },
      onmouseover: function(d, i) {
        console.log("onmouseover", d, i);
      },
      onmouseout: function(d, i) {
        console.log("onmouseout", d, i);
      }
    },
    color: {
        pattern: ['#bdf0a4','#b3d8f6','#ffd5be']
    },
    padding: {
        top: 0,
        right:0,
        bottom:30,
        left: 0,
    }
  });

</script>
	<script>
var timer;

var compareDate = new Date('<?php echo $time;?>');
compareDate.setDate(compareDate.getDate() + 7); //just for this demo today + 7 days

timer = setInterval(function() {
  timeBetweenDates1(compareDate);
}, 1000);

function timeBetweenDates1(toDate) {
  var dateEntered = toDate;
  var now = new Date();
  var difference = dateEntered.getTime() - now.getTime();

  if (difference <= 0) {

    // Timer done
    clearInterval(timer);
  
  } else {
    
    var seconds = Math.floor(difference / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours %= 24;
    minutes %= 60;
    seconds %= 60;

    $("#days").text(days);
    $("#hours").text(hours);
    $("#minutes").text(minutes);
    $("#seconds").text(seconds);
  }
}


</script>

<script>
var timerd;

var compareDated = new Date('<?php echo  $dailytime;?>');
compareDated.setDate(compareDated.getDate()+1); //just for this demo today + 7 days

timerd = setInterval(function() {
  timeBetweenDatesd(compareDated);
}, 1000);

function timeBetweenDatesd(toDate) {
  var dateEntered = toDate;
  var now = new Date();
  var difference = dateEntered.getTime() - now.getTime();

  if (difference <= 0) {

    // Timer done
    clearInterval(timerd);
   alert('Daily Binsry Open!!');
   location.href='Binsry-winner.php?Binsry=1';
  
  } else {
    
    var seconds = Math.floor(difference / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours %= 24;
    minutes %= 60;
    seconds %= 60;

    $("#daysd").text(days);
    $("#hoursd").text(hours);
    $("#minutesd").text(minutes);
    $("#secondsd").text(seconds);
  }
}
</script>

<script>
    
 $(function() {

  
  // default is varying levels of transparent white sparkles
  $(".sparkley:first").sparkleh();
  
  // rainbow as a color generates random rainbow colros
  // count determines number of sparkles
  // overlap allows sparkles to migrate... watch out for other dom elements though.
  $(".sparkley:last").sparkleh({
    color: "rainbow",
    count: 100,
    overlap: 10
  });
  
  // here we create fuscia sparkles
  $("h1").sparkleh({
    count: 80,
    color: ["#ff0080","#ff0080","#0000FF"]
  });
  
  
  
  $("p").sparkleh({
    count: 20,
    color: "#00ff00",
    speed: 0.05
  });
  
  
  // an array can be passed, too for colours
  // for an image, the image needs to be fully loaded to set
  // the canvas to it's height/width.
  // speed allows us to control... the ... velocity 
  $("#image").imagesLoaded( function() {
    $(".img").sparkleh({
      count: 25,
      color: ["#f3edc4","#253943","#659e3f"],
    speed: 0.4
    });
  });


});

$.fn.sparkleh = function( options ) {
    
  return this.each( function(k,v) {
    
    var $this = $(v).css("position","relative");
    
    var settings = $.extend({
      width: $this.outerWidth(),
      height: $this.outerHeight(),
      color: "#FFFFFF",
      count: 30,
      overlap: 0,
      speed: 1
    }, options );
    
    var sparkle = new Sparkle( $this, settings );
    
    $this.on({
      "mouseover focus" : function(e) {
        sparkle.over();
      },
      "mouseout blur" : function(e) {
        sparkle.out();
      }
    });
    
  });
  
}




function Sparkle( $parent, options ) {
  this.options = options;
  this.init( $parent );
}

Sparkle.prototype = {
  
  "init" : function( $parent ) {
    
    var _this = this;
    
    this.$canvas = 
      $("<canvas>")
        .addClass("sparkle-canvas")
        .css({
          position: "absolute",
          top: "-"+_this.options.overlap+"px",
          left: "-"+_this.options.overlap+"px",
          "pointer-events": "none"
        })
        .appendTo($parent);
    
    this.canvas = this.$canvas[0];
    this.context = this.canvas.getContext("2d");
    
    this.sprite = new Image();
    this.sprites = [0,6,13,20];
    this.sprite.src = this.datauri;
    
    this.canvas.width = this.options.width + ( this.options.overlap * 2);
    this.canvas.height = this.options.height + ( this.options.overlap * 2);
    
    
    this.particles = this.createSparkles( this.canvas.width , this.canvas.height );
    
    this.anim = null;
    this.fade = false;
    
  },
  
  "createSparkles" : function( w , h ) {
    
    var holder = [];
    
    for( var i = 0; i < this.options.count; i++ ) {
      
      var color = this.options.color;
      
      if( this.options.color == "rainbow" ) {
        color = '#'+ ('000000' + Math.floor(Math.random()*16777215).toString(16)).slice(-6);
      } else if( $.type(this.options.color) === "array" ) {
        color = this.options.color[ Math.floor(Math.random()*this.options.color.length) ];
      }

      holder[i] = {
        position: {
          x: Math.floor(Math.random()*w),
          y: Math.floor(Math.random()*h)
        },
        style: this.sprites[ Math.floor(Math.random()*4) ],
        delta: {
          x: Math.floor(Math.random() * 1000) - 500,
          y: Math.floor(Math.random() * 1000) - 500
        },
        size: parseFloat((Math.random()*2).toFixed(2)),
        color: color
      };
            
    }
    
    return holder;
    
  },
  
  "draw" : function( time, fade ) {
        
    var ctx = this.context;
    
    ctx.clearRect( 0, 0, this.canvas.width, this.canvas.height );
          
    for( var i = 0; i < this.options.count; i++ ) {

      var derpicle = this.particles[i];
      var modulus = Math.floor(Math.random()*7);
      
      if( Math.floor(time) % modulus === 0 ) {
        derpicle.style = this.sprites[ Math.floor(Math.random()*4) ];
      }
      
      ctx.save();
      ctx.globalAlpha = derpicle.opacity;
      ctx.drawImage(this.sprite, derpicle.style, 0, 7, 7, derpicle.position.x, derpicle.position.y, 7, 7);
      
      if( this.options.color ) {  
        
        ctx.globalCompositeOperation = "source-atop";
        ctx.globalAlpha = 0.5;
        ctx.fillStyle = derpicle.color;
        ctx.fillRect(derpicle.position.x, derpicle.position.y, 7, 7);
        
      }
      
      ctx.restore();

    }
    
        
  },
  
  "update" : function() {
    
     var _this = this;
    
     this.anim = window.requestAnimationFrame( function(time) {

       for( var i = 0; i < _this.options.count; i++ ) {

         var u = _this.particles[i];
         
         var randX = ( Math.random() > Math.random()*2 );
         var randY = ( Math.random() > Math.random()*3 );
         
         if( randX ) {
           u.position.x += ((u.delta.x * _this.options.speed) / 1500); 
         }        
         
         if( !randY ) {
           u.position.y -= ((u.delta.y * _this.options.speed) / 800);
         }

         if( u.position.x > _this.canvas.width ) {
           u.position.x = -7;
         } else if ( u.position.x < -7 ) {
           u.position.x = _this.canvas.width; 
         }

         if( u.position.y > _this.canvas.height ) {
           u.position.y = -7;
           u.position.x = Math.floor(Math.random()*_this.canvas.width);
         } else if ( u.position.y < -7 ) {
           u.position.y = _this.canvas.height; 
           u.position.x = Math.floor(Math.random()*_this.canvas.width);
         }
         
         if( _this.fade ) {
           u.opacity -= 0.02;
         } else {
           u.opacity -= 0.005;
         }
         
         if( u.opacity <= 0 ) {
           u.opacity = ( _this.fade ) ? 0 : 1;
         }
         
       }
       
       _this.draw( time );
       
       if( _this.fade ) {
         _this.fadeCount -= 1;
         if( _this.fadeCount < 0 ) {
           window.cancelAnimationFrame( _this.anim );
         } else {
           _this.update(); 
         }
       } else {
         _this.update();
       }
       
     });

  },
  
  "cancel" : function() {
    
    this.fadeCount = 100;

  },
  
  "over" : function() {
    
    window.cancelAnimationFrame( this.anim );
    
    for( var i = 0; i < this.options.count; i++ ) {
      this.particles[i].opacity = Math.random();
    }
    
    this.fade = false;
    this.update();

  },
  
  "out" : function() {
    
    this.fade = true;
    this.cancel();
    
  },
  
  
  
  "datauri" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAHCAYAAAD5wDa1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDozNDNFMzM5REEyMkUxMUUzOEE3NEI3Q0U1QUIzMTc4NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDozNDNFMzM5RUEyMkUxMUUzOEE3NEI3Q0U1QUIzMTc4NiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjM0M0UzMzlCQTIyRTExRTM4QTc0QjdDRTVBQjMxNzg2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjM0M0UzMzlDQTIyRTExRTM4QTc0QjdDRTVBQjMxNzg2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+jzOsUQAAANhJREFUeNqsks0KhCAUhW/Sz6pFSc1AD9HL+OBFbdsVOKWLajH9EE7GFBEjOMxcUNHD8dxPBCEE/DKyLGMqraoqcd4j0ChpUmlBEGCFRBzH2dbj5JycJAn90CEpy1J2SK4apVSM4yiKonhePYwxMU2TaJrm8BpykpWmKQ3D8FbX9SOO4/tOhDEG0zRhGAZo2xaiKDLyPGeSyPM8sCxr868+WC/mvu9j13XBtm1ACME8z7AsC/R9r0fGOf+arOu6jUwS7l6tT/B+xo+aDFRo5BykHfav3/gSYAAtIdQ1IT0puAAAAABJRU5ErkJggg=="

}; 





// $('img.photo',this).imagesLoaded(myFunction)
// execute a callback when all images have loaded.
// needed because .load() doesn't work on cached images
 
// mit license. paul irish. 2010.
// webkit fix from Oren Solomianik. thx!
 
// callback function is passed the last image to load
//   as an argument, and the collection as `this`
 
 
$.fn.imagesLoaded = function(callback){
  var elems = this.filter('img'),
      len   = elems.length,
      blank = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
      
  elems.bind('load.imgloaded',function(){
      if (--len <= 0 && this.src !== blank){ 
        elems.unbind('load.imgloaded');
        callback.call(elems,this); 
      }
  }).each(function(){
     // cached images don't fire load sometimes, so we reset src.
     if (this.complete || this.complete === undefined){
        var src = this.src;
        // webkit hack from https://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
        // data uri bypasses webkit log warning (thx doug jones)
        this.src = blank;
        this.src = src;
     }  
  }); 
 
  return this;
};
   
    
    
</script>


<script>
    
    var HeartsBackground = {
  heartHeight: 60,
  heartWidth: 64,
  hearts: [],
  heartImage: 'assets/images/haert_new.jpg', 
  maxHearts: 8,
  minScale: 0.4,
  draw: function() {
    this.setCanvasSize();
    this.ctx.clearRect(0, 0, this.w, this.h);
    for (var i = 0; i < this.hearts.length; i++) {
      var heart = this.hearts[i];
      heart.image = new Image();
      heart.image.style.height = heart.height;
      heart.image.src = this.heartImage;
      this.ctx.globalAlpha = heart.opacity;
      this.ctx.drawImage (heart.image, heart.x, heart.y, heart.width, heart.height);
    }
    this.move();
  },
  move: function() {
    for(var b = 0; b < this.hearts.length; b++) {
      var heart = this.hearts[b];
      heart.y += heart.ys;
      if(heart.y > this.h) {
        heart.x = Math.random() * this.w;
        heart.y = -1 * this.heartHeight;
      }
    }
  },
  setCanvasSize: function() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.w = this.canvas.width;
    this.h = this.canvas.height;
  },
  initialize: function() {
    this.canvas = $('#canvas')[0];

    if(!this.canvas.getContext)
      return;

    this.setCanvasSize();
    this.ctx = this.canvas.getContext('2d');

    for(var a = 0; a < this.maxHearts; a++) {
      var scale = (Math.random() * (1 - this.minScale)) + this.minScale;
      this.hearts.push({
        x: Math.random() * this.w,
        y: Math.random() * this.h,
        ys: Math.random() + 1,
        height: scale * this.heartHeight,
        width: scale * this.heartWidth,
        opacity: scale
      });
    }

    setInterval($.proxy(this.draw, this), 30);
  }
};

$(document).ready(function(){
  HeartsBackground.initialize();
});
    
</script>
 -->

