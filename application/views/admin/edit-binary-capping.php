<?php
 echo$id;

?>
<style>
    .circle-wrapper {
        position: relative;
        width: 170px;
        top: -40px;
        left: -48px;
        height: 142px;
        padding: 110px 0px 0px 0px;
    }
    
    .circle-zero:before {
        content: "";
        background: rgba(0, 0, 0, 0.07);
        position: absolute;
        width: 168px;
        height: 2px;
        left: -60%;
        top: 50%;
        transform: rotate(45deg);
        z-index: -2;
    }
    
    .circle-zero {
        background: #1c1e21;
        border-radius: 50%;
        box-shadow: 0px 3px 7px 0.7px rgba(0, 0, 0, .5);
        box-sizing: content-box;
        height: 68px;
        width: 68px;
        position: absolute;
        top: 35%;
        left: 35%;
        text-align: center;
    }
    
    .circle-one:before {
        content: "";
        background: rgba(21, 167, 235, .7);
        border-radius: 50%;
        position: absolute;
        width: 4px;
        height: 4px;
        right: -56%;
        top: 45%;
        animation: circle-move 10s ease infinite;
    }
    
    .circle-one {
        border: 7px solid;
        border-color: rgb(28, 30, 33) rgb(132, 171, 52) transparent transparent;
        border-radius: 50%;
        box-sizing: content-box;
        height: 90px;
        width: 92px;
        position: absolute;
        top: 27%;
        left: 25%;
        z-index: -1;
        animation: circle-move 10s ease infinite;
    }
    
    .circle-two {
        background: rgba(0, 0, 0, .017);
        border: 15px solid;
        border-color: rgb(243, 162, 77) transparent transparent;
        border-radius: 50%;
        box-shadow: 15px 0 25px -20px rgba(0, 0, 0, 0.65);
        box-sizing: content-box;
        height: 90px;
        width: 90px;
        position: absolute;
        top: 25%;
        left: 21%;
        z-index: -2;
        animation: circle-move 14s ease infinite;
    }
    
    .circle-three {
        background: rgba(0, 0, 0, .012);
        border: 2px solid;
        border-color: #CE93D8 transparent;
        border-radius: 50%;
        box-sizing: content-box;
        height: 168px;
        width: 168px;
        position: absolute;
        z-index: -3;
        top: 15%;
        left: 15%;
        animation: circle-move 15s ease infinite;
    }
    
    .circle-shadow {
        background: rgba(0, 0, 0, 0.2);
        border-radius: 50%;
        filter: blur(2px);
        height: 20px;
        width: 160px;
        position: absolute;
        top: 100%;
        left: 18%;
    }
    
    @keyframes circle-move {
        0% {
            transform: rotate(0deg);
        }
        70% {
            transform: rotate(180deg);
        }
        100% {
            transform: rotate(0deg);
        }
    }
    
    .circle-zero h1 {
        position: relative;
        top: 0px;
        font-size: 22px;
        color: #ffffff;
    }
    
    .circle-zero p {
        position: relative;
        top: -25px;
        font-size: 14px;
        color: white;
    }
    
    .min-height-add:hover .add-animatio_a {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_b {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_c {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_d {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .gradient-bloody {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .extra_wrap .card-img-top {
        width: 20%;
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px);
        margin: 0 auto;
        padding: 10px 0px 0px 0px;
    }
    
    .Binsry_bg {
        background-image: url(./././assets/images/gallery/12.jpg);
    }
    
    .Binsry_text {
        padding: 0px 10px 20px 10px;
        text-align: center;
        font-size: 1.2rem;
        /* float: left; */
        /* font-family: cursive; */
        /* display: inline-block; */
        color: #f77e03;
    }
    
    .sparkley {
        background: lighten( $buttonBackground, 10%);
        display: inline-block;
        border: none;
        padding: 16px 36px;
        font-weight: normal;
        border-bottom: 11px solid #87ad37;
        background: #151515;
        color: white;
        margin-bottom: 21px;
        margin: 0 auto;
        margin-bottom: 10px;
        border-radius: 3px;
        transition: all 0.25s ease;
        box-shadow: 0 38px 32px -23px black;
        &:hover {
            background: $buttonBackground;
            color: transparentize( $buttonColor, 0.8);
        }
    }
    
    .prize3 {
        margin: 58px 0px 0px 0px;
    }
    
    header a,
    a:visited {
        text-decoration: none;
        color: #fcfcfc;
    }
    
    @keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-webkit-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-moz-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-ms-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-webkit-keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-moz-keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-ms-keyframes move-clouds-back {
        from {
            background-position: 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    .stars,
    .twinkling,
    .clouds {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        display: block;
    }
    
    .stars {
        background: #000 url(<?=base_url('resource/admin')?>/images/stars.png) repeat top center;
        z-index: 0;
    }
    
    .twinkling {
        z-index: 1;
        -moz-animation: move-twink-back 200s linear infinite;
        -ms-animation: move-twink-back 200s linear infinite;
        -o-animation: move-twink-back 200s linear infinite;
        -webkit-animation: move-twink-back 200s linear infinite;
        animation: move-twink-back 200s linear infinite;
    }
    
    .clouds {
        background: transparent url(http://www.script-tutorials.com/demos/360/images/clouds3.png) repeat top center;
        z-index: 3;
        -moz-animation: move-clouds-back 200s linear infinite;
        -ms-animation: move-clouds-back 200s linear infinite;
        -o-animation: move-clouds-back 200s linear infinite;
        -webkit-animation: move-clouds-back 200s linear infinite;
        animation: move-clouds-back 200s linear infinite;
    }
    
    .text_highlightani {
        font-size: 22px;
        font-weight: bold;
    }
    
    .hight_light_text_two {
        font-size: 26px;
        font-size: 26px;
        font-weight: bolder;
        color: black;
        padding: 0px 4px;
        background: linear-gradient(to right, #87ad37 20%, #929292 40%, #f5881f 60%, #d89328 80%);
        background-size: 200% auto;
        background-clip: text;
        text-fill-color: transparent;
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        animation: shine 1s linear infinite;
    }
    
    @keyframes shine {
        to {
            background-position: 200% center;
        }
    }
    
    #canvas {
        position: absolute;
        z-index: 1;
    }
    
    .overflow {
        overflow: hidden;
    }
    
    .min_height_set {
        min-height: 492px;
    }
    
    .prize1 {
        margin: 44px 0px;
    }
</style>

<body>
<?php


//echo $data[0]['amount'];
foreach ($data as $key => $value) {
  
}
?>
    <!-- Start wrapper-->
    <div id="wrapper">
        <div class="demo"></div>
        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">
                
                 <div class="row">
                    
                          <div class="col-lg-6">
                              <div class="card">
                                  <div class="card-header"><i class="fa fa-table"></i>Change Packages</div>
                                  
                                  <div class="alert alert-warning" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
                                  
                                  <div class="card-body">
                                      <div class="card-content p-2">
                                           
                                        
                        <!----add new package-->
                          <div class="col-lg-6">
                              <div class="card">
                                  <div class="card-header"><i class="fa fa-table"></i>Add New Package</div>
                                  <div class="card-body">
                                      <div class="card-content p-2">
                                          <h2><?php echo $this->session->flashdata('message'); ?></h2>
                                       <?php echo form_open_multipart("admin/binary_capping_update/".$value['id']);?>
                                          <div class="form-group">
                                             <div><?php echo $this->session->flashdata('success'); ?></div>
                                                  <div class="position-relative has-icon-right">
                                                      

          <label for="timesheetinput1">Amount($)</label>
          <input type="text" id="amount" class="form-control form-control-rounded" name="amount"  value="<?= $value['amount'];?>" >

          <label for="timesheetinput1">Capping%</label>
        <input type="text" id="amount" class="form-control form-control-rounded" name="capping"  value="<?= $value['capping_percent'];?>">

          <label for="timesheetinput1">Capping-Amount</label>
<input type="text" id="amount" class="form-control form-control-rounded" name="percent"  value="<?= $value['capping_amount'];?>">

                                                      
                                                      
                                                  </div>
                                                  <span class="error_msg amt_error"></span>
                                          </div>
                                            <button type="submit" name="withdraw" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Update
                                            </button>
                                          <?php echo form_close();?>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        
                                 
                      
            

       


               