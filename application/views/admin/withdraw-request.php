

<?php 
/*$con=connectdb();
$data=mysqli_query($con,"select * from withdrawal_request order by status DESC");*/
?>

<body>

    <!-- Start wrapper-->
    <div id="wrapper">
        
        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Withdraw Request</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Withdraw Request</div>
                            <div class="card-body">
                                
                                <div class="text-danger"><strong>
                                      
                                  <?php
                                    if ( $this->session->flashdata('withdwal_message')) {
                                        echo $this->session->flashdata('withdwal_message'); 
                                
                                    }else{
                                        echo $this->session->flashdata('failed');  
                                    }
                                  ?>                  
                                      
                                 </strong></div>     

<!--                                <h5><i class="fa fa-search"></i> Search By</h5>
                                <div class="row mt-3">
                                    <div class="col-md-1 text-center">
                                        <label>User Id</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="text" name="" class="form-control" placeholder="User Id" id='withdraw_userid'>
                                    </div>

                                    <div class="col-md-1 text-center">
                                        <label>Status</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <select class="form-control" id='withdraw_planid'>
                                            <option value="">--Select Status--</option>
                                            <option value="2">New</option>
                                            <option value="1">Accept</option>
                                            <option value="0">Decline</option>

                                        </select>
                                    </div>

                                    <div class="col-md-1 text-center">
                                        <label>From</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="date" name="" class="form-control" placeholder="From" id="withdraw_date_from">
                                    </div>

                                    <div class="col-md-1 text-center">
                                        <label>To</label>
                                    </div>
                                    <div class="col-md-2 form-group no-padding">

                                        <input type="date" name="" class="form-control" placeholder="To" id="withdraw_date_to">
                                    </div>
                                </div>-->

                                <div class="table-responsive actual_data">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>UserName</th>
                                                <th>Email </th>
                                                <th>BTC Address</th>
                                                <th>BTC Value</th>
                                                <th>Request Amt($)</th>
                                                <th>Status</th>
                                                <th>Date</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                           <?php 
                                           $i=1;
                                           foreach($all_withrowal as $row){
                                            
                                            if($row['status']=='1'){
                                              $checked="checked";
                                            }
                                            else{
                                              $checked="";
                                            }
                                            ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $i;?>
                                                    </td>
                                                    <td>
                                                        <?php echo $row['user_id'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $row['first_name']." ".$row['last_name'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $row['user_email'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $row['btc_address'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $row['btc_value'];?>
                                                    </td>                                                    
                                                    <td>
                                                        <?php echo $row['request_amount'];?>
                                                    </td>

                                                   <?php if($row['status']=='2'){ ?>
                                                    <td>
                                                        
                                                        <a href="javascript:;"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" id="accept<?php echo $i; ?>"> Accept</a>
                                                        <a href="javascript:;"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" id="cancel<?php echo $i; ?>"> Cancel</a>
                                                        <input type="hidden" id="user_id<?php echo $i; ?>" value="<?php echo $row['id'];?>">
                                                    </td>
                                                    
                                                  <?php }else if($row['status']=='0'){
                                                      
                                                     echo '<td><span class="badge badge-danger shadow-danger">Cancel</span></td>'; 
                                                  }else{ 
                                                      
                                                    echo '<td><span class="badge badge-success shadow-success">Accept</span></td>'; 
                                                   } ?>
                                                           
                                                    <td>
                                                        <?php echo $row['request_date'];?>
                                                    </td>
                                                </tr>
                                                
                                                
                                                <div class="modal" tabindex="-1" role="dialog" id="reply_modal<?php echo $i; ?>">
                                                    <div class="modal-dialog noti_modal">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" id="close1<?php echo $i; ?>">&times;</button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="card-content p-2">
                                                                            <form method="post" action="<?= base_url('admin/accept_withdrawal')?>">

                                                                                <input type="hidden" name="withd_id" value="<?php echo $row['id']; ?>">

                                                                                <div class="form-group">
                                                                                    <div class="position-relative has-icon-right">
                                                                                        <label for="timesheetinput1">Reply</label>

                                                                                        <textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea>

                                                                                    </div>
                                                                                    <span class="error_msg amt_error"></span>
                                                                                </div>

                                                                                <button type="submit" name="reply" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                                                                    Accept
                                                                                </button>

                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>     
                                                
                                                
                                                <div class="modal" tabindex="-1" role="dialog" id="reply_modal_cancel<?php echo $i; ?>">
                                                    <div class="modal-dialog noti_modal">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" id="close2<?php echo $i; ?>">&times;</button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="card-content p-2">
                                                                            <form method="post" action="<?= base_url('admin/cancel_withdrawal')?>">

                                                                                <input type="hidden" name="withd_id" value="<?php echo $row['id']; ?>">

                                                                                <div class="form-group">
                                                                                    <div class="position-relative has-icon-right">
                                                                                        <label for="timesheetinput1">Reply</label>

                                                                                        <textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea>

                                                                                    </div>
                                                                                    <span class="error_msg amt_error"></span>
                                                                                </div>

                                                                                <button type="submit" name="reply" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                                                                    Cancel
                                                                                </button>

                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>                                                 

                                                <script type="text/javascript">
                                                
                                                    $("#accept<?php echo $i; ?>").click(function() {
                                                        
                                                        /*alert('accept jai');*/
                                                        $("#reply_modal<?php echo $i; ?>").modal('show');
                                                    });
                                                
                                                
                                                    $("#cancel<?php echo $i; ?>").click(function() {
                                                        
                                                        /*alert('cancel jai');*/
                                                        $("#reply_modal_cancel<?php echo $i; ?>").modal('show');
                                                    });
                                            
                                                    $("#close1<?php echo $i; ?>").click(function() {
                                                        $("#reply_modal<?php echo $i; ?>").modal('hide');
                                                        //history.go();
                                                    });  
                                                    
                                                    $("#close2<?php echo $i; ?>").click(function() {
                                                        $("#reply_modal_cancel<?php echo $i; ?>").modal('hide');
                                                        //history.go();
                                                    });                                                     
                                                
                                                
                                                    $("#status<?php echo $i; ?>").change(function() {
                                                        var user = $("#user_id<?php echo $i; ?>").val();
                                                        if ($(this).prop("checked") == true) {
                                                            //alert("Checkbox is checked.");
                                                            $.ajax({
                                                                type: 'post',
                                                                url: 'ajax.php',
                                                                data: 'user=' + user + '&action=1',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        } else if ($(this).prop("checked") == false) {
                                                            //alert("Checkbox is unchecked.");
                                                            $.ajax({
                                                                type: 'post',
                                                                url: 'ajax.php',
                                                                data: 'user=' + user + '&action=0',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                    $("#status1<?php echo $i; ?>").change(function() {
                                                        var user = $("#user_id<?php echo $i; ?>").val();
                                                        if ($(this).prop("checked") == true) {
                                                            //alert("Checkbox is checked.");
                                                            $.ajax({
                                                                type: 'post',
                                                                url: 'ajax.php',
                                                                data: 'userde=' + user + '&action=0',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        } else if ($(this).prop("checked") == false) {
                                                            //alert("Checkbox is unchecked.");
                                                            $.ajax({
                                                                type: 'post',
                                                                url: 'ajax.php',
                                                                data: 'user=' + user + '&action=0',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                </script>
                                                
                                              <?php
                                              $i++;
                                               }
                                               ?>
                                               
                                            </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>UserName</th>
                                                <th>Email </th>
                                                <th>BTC Address</th>
                                                <th>BTC Value</th>
                                                <th>Request Amt($)</th>
                                                <th>Status</th>
                                                <th>Date</th>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="table-responsive filter_data">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                        