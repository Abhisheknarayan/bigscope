<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Binary Capping</h4>
                        <ol class="breadcrumb">

                        </ol>
                    </div>
                </div>

                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Binary Capping</div>

                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Amount</th>
                                                <th>Capping Percent</th>
                                                <th>Capping Amount</th>
                                                <th>Action</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        
                                            <?php $i=1;
                                             foreach ($code->result() as $row) : 
                                            ?>
                                        
                                        <tbody>

                                            <tr>
                                                <td>
                                                    <?php echo $i++;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->amount;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->capping_percent;?>
                                                </td>

                                                <td>
                                                    <?php echo $row->capping_amount;?>
                                                </td>
                                                
                                                 
                                                <?php 
                                                
                                                    /*if ($row->status==0) {
                                                      echo "<td>active</td>"; 
                                                    }else{
                                                      echo "<td></td>";
                    
                                                    }*/
                                                ?> 
                                                       

                                                <td>
                                                    <?php echo $row->create_date;?>
                                                </td>
                                                <td>
                                                    <a href="<?= base_url('admin/edit_binary_capping/').$row->id ;?>">
                                                        <button class="btn btn-info" type="button" name="Edit">Edit</button>
                                                    </a>&nbsp
                                                </td>

                                            </tr>

                                            <?php  endforeach; ?>
                                            
                                            
                                            
                                        </tbody>
                                            
                                            <tfoot>
                                                <tr>
                                                    <th>Sr No</th>
                                                    <th>Amount</th>
                                                    <th>Capping Percent</th>
                                                    <th>Capping Amount</th>
                                                    <th>Date</th>
                                                    <th>Action</th>

                                                </tr>
                                            </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>