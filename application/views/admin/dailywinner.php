

<body>
<!-- Start wrapper-->
 <div id="wrapper">
  <!--Start sidebar-wrapper-->
  
<!--End topbar header-->
<div class="clearfix"></div>
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">
 <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Activation Code history</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
      </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Activation Code history</div>
            <div class="card-body">

              

              <div class="table-responsive actual_data">
              <table id="example" class="table table-bordered">
                      <thead>
                        <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                              
                               <th>Lucky Number</th>
                               <th>User Winning Amount</th>
                               <th>Total Winning Amount</th>
                               <th>Date</th>
              
                        </tr>
                      </thead>
                      <tbody>
                        </tbody>
                      
                   </table>
                  </div>
                  <div class="table-responsive filter_data">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>        
        

     