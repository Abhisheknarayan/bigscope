<!-- Start wrapper-->
    <div id="wrapper">
        <div class="demo"></div>
        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Packages</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table"></i> Packages List
                            </div>
                            <div class="card-body">
                                <a class=" pull-right" href="<?= base_url('admin/constant/');?>"><button class="btn btn-info ">Add<i class="fa fa-plus " aria-hidden="true" title="ADD NEW PACKAGE" style="color:#008cff;"></i> </button></a>
                                <div class="table-responsive actual_data">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sno</th>
                                                <th>Name</th>
                                                <th>Amount($)</th>
                                                <th>Days</th>
                                                <!--<th>Percent(%)</th>-->
                                                <th>withdraw Limit($)</th>
                                                <th>Action</th>


                                            </tr>
                                        </thead>
                                         <?php $i=1; foreach ($package->result() as $row) : ?>
                                        <tbody>

                                           


                                            <tr>
                                                <td>
                                                    <?php echo $i++;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->name;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->amount;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->days;?>
                                                </td>
                                                <!--<td>-->
                                                <!--    <?php echo $row->percent;?>-->
                                                <!--</td>-->
                                                <td>
                                                    <?php echo $row->withdraw_limit;?>
                                                </td>
                                                <td><a href="<?= base_url('admin/constant/').$row->id ;?>"><button class="btn btn-info" type="button" name="Edit" >Edit</button></a>&nbsp
                                                    <a href="<?= base_url('admin/package_delete/').$row->id ;?>"><button class="btn btn-danger" type="button" name="delete" onclick="return confirm('Are you sure?')">Delete</button></a>
                                                </td>

                                            </tr>

                                            <?php  endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sno</th>
                                                <th>Name</th>
                                                <th>Amount($)</th>
                                                <th>Days</th>
                                                <!--<th>Percent(%)</th>-->
                                                <th>withdraw Limit($)</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="table-responsive filter_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            </div>
            <!-- End container-fluid-->

       