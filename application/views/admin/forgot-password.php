<?php include("panel/header.php"); ?>

	<body>
		<div  class="login-img">
			<div id="global-loader" ></div>
			<div class="page">
				<div class="page-single">
					<div class="container ">
						<div class="row authentication">
							<div class="col col-login mx-auto">
								<div class="text-center mb-6">
									<img src="assets/images/brand/logo.png" class="h-8" alt="">
								</div>
								<form class="card" method="post">
									<div class="card-body p-6">
										<div class="text-center card-title">Forgot password</div>
											<div class="input-icon form-group">
												<span class="input-icon-addon search-icon">
													<i class="zmdi zmdi-email"></i>
												</span>
												<input type="text" class="form-control" placeholder="Email">
											</div>
											<div class="form-footer">
												<button type="submit" class="btn btn-primary btn-block">Send</button>
											</div>
											<div class="text-center text-muted mt-3 ">
											Forget it, <a href="login.php">send me back</a> to the sign in screen.
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		

		<!-- Dashboard Js -->
		<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
		<script src="assets/js/vendors/selectize.min.js"></script>
		<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="assets/js/vendors/circle-progress.min.js"></script>
		<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
		
		<!-- Fullside-menu Js-->
		<script src="assets/plugins/fullside-menu/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/fullside-menu/waves.min.js"></script>
		
		<!-- Custom scroll bar Js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- Custom Js-->
		<script src="assets/js/custom.js"></script>
	</body>

<!-- Mirrored from www.spruko.com/demo/adminor/html/Leftmenu-Fullwidth-Draksidebar/forgot-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Dec 2018 06:34:19 GMT -->
</html>
