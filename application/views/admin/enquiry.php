

<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">All Enquiry</h4>
                        <ol class="breadcrumb">
                            
                        </ol>
                    </div>
                </div>
                    
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> All Enquiry</div>

                            <div class="card-body">
                                
                                <div class="text-danger"><strong>
                                      
                                                
                                      
                                 </strong></div> 
                                 
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">

                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Action</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Message</th>
                                               
                                               
                                                <th>Date</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            $support_tickect = $this->db->query("select * from enquiry order by id desc ")->result_array();
                                            //echo "<pre>"; print_r($support_tickect); die();
                                            $i = 0;
                                            foreach($support_tickect as $display_plan) {
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td class="center">
                                                        <?php echo ++$i; ?>
                                                    </td>
                                                    
                                                    <td class="center">
                                                        <?php echo $display_plan['name']; ?>
                                                    </td>
                                                    <td class="center">
                                                        <?php echo $display_plan['email']; ?>
                                                    </td>
                                                    <td class="center"><p>
                                                        <?php echo $display_plan['comment']; ?>
                                                        </p>
                                                    </td>
                                                    
                                                    <td class="center">
                                                        <?php echo $display_plan['date']; ?>
                                                    </td>
                                                    
                                                    <td class="menu-action">
                                                        <a href="#"  id = "<?php echo $display_plan['id'] ?>" data-original-title="Delete" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red delete_enquiry"><i class="fa fa-times"></i></a>
                                                    </td>
                                                    
                                                </tr>

                                                <div class="modal" tabindex="-1" role="dialog" id="reply_modal<?php echo $i; ?>">
                                                    <div class="modal-dialog noti_modal">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" id="close1<?php echo $i; ?>">&times;</button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="card-content p-2">
                                                                        
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Action</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Message</th>
                                               
                                               
                                                <th>Date</th>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
         <script type="text/javascript">
                $("#status<?php echo $i; ?>").change(function() {
                    var user = $("#user_id<?php echo $i; ?>").val();
                    if ($(this).prop("checked") == true) {
                        //alert("Checkbox is checked.");
                        $.ajax({
                            type: 'post',
                            url: "<?= base_url('admin/change_enquiry_status')?>",
                            data: 'support=' + user + '&action=1',
                            success: function(data) {
                                if (data == '1') {
                                    history.go();
                                } else {
                                    alert("Please Try Again");
                                }
                            }
                        });
                    } 
                    else if ($(this).prop("checked") == false) {
                        //alert(user);
                        $.ajax({
                            type: 'post',
                            url: "<?= base_url('admin/change_enquiry_status')?>",
                            data: 'support=' + user + '&action=0',
                            success: function(data) {
                                if (data == '1') {
                                    history.go();
                                } else {
                                    alert("Please Try Again");
                                }
                            }
                        });
                    }
                });
                    
                    
                $("#reply<?php echo $i; ?>").click(function() {
                    $("#reply_modal<?php echo $i; ?>").modal('show');
                });
                
                $("#close1<?php echo $i; ?>").click(function() {
                    $("#reply_modal<?php echo $i; ?>").modal('hide');
                    //history.go();
                });
                
                
                
                $(".delete_enquiry").on('click',function(event) {
                    console.log('are you sure');
                    event.preventDefault();
                    if(!confirm('Are you sure!')){
                       return false; 
                    }
                    var id = $(this).attr('id');
                    $.ajax({
                            type: 'post',
                            url: "<?= base_url('admin/delete_enquiry')?>",
                            data: 'support=' + id,
                            success: function(data) {
                                if (data == '1') {
                                    history.go();
                                    alert("Query Deleted Successfully");
                            
                                } else {
                                    alert("Please Try Again");
                                }
                            }
                    });                                                         
                
                })                                                    
                
        </script>   
 


