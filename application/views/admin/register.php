

	<body>
		<div  class="login-img">
			<div id="global-loader" ></div>
			<div class="page">
				<div class="page-single">
					<div class="container">
						<div class="row">
							<div class="col col-login mx-auto">
								<div class="text-center mb-6">
									<img src="assets/images/brand/logo.png" class="h-8" alt="">
								</div>
								<form class="card authentication"  method="post">
									<div class="card-body sign-up-page p-7">
										<div class="card-title text-center">Create New Account</div>
										<div class="input-icon form-group">
											<span class="input-icon-addon search-icon">
												<i class="mdi mdi-account"></i>
											</span>
											<input type="text" class="form-control" placeholder="Username">
										</div>
										<div class="input-icon form-group">
											<span class="input-icon-addon search-icon">
												<i class="zmdi zmdi-email"></i>
											</span>
											<input type="text" class="form-control" placeholder="Email">
										</div>
										<div class="input-icon form-group">
											<span class="input-icon-addon search-icon">
												<i class="zmdi zmdi-lock"></i>
											</span>
											<input type="password" class="form-control mb-0" id="exampleInputPassword1" placeholder="Password">
										</div>
										<div class="form-group mt-5">
											<label class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" />
												<span class="custom-control-label">Agree the <a href="terms.html">terms and policy</a></span>
											</label>
										</div>
										<div class="form-footer">
											<button type="submit" class="btn btn-primary btn-block">Create new account</button>
										</div>
										<div class="text-center text-muted mt-4">
										Already have account? <a href="login.php">Sign in</a>
									</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Dashboard js -->
		<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
		<script src="assets/js/vendors/selectize.min.js"></script>
		<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="assets/js/vendors/circle-progress.min.js"></script>
		<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
		
		<!-- Fullside-menu Js-->
		<script src="assets/plugins/fullside-menu/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/fullside-menu/waves.min.js"></script>
		
		<!-- Custom scroll bar Js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- Custom Js-->
		<script src="assets/js/custom.js"></script>
	</body>

<!-- Mirrored from www.spruko.com/demo/adminor/html/Leftmenu-Fullwidth-Draksidebar/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Dec 2018 06:34:19 GMT -->
</html>