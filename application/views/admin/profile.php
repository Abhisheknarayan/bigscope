
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
 

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

    	<div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">User Profile</h4>
		    <!-- <ol class="breadcrumb"> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Pages</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">User Profile</li> -->
         <!-- </ol> -->
	   </div>
	    </div>
    

   
      <div class="row">
        <div class="col-lg-4">
           <div class="profile-card-3">
            <div class="card">
            <form id="myform" enctype="multipart/form-data">
				 <div class="user-fullimage">
				   <img src="<?=base_url('resource/admin')?>/images/avatars/avatar-1.png" alt="user avatar" class="img-circle mCS_img_loaded img-responsive" id="show_img">
				   <div class="image-upload">
	<label for="file-input">
		<i class="fa fa-camera"></i>
				 </label>

				<input id="file-input" type="file" name="profile" />
					</div>
				    <div class="details">
				      <h5 class="mb-1 text-white ml-3"></h5>
					  <h6 class="text-white ml-3"></h6>
					 </div>
				  </div>
			</form>
             </div>
			</div>
        </div>
        <div class="col-lg-8">
           <div class="card">
            <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                <li class="nav-item">
                    <a href="javascript:void();" data-target="#profile" data-toggle="pill" class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">Profile</span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void();" data-target="#messages" data-toggle="pill" class="nav-link"><i class="icon-envelope-open"></i> <span class="hidden-xs">Change Password</span></a>
                </li>
               
            </ul>
            <div class="tab-content p-3">
                <div class="tab-pane active" id="profile">
           
                    <div class="row">
                    	<div class="col-lg-10 ">
						   <div class="card">
						     <div class="card-body">
							   <div class="card-title">Edit Profile</div>
							   <hr>
							    <form method="post" enctype="multipart/form-data">
								 <div class="form-group">
								  <label for="input-1">Name</label>
								  <input type="text" class="form-control" name="name" value="">
								 </div>
								 <div class="form-group">
								  <label for="input-2">Email</label>
								  <input type="email" id="input-2" class="form-control" name="email" value="">
								 </div>
								 <div class="form-group">
								  <label for="input-3">Mobile Number</label>
								  <input type="text" id="input-3" class="form-control" name="mobile" value="">
								 </div>
								
								 <div class="form-group">
								  <button type="submit" name="edit_profile" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Update</button>
								</div>
								</form>
							 </div>
						   </div>
						  
						</div>
								
					</div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="messages">
                    <div class="alert alert-info alert-dismissible" role="alert">
				   <button type="button" class="close" data-dismiss="alert">&times;</button>
				   
                  </div>
                   <div class="col-lg-10 ">
			   <div class="card">
			     <div class="card-body">
				   <div class="card-title">Change Password</div>
				   <hr>
				    <form method="post" action="">
					 <div class="form-group">
					  <label for="input-1">Old Password</label>
					  <input type="password" id="timesheetinput1" class="form-control" name="oldpass">
					 </div>
					 <div class="form-group">
					  <label for="input-2">New Password</label>
					  <input type="password" id="timesheetinput1" class="form-control" name="newpass">
					 </div>
					 <div class="form-group">
					  <label for="input-3">Confirm Password</label>
					  <input type="password" id="timesheetinput1" class="form-control" name="confirmpass">
					 </div>
					 <div class="form-group">
					  <button type="submit" name="change_pass" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Change</button>
					</div>
					</form>
				 </div>
			   </div>
			  
			</div>
                </div>
               
            </div>
        </div>
      </div>
      </div>
        
    </div>

    </div>
    <!-- End container-fluid-->
   </div>	
								
					  
			<!--footer-->
			

			<!-- <script type="text/javascript">
				$("#file-input").change(function(){
					if (this.files && this.files[0]) {
					    var reader = new FileReader();

					    reader.onload = function(e) {
									      $('#show_img').attr('src', e.target.result);
									    }
							     // view uploaded file.
							     reader.readAsDataURL(this.files[0]);

					    $.ajax({
						    url: "ajax.php",
						   type: "POST",
						   data:  new FormData($("#myform")[0]),
						   contentType: false,
						   cache: false,
						   processData:false,
						  
						   success: function(data)
						     {
							    if(data=='0' || data=="")
							    {
							     // invalid file format.
							     alert("invalid");
							    }
							    else
							    {
							    	history.go();
							    }
						     }
						               
						    });
					}
					
				});
			</script> -->
			<script>
$(document).ready(function() {
    $('#example').DataTable( {  
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],   
        dom: 'Bfrtip',        
        buttons: [            
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'
            
        ]
    } );
} );
</script>