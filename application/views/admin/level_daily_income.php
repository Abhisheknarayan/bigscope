



<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Binary Income</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Binary Income</div>
            <div class="card-body">

              <h5><i class="fa fa-search"></i> Search By</h5>
                  <div class="row mt-3">
                    <div class="col-md-1 text-center">
                      <label>User Id</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <input type="text" name="" class="form-control" placeholder="User Id" id='level_userid'>
                    </div>

                    <!--<div class="col-md-1 text-center">-->
                    <!--  <label>Level Id</label>-->
                    <!--</div>-->
                    <!--<div class="col-md-1 form-group no-padding">-->
                    <!--  <input type="text" name="" class="form-control" placeholder="Level Id" id='level_planid'>-->
                      
                    <!--</div>-->

                    <div class="col-md-1 text-center">
                      <label>Level</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      <input type="number" name="" class="form-control" placeholder="Level" id='level_level' min="1">
                      
                    </div>

                    <div class="col-md-1 text-center">
                      <label>Daily Return PER(%)</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      <input type="number" name="" class="form-control" placeholder="ROI %" id='level_roiper' min="1">
                      
                    </div>

                    <div class="col-md-1 text-center">
                      <label>Date From</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="From" id="level_date_from">
                    </div>

                    <div class="col-md-1 text-center">
                      <label>Date To</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="To" id="level_date_to">
                    </div>
                  </div>
              <div class="table-responsive actual_data">
              <table id="example" class="table table-bordered">
                      <thead>
                        <tr>
                         <th>SNo</th>
                              <th>User Id</th>
                               <th>Team Id</th>
                               <th>Level</th>
                               <th>Purchase Amount ($)</th>
                               <th>Daily Return Income</th>
                                <th>Daily Return Per(%)</th>
                                <th>Income($)</th>
                                <th>Date</th>
              
                        </tr>
                      </thead>
                      <tbody>
                         <!--  <?php
                           $cnt=1;
                            while($row = mysqli_fetch_array($query)) 
                            {  
                                $rf_id_gt = $row['ref_id'];
                                $ref_id_gt_qry = mysqli_query($con, "SELECT * FROM `users` WHERE `refid`='".$rf_id_gt."'"); 
                                $ref_id_gt_result = mysqli_fetch_array($ref_id_gt_qry);

                                $gt_qry1 = mysqli_query($con, "SELECT * FROM purchase_list WHERE user_rf_id='".$row['ref_id']."'"); 
                                $gt_result1 = mysqli_fetch_array($gt_qry1);
                            ?>
                            <tr>
                                <td><?php echo $cnt; ?></td>
                                 <td><?php echo $row['register_id']; ?></td>
                                <td><?php echo $row['toroi_id']; ?></td>
                                <td><?php echo $row['level']; ?></td>
                               
                                <td><?php echo $row['amount']; ?></td>
                                 <td><?php echo $row['dailyreturn_amt']; ?></td>
                                <td><?php echo $row['level_percent']; ?></td>
                                   
                                     <td><?php echo $row['level_roi_amount']; ?></td>
                                 <td><?php echo $row['level_roi_date']; ?></td>
                            </tr>
                            <?php $cnt++; }  ?> -->
                        </tbody>
                         
                    </table>
                  </div>
                  <div class="table-responsive filter_data">
                  </div>
                </div>
              </div>
            </div>
          </div>
        
  