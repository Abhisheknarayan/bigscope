
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  

<!--Start topbar header-->
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title text-center"> Notification</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Send Notification</div>
            <div class="card-body">
						 <div class="card-content p-2">
							<form method="post" action="">
								
								<div class="form-group">
											<div class="position-relative has-icon-right">
											<label for="timesheetinput1">Users</label>
									
										<select id="chkveg" multiple="multiple" class="form-control form-control-rounded" name="users[]">
										<!-- <?php
											$user_qry=mysqli_query($con,"select * from users where status='1'");
											while($user_row=mysqli_fetch_array($user_qry)){
										?>
										    <option value="<?php echo $user_row['refid'] ?>"><?php echo $user_row['refid'] ?></option>
										<?php } ?> -->

										</select>

										
										
									</div>
									
								</div>	
									
								<div class="form-group">
											<div class="position-relative has-icon-right">
											<label for="timesheetinput1">Subject</label>
									
										<input type="text" class="form-control form-control-rounded" name="subject" required>

										
										
									</div>
									
								</div>

										<div class="form-group">
											<div class="position-relative has-icon-right">
									<label for="timesheetinput1">Message</label>
									
										<textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea> 
										
									</div>
									<span class="error_msg amt_error"></span>
								</div>
								
								
							

							
								<button type="submit" name="notification" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
									 Send
								</button>
								
							
								</form>
							
						</div>
					</div>
				</div>
			</div>

			<!--footer-->
			
<script type="text/javascript">

$(function() {

    $('#chkveg').multiselect({
    	//columns: 1,     // how many columns should be use to show options
  search : false, // include option search box
 

    	texts: {

		      placeholder    : 'Select Users', // text to use in dummy input
		      search         : 'Search',         // search input placeholder text
		      selectedOptions: ' Selected',      // selected suffix text
		      selectAll      : 'Select All',     // select all text
		      unselectAll    : 'Unselect all',   // unselect all text
		      noneSelected   : 'None Selected'   // None selected text
		},

  selectAll          : true, // add select all option
  selectGroup        : false, // select entire optgroup
  minHeight          : 200,   // minimum height of option overlay
  maxHeight          : null,  // maximum height of option overlay
  maxWidth           : null,  // maximum width of option overlay (or selector)
  maxPlaceholderWidth: null, // maximum width of placeholder button
  maxPlaceholderOpts : 10, // maximum number of placeholder options to show until "# selected" shown instead
  showCheckbox       : true,  // display the checkbox to the user
  optionAttributes   : [],

   onLoad        : function( element ){},  // fires at end of list initialization
  onOptionClick : function( element, option ){}, // fires when an option is clicked
  onControlClose: function( element ){}, // fires when the options list is close
  onSelectAll   : function( element ){$("#ms-list-1").removeClass("ms-active");},         // fires when (un)select all is clicked

  minSelect: false, // minimum number of items that can be selected

  maxSelect: false,
    });

   
});

</script>