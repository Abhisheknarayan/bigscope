<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Binary Income</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Binary Income</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>SNo</th>
                            <th>UserId</th>
                            <th>Binary Income($)</th>
                            <th>Income Type</th>
                            <th>Matching Amt($)</th>
                            <th>Left Carryforward($)</th>
                            <th>Right Carryforward($)</th>
                            <th>Percent(%)</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    
                    <?php 
                    
                      $i=1;
                      foreach($binary as $row):?>
                        <tbody>
                          
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $row->usid; ?></td>
                                <td><?php echo $row->amount; ?></td>
                                <td><?php echo $row->income_type; ?></td>
                                <td><?= $row->matching_amt ?></td>
                                <td><?= $row->leftmatching ?></td>
                                <td><?= $row->rightmatching ?></td>
                                <td><?= $row->percentage ?></td>
                                <td><?php echo $row->tm; ?></td>
                            </tr>
                            
                        </tbody>
                    <?php
                        endforeach;
                    ?>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>

    
          
