<?php

// foreach($list as $val){
    
// }
?>
<body>
    <div id="wrapper">
      <div class="demo"></div>
        <div class="clearfix"></div>
        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">User profile</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
            
                            <!---update %--->
                            
                              <div class="card"> 
                             
                                  <div class="card-header"><i class="fa fa-table"></i>Edit Details</div>
                                  <div class="card-body" >
                                     <?php foreach($list as $val){ } ; ?> 
                                     <?php echo form_open_multipart('admin/update_user_profile/'.$val->user_id.'');?>
                                     
                                     
                                      <div class="card-content p-2">
                                          
                                          <div class="form-group">
                                                  <div class="position-relative has-icon-right">
                                                      
                                                      <label for="timesheetinput1">First name</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="fname" value="<?= $val->first_name; ?>">
                                                      
                                                      <label for="timesheetinput1">Last name</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="lname" value="<?= $val->last_name; ?>">
                                                      
                                                      
                                                      <label for="timesheetinput1">Email</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="email" value="<?= $val->user_email; ?>">
                                                      
                                                      <label for="timesheetinput1">Phone</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="mobile" value="<?= $val->user_mobile; ?>">
                                                      
                                                      <label for="timesheetinput1">Password</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="pass" value="<?= $val->password; ?>">
                                                      
                                                      <label for="timesheetinput1">Bitcoin wallet address</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="btcadd" value="<?= $val->bitcoin_wallet_address; ?>" maxlength="34">
                                                      <label for="timesheetinput1">Status</label>
                                                      <select name="status">
                                                          <option value="<?= $val->user_status; ?>" name="status"><?= $val->user_status; ?></option>
                                                           <option value="Active" name="status">Active</option>
                                                            <option value="Inactive" name="status">Inactive</option>
                                                      </select>
                                                      
                                                      <!--<label for="timesheetinput1">Image</label>-->
                                                      <!--<input type="file"  class="form-control form-control-rounded" name="file" value="">-->
                                                      
                                                      
                                                      
                                                  </div>
                                                  <span class="error_msg amt_error"></span>
                                          </div>
                                          <button type="submit"  class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Update</button>
                                        </div>
                                      
                                  </div>
                              </div>
                              

                          <!---end %--->
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            </div>
            <!-- End container-fluid-->

        </div>
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
            
            
           
        </script>