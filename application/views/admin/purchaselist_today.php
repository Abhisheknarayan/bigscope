
<?php include('panel/header.php');?>

<?php 
$con=connectdb();
 $date=date('Y-m-d');
$data=mysqli_query($con,"SELECT * FROM `purchase_list` where added_date like '%".$date."%'");
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Daily Joining Amount List</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Daily Joining Amount List</div>
            <div class="card-body">
    

              <div class="table-responsive actual_data">
              <table id="example" class="table table-bordered">
                      <thead>
                        <tr>
                                 <th>SNo</th>
                                 <th>User id</th>
                               <th>Amount($)</th>
                                 <th>Status</th>
                               
                             
                               <th>Date</th>
              
                        </tr>
                      </thead>
                      <tbody>
                           <?php 
                            $i=1;
                            while($row=mysqli_fetch_array($data))
                                {
                                  
                            ?>
                               <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $row['user_rf_id'];?></td>
                                <td><?php echo $row['total_price'];?></td>
                                <td ><?php echo $row['status'];?></td>
                                <td><?php echo $row['added_date'];?></td>
                                
                               
                               </tr>
                            <?php
                                $i++;
                                }
                            ?>
                        </tbody>
                       
                   </table>
                  </div>
                  <div class="table-responsive filter_data">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
         
<?php include('panel/footer.php');?>