<?php 
 include('function.php');
$con=connectdb();
session_start();
if (@$_REQUEST['delete']) {
    // mysqli_query("SET autocommit=FALSE");
    $delete_plan = mysqli_query($con,"delete from `upgrade_plan` where `id`='" . @$_REQUEST['delete'] . "'");
    if ($delete_plan) {
        // mysql_query("COMMIT");
        echo "<script>alert('Upgrade Plan successfully deleted !');window.location='upgrade_plan.php'</script>";
    } else {
        // mysql_query("ROLLBACK");
        echo "<script>alert('Process not completed. Please try again!');window.location='upgrade_plan.php'</script>";
    }
}
?>
<?php include('inc/header.php');?>
 <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">
            <div class="container-fluid"><!--Statistics cards Starts-->

<section id="file-export-plus">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">View Upgrade Plan</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                   
                      <table class="table table-striped table-bordered file-export" id="example">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Action</th>
                                                                <th>Plan($)</th>
                                                                <th>Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $select_plan = mysqli_query($con,"select * from `upgrade_plan`  order by id asc") or die(mysqli_error());
                                                            $i = 0;
                                                            while ($display_plan = mysqli_fetch_array($select_plan)) {
                                                                ?>
                                                                <tr class="odd gradeX">
                                                                    <td class="center"><?php echo ++$i; ?></td>
                                                                    <td class="menu-action">       
                                                                    
                                                                     
                                                                        <a href="upgrade_plan.php?delete=<?php echo $display_plan['id'] ?>" data-original-title="Delete" data-toggle="tooltip" data-placement="top" class="btn btn-success mr-1" onclick="return confirm('Are you sure delete this plan?')"> Delete </a>
                                                                          <input type='button' class="btn btn-success mr-1" id="edit_button<?php echo $display_plan['id'];?>" value="Edit" onclick="edit_row('<?php echo $display_plan['id'];?>');">
                                                                  <input type='button' class="btn btn-success mr-1" id="save_button<?php echo $display_plan['id'];?>" value="Save" onclick="save_row('<?php echo $display_plan['id'];?>');" style="display:none;">

                                                                    </td>
                                                                    <td class="center" id="amount<?php echo $display_plan['id'];?>"><?php echo $display_plan['amount']; ?></td>
                                                                    <td class="center"><?php echo $display_plan['date']; ?></td></tr>
                                                                <?php
                                                            }
                                                            ?>
                                                    </tbody>
                                                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
 </div>
          </div>
        </div>
<?php include('inc/footer.php');?>
<script>
 function numericFilter(txb)
      {
        txb.value = txb.value.replace(/[^\0-9]/ig,'');
      }
    
     function edit_row(id)
    {
     
     var amount=document.getElementById("amount"+id).innerHTML;
    //   var day=document.getElementById("day"+id).innerHTML;

      document.getElementById("amount"+id).innerHTML="<input class='form-control' type='text' id='amount1"+id+"' value='"+amount+"' onkeyup = 'numericFilter(this);'>";
      document.getElementById("edit_button"+id).style.display="none";
     document.getElementById("save_button"+id).style.display="inline";
    }
function save_row(id)
{
 var amount1 = document.getElementById("amount1"+id).value;
$.ajax
 ({
  type:'post',
  url:'fast-way-income-update.php',
  data:{
   edit_row_phase:'edit_upgrade_phase',
   row_id:id,
   amount:amount1,
},
  success:function(response) {
   if(response=="success")
   {
    document.getElementById("amount"+id).innerHTML=amount1;
    document.getElementById("edit_button"+id).style.display="inline";
    document.getElementById("save_button"+id).style.display="none";
   }
  }
 });
}
</script>
     