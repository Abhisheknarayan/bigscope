
    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Direct Referral Income</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Direct Referral Income</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Direct users</th>
                                                <th>Package Amount($)</th>
                                                <th>Percentage(%)</th>
                                                <th>Direct Income($)</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                         <?php $i=1; foreach($data as $val): ;?>
                                        <tbody>
                                       
                                        <tr>
                                            <td><?= $i++;?></td>
                                            <td><?= $val->user_id;?></td>
                                            <td><?= $val->direct_user;?></td>
                                            <td><?= $val->package_amount;?></td>
                                            <td><?= $val->percent;?></td>
                                            <td><?= $val->direct_amount;?></td>
                                            <td><?= $val->status;?></td>
                                            <td><?= $val->date;?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Direct users</th>
                                                <th>Package Amount($)</th>
                                                <th>Percentage(%)</th>
                                                <th>Direct Income($)</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


     