
<?php include('panel/header.php');?>

<?php 
$con=connectdb();
$data=mysqli_query($con,"select * from jackpot_willing_users where status='1' order by flag DESC");
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Jackpot Winners List</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Jackpot Winners List</div>
            <div class="card-body">

              <h5><i class="fa fa-search"></i> Search By</h5>
                  <div class="row mt-3">
                    <div class="col-md-1 text-center">
                      <label>User Id</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <input type="text" name="" class="form-control" placeholder="User Id" id='jackpot_userid'>
                    </div>

                    <div class="col-md-1 text-center">
                      <label>User Type</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <select class="form-control" id='jackpot_planid'>
                        <option value="">--Select Type--</option>
                        <option value="leader">Leader</option>
                        <option value="investor">Investor</option>

                      </select>
                    </div>

                    <div class="col-md-1 text-center">
                      <label>Jackpot Type</label>
                    </div>
                    <div class="col-md-1 form-group no-padding">
                      
                      <select class="form-control" id='jackpot_jackpot'>
                        <option value="">--Select Type--</option>
                        <option value="weekly">Weekly</option>
                        <option value="monthly">Monthly</option>

                      </select>
                    </div>

                    <div class="col-md-1 text-center">
                      <label>From</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="From" id="jackpot_date_from">
                    </div>

                    <div class="col-md-1 text-center">
                      <label>To</label>
                    </div>
                    <div class="col-md-2 form-group no-padding">
                      
                      <input type="date" name="" class="form-control" placeholder="To" id="jackpot_date_to">
                    </div>
                  </div>

              <div class="table-responsive actual_data">
              <table id="example" class="table table-bordered">
                      <thead>
                        <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Type</th>
                                 <th>UserName</th>
                                <th>Email </th>
                               
                               <th>Jackpot</th>
                               <th>Ticket Number</th>
                               <th>Winning Amount</th>
                               <th>Date</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   <?php 
                                                   $i=1;
                                                   while($row=mysqli_fetch_array($data))
                                                   {
                                                    $data1=mysqli_query($con,"select * from users where refid='".$row['user_id']."'");
                                                    $row1=mysqli_fetch_array($data1);
                                                   
                                                   ?>
                                                    <tr>
                                <td><?php echo $i;?></td>
                                  <td><?php echo $row['user_id'];?></td>
                                 <td>
                                    <?php if($row['user_type']=='investor'){ ?>
                                    <span class="badge badge-success shadow-success">Investor</span>
                                  <?php }else if($row['user_type']=='leader'){ ?>
                                    <span class="badge badge-warning shadow-warning">Leader</span>
                                  <?php } ?>
                                  </td>
                                 <td><?php echo $row1['fname']." ".$row1['lname'];?></td>
                                  <td><?php echo $row1['email'];?></td>
                                  
                                  <td>
                                    <?php if($row['jackpot_type']=='weekly'){ ?>
                                    <span class="badge badge-info shadow-info">Weekly</span>
                                  <?php }else if($row['jackpot_type']=='monthly'){ ?>
                                    <span class="badge badge-danger shadow-danger">Monthly</span>
                                  <?php } ?>
                                  </td>
                                  <td><?php echo $row['ticket_no'];?></td>
                                  <td><?php echo $row['winning_amount'];?></td>
                                    <td><?php echo $row['join_date'];?></td>
                                          </tr>

                                         
                                          <?php
                                          $i++;
                                          }
                                           ?>
                        </tbody>
                        <tfoot>
                        <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Type</th>
                                 <th>UserName</th>
                                <th>Email </th>
                               
                               <th>Jackpot</th>
                               <th>Ticket Number</th>
                               <th>Winning Amount</th>
                               <th>Date</th>
              
                        </tr>
                      </tfoot>
                   </table>
                  </div>
                  <div class="table-responsive filter_data">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>        
        
<?php include('panel/footer.php');?>
     