<?php
//print_r($data);
?>
<body>
    <div id="wrapper">
      <div class="demo"></div>
        
        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Constant Income Percent</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
            
                            <!---update %--->
                            
                              <div class="card"> 
                                  <div class="card-header"><i class="fa fa-table"></i>Edit constant</div>
                                  <div class="card-body" >
                                      <?php foreach($data as $val): ?>
                                     <?php echo form_open_multipart('ShailuController/edit_constant/'.$val->id.'');?>
                                     
                                     
                                      <div class="card-content p-2">
                                          <div class="form-group">
                                                  <div class="position-relative has-icon-right">
                                                      
                                                      <label for="timesheetinput1">Name</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="name" value="<?= $val->name;?>">
                                                      
                                                      <label for="timesheetinput1">Type</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="type" value="<?= $val->type;?>">
                                                      
                                                      <label for="timesheetinput1">Value(%)</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="value" value="<?= $val->value;?>">
                                                        <label for="timesheetinput1">Symbol</label>
                                                      <input type="text"  class="form-control form-control-rounded" name="symbol" value="<?= $val->symbol;?>">
                                                      
                                                  </div>
                                                  <span class="error_msg amt_error"></span>
                                          </div>
                                          <button type="submit"  class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Update</button>
                                        </div>
                                      
                                  </div>
                              </div>
                               <?php endforeach;?>

                          <!---end %--->
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            </div>
            <!-- End container-fluid-->

        </div>
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
            
            
           
        </script>