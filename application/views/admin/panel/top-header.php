
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title> Bigscope</title>
  <!--favicon-->
 <link rel="icon" href="<?=base_url('resource/admin')?>/images/favicon.png" type="image/x-icon"> 
  
  <!-- simplebar CSS-->
  <link href="<?=base_url('resource/admin')?>/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?=base_url('resource/admin')?>/css/bootstrap.min.css" rel="stylesheet"/>
   <!--Data Tables -->
  <link href="<?=base_url('resource/admin')?>/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?=base_url('resource/admin')?>/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <!-- Vector CSS -->
  <link href="<?=base_url('resource/admin')?>/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />

   <link href="<?=base_url('resource/admin')?>/plugins/jquery-multi-select/jquery.multiselect.css" rel="stylesheet" />
  <!-- animate CSS-->
  <link href="<?=base_url('resource/admin')?>/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?=base_url('resource/admin')?>/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="<?=base_url('resource/admin')?>/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?=base_url('resource/admin')?>/css/bootstrap-toggle.css" rel="stylesheet" />
  <link href="<?=base_url('resource/admin')?>/css/app-style.css" rel="stylesheet"/>
   <script src="<?=base_url('resource/admin')?>/js/jquery.min.js"></script>
</head>

