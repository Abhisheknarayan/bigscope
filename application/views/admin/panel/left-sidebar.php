 <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true" class="animated bounceInDown" >
     <div class="brand-logo">
      <a href="<?=base_url('admin/dashboard')?>">
       <img src="<?=base_url('resource/user/')?>bigscoplogo.png" class="logo-icon" alt="logo icon">
       
     </a>
	 </div>
	  <ul class="sidebar-menu do-nicescrol">
      
      <li>
        <a href="<?=base_url('admin/dashboard')?>" class="waves-effect"> 
          <i class="icon-home"></i> <span>Dashboard</span> 
		  <!-- <i class="fa fa-angle-left pull-right"></i> -->
        </a>
        <!-- <ul class="sidebar-submenu"> -->
          <!-- <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li> -->
          <!-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li> -->
          <!-- <li><a href="index3.html"><i class="fa fa-circle-o"></i> Dashboard v3</a></li> -->
          <!-- <li><a href="index4.html"><i class="fa fa-circle-o"></i> Dashboard v4</a></li> -->
        <!-- </ul> -->
      </li>
     
      <li>
          <a href="#" class="waves-effect">
          <i class="fa fa-users"></i> <span>User List</span> 
		      <i class="fa fa-angle-left pull-right"></i>
        </a>
         <ul class="sidebar-submenu"> 
            <li><a href="<?=base_url('admin/users')?>"><i class="fa fa-user"></i> User list</a></li>
           <li><a href="<?= base_url('admin/paid_users')?>"><i class="fa fa-user"></i> Paid users</a></li> 
           <li><a href="<?= base_url('admin/unpaid_users')?>"><i class="fa fa-user"></i> Unpaid users</a></li> 
            
           <!--<li><a href="index4.html"><i class="fa fa-circle-o"></i> Dashboard v4</a></li> -->
         </ul> 
      </li>
      
      <!--<li>-->
      <!--  <a href="<?=base_url('admin/packages')?>" class="waves-effect">-->
      <!--    <i class="fa fa-money"></i> <span>Update Packages</span> -->
      
      <!--  </a>-->
      <!--</li>-->
      
      
      <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-hospital-o"></i> <span>Packages</span> 
		      <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?=base_url('admin/packages')?>"><i class="fa fa-circle-o"></i>Update all details</a></li>
         
          <!--<li><a href="<?=base_url('admin/direct_income')?>"><i class="fa fa-circle-o"></i>Direct Income Users</a></li>-->
          <!-- <li><a href="<?=base_url('admin/daily_return')?>"><i class="fa fa-circle-o"></i>Daily Return Users</a></li>-->
         
        </ul> 
      </li>
      
      <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-percent"></i> <span> Income Constant</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?= base_url('ShailuController/income_constant_list');?>"><i class="fa fa-eye"></i>View income constant</a></li>
        </ul>
      </li>
      
	  
	     <li>
        <a href="<?=base_url('admin/transaction_history')?>" class="waves-effect">
          <i class="fa fa-history"></i> <span>Transaction History</span> 
		   <!-- <i class="fa fa-angle-left pull-right"></i> -->
        </a>
        <!-- <ul class="sidebar-submenu"> -->
          <!-- <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li> -->
          <!-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li> -->
          <!-- <li><a href="index3.html"><i class="fa fa-circle-o"></i> Dashboard v3</a></li> -->
          <!-- <li><a href="index4.html"><i class="fa fa-circle-o"></i> Dashboard v4</a></li> -->
        <!-- </ul> -->
      </li>
	  
	  
	  
	     <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-money"></i> <span>BCO</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?=base_url('admin/tokens')?>"><i class="fa fa-circle-o"></i>Total BCO</a></li>
          <li><a href="<?=base_url('ShailuController/token_launch')?>"><i class="fa fa-circle-o"></i>BCO launch </a></li>
          <li><a href="<?=base_url('ShailuController/token_value_list')?>"><i class="fa fa-circle-o"></i>BCO value</a></li>
          <li><a href="<?=base_url('admin/token_transaction_history')?>"><i class="fa fa-circle-o"></i>BCO Transaction History </a></li>
         <!-- 
          <li><a href="<?=base_url('admin/daily_return')?>"><i class="fa fa-circle-o"></i>Tokens History </a></li>
           <li><a href="<?=base_url('admin/direct_income')?>"><i class="fa fa-circle-o"></i>Direct Income Users</a></li> -->
         
        </ul> 
      </li>
	  
	  
	  
	     <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-usd"></i> <span>Income Users</span> 
		      <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?=base_url('admin/binary_income')?>"><i class="fa fa-circle-o"></i>Binary Income Users</a></li>
         
          <li><a href="<?=base_url('admin/direct_income')?>"><i class="fa fa-circle-o"></i>Direct Income Users</a></li>
           <li><a href="<?=base_url('admin/daily_return')?>"><i class="fa fa-circle-o"></i>Daily Return Users</a></li>
         
        </ul> 
      </li>
     
	     <li>
        <a href="<?=base_url('admin/withdraw_request')?>" class="waves-effect">
          <i class="fa fa-reply"></i> <span>Withdrawal Request</span> 
		   <!-- <i class="fa fa-angle-left pull-right"></i> -->
        </a>
        <!-- <ul class="sidebar-submenu"> -->
          <!-- <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li> -->
          <!-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li> -->
          <!-- <li><a href="index3.html"><i class="fa fa-circle-o"></i> Dashboard v3</a></li> -->
          <!-- <li><a href="index4.html"><i class="fa fa-circle-o"></i> Dashboard v4</a></li> -->
        <!-- </ul> -->
      </li>
	     
       <li>
        <a href="<?=base_url('admin/pay_by_admin')?>" class="waves-effect">
          <i class="fa fa-money"></i> <span>Pay By Admin</span> 
      
        </a>
      </li>
      <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-check-square-o"></i> <span> Activation code</span> 
      <i class="fa fa-angle-left pull-right"></i>
        </a>
          <ul class="sidebar-submenu"> 
           <li><a href="<?=base_url('admin/generate_new_activation')?>"><i class="fa fa-circle-o"></i>Genarate pin</a></li> 
          <li><a href="<?=base_url('admin/activation_code_history')?>"><i class="fa fa-circle-o"></i>Total Generated pin </a></li>
          <li><a href="<?=base_url('admin/used_pin')?>"><i class="fa fa-circle-o"></i>Total used pins</a></li>
          <li><a href="<?=base_url('admin/unused_pin')?>"><i class="fa fa-circle-o"></i>Total Unused pins</a></li>
          </ul>
        </li>
	     <li>
	         
     <!--   <a href="#" class="waves-effect">-->
     <!--     <i class="fa fa-tasks "></i> <span>Plan</span> -->
		   <!--<i class="fa fa-angle-left pull-right"></i> -->
     <!--   </a>-->
     <!--   <ul class="sidebar-submenu"> -->
     <!--     <li><a href="<?=base_url('admin/withdraw_request')?>"><i class="fa fa-circle-o"></i> Direct-binary  Plan</a></li>-->
     <!--     <li><a href="<?=base_url('admin/withdraw_request')?>"><i class="fa fa-circle-o"></i> Level Plan</a></li>-->
     <!--     <li><a href="<?=base_url('admin/withdraw_request')?>"><i class="fa fa-circle-o"></i> Binary Plan</a></li>-->
          <!-- <li><a href="index4.html"><i class="fa fa-circle-o"></i> Dashboard v4</a></li> -->
     <!--    </ul>-->
         
      </li>

       
	   <li>
        <a href="#" class="waves-effect">
          <i class="fa fa-bell"></i> <span>Support</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?=base_url('admin/support_ticket_history')?>"><i class="fa fa-circle-o"></i> Support Ticket History</a></li>
          <!--<li><a href="<?=base_url('admin/send_notifications')?>"><i class="fa fa-circle-o"></i> Send Notification</a></li>
          <li><a href="<?=base_url('admin/notification_history')?>"><i class="fa fa-circle-o"></i> Notification History</a></li>-->
        </ul>
      </li>
       <li>
        <a href="<?=base_url('admin/all_enquiry')?>" class="waves-effect">
          <i class="fa fa-phone"></i> <span>Enquiry</span>
          
      
        </a>
      </li>
      <li>
        <a href="<?=base_url('admin/binary_capping')?>" class="waves-effect">
          <i class="fa fa-sitemap"></i> <span>Binary Capping</span>
          
      
        </a>
      </li>

      
    </ul>
	 
   </div>