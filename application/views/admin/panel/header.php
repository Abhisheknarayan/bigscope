<div id="wrapper" class="">
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top  animated bounceInDown">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <!-- <li class="nav-item"> -->
      <!-- <form class="search-bar"> -->
        <!-- <input type="text" class="form-control" placeholder="Enter keywords"> -->
         <!-- <a href="javascript:void();"><i class="icon-magnifier"></i></a> -->
      <!-- </form> -->
    <!-- </li> -->
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
    
    
   
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile"><img src="<?= base_url('resource/front/uat_public/image/logo.png')?>" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right bg-black">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="<?=base_url('resource/front/uat_public/image/logo.png')?>" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title"></h6>
            <p class="user-subtitle"></p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-divider"></li>
        <!--<li class="dropdown-item" style="text-decoration-color: #0e0d0d"><a href="<?=base_url('admin/profile')?>"><i class="icon-envelope mr-2"></i> Profile</a></li>-->
        <li class="dropdown-divider"></li>
        <li class="dropdown-item" style="color: #4c3a3a!important;"><a href="<?=base_url('home/logout')?>"><i class="icon-wallet mr-2"></i>  Sign out</a></li>
        <li class="dropdown-divider"></li>
        
      </ul>
    </li>
  </ul>
</nav>
</header>