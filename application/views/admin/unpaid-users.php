
 <!-- Start wrapper-->
    <div id="wrapper">
        <div class="clearfix"></div>
        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">
                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">UnPaid User List</h4>
                        <ol class="breadcrumb"></ol>
                    </div>
                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table"></i> Unpaid User List
                            </div>
                            <div class="card-body">
                                 <?= $this->session->flashdata('update_user'); ?>
                                <div class="table-responsive actual_data">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <!--<th>User Name</th>-->
                                                <th>Package Name</th>
                                                <th>Package Amount </th>
                                                <th>Status</th>
                                                <th>Date</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach ($unpaidusers->result() as $row) : ?>
                                            <tr>
                                                <td><?php echo $i++;?></td>
                                                <td> <?php echo $row->user_id;?></td>
                                               <td>
                                                    <?php 
                                                        $user=explode(',',$row->result);
                                                        $couts=array_count_values($user);
                                                        foreach($couts as $key=>$rows){
                                                            echo $key.' ('.$rows.'), ';
                                                        }
                                                    ?>
                                                </td>
                                                <td><?php echo $row->totalamount;?></td>
                                                <td><?php echo $row->status;?></td>
                                                <td><?php echo $row->date;?></td>
                                            </tr>
                                            <?php  endforeach; ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Package Name</th>
                                                <th>Package Amount </th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="table-responsive filter_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            

       
        

 
