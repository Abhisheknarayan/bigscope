<body>

<!-- Start wrapper-->
 <div id="wrapper">
     
     <div class="demo"></div>
 
  <!--Start sidebar-wrapper-->
  
   <!--End sidebar-wrapper-->

<!--Start topbar header-->

<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

   <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Pay By Admin</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
    </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Pay By Admin</div>
            <div class="card-body">
                <?= $this->session->flashdata('purchage_ticket')?>
						 <div class="card-content p-2" style="color: white">
						     <?= $this->session->flashdata('user'); ?>
								<?php echo form_open_multipart('admin/paid_by_admin') ?>
									
								 <!--onsubmit="return validate()"	-->
					<div class="form-group">
					<div class="position-relative has-icon-right">
					<label for="timesheetinput1">User Id</label>
									
		<input type="text" class="form-control form-control-rounded" name="userid"   required="">
	</div>
	</div>
	
	<div class="form-group">
	<div class="position-relative has-icon-right">
	<label for="timesheetinput1">Plan</label>	

	<select class="form-control form-control-rounded"  id="planid" name="planid1" required="">
		<option value="0" name="planid1">Select package plan</option>
		<?php foreach ($package->result() as $row) : ?>
				<option value="<?php echo $row->amount.'_'.$row->id; ?>"  name="planid1"><?php echo $row->name;?></option>
		<?php endforeach; ?>

	</select>
	
	<input type="hidden" value="<?php echo $row->name;?>" class="" id="" name="plan_name" />
	
	<input type="hidden" value="" class="" id="selected_plan" name="planid" />


	</div>
	</div>
	<div class="form-group">
	<div class="position-relative has-icon-right">
	<label for="timesheetinput1">Amount</label>
	<input type="number" id="amount" class="form-control form-control-rounded" name="amount" readonly="">
	</div>
	<span class="error_msg amt_error"></span>
	</div>
	<button type="submit" name="withdraw" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Pay</button>
	<?php echo form_close() ?>
							
						</div>
					</div>
				</div>
			</div>

			<!--footer-->
			

 <script type="text/javascript">
	$(document).on('change','#planid',function(){
		var vals=$(this).val();
		var exval=vals.split('_');
		$('#amount').val(exval[0]);
		$('#selected_plan').val(exval[1]);
		
	});
</script> 
			
			
			

	 <script type="text/javascript">
		$("#planid").change(function(){
			var plan=$("#planid").val();
			if(plan=='1'){
				$("#amount").val(1);
			}
			else if(plan=='2'){
				$("#amount").val(100);
			}
			else if(plan=='3'){
				$("#amount").val(200);
			}
			else if(plan=='4'){
				$("#amount").val(400);
			}
			else if(plan=='5'){
				$("#amount").val(800);
			}
			else if(plan=='6'){
				$("#amount").val(1600);
			}
			else if(plan=='7'){
				$("#amount").val(3200);
			}
			
		});
	</script> 