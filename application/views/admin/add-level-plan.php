<?php
 include('panel/header.php');
$con=connectdb();
if (isset($_POST['add'])) {
    $date = date('Y-m-d G:i:s');
    $add_plan = mysqli_query($con,"insert into `level_commission_plan`(`level`,`percentage`,`date`)values('" . $_POST['txtlevel'] . "','" . $_POST['percentage'] . "','" . $date . "')");
    if ($add_plan) {
         echo "<script>alert('Level plan successfully inserted !');window.location='view-roi-plan.php';</script>";
    } else {
        echo "<script>alert('Process not completed. Please try again!');window.location='view-roi-plan.php'</script>";
    }
}
if (isset($_POST['edit'])) {
    $date = date('Y-m-d G:i:s');
    $edit_plan = mysqli_query($con,"update `level_commission_plan` set `level`='" . $_POST['txtlevel'] . "',`percentage`='" . $_POST['percentage'] . "',`date`='" . $date . "' where `id`='" . @$_REQUEST['id'] . "'");
    if ($edit_plan) {
        echo "<script>alert('Level plan successfully updated !');window.location='level-plan.php';</script>";
    } else {
        echo "<script>alert('Process not completed. Please try again!');window.location='level-plan.php'</script>";
    }
}
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Add/Edit Level Plan</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Add/Edit Level Plan</div>
            <div class="card-body">
					<div class="px-3">
   <?php 
						 if (@$_REQUEST['id']) {
                                                    $select_plan = mysqli_query($con,"select * from `level_commission_plan` where `id`='" . @$_REQUEST['id'] . "'");
                                                    $display_plan = mysqli_fetch_array($select_plan);
                                                    ?>
                                                    <form class="form" enctype="multipart/form-data" method="post"  action="#" role="form" id="form_register">

                                                        <div class="form-group">
                                                            
                                                                <label >Level <span class="vd_red">*</span></label>
                                                                
                                                                    <input  type="text" onkeypress="return isNumberKey(event)" name="txtlevel" placeholder="Enter No. level"  class="form-control" required="" value="<?php echo $display_plan['level'] ?>">

                                                               
                                                        </div>

                                                        <div class="form-group">
                                                            
                                                                <label >Percentage <span class="vd_red">*</span></label>
                                                                
                                                                    <input  type="text" onkeypress="return isNumberKey(event)" name="percentage" placeholder="Enter percentage"  class="form-control" required="" value="<?php echo $display_plan['percentage'] ?>">

                                                                
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            
                                                         <button type="submit" class="btn btn-primary shadow-primary px-5" name="edit"><i class="icon-lock"></i> Save</button>  

                                                               
                                                        </div>
                                                    </form>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <form class="form-horizontal" enctype="multipart/form-data" method="post"  action="#" role="form" id="form_register">
                                                        <div class="form-group">
                                                           
                                                                <label class="control-label">Level <span class="vd_red">*</span></label>
                                                               
                                                                    <input  type="text" name="txtlevel" onkeypress="return isNumberKey(event)" placeholder="Enter No. of level"  class="form-control" required="">
                                                               
                                                        </div>
                                                        <div class="form-group">
                                                            
                                                                <label class="control-label">Percentage <span class="vd_red">*</span></label>
                                                               
                                                                    <input  type="text" name="percentage" onkeypress="return isNumberKey(event)" placeholder="Enter percentage"  class="form-control" required="">
                                                                
                                                        </div>
                                                        <div class="form-group">
                                                             <button type="submit" class="btn btn-primary shadow-primary px-5" name="add"><i class="icon-lock"></i> Add</button>
                                                        </div>
                                                    </form>
                                                    <?php
                                                }
                                                ?>

					</div>
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
         
        
<?php include('panel/footer.php');?>
