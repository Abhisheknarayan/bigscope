
<?php include('panel/header.php');?>

<?php 
$con=connectdb();

if(isset($_POST['reply'])){
  $support_id=$_POST['support_id'];
  $message=$_POST['message'];
  $date=date("Y-m-d");

  $qry=mysqli_query($con,"insert into support_ticket_log (reply_by, support_id, message, date) values('1', '$support_id', '$message', '$date') ");
  if($qry){
    echo "<script>alert('Your Reply sent successfully!!');location.href='view-support-ticket.php?id=".$support_id."';</script>";
  }
  else{
    echo "<script>alert('Some issue occur. Please Try Again!!!');</script>";
  }
}
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Support Ticket log</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
      <div class="col-sm-3 pull-right text-right">
          <a href="javascript:;" class="btn btn-primary shadow-primary px-2" name="reply" id="reply"> Reply</a>
      </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Support Ticket log</div>
           
            <div class="card-body">
              <div class="table-responsive">
              <table id="" class="table table-bordered">

            
               
                                                        <thead>
                                                            <tr>
                                                                
                                                                <th>Reply By</th>
                                                                <th>Message</th>
                                                                <th>Date</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $select_plan = mysqli_query($con,"select * from `support_ticket_log` where support_id='".$_GET['id']."' order by id desc") or die(mysqli_error());
                                                            $i = 0;
                                                            while ($display_plan = mysqli_fetch_array($select_plan)) {
                                                                ?>
                                                                <tr class="odd gradeX">
                                                                    
                                                                    <?php if($display_plan['reply_by']=='1'){ 
                                                                      echo '<td style="color:red;">Admin</td>'; 
                                                                   }else{ 
                                                                    echo '<td style="color:green;">You</td>'; 
                                                                   } ?>
                                                                    <td class="center"><?php echo $display_plan['message']; ?> </td>
                                                                     
                                                                    <td class="center"><?php echo $display_plan['date']; ?></td>

                                                                </tr>

                                                                <?php
                                                            }
                                                            ?>


                                                        </tbody>
                                                     </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
         <div class="modal" tabindex="-1" role="dialog" id="reply_modal">
          <div class="modal-dialog noti_modal">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
        <button type="button" class="close" id="close1">&times;</button>
      </div>

              <div class="modal-body">
                <div class="card">
            <div class="card-body">
             <div class="card-content p-2">
                <form method="post" action="">
                  
                  
                                  
                  <input type="hidden" name="support_id" value="<?php echo $_GET['id']; ?>">

                    <div class="form-group">
                      <div class="position-relative has-icon-right">
                  <label for="timesheetinput1">Reply</label>
                  
                    <textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea> 
                    
                  </div>
                  <span class="error_msg amt_error"></span>
                </div>
                
                
              

              
                <button type="submit" name="reply" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                   Send
                </button>
                
              
                </form>
              </div>
            </div>
          </div>
              </div>
              
            </div>
            
          </div>
      </div>
        
<?php include('panel/footer.php');?>
     
     <script type="text/javascript">

      $("#reply").click(function(){
        $("#reply_modal").modal('show');
      }); 

       $("#close1").click(function(){
        $("#reply_modal").modal('hide');
        history.go();
      })
     </script>