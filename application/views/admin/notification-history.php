
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
 
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Notification</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
      <!-- <div class="col-sm-3 pull-right text-right">
          <a href="add-locking-plan.php" class="btn btn-primary shadow-primary px-2" name="add">  Add New ROI Plan</a>
      </div> -->
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Notification History</div>
           
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">

            
               
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Action</th>
                                                                <th>User Id</th>
                                                                <th>Subject</th>
                                                                <th>Message</th>
                                                                <th>Date</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                                <tr class="odd gradeX">
                                                                    <td class="center"></td>
                                                                    <td class="menu-action">
                                                                        <a href="notification-history.php?delete=" data-original-title="Delete" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red" onclick="return confirm('Are you sure delete this ticket?')"> <i class="fa fa-times"></i> </a>
                                                                        <a href="notification-view.php?id=" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red" > <i class="fa fa-eye"></i> </a>
                                                                    </td>
                                                                    <td class="center"></td>
                                                                    <td class="center"></td>
                                                                    <td class="center">... </td>
                                                                    <td class="center"></td>

                                                                </tr>

                                                                
                                                             


                                                        </tbody>
                                                         <tfoot>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Action</th>
                                                                <th>User Id</th>
                                                                <th>Subject</th>
                                                                <th>Message</th>
                                                                <th>Date</th>

                                                            </tr>
                                                        </tfoot>
                                                     </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   <script>
$(document).ready(function() {
    $('#example').DataTable( {  
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],   
        dom: 'Bfrtip',        
        buttons: [            
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'
            
        ]
    } );
} );
</script>
         
        

     