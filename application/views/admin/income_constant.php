<?php
//print_r($data);
?>
<body>
    <div id="wrapper">
        <div class="demo"></div>
        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title"> Income Percent</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">

                                <i class="fa fa-table"></i>Income Percent  List

                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                            <div class="card-body">
                               
                                <div class="table-responsive actual_data">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Value</th>
                                                <th>Symbol</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach ($data as $key => $values) : ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?= $values->name; ?></td>
                                                <td><?= $values->type; ?></td>
                                                <td><?= $values->value; ?></td>
                                                <td><?= $values->symbol; ?></td>
                                                
                                                <td><a href="<?= base_url('ShailuController/edit_constant_list/'.$values->id.'');?>"><button class="btn btn-info" type="button" id="edit"><i class="fa fa-edit" title="Edit"></i></button></a>&nbsp
                                                    
                                                </td>
                                            </tr>
                                           <?php  endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                 <th>Id</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Value</th>
                                                <th>Symbol</th>
                                                 <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="table-responsive filter_data">

                                </div>
                            </div>
                            
            
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            </div>
            <!-- End container-fluid-->

        </div>
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
            
            
            $(document).on('click','#edit',function()){
                var id = $(this).val();
                alert(id);
            }
        </script>