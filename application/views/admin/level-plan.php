
<?php include('panel/header.php');

$con=connectdb();

if (@$_REQUEST['delete']) {
    // mysqli_query("SET autocommit=FALSE");
    $delete_plan = mysqli_query($con,"delete from `level_commission_plan` where `id`='" . @$_REQUEST['delete'] . "'");
    if ($delete_plan) {
        // mysql_query("COMMIT");
        echo "<script>alert('Level Plan successfully deleted !');window.location='level-plan.php'</script>";
    } else {
        // mysql_query("ROLLBACK");
        echo "<script>alert('Process not completed. Please try again!');window.location='level-plan.php'</script>";
    }
}
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Level Plan</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     <!--  <div class="col-sm-3 pull-right text-right">
          <a href="add-locking-plan.php" class="btn btn-success" name="add">  Add New Roi Plan</a>
      </div> -->
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level Plan</div>
           
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Action</th>
                                                                <th>Level</th>
                                                                <th>Percentage</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $select_plan = mysqli_query($con,"select * from `level_commission_plan`  order by id asc") or die(mysqli_error());
                                                            $i = 0;
                                                            while ($display_plan = mysqli_fetch_array($select_plan)) {
                                                                ?>
                                                                <tr class="odd gradeX">
                                                                    <td class="center"><?php echo ++$i; ?></td>
                                                                    <td class="menu-action">
                                                                        <a href="level-plan.php?delete=<?php echo $display_plan['id'] ?>" data-original-title="Delete" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red" onclick="return confirm('Are you sure delete this plan?')"> <i class="fa fa-times"></i> </a>
                                                                        <a  href="add-level-plan.php?id=<?php echo $display_plan['id']; ?>"   data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>
                                                                    </td>
                                                                    <td class="center"><?php echo $display_plan['level']; ?></td>
                                                                     <td class="center"><?php echo $display_plan['percentage']; ?>%</td>

                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>


                                                        </tbody>
                                                         <tfoot>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Action</th>
                                                                <th>Level</th>
                                                                <th>Percentage</th>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
         
        
<?php include('panel/footer.php');?>
     