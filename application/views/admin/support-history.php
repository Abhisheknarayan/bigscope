
<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Support Ticket History</h4>
                        <ol class="breadcrumb">
                            
                        </ol>
                    </div>
                </div>
                    
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Support Ticket History</div>

                            <div class="card-body">
                                
                                <div class="text-danger"><strong>
                                      
                                  <?php
                                    if ( $this->session->flashdata('inset_message')) {
                                        echo $this->session->flashdata('inset_message'); 
                                
                                    }else{
                                        echo $this->session->flashdata('failed');  
                                    }
                                  ?>                  
                                      
                                 </strong></div> 
                                 
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">

                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Action</th>
                                                <th>User Id</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Status</th>
                                                <th>Files</th>
                                                <th>Date</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            $support_tickect = $this->db->query("select * from support_ticket order by id desc ")->result_array();
                                            //echo "<pre>"; print_r($support_tickect); die();
                                            $i = 0;
                                            foreach($support_tickect as $display_plan) {
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td class="center">
                                                        <?php echo ++$i; ?>
                                                    </td>
                                                    <td class="menu-action">
                                                        <!--support-history.php?delete=-->
                                                        <a href="#"  id = "deletetick<?php echo $display_plan['id'] ?>" data-original-title="Delete" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red" > <i class="fa fa-times"></i> </a>
                                                        <a href="javascript:;" data-original-title="Reply" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow" id="reply<?php echo $i; ?>"> <i class="fa fa-reply"></i> </a>
                                                        
                                                    </td>
                                                    <td class="center">
                                                        <?php echo $display_plan['user_id']; ?>
                                                    </td>
                                                    <td class="center">
                                                        <?php echo $display_plan['subject']; ?>
                                                    </td>
                                                    <td class="center">
                                                        <?php echo $display_plan['message']; ?>
                                                    </td>
                                                    <?php
                                                    if($display_plan['status']=='1'){ 
                                                    ?>
                                                        <td>
                                                            <input type="checkbox" id="status<?php echo $i; ?>" data-toggle="toggle" data-on="Close" data-off="Open" checked >
                                                            <input type="hidden" id="user_id<?php echo $i; ?>" value="<?php echo $display_plan['id'];?>">
                                                        </td>
                                                    <?php 
                                                        
                                                    }else{ 
                                                        echo '<td><span class="badge badge-success shadow-success"> Closed </span></td>'; 
                                                    } 
                                                    ?>
                                                    <td class="center">
                                                        <?php
                                                        if(!empty($display_plan['img_path']) || $display_plan['img_path'] !== ''){
                                                            
                                                            $img = base_url('uploads/supporttikts/').$display_plan['img_path'];
                                                            
                                                            
                                                        }else{
                                                            
                                                            $img = base_url('uploads/supporttikts/nofile.png');
                                                            
                                                        }
                                                        
                                                        ?>
                                                        <a href="<?= $img ?>" target="_black"><img src="<?= $img ?>" height="50px" wight="50px">  </a>
                                                    </td>
                                                    
                                                    <td class="center">
                                                        <?php echo date('Y-m-d', strtotime($display_plan['date'])); ?>
                                                    </td>

                                                </tr>

                                                <div class="modal" tabindex="-1" role="dialog" id="reply_modal<?php echo $i; ?>">
                                                    <div class="modal-dialog noti_modal">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" id="close1<?php echo $i; ?>">&times;</button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="card-content p-2">
                                                                            <form method="post" action="<?= base_url('admin/admin_tickect_replay')?>">

                                                                                <input type="hidden" name="support_id" value="<?php echo $display_plan['id']; ?>">

                                                                                <div class="form-group">
                                                                                    <div class="position-relative has-icon-right">
                                                                                        <label for="timesheetinput1">Reply</label>

                                                                                        <textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea>

                                                                                    </div>
                                                                                    <span class="error_msg amt_error"></span>
                                                                                </div>

                                                                                <button type="submit" name="reply" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                                                                    Send
                                                                                </button>

                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                                
                                                    <script type="text/javascript">
                                                    $("#status<?php echo $i; ?>").change(function() {
                                                        var user = $("#user_id<?php echo $i; ?>").val();
                                                        if ($(this).prop("checked") == true) {
                                                            //alert("Checkbox is checked.");
                                                            $.ajax({
                                                                type: 'post',
                                                                url: "<?= base_url('admin/change_tick_status')?>",
                                                                data: 'support=' + user + '&action=1',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        } else if ($(this).prop("checked") == false) {
                                                            //alert(user);
                                                            $.ajax({
                                                                type: 'post',
                                                                url: "<?= base_url('admin/change_tick_status')?>",
                                                                data: 'support=' + user + '&action=0',
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                    
                                                    
                                                    $("#reply<?php echo $i; ?>").click(function() {
                                                        $("#reply_modal<?php echo $i; ?>").modal('show');
                                                    });
                                            
                                                    $("#close1<?php echo $i; ?>").click(function() {
                                                        $("#reply_modal<?php echo $i; ?>").modal('hide');
                                                        //history.go();
                                                    });
                                                    
                                                    
                                                    
                                                    $("#deletetick<?php echo $display_plan['id'] ?>").click(function(event) {
                                                        event.preventDefault();
                                                        var id = <?php echo $display_plan['id'] ?>;
                                                        var x = confirm("Are you sure you want to delete?");
                                                        if (x){
                                                            
                                                            $.ajax({
                                                                type: 'post',
                                                                url: "<?= base_url('admin/delete_ticket')?>",
                                                                data: 'support=' + id,
                                                                success: function(data) {
                                                                    if (data == '1') {
                                                                        history.go();
                                                                        alert("Tickect Deleted Successfully");
                                                                
                                                                    } else {
                                                                        alert("Please Try Again");
                                                                    }
                                                                }
                                                            });  
                                                            
                                                        }else{
                                                            return false;
                                                        }
                                                                                                                
                                                    
                                                    })                                                    
                                                    
                                                </script>   
                                                
                                                


                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Action</th>
                                                <th>User Id</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Status</th>
                                                <th>Files</th>
                                                <th>Date</th>

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
 


