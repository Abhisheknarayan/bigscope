<?php
//print_r($data);
?>
<body>
    <div id="wrapper">
        <div class="demo"></div>
        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title"> Token Value List</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">

                                <i class="fa fa-table"></i>Token Value List

                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                            <div class="card-body">
                               <div class="pull-right"><a href="<?= base_url('ShailuController/token_value');?>"><button class="btn btn-info" type="button" title="Add"><i class="fa fa-plus" ></i> ADD </button></a></div>
                                <div class="table-responsive actual_data">
                                    
                                    <table id="example" class="table table-bordered">
                                        
                                        <thead>
                                            <tr>
                                                <th>Sno</th>
                                                <th>From: Date </th>
                                                <th>To: Date </th>
                                                <th><?= COIN ?> Value($)</th>
                                                <th><?= COIN ?> Future Value($)</th>
                                                <th>Status</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i =1; foreach ($data as $key => $values) : ?>
                                            <tr>
                                                <td><?= $i++; ?></td>
                                                <td><?= $values->from_date; ?></td>
                                                <td><?= $values->to_date; ?></td>
                                                <td><?= $values->value; ?></td>
                                                <td><?= $values->future_value; ?></td>
                                                <td><?= $values->status ; ?></td>
                                                
                                            </tr>
                                           <?php  endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sno</th>
                                                <th>From Date:</th>
                                                <th>To Date:</th>
                                                <th><?= COIN ?> Value($)</th>
                                                <th><?= COIN ?> Future Value($)</th>
                                                <th>Status</th>
                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="table-responsive filter_data">

                                </div>
                            </div>
                            
            
                        </div>
                    </div>
                </div>
                <!-- End Row-->

            </div>
            <!-- End container-fluid-->

        </div>
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
            
            
            $(document).on('click','#edit',function()){
                var id = $(this).val();
                alert(id);
            }
        </script>