<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="zytheme"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bigscope">
<link href="<?= base_url('resource/frontend/') ?>images/favicon/favicon.png" rel="icon">

<!-- Fonts
    ============================================= -->
<link href="http:/fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,800%7CRoboto:300i,400,400i,500,500i,700" rel="stylesheet" type="text/css">

<!-- Stylesheets
    ============================================= -->
<link href="<?= base_url('resource/frontend/') ?>css/external.css" rel="stylesheet">
<link href="<?= base_url('resource/frontend/') ?>css/style.css" rel="stylesheet">

<!-- RS5.0 Main Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?= base_url('resource/frontend/') ?>revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('resource/frontend/') ?>revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('resource/frontend/') ?>revolution/css/navigation.css"> 

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
      <script src="<?= base_url('resource/frontend/') ?>js/html5shiv.js"></script>
      <script src="<?= base_url('resource/frontend/') ?>js/respond.min.js"></script>
    <![endif]-->
    
    
    
    <style>
        
        .pricing-panel .pricing--heading {
    box-shadow: 0 10px 20px rgba(224, 224, 224, 0.4);
    border-radius: 2px;
    background-color: #ffffff;
    padding: 20px 0px;
    margin-bottom: 30px;
    border-bottom: 4px solid #f48820;
        transition: all 0.2s;
}
.pricing-panel .pricing--heading p {
    font-family: 'Montserrat';
    color: #333333;
    font-size: 20px;
    font-weight: 400;
    line-height: 1;
    margin-bottom: 16px;
}

.pricing-panel .pricing--heading:hover {
    box-shadow: 5px 5px 5px #00000038;
  
}


.start_style {    text-align: center;}


.start_style ul {
       display: inline-block;
    text-align: justify;
}


.start_style ul li {           color: #26292db8;
    font-weight: bold;
    transition: all 0.2s;}

  .start_style ul li:hover{color: #eb9d0c;}


.pricing-panel .pricing--heading .pricing--desc {
    color: #a5a5a5;
    font-size: 15px;
    font-weight: 400;
    line-height: 21px;
    margin-bottom: 0px; 
}

.pricing-active .pricing-panel .pricing--heading {
    background-image: -moz-linear-gradient(to top, #a2d536 0%, #98cb2b 100%);
    background-image: -webkit-linear-gradient(to top, #a2d536 0%, #98cb2b 100%);
    background-image: -ms-linear-gradient(to top, #a2d536 0%, #98cb2b 100%);
    background-image: linear-gradient(to top, #a2d536 0%, #98cb2b 100%);
      margin-top: 0px;
    padding-top: 20px; 
}
.icon_size {
    border: 1px solid #48494a;
    border-radius: 50%;
    padding: 4px -6px;
    background: #26292d;
    box-shadow: 5px 5px 5px #00000075;
    width: 58%;
}


        
    </style>
    
    

<!-- Document Title
    ============================================= -->
<title>Bigscope.co</title>
</head>
<body>
<div class="preloader">
	<div class="reverse-spinner"></div>
</div><!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">
<header id="navbar-spy" class="header header-1 header-transparent">
	<nav id="primary-menu" class="navbar navbar-expand-lg navbar-light navbar-bordered">
		<div class="container">
			<a class="navbar-brand" href="<?=base_url()?>">
				<img class="logo logo-light" src="<?= base_url('resource/frontend/') ?>logo-light.png" alt="Consultivo Logo">
				<img class="logo logo-dark" src="<?= base_url('resource/frontend/') ?>logo-dark.png" alt="Consultivo Logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
							
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbarContent">
				<ul class="navbar-nav ml-auto">
				<!-- Home Menu-->
<li class="has-dropdown active">
    <a href="<?=base_url()?>"  class="dropdown-toggle menu-item">Home</a>
   
</li>
<!-- li end -->

<!-- Pages Menu -->
<li class="has-dropdown">
    <a href="#about1" >About us</a>
    
</li>
<!-- li end -->
<!-- Services Menu-->
<li class="has-dropdown">
    <a href="#pricing1" >Business Packages</a>
    
</li>
<!-- li end -->


<!-- Blog Menu-->
<li class="has-dropdown">
    <a href="#featured1" >Contact us</a>
   
</li>

<!-- shop Menu -->
<li class="has-dropdown">
    <a href="<?= base_url("home/login") ?>"  class="dropdown-toggle menu-item" data-hover="shop">Log in </a>
    
</li>
<li class="has-dropdown">
    <a href="<?= base_url("home/register") ?>"  class="dropdown-toggle menu-item" data-hover="shop">Register</a>
    
</li>

<!-- li end -->
</ul>
				
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>
</header>
<!-- Hero Section  
====================================== -->
<section id="slider" class="slider slide-overlay-dark">
	<!-- START REVOLUTION SLIDER 5.0 -->
	<div class="rev_slider_wrapper">
		<div id="slider1" class="rev_slider"  data-version="5.0">
			<ul>
				<?php echo $this->session->flashdata('user_loggedout'); ?>
				<!-- slide 1 -->
				<li data-transition="zoomout" 
					data-slotamount="default" 
					data-easein="Power4.easeInOut" 
					data-easeout="Power4.easeInOut" 
					data-masterspeed="2000">
					<!-- MAIN IMAGE -->
					<img src="<?= base_url('resource/frontend/') ?>images/sliders/slide-bg/1.jpg" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-110','-100','-110','-110']" 
                        data-fontsize="['16','16','16','12']"
                        data-lineheight="['25','25','25','25']"
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":750,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
 						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<!-- <div class="slide--subheadline">Free and impartial money advice</div> -->
 					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-5','-5','-5','10']"
						data-fontsize="['60', '50', '40', '30']" 
                        data-lineheight="['60','60','60','60']"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
 						data-splitin="none" 
						data-splitout="none" 
  						data-responsive_offset="on">
 						<div class="slide--headline">We're in the business   of <br> helping you start</div>
 					</div>		
 								
					<!-- LAYER NR. 3 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['100','100','100','100']"
						data-fontsize="['16', '16', '16', '12']" 
                        data-lineheight="['25','25','25','25']"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":1250,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
 						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<div class="slide--bio">The market is incredibly competitive and hard to understand what exactly is on offer.</div>
 					</div>
					
					
				</li>
				
				<!-- slide 2 -->
				<li data-transition="zoomin" 
					data-slotamount="default" 
					data-easein="Power4.easeInOut" 
					data-easeout="Power4.easeInOut" 
					data-masterspeed="2000">
					<!-- MAIN IMAGE -->
					<img src="<?= base_url('resource/frontend/') ?>images/sliders/slide-bg/2.jpg" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-110','-100','-110','-110']" 
                        data-fontsize="['16','16','16','12']"
                        data-lineheight="['25','25','25','25']"
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":750,"speed":750,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<!-- <div class="slide--subheadline">Instant, Secure & Private</div> -->
 					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-13','-13','-13','10']"
						data-fontsize="['60', '50', '40', '30']" 
                        data-lineheight="['60','60','60','60']"
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
                        data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
 						data-splitin="none" 
						data-splitout="none" 
 						data-responsive_offset="on">
 						<div class="slide--headline">Join the Leading Financial  <br> investment Plan</div>
  					</div>		
 								
					<!-- LAYER NR. 3 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['100','100','100','100']"
						data-fontsize="['16', '16', '16', '12']" 
                        data-lineheight="['60','60','60','60']"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":1250,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<div class="slide--bio">smart investment solutions near you!!!</div>
 					</div>
					
					
				</li>
				
				<!-- slide 3 -->
				<li data-transition="slideoverleft"
					data-slotamount="default" 
					data-easein="Power4.easeInOut" 
					data-easeout="Power4.easeInOut" 
					data-masterspeed="2000">
 					<!-- MAIN IMAGE -->
					<img src="<?= base_url('resource/frontend/') ?>images/sliders/slide-bg/3.jpg" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-110','-100','-110','-110']" 
                        data-fontsize="['16','16','16','12']"
                        data-lineheight="['25','25','25','25']"
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'						data-splitin="none" 
						data-splitout="none" 
 						data-responsive_offset="on">
 						<div class="slide--subheadline">Instant, Secure & Private</div>
 					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['-5','-5','-5','10']"
						data-fontsize="['60', '50', '40', '30']" 
                        data-lineheight="['6','60','60','60']"
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
                        data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
 						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<div class="slide--headline">Binary  <br>Best Financial Investment Solution</div>
  					</div>		
 								
					<!-- LAYER NR. 3 -->
					<div class="tp-caption" 
				        data-x="['left','left','left','left']" data-hoffset="['70','50','50','20']" 
                        data-y="['middle','middle','middle','middle']" data-voffset="['100','100','100','100']"
						data-fontsize="['16', '16', '16', '12']" 
                        data-lineheight="['60','60','60','60']"
						data-width="none"
						data-height="none"
                        data-frames='[{"delay":1250,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
 						<!-- <div class="slide--bio">Buy And Sell Cryptocurrency Near You!</div> -->
 					</div> 
					
					
				</li>

			</ul>
		</div>
		<!-- END REVOLUTION SLIDER -->
	</div>
	<!-- END OF SLIDER WRAPPER -->
</section>
<!-- #hero end --> 

<!-- about  #1
============================================= -->
<section id="about1" class="about about-1 bg-gray pt-110 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="heading">
                    <p class="heading--subtitle">All About Us</p>
                    <h2 class="heading--title">About Binary</h2>
                </div>
                <div class="about--text">
				 <p>More then Decade of knowledge with aggravation and attention bring with us to upgrade the strategic investment.Binary Known for financial services advisor for a decade.we brings a smart structured & organised plans for leader and investors </p>
                    <p>This is a process to enhance the market & make a magnificient changes to world.which works on marketing strategy for professionals who looking for world calss plan for individual benifits.</p>
                   
                </div>
                <!-- <a href="#" class="btn btn--secondary btn--rounded mb-30-xs mb-30-sm">More About Us</a> -->
            </div>
            <!-- .col-lg-6 end -->
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="about--img">
                    <img src="<?= base_url('resource/frontend/') ?>images/about/1.jpg" alt="img" class="img-fluid">
                </div>
            </div>
            <!-- .col-lg-6 end -->
        </div>
        <!-- .row end -->
      
        <!-- .counter end -->
    </div>
    <!-- .container end -->
</section>
<!-- #about1 end -->
<!-- Pricing Table #1
============================================= -->
<section id="pricing1" class="pricing pricing-1 bg-overlay bg-overlay-dark2 pt-110 pb-0">
    <div class="bg-section">
        <img src="<?= base_url('resource/frontend/') ?>images/background/2.jpg" alt="background">
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-12 col-md-12 col-lg-6 offset-md-3">
                <div class="heading heading-1 text--center mb-40">
                    <!-- <p class="heading--subtitle">Get Started Now</p> -->
                    <h2 class="heading--title text-white">Business Packages</h2>
                    <!-- <p class="heading--desc heading--light mb-0">Get started now with us to earn every day and forever in your business. We accept Investment from all over the world.</p> -->
                </div>
            </div>
            <!-- .col-md-8 end -->
        </div>
        <!-- .row end -->
    </div>
    <div class="container-fluid pt-40">
        <div class="row">
            <!-- Pricing Packge #1
			============================================= -->
            <div class="col-sm-12 col-md-4 col-lg-2 offset-lg-1 price-table">
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                          
						  <img class="icon_size" src="<?= base_url('resource/frontend/') ?>icon_logo.png">
						  
						  
                        </div>
                        <!--<h4>Starter Plan </h4>-->
                        <p><span class="currency"></span>Bigscope 180</p>
                        <div class="pricing--desc">
						  <div class="start_style">
                         <ul><li>  Invest - 100 USD</li>
<li> Daily Return - 1%</li>
<li> Return Days - 180</li>
<li> Return - 180 USD</li>
</ul>
 </div>
                        </div>
                        <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="<?= base_url("home/login") ?>"> Join Now</a>
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            <!-- .pricing-table End -->
            <!-- Pricing Packge #2
			============================================= --> 
             <div class="col-sm-12 col-md-4 col-lg-2 price-table pricing-active">
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                             <img class="icon_size" src="<?= base_url('resource/frontend/') ?>icon_logo.png">
                        </div>
                        <!--<h4>Basic Plan </h4>-->
                        <p><span class="currency"></span>Bigscope 225</p>
                        <div class="pricing--desc">
						  <div class="start_style">
                         <ul><li>   Invest - 500 USD</li>
<li> Daily Return - 1.25%</li>
<li> Return Days - 180</li>
<li> Return - 1125 USD</li>
</ul>
 </div>
 

 
 
 
 
                        </div>
                        <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="<?= base_url("home/login") ?>"> Join Now</a>
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            <!-- .pricing-table End -->
            <!-- Pricing Packge #3
			============================================= -->
            <div class="col-sm-12 col-md-4 col-lg-2 price-table">
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                             <img class="icon_size" src="<?= base_url('resource/frontend/') ?>icon_logo.png">
                        </div>
                        <!--<h4>Advance Plan </h4>-->
                        <p><span class="currency"></span>Bigscope 270</p>
                        <div class="pricing--desc">
						  <div class="start_style">
                         <ul><li>  Invest - 1000 USD</li>
<li> Daily Return - 1.50%</li>
<li> Return Days - 180</li>
<li> Return - 2700 USD</li>
</ul>


 </div>
                        </div>
                        <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="<?= base_url("home/login") ?>"> Join Now</a>
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            <!-- .pricing-table End -->
            
            
            
            
           <div class="col-sm-12 col-md-4 col-lg-2 price-table pricing-active">
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                             <img class="icon_size" src="<?= base_url('resource/frontend/') ?>icon_logo.png">
                        </div>
                        <!--<h4>Basic Plan </h4>-->
                        <p><span class="currency"></span>Bigscope 315</p>
                        <div class="pricing--desc">
						  <div class="start_style">
                         <ul><li>   Invest - 2500 USD</li>
<li> Daily Return - 1.75%</li>
<li> Return Days - 180</li>
<li> Return - 7875 USD</li>
</ul>
 </div>
 

 
 
 
 
                        </div>
                        <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="<?= base_url("home/login") ?>"> Join Now</a>
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            
            
            
             <!--	============================================= -->
            <div class="col-sm-12 col-md-4 col-lg-2 price-table">
                <div class="pricing-panel">
                    <!--  Pricing heading  -->
                    <div class="pricing--heading text--center">
                        <div class="pricing--icon">
                             <img class="icon_size" src="<?= base_url('resource/frontend/') ?>icon_logo.png">
                        </div>
                        <!--<h4>Advance Plan </h4>-->
                        <p><span class="currency"></span>Binary</p>
                        <div class="pricing--desc">
						  <div class="start_style">
                         <ul><li>  Invest - 5000 USD</li>
<li> Daily Return - 2%</li>
<li> Return Days - 180</li>
<li> Return - 18000 USD</li>
</ul>


 </div>
                        </div>
                        <a class="btn btn--secondary btn--bordered btn--rounded" target="_blank" href="<?= base_url("home/login") ?>"> Join Now</a>
                    </div>
                    <!--  Pricing heading  -->
                </div>
            </div>
            <!-- .pricing-table End -->
            
            
            
            
            
            
            
            
            
            
            
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
    <div class="section-divider"></div>
</section>
<!-- #pricing1 end -->





<!-- Blog Grid
======================================= -->
<section id="blog" class="blog blog-grid pt-110 pb-60">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                <div class="heading  mb-50 text--center">
                    <p class="heading--subtitle">Don't Miss</p>
                    <h2 class="heading--title">Why Us</h2>
                    <!-- <p class="heading--desc mb-0">Follow our latest news and thoughts which focuses exclusively on design, art, vintage, and also our latest work updates.</p> -->
                </div>
            </div>
            <!-- .col-lg-6 end -->
        </div>
        <!-- .row end -->
        <div class="row">
            <!-- Blog Entry #1 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="blog-entry">
                    <div class="entry--img">
                        <a href="#">
                            <img src="<?= base_url('resource/frontend/') ?>images/blog/grid/2.jpg" alt="entry image"/>
                            <div class="entry--overlay"></div>
                        </a>
                    </div>
                    <!-- .entry-img end -->
                    <div class="entry--content" style="    min-height: 182px;" >
                        <!-- <div class="entry--meta"> -->
                            <!-- <a href="#">Investment tips</a> -->
                        <!-- </div> -->
                        <div class="entry--title">
                            <h4><a href="#">Daily Return</a></h4>
                        </div>
                        <!-- <div class="entry--date"> -->
                            <!-- Apr 15, 2018 -->
                        <!-- </div> -->
                        <div class="entry--bio">
                            <p> This set of Return will provide a regular growth to an individual.Bigscope Offers 180 Days Return</p>
                        </div>
                        <!-- <div class="entry--more"> -->
                            <!-- <a href="#"><i class="fa fa-plus"></i>Read More</a> -->
                        <!-- </div> -->
                    </div>
                    <!-- .entry-content end -->
                </div>
                <!-- .blog-entry end -->
            </div>
            <!-- .col-md-4 end -->

            <!-- Blog Entry #2 -->
             <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="blog-entry">
                    <div class="entry--img">
                        <a href="#">
                            <img src="<?= base_url('resource/frontend/') ?>images/blog/grid/1.jpg" alt="entry image"/>
                            <div class="entry--overlay"></div>
                        </a>
                    </div>
                    <!-- .entry-img end -->
                    <div class="entry--content" style="
    min-height: 182px;
">
                        <!-- <div class="entry--meta"> -->
                            <!-- <a href="#">Investment tips</a> -->
                        <!-- </div> -->
                        <div class="entry--title">
                            <h4><a href="#">Revenue sharing Income</a></h4>
                        </div>
                        <!-- <div class="entry--date"> -->
                            <!-- Apr 15, 2018 -->
                        <!-- </div> -->
                        <div class="entry--bio">
    <div class="entry--bio_add">
<p> Bigscope brings the instant growth by level income.An excellent opportunity by enhancing network.Bigscope offers upto 25 Level sharing Retunrs</p>
	  </div>	
		
                        </div>
                        <!-- <div class="entry--more"> -->
                            <!-- <a href="#"><i class="fa fa-plus"></i>Read More</a> -->
                        <!-- </div> -->
                    </div>
                    <!-- .entry-content end -->
                </div>
                <!-- .blog-entry end -->
            </div>
            <!-- .col-md-4 end -->

           <!-- Blog Entry #2 -->
             <div class="col-sm-12 col-md-6 col-lg-4"> 
                <div class="blog-entry">
                    <div class="entry--img">
                        <a href="#">
                            <img src="<?= base_url('resource/frontend/') ?>images/blog/grid/3.jpg" alt="entry image"/>
                            <div class="entry--overlay"></div>
                        </a>
                    </div>
                    <!-- .entry-img end -->
                    <div class="entry--content" style="    min-height: 182px;">
                        <!-- <div class="entry--meta"> -->
                            <!-- <a href="#">Investment tips</a> -->
                        <!-- </div> -->
                        <div class="entry--title">
                            <h4><a href="#">Binary Income</a></h4>
                        </div>
                        <!-- <div class="entry--date"> -->
                            <!-- Apr 15, 2018 -->
                        <!-- </div> -->
                        <div class="entry--bio">
                            <p> Bigscope introducing Magic income which defines Binary income forusers.Bigscope offers wekkly Binary and Daily Binary Game!!!</p>
                        </div>
                        <!-- <div class="entry--more"> -->
                            <!-- <a href="#"><i class="fa fa-plus"></i>Read More</a> -->
                        <!-- </div> -->
                    </div>
                    <!-- .entry-content end -->
                </div>
                <!-- .blog-entry end -->
            </div>
            </div>
            <!-- .col-md-4 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- #blog end -->


<!-- video  #2
============================================= -->
<section id="video2" class="video-2 bg-overlay bg-overlay-dark2 text-center pb-0">
    <div class="bg-section"><img src="<?= base_url('resource/frontend/') ?>images/background/3.jpg" alt=""></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1">
                <div class="heading">
                    <p class="heading--subtitle ">Profitable And Successful Investments</p>
                    <h2 class="heading--title color-white mb-0">Quality is our obsession </h2>
					  <h2 class="heading--title color-white mb-0"> & service is our passion</h2>
                </div>
            </div>
            <!-- .col-lg-10 end -->
        </div>
        <!-- .row end -->
        <div class="row">
             <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="video--content text-center">
                    <div class="bg-section">
                        <!-- <img src="<?= base_url('resource/frontend/') ?>images/video/1.jpg" alt="Background" /> -->
                    </div>
                   <!--  <div class="video--button">
                        <div class="video-overlay">
                           
                            

<video  controls muted loop autoplay id="myVideo" class="video_style" style="    width: 80%;">
  <source src="videoblocks.mp4"  type="video/mp4"  >
  Your browser does not support HTML5 video.
</video> -->


							<!-- <a class="popup-video" href=""> -->
                                    <!-- <span class="btn--animation"></span> -->
                                    <!-- <i class="fa fa-play"></i> -->
                                <!-- </a> -->
								
								
								
								
                            
                            <!-- .video-player end -->
                       <!--  </div>
                    </div> -->
               
                <!-- .video-content end -->
            </div>
            <!-- .col-lg-12 end -->
        </div>
            <!-- .col-lg-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
    <div class="section-divider"></div>
</section>
<!-- #video2 end -->

<!-- Info Cards
============================================= -->
<section id="infoCards" class="info-cards pt-80 bg-gray">
    <div class="container">
        <div class="row">
          


 <div class="col-sm-12 col-md-12 col-lg-4">
                <div class="info-card">
                   <img src="<?= base_url('resource/frontend/') ?>qwer.png">
                </div>
                <!-- .info-card end -->
            </div>

		  <div class="col-sm-12 col-md-12 col-lg-4">
                <div class="info-card">
                    <div class="info-card-step"></div>
                    <div class="info-card-content">
                        <!-- <h4 class="info-card-subtitle">We are helpers</h4> -->
                        <h3 class="info-card-title">Our Vision</h3>
                        <p class="info-card-desc">Our vision is to specializing in providing our esteemed clients with cost, effective, high quality and commercially Financial Business Services.</p>
                        <!-- <a class="info-card-links" href="#"><i class="fa fa-plus"></i>Read More</a> -->
                    </div>
                </div>
                <!-- .info-card end -->
            </div>
            <!-- .col-lg-4 end -->
            <div class="col-sm-12 col-md-12 col-lg-4">
                <div class="info-card">
                    <div class="info-card-step"></div>
                    <div class="info-card-content">
                        <!-- <h4 class="info-card-subtitle">We are helpers</h4> -->
                        <h3 class="info-card-title">Our Mission</h3>
                        <p class="info-card-desc">Our mission is to strengthen the business growth of our customers with innovative marketing technologies to deliver high quality solutions.</p>
                        <!-- <a class="info-card-links" href="#"><i class="fa fa-plus"></i>Read More</a> -->
                    </div>
                </div>
                <!-- .info-card end -->
            </div>
           
            <!-- .col-lg-4 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- #infoCards end -->

<!-- Featured #1
============================================= -->
<section id="featured1" class="featured featured-1 text-center pt-110">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                <div class="heading  mb-50 text--center">
                    <p class="heading--subtitle">What We Do!</p>
                    <h2 class="heading--title">Our Features</h2>
                    <!-- <p class="heading--desc mb-0">When your people get up every day wanting to come to work, success happens, we help you to ensure everyone is in the right jobs.</p> -->
                </div>
            </div>
            <!-- .col-lg-6 end -->
        </div>
        <!-- .row end -->
        <div class="row">
            <!-- Feature Card #1 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-presentation"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Easy to operate</h3>
                        <p class="feature-card-desc">The Binary Financial Software user interface is very simple so it makes easy for anyone with basic internet knowledge. Altogether it’s a 100% user-friendly Software available in online market.</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
            <!-- Feature Card #2 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-search"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Fast, Secure, Reliable</h3>
                        <p class="feature-card-desc">Binary Financial Software is the Fastest, Secure and a Reliable solution to make the system work smoothly. Users can process their payment transactions much secure by using this.</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
            <!-- Feature Card #3 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-piechart"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Promotional Tools</h3>
                        <p class="feature-card-desc">This feature provides promotional tools for making your business easy and successful. We provide text invite, banner invite and social invites.</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
            <!-- Feature Card #4 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-map"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Genealogy Tree </h3>
                        <p class="feature-card-desc">Genealogy tree is a representation of the members in the form of a tree. The tree is based on the user position and forms team in form of a tree.</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
            <!-- Feature Card #5 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-bargraph"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Auto Responder</h3>
                        <p class="feature-card-desc">It is the best internet marketing method which helps to capture names and e-mail addresses. This includes sales messages, marketing messages and promotes your product.</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
            <!-- Feature Card #6 -->
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="feature-card">
                    <div class="feature-card-icon">
                        <i class="icon-shield"></i>
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Help and Support</h3>
                        <p class="feature-card-desc">Provides an online help tool to study more about Binary MLM Software along with 24*7 support. Contact Us to know more about Help and Support</p>
                    </div>
                </div>
            </div>
            <!-- .col-lg-4 end -->
        </div>
    

    </div>
    <!-- .container end -->
</section>
<!-- #featured1 end -->








<!-- Clients #1
============================================= -->
<section id="clients1" class="clients clients-1 logo_style">
    <div class="container">
	
	
	
	<div class="row clearfix">
            <div class="col-sm-12 col-md-12 col-lg-6 offset-md-3">
                <div class="heading heading-1 text--center mb-40">
                    <!-- <p class="heading--subtitle">Get Started Now</p> -->
                    <h2 class="heading--title text-white">We Accept</h2>
                    <!-- <p class="heading--desc heading--light mb-0">Get started now with us to earn every day and forever in your business. We accept Investment from all over the world.</p> -->
                </div>
            </div>
            <!-- .col-md-8 end -->
        </div>
	

	
        <div class="row">
            <marquee behavior="alternate" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
               
			   <img src="<?= base_url('resource/frontend/') ?>images/clients/1.png" alt="">
           
		        <img src="<?= base_url('resource/frontend/') ?>images/clients/2.png" alt="">
			
			    <img src="<?= base_url('resource/frontend/') ?>images/clients/3.png" alt="">
			 
			 
			    <img src="<?= base_url('resource/frontend/') ?>images/clients/4.png" alt="">
			  
			  
			    <img src="<?= base_url('resource/frontend/') ?>images/clients/5.png" alt="">
			   
			   
			   <img src="<?= base_url('resource/frontend/') ?>images/clients/6.png" alt="">
			   
			     <img src="<?= base_url('resource/frontend/') ?>images/clients/7.png" alt="">
				 
				  <img src="<?= base_url('resource/frontend/') ?>images/clients/8.png" alt="">
			 
             </marquee>
        </div>
        <!-- .row-clients end -->
    </div>
    <!-- .container end -->
</section>
<!-- #clients1 end -->




<!-- Team #1
============================================= -->
<!-- <section id="team1" class="team team-1 pt-110 pb-60" style="    padding: 40px 0px 0px 0px;background: #1b1a1a;color: white;">
    <div class="containe-fluid">
        <div class="row clearfix">
            <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                <div class="heading  mb-50 text--center">
                   <p class="heading--subtitle">Bigscope</p>-->
                  <!--   <h2 class="heading--title" style="color:white;">Business Process </h2> -->
                    <!-- <p class="heading--desc mb-0">We love what we do and we do it with passion. We value the reformation of the message, and the smart incentives.</p> -->
              <!--   </div>
            </div>
            <! .col-lg-6 end -->
        <!-- </div> -->
       
        <!-- < --><!-- div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="<?=base_url('resource')?>OurTeam.jpg ">
            </div>
         </div>
       
    </div> -->
    <!-- .container end -->
<!-- </section>  --> -->
<!-- #team1 end -->



===========
<section id="featured3" class="featured featured-2 featured-3 featured-left bg-dark3 pt-0 pb-0">
    <div class="container-fluid pr-0 pl-0">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-content">
                <div class="heading">
                    <h2 class="heading--title color-white">Amazing Business Opportunity</h2>
                    <!-- <p class="heading--desc color-white">The choice is in your hands: Where do you go to get an advice and where you purchase products?!!</p> -->
                </div>
                <!-- Feature Card #1 -->
                <div class="feature-card wow fadeInUp" data-wow-delay="100ms">
                    <div class="feature-card-icon">
                       <img src="<?= base_url('resource/frontend/') ?>images/icons/21.png">
                    </div>
                    <div class="feature-card-content">
                        <h3 class="feature-card-title">Investment Income:</h3>
                         <h3 class="feature-card-title he_margin">Daily Return income</h3>
                        
                      
						 <p class="feature-card-desc">1% -2% Daily Return for 180 days</p>
						 
						   <h3 class="feature-card-title he_margin">Revenue Sharing Income </h3>
						  
						   <p class="feature-card-desc">This income gives upto 25 level income of team sharing daily return joining Packages</p>
                    </div>
                </div>

				
				
                <!-- feature-card end -->
                <!-- Feature Card #2 -->
                <div class="feature-card wow fadeInUp" data-wow-delay="100ms"> 
                    <div class="feature-card-icon">
                         <img src="<?= base_url('resource/frontend/') ?>images/icons/22.png">
                    </div>
                    <div class="feature-card-content"> 
                        <h3 class="feature-card-title he_margin">Binary Income:</h3>
                         <h3 class="feature-card-title he_margin">Weekly Binary</h3>
						 <p class="feature-card-desc">Bigscope offers Weekly Binary 3% of company turnover every week</p>
						 
						
						   <h3 class="feature-card-title he_margin">Daily Binary</h3>
						   
						   <p class="feature-card-desc">Bigscope offers 3% of Daily Turnover and distributes to winners </p>

						    
						   
                    </div>
                </div>
				
				
	
                <!-- feature-card end -->
                <!-- Feature Card #3 -->
                <!-- <div class="feature-card wow fadeInUp" data-wow-delay="300ms"> -->
                    <!-- <div class="feature-card-icon"> -->
                        <!-- <i class="icon-lifesaver"></i> -->
                    <!-- </div> -->
                    <!-- <div class="feature-card-content"> -->
                        <!-- <h3 class="feature-card-title">Best Support</h3> -->
                        <!-- <p class="feature-card-desc">We bring the right people business solutions to challenge established thinking and drive transformation.</p> -->
                    <!-- </div> -->
                <!-- </div> -->
                <!-- feature-card end -->
            </div>
            <!-- .col-lg-6 end -->
            <div class="col-sm-12 col-md-12 col-lg-6 pr-0">
                <div class="banner--img">
                    <img src="<?= base_url('resource/frontend/') ?>images/banners/1.jpg" alt="banner img">
                </div>
            </div>
            <!-- .col-lg-6 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- #featured3 end -->

<!-- CTA #1
============================================= -->
<section id="cta1" class="cta cta-1 bg-theme">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9">
                <h3>Doing the Right Thing, at the Right Time!</h3>
            </div>
            <!-- .col-lg-9 -->
            <div class="col-sm-12 col-md-12 col-lg-3 text-right">
                <a href="<?= base_url("home/login") ?>" target="_blank" class="btn btn--white btn--bordered btn--rounded">Join now</a>
            </div>
            <!-- .col-lg-3 -->
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</section>
<!-- #cta1 end -->




<!-- Footer #1
============================================= -->
<footer id="footer" class="footer footer-1">
	<!-- Widget Section
	============================================= -->
	<div class="footer-widget">
		<div class="container">
			<div class="row clearfix">
				<div class="col-12 col-md-6 col-lg-3 footer--widget widget-about">
					<div class="widget-content">
						<img class="footer-logo" src="<?= base_url('resource/frontend/') ?>logo-small.png" alt="logo">
						<p>Binary Known for financial services advisor for a decade.we brings a smart structured & organised plans for leader and investors.This is a process to enhance the market & make a magnificient changes to world.which works on marketing strategy for professionals who looking for world calss plan for individual benifits.</p>
					
					</div>
				</div><!-- .col-md-3 end -->
				<div class="col-12 col-sm-4 col-md-6 col-lg-2 footer--widget widget-links">
					<div class="widget-title">
						<h5>Company</h5>
					</div>
					
					
					
					<div class="widget-content">
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><a href="#about1">About us</a></li>
							<li><a href="#pricing1">Business Packages</a></li>
						
						
						</ul>
					</div>
				</div><!-- .col-md-2 end -->
				
				<div class="col-12 col-sm-8 col-md-6 col-lg-4 footer--widget widget-links widget-links-inline">
					<div class="widget-title">
						<h5>Contact Us</h5>
					</div>
					<div class="widget-content">
						<ul>
							<li><a href="#">If you have any query, Please contact us on given email id.</a></li>
							<li><a href="mailto:webmaster@example.com">info@Bigscope.co</a></li>
						
							
						
						</ul>
						
						
						
						
					</div>
				</div><!-- .col-md-4 end -->

				<div class="col-12 col-md-6 col-lg-3 footer--widget widget-newsletter">
					<div class="widget-title">
						<h5>Subscribe</h5>
					</div>
					<div class="widget-content">
						<form class="form-newsletter mailchimp">
							<input type="email" name="email" class="form-control" placeholder="Subscribe Our Newsletter">
							<button type="submit"><i class="fa fa-long-arrow-right"></i></button>
						</form>
						<div class="subscribe-alert"></div>
						<div class="clearfix"></div>
					
						<div class="social-icons">
							<!--<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>-->
							
						<h5 style="
    font-size: 16px;
    color: #a0d334;
" >Follow us</h5>
						
						
						
							<a href="https:/twitter.com/Bigscope3604" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
							<!--<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>-->
							<!--<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>-->
						</div>
					
					
 					</div>
				</div><!-- .col-md-3 end -->
				<div class="clearfix"></div>
			</div>
		</div><!-- .container end -->
	</div><!-- .footer-widget end -->
	
	<!-- Copyrights
	============================================= -->
	<div class="footer--bar">
 			<div class="row">
                <div class="col-12 col-md-12 col-md-12 text--center footer--copyright">
					<div class="copyright">
						<span>© 2019, With</span> <a href=""> Bigscope Team</a>
					</div>
                </div>
 			</div>
		 <!-- .row end -->
	</div><!-- .footer-copyright end -->
	 <!-- The Modal -->
  <div class="modal fade" id="myModal5">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal5">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Modal body..
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal5">Close</button>
        </div>
        
      </div>
    </div>
  </div>

</footer>


<div id="back-to-top" class="backtop"><i class="fa fa-long-arrow-up"></i></div>
 </div><!-- #wrapper end -->

<!-- Footer Scripts
============================================= -->
<script src="<?= base_url('resource/frontend/') ?>js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>js/plugins.js"></script>
<script src="<?= base_url('resource/frontend/') ?>js/functions.js"></script>
<!-- RS5.0 Core JS Files -->
<script src="<?= base_url('resource/frontend/') ?>revolution/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?= base_url('resource/frontend/') ?>revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<!-- RS Configration JS Files -->
<script src="<?= base_url('resource/frontend/') ?>js/rsconfig.js"></script>

<script>
$(document).ready(function(){
 / Add smooth scrolling to all links
 $(".navbar-nav  li a").on('click', function(event) {

   / Make sure this.hash has a value before overriding default behavior
   if (this.hash !== "") {
     / Prevent default anchor click behavior
     event.preventDefault();

     / Store hash
     var hash = this.hash;

     / Using jQuery's animate() method to add smooth page scroll
     / The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
     $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 800, function(){
  
       / Add hash (#) to URL when done scrolling (default click behavior)
       window.location.hash = hash;
     });
   } / End if
 });
});
</script>



</body>


</html>