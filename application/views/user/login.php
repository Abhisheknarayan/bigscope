<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>My Coupon Mall</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="<?=base_url('resource/user')?>/images/favicon.png" rel="icon"> -->

    <style>
        .wrap_add {
            margin: 0;
            color: #6a6f8c;
            <!-- background-image: url(<?=base_url('resource/user/')?>1.png);
            --> font: 600 16px/18px 'Open Sans', sans-serif;
            background-size: 120%;
        }
        
        *,
        :after,
        :before {
            box-sizing: border-box
        }
        
        .clearfix:after,
        .clearfix:before {
            content: '';
            display: table
        }
        
        .clearfix:after {
            clear: both;
            display: block
        }
        
        a {
            color: inherit;
            text-decoration: none
        }
        
        .login-wrap {
            width: 100%;
            margin: auto;
            max-width: 525px;
            min-height: 670px;
            position: relative;
            box-shadow: 0 12px 15px 0 rgba(0, 0, 0, .24), 0 17px 50px 0 rgba(0, 0, 0, .19);
        }
        
        .login-html {
           /* margin: 0;
            position: absolute;
            top: 60%;
            left: 50%;s
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            width: 100%;*/
            /* min-height: 1000px; */
            /* position: absolute; */
            margin: 20px 0px 20px 0px;
            padding: 10px 60px 50px 60px;
            background: linear-gradient(45deg, #330d50, #0888af);
            /* background-image: url(12.png); */
        }
        
        .login-html .sign-in-htm,
        .login-html .sign-up-htm {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            transform: rotateY(180deg);
            backface-visibility: hidden;
            transition: all .4s linear;
        }
        
        .login-html .sign-in,
        .login-html .sign-up,
        .login-form .group .check {
            display: none;
        }
        
        .login-html .tab,
        .login-form .group .label,
        .login-form .group .button {
            text-transform: uppercase;
        }
        
        .login-html .tab {
            font-size: 22px;
            margin-right: 15px;
            padding-bottom: 5px;
            margin: 0 15px 10px 0;
            display: inline-block;
            border-bottom: 2px solid transparent;
        }
        
        .login-html .sign-in:checked + .tab,
        .login-html .sign-up:checked + .tab {
            color: #f4f8ff;
            border-color: #e9b327;
        }
        
        .login-form {
            min-height: 345px;
            position: relative;
            perspective: 1000px;
            transform-style: preserve-3d;
        }
        
        .login-form .group {
            margin-bottom: 15px;
        }
        
        .login-form .group .label,
        .login-form .group .input,
        .login-form .group .button {
            width: 100%;
            color: #1b1c1d;
            display: block;
        }
        
        .login-form .group .input,
        .login-form .group .button {
            border: none;
            padding: 15px 20px;
            border-radius: 8px;
            /*	background: #4e3333; */
        }
        
        .login-form .group input[data-type="password"] {
            text-security: circle;
            -webkit-text-security: circle;
        }
        
        .login-form .group .label {
            color: #ffffff;
            font-size: 12px;
            margin: 10px 0px 10px 0px;
        }
        
        .login-form .group .button {
            /* background: #d9b866; */
           background: linear-gradient(to right, #0069eb 10%, #00ebe2 100%);
    color: white;
    box-shadow: 0px 2px 1px 0px #42ffdc; 
        }
        
        .login-form .group label .icon {
            width: 15px;
            height: 15px;
            border-radius: 2px;
            position: relative;
            display: inline-block;
        }
    }
    .login-form .group label .icon:before,
    .login-form .group label .icon:after {
        content: '';
        width: 10px;
        height: 2px;
        background: #fff;
        position: absolute;
        transition: all .2s ease-in-out 0s;
    }
    .login-form .group label .icon:before {
        left: 3px;
        width: 5px;
        bottom: 6px;
        transform: scale(0) rotate(0);
    }
    .login-form .group label .icon:after {
        top: 6px;
        right: 0;
        transform: scale(0) rotate(0);
    }
    .login-form .group .check:checked + label {
        color: #ffffff;
    }
    .login-form .group .check:checked + label .icon {
        background: #ffffff;
    }
    .login-form .group .check:checked + label .icon:before {
        transform: scale(1) rotate(45deg);
    }
    .login-form .group .check:checked + label .icon:after {
        transform: scale(1) rotate(-45deg);
    }
    .login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm {
        transform: rotate(0);
    }
    .login-html .sign-up:checked + .tab + .login-form .sign-up-htm {
        transform: rotate(0);
    }
    .hr {
        height: 2px;
        margin: 10px 0 10px 0;
        background: rgba(255, 255, 255, .2);
    }
    .foot-lnk {
        text-align: center;
    }
    .white {
        color: white;
    }
    .logo_style {
        width: 60%;
    }
    .logo {
        text-align: center;
        margin: 20px 0px;
    }
    .ab_se {
        position: absolute;
        padding: 14px 0px;
        font-size: 12px;
        color: #ffffff;
        background: color: #1b1c1d;
        border-radius: 33px;
        border-color: #ced4da;
        border-right: none;
    }
    .login-html .tab {
        font-size: 22px;
        margin-right: 15px;
        padding-bottom: 5px;
        margin: 0 15px 10px 0;
        display: inline-block;
        border-bottom: 2px solid #36216b;
    }
    .demo {
        position: absolute;
      
        width: 100%;
    }
    .creat-account {
        font-size: 18px;
        text-transform: capitalize;
        font-weight: 600;
        /* background: linear-gradient(to right, #eeb828 10%, #ff572236 100%); */
        
        color: #ffffff;
        margin-top: 10px;
        display: block;
        padding: 0px 40px;
        color: #00e6e1;
    }
    .add-padding-left {
        padding-left: 110px;
    }
    .login-remember {
        color: white;
        font-size: 14px;
        margin-right: 0px;
        padding-bottom: 5px;
        margin: 0 0px 0px 0;
        display: inline-block;
        /* border-bottom: 2px solid #36216b;}
    </style>

</head>

<body class="wrap_add">
    <!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

    <div class="container">

        <div class="demo">

        <div class="row">

            <div class="login-add">
                <div class="login-wrap">
                    <div class="login-html">
                        <div class="logo">
                            <a href="<?= base_url() ?>"> <img class="logo_style" src="<?= base_url('') ?>resource/front/images/logo.png"> </a>
                        </div>
                        <div class="sign-in-htm"> </div>
                        <?php echo $this->session->flashdata('user_registered');  ?>
                            <input id="tab-1" type="radio" name="tab" class="sign-in" checked>
                            <label for="tab-1" class="tab" style="margin-left: 32%;">Sign In</label>

                            <div class="login-form">

                                <div class="jagdeep1">
                                    <?php echo form_open_multipart('home/signin');?>
                                        <div class="group">
                                            <label for="user" class="label">user id</label>
                                            <input id="user" name="userid" type="text" class="input" required="">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Password</label>
                                            <input id="pass" name="password" type="password" class="input" data-type="password" required="">
                                        </div>
                                        <div class="group">
                                            <input type="checkbox" id="myCheck" onclick="myFunction()">
                                            <label for="check" class="login-remember"><span class="icon"></span> Keep me Signed in</label>
                                        </div>
                                        <div class="group">
                                            <input type="submit" name="submit" class="button" value="Sign In">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                            <a class="white" href="<?= base_url('home/forgot_password') ?>">FORGOT PASSWORD?</a>
                                        </div>
                                        <div class="foot-lnk">
                                            <a class="white creat-account" href="<?= base_url();?>home/register">Create an accounts</a>
                                        </div>

                                        <?php echo form_close(); ?>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

        <script src="<?= base_url();?>/resource/front/uat_public/js/jquery.min.js">
        </script>
        <script src="<?= base_url() ?>/resource/front/uat_public/js/jquery-starfield.js">
        </script>
        <script type="application/javascript">
            $('.demo').starfield({
                starDensity: 0.2,
                mouseScale: 0.2
            });
        </script>

        <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
        <script>
            grecaptcha.ready(function() {
                grecaptcha.execute('reCAPTCHA_site_key', {
                    action: 'homepage'
                }).then(function(token) {
                    ...
                });
            });
        </script>

        <script>
            function myFunction() {
                var checkBox = document.getElementById("myCheck");
                var text = document.getElementById("text");
                if (checkBox.checked == true) {
                    text.style.display = "block";
                } else {
                    text.style.display = "none";
                }
            }
        </script>

</body>

</html>