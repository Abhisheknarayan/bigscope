<?php

//echo "<pre>"; print_r($tckects_history_log); die();

?>

    <body>

        <!-- Start wrapper-->
        <div id="wrapper">

            <!--Start sidebar-wrapper-->

            <!--End topbar header-->

            <div class="clearfix"></div>

            <div class="content-wrapper" style="min-height: 840px;">
                <div class="container-fluid">

                    <div class="row pt-2 pb-2">
                        <div class="col-sm-9">
                            <h4 class="page-title">Support Ticket log</h4>
                            <ol class="breadcrumb">
                                <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                                <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                                <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                            </ol>
                        </div>
                        <div class="col-sm-3 pull-right text-right">
                            <!--<a href="javascript:;" class="btn btn-primary shadow-primary px-2 " name="reply" id="reply"> Reply</a>-->
                        </div>
                    </div>
                    <!-- End Breadcrumb-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><i class="fa fa-table"></i> Support Ticket log</div>

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="" class="table table-bordered">

                                            <thead>
                                                <tr>

                                                    <th>Sr No</th>
                                                    <th>Reply By</th>
                                                    <th>Message</th>
                                                    <th>Date</th>

                                                </tr>
                                            </thead>
                                            <?php 
                                            $i=1;
                                            foreach($tckects_history_log as $row){ ?>
                                            <tbody>
                                                <td><?php echo $i;?></td>
                                                 <td><?php echo $row->message;?></td>
                                                 <td><?php echo $row->message;?></td>
                                                 <td><?php echo $row->message;?></td>
                                                
                                            </tbody>
                                            <?php 
                                            $i++;
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="reply-Supportticket">
                <div class="modal" tabindex="-1" role="dialog" id="reply_modal">
                    <div class="modal-dialog noti_modal">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" id="close1">&times;</button>
                            </div>

                            <div class="modal-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-content p-2">
                                            <form method="post" action="">

                                                <input type="hidden" name="support_id" value="">

                                                <div class="form-group">
                                                    <div class="position-relative has-icon-right">
                                                        <label for="timesheetinput1" class="text-black">Reply</label>

                                                        <textarea id="message" class="form-control form-control-rounded" name="message" required=""></textarea>

                                                    </div>
                                                    <span class="error_msg amt_error"></span>
                                                </div>

                                                <button type="submit" name="reply" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                                    Send
                                                </button>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

            <script type="text/javascript">
                $("#reply").click(function() {
                    $("#reply_modal").modal('show');
                });

                $("#close1").click(function() {
                    $("#reply_modal").modal('hide');
                    history.go();
                })
            </script>