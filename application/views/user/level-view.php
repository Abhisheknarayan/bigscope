<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  
<!--End topbar header-->

<div class="clearfix"></div>
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Level View</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     <div class="col-sm-3 pull-right text-right">
          <a href="" class="btn btn-primary shadow-primary px-2" name="add">  Download View</a>
      </div>
     </div>
    <!-- End Breadcrumb-->
    
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 1</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                           
                            <tr>
                              <td></td>
                                <td></td>
                                <td></td>
                               <td></td>
                                <td></td>
                                <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                  <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                            </tr>
                                
                        </tbody>
                        
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 2</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                                 <th>Amount</th>
                               <th>Date</th>
                              <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td>
                                   
                                   </td>
                                   <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="#" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 3</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example2" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                                 <th>Amount</th>
                               <th>Date</th>
                                <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                   <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                        
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 4</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example3" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                                 <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                        
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 5</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example4" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 6</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example5" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                   
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 7</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example6" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 8</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example7" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                  
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 9</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example8" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 10</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example9" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 11</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example10" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                  
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 12</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example11" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td>/td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 13</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example12" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 14</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example13" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 15</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example14" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 16</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example15" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                  
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 17</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example16" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 18</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example17" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 19</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example18" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                 
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 20</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example19" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 21</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example20" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 22</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example21" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                  
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 23</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example22" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 24</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example23" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                   
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level 25</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example24" class="table table-bordered">
                      <thead>
                         <tr>
                                 <th>SNo</th>
                                 <th>User Id</th>
                                 <th>Sponser Id</th>
                                 <th>UserName</th>
                                <th>Email </th>
                                <th>Level</th>
                               <th>Status</th>
                               <th>Amount</th>
                               <th>Date</th>
                               <th>Action</th>
              
                        </tr>
                      </thead>
                      <tbody>
                                                  
                                                    <tr>
                                <td></td>
                                  <td></td>
                                  <td></td>
                                 <td></td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                  
                                    <td></td>
                                    <td></td>
                                    <td class="menu-action"><a href="" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a></td>
                                          </tr>
                                          
                        </tbody>
                       
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
           
          <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Level View</div>
            <div class="card-body">
                  <p>No Level found!!</p>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>
    </td>

  </tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
    
         
   <!--  <script type="text/javascript">
      var table1 = $('#example1').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table1.buttons().container()
        .appendTo( '#example1_wrapper .col-md-6:eq(0)' );

      var table2 = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table2.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );

      var table3 = $('#example3').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table3.buttons().container()
        .appendTo( '#example3_wrapper .col-md-6:eq(0)' );

      var table4 = $('#example4').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table4.buttons().container()
        .appendTo( '#example4_wrapper .col-md-6:eq(0)' );
        
         var table5 = $('#example5').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table5.buttons().container()
        .appendTo( '#example5_wrapper .col-md-6:eq(0)' );
         var table6 = $('#example6').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table6.buttons().container()
        .appendTo( '#example6_wrapper .col-md-6:eq(0)' );
          var table7 = $('#example7').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table7.buttons().container()
        .appendTo( '#example7_wrapper .col-md-6:eq(0)' );
          var table8 = $('#example8').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table8.buttons().container()
        .appendTo( '#example8_wrapper .col-md-6:eq(0)' );
          var table9 = $('#example9').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table9.buttons().container()
        .appendTo( '#example9_wrapper .col-md-6:eq(0)' );
          var table10 = $('#example10').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table10.buttons().container()
        .appendTo( '#example10_wrapper .col-md-6:eq(0)' );
         var table11 = $('#example11').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table11.buttons().container()
        .appendTo( '#example10_wrapper .col-md-6:eq(0)' );
         var table12 = $('#example12').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table12.buttons().container()
        .appendTo( '#example12_wrapper .col-md-6:eq(0)' );
         var table13 = $('#example13').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table13.buttons().container()
        .appendTo( '#example13_wrapper .col-md-6:eq(0)' );
         var table14 = $('#example14').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table14.buttons().container()
        .appendTo( '#example14_wrapper .col-md-6:eq(0)' );
         var table15 = $('#example15').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table15.buttons().container()
        .appendTo( '#example15_wrapper .col-md-6:eq(0)' );
         var table16 = $('#example16').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table16.buttons().container()
        .appendTo( '#example16_wrapper .col-md-6:eq(0)' );
         var table17 = $('#example17').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table17.buttons().container()
        .appendTo( '#example17_wrapper .col-md-6:eq(0)' );
         var table18 = $('#example18').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table18.buttons().container()
        .appendTo( '#example18_wrapper .col-md-6:eq(0)' );
         var table19 = $('#example19').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table19.buttons().container()
        .appendTo( '#example19_wrapper .col-md-6:eq(0)' );
         var table20 = $('#example20').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table20.buttons().container()
        .appendTo( '#example20_wrapper .col-md-6:eq(0)' );
         var table21 = $('#example21').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table21.buttons().container()
        .appendTo( '#example21_wrapper .col-md-6:eq(0)' );
         var table22 = $('#example22').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table22.buttons().container()
        .appendTo( '#example22_wrapper .col-md-6:eq(0)' );
         var table23 = $('#example23').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table23.buttons().container()
        .appendTo( '#example23_wrapper .col-md-6:eq(0)' );
         var table24 = $('#example24').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table24.buttons().container()
        .appendTo( '#example24_wrapper .col-md-6:eq(0)' );
         var table25 = $('#example25').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
      table25.buttons().container()
        .appendTo( '#example25_wrapper .col-md-6:eq(0)' );
    </script>  -->