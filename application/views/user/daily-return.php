
<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Daily Return</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Daily Return Income</div>
                            <div class="card-body">
                                
                         
                         
                         
                         
                         
                               
                    			                              
                                
                                
                <div class="row">
                    
                   <?php
					 $i = 1;
					 $package = $this->db->query("select * from purchase_package where user_id='".$logged_in->user_id."' && status = 'success'")->result_array();
                     foreach($package as $sql_rows ){
					?>                    
                                
                        <div class="col-12 col-lg-2 col-xl-2 ">
                            <div class="card border-info border-left-sm animated bounceInDown min-height-add add gradient-scooter daily-return ">
                                <div class="card-body">
                                    <div class="media">
                                        <div class="media-body text-left">
    
                                            <h4 class=" text_white"><?= $sql_rows['name'];?> </h4> 
                                            <span class="text_white highlight_text"> ($ <?= $sql_rows['amount'];?> )</span>
                                            <p class="btn btn-primary viewincome" id="view"><a  href="<?= base_url('user/daily_return/').$sql_rows['id'].'/'.$sql_rows['package_id'] ?>" >View Daily Return </a></p>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>      
                    
                	<?php }?>               
                                
                </div>            
                                
                                
                                
                                
                                
                                <!--package list end -->
                                
                                
                                
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Plan Name</th>
                                                <th>Package Amount($)</th>
                                                <th>Percent(%)</th>
                                                <th>Daily Return Income($)</th>
                                                <th>Date</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            
                                            <?php 
                                           
                                            $i=1;
                                            foreach ($dailyreturn_roi as $row){
                                            ?>
                                                 <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $row->register_id;?></td>
                                                   <td><?php echo $row->name;?></td>
                                                   <td ><?php echo $row->amount;?></td> 
                                                   <td><?php echo $row->percentage;?></td>
                                                   <td><?php echo $row->roi_amount;?></td>
                                                   <td ><?php echo $row->roi_date;?></td> 
                                                  
                                                  </tr>
                                            <?php 
                                            
                                            $i++;
                                            }; 
                                            ?>
                                            
                                            
                                        </tbody>                                        
                                            <tfoot>
                                                <tr>
                                                    <th>SNo</th>
                                                    <th>User Id</th>
                                                    <th>Plan Name</th>
                                                    <th>Package Amount($)</th>
                                                    <th>Percent(%)</th>
                                                    <th>Daily Return Income($)</th>
                                                    <th>Date</th>
                                                </tr>
                                            </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>