<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-12">
                      <!--  <h4 class="page-title text-center">Withdraw</h4> -->
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Withdraw Request</div>
                            <div class="card-body">
                                
                                <div class="text-danger"><strong>
                                      
                                  <?php
                                    if ( $this->session->flashdata('wihdrawal_btc')) {
                                        echo $this->session->flashdata('wihdrawal_btc'); 
                                
                                    }else{
                                        echo $this->session->flashdata('failed');  
                                    }
                                  ?>                  
                                      
                                 </strong></div>                                 

                                <div class="card-content p-2">
                                    <form method="post" onsubmit="return validate()" action="<?= base_url('user/withdrwal_request');?>">
                                        
                                        <input type="hidden" id="id" class="form-control" name="id" value="<?php echo $user_data[0]->id ?>">
                                        <input type="hidden" id="user_id" class="form-control" name="user_id" value="<?php echo $user_data[0]->user_id ?>">
  
                                        <div class="form-group">
                                            <div class="position-relative has-icon-right">
                                                <label for="timesheetinput1">Balance Amount($)</label>

                                                <input type="text" class="form-control form-control-rounded" id="avail_amt" value="<?= total_income($logged_in->user_id) ?>" name="balamount" readonly>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="position-relative has-icon-right">
                                                <label for="timesheetinput1">Amount($)</label>

                                                <input type="number" id="amount" class="form-control form-control-rounded" name="amount" required="" min="20" >

                                            </div>
                                            <span class="error_msg amt_error"></span>
                                        </div>
                                        <div class="form-group">
                                            <div id="btc_value_field"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="timesheetinput1">BTC Address</label>
                                            <div class="position-relative has-icon-right">
                                                <input type="text" class="form-control form-control-rounded" name="btc_address" id="btc_address" value="<?= $user_data[0]->bitcoin_wallet_address;?>" readonly required>
                                                <span class="error_msg btc_err"></span><span id ='btc_update'></span>
                                            </div>
                                        </div>

                                        <button type="submit" name="withdraw" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                            Withdraw
                                        </button>

                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <!--footer-->

         <script type="text/javascript">
				
				function validate(){
				    //var amt=parseInt($(this).val());
				    var amt=parseInt($("#amount").val());
					var avail_amt=parseInt($("#avail_amt").val());
					//alert(avail_amt);
					var btc=$("#btc_address").val();
					if(btc.length !==34){
					    
					    $(".btc_err").html("BTC Address is wrong you should be update your profile page");
						$("#btc_update").html('<a href="user_profile">click here to update </a>');
						$("#btc_address").focus();
						return false;
						
					}
					
					if(amt>avail_amt){
						$(".amt_error").html("Exceed the account limit");
						$("#amount").focus();
						return false;
					}
					
					if(amt<20){
					    $(".amt_error").html("Minimum withdwal amount is 20 $");
						$("#amount").focus();
						return false;
					        
					}					
				}
				
				$(document).on('input','#amount',function(){
				    var amount=$(this).val();
                    if(typeof(amount)=='undefined' || amount.length==''){
                        amount=0;
                    }
                    
    				$.get("http://bigscope.co/token/btc_price/"+amount, function(data){
    				        var convert_amout = data;
    				        var admin_percent = "<?= withraw_admin_charge() ?>";
    				        var admin_convert_amout = (data * (admin_percent/100)) ;
                            $("#btc_value_field").html('<div class="position-relative has-icon-right"><label for="timesheetinput1">BTC Value</label><input type="number" id="btc_value" value= "'+convert_amout+'"class="form-control form-control-rounded" name="btc_value" readonly required="" ></div><div class="position-relative has-icon-right"><label for="timesheetinput1">'+admin_percent+' % Admin charge </label><input type="number" id="admin_btc_value" value= "'+admin_convert_amout+'"class="form-control form-control-rounded" name="admin_btc_value" required="" readonly ></div>');
                        });
				});
                
				
			</script> 