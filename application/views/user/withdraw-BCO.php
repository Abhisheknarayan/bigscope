<body>
    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-12">
          
                        <h4 class="page-title text-center"><?= COIN ?></h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Withdraw In <?= COIN ?></div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message') ?>
                                <div class="card-content p-2">
                                    
                                    <?php echo form_open_multipart('token/withdraw_token_submit',array("onsubmit" => "return validate()"));?>
                                        <div class="form-group">
        
        <div class="position-relative has-icon-right">
            <label for="timesheetinput1">Total BCO</label>
            <input type="number" id="balance" class="form-control form-control-rounded" name="balance" placeholder="0.00" value="<?= total_purchase_coin($this->session->userdata('logged_in')->user_id)->coin ?>" readonly >
        </div>
        
        <div class="position-relative has-icon-right">
            <label for="timesheetinput1">BCO Avilable to Withdraw</label>
            <input type="number" id="amount" class="form-control form-control-rounded" name="amount" placeholder="0.00" value="<?= total_purchase_coin_user($this->session->userdata('logged_in')->user_id)->coin ?>" readonly autocomplete="off" required>
            <span class="error_msg amt_error"></span>
        </div>
        
        <div class="position-relative has-icon-right">
            <label for="timesheetinput1"><?= COIN ?> Price</label>
            <input type="number" id="coin_qty" class="form-control form-control-rounded" name="coin_qty" placeholder="0.00" value="" readonly >
        </div>
        
                                                
                                        </div>
                                        <button type="submit" name="buy_token" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Withdraw</button>
                                    <?php echo form_close();?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>


        <!--footer-->
        <script>
            
            $(document).on('input','#amount',function(){
                var amount=$(this).val();
                
                
                var amt=parseInt($("#amount").val());
				var coin_qty=parseInt($("#coin_qty").val());
				//alert(coin_qty);
                return false;
				
				if(amt>coin_qty){
					$(".amt_error").html("Exceed the Available Total balance");
					$("#amount").focus();
					return false;
				}else{
				    $(".amt_error").html("");
					$("#amount").focus();
                    return false;
				}
                
                if(typeof(amount)=='undefined' || amount.length==''){
                    amount=0;return false;
                }
                
                
                $.get("<?= base_url('token/tokenPrice/') ?>"+amount, function(data){
                    $('input[name="coin_qty"]').val(data);
                });
                
                
                var coin_qty=amount*tokenprice;
                
                $('input[name="coin_qty"]').val(coin_qty);
                return false;
            });
            
        	function validate(){
			    var amt=parseInt($("#amount").val());
				var coin_qty=parseInt($("#coin_qty").val());
                $(".amt_error").html("no token is Avilable to withdraw please check after few days back,You can only withdraw after 45 days of purchaseing");
                    $("#amount").focus();
				return false;
				
				if(amt>coin_qty){
					$(".amt_error").html("Exceed the Available Total balance, You can only withdraw after 45 days of purchaseing");
					$("#amount").focus();
					return false;
				}
                if (amt=coin_qty=0) {

                    $(".amt_error").html("no token is Avilable to withdraw please check after few days back");
                    $("#amount").focus();
                    return false;
                }
									
			}
			
        </script>