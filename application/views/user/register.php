<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>My Coupon Mall</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="<?=base_url('resource/user')?>/images/favicon.png" rel="icon">  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
      .wrap_add{
        margin: 0;
        color: #ffffff;
        background-image: url(<?=base_url('resource/user/')?>1.png);
        
        font: 600 16px/18px 'Open Sans',sans-serif;
        background-size: 120%;
      }
      *,:after,:before{
        box-sizing:border-box}
      .clearfix:after,.clearfix:before{
        content:'';
        display:table}
      .clearfix:after{
        clear:both;
        display:block}
      a{
        color:inherit;
        text-decoration:none}
      .login-wrap{
        width:100%;
        margin:auto;
        max-width:525px;
       
        position:relative;
        box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
      }
      .login-html{
        width: 100%;
        margin: 20px 0px 20px 0px;
         min-height: 1050px;
        /* position: absolute;*/
        padding: 10px 60px 50px 60px;
            background: linear-gradient(45deg, #330d50, #0888af);
        


        
      }
      .login-html .sign-in-htm,
      .login-html .sign-up-htm{
        top:0;
        left:0;
        right:0;
        bottom:0;
        position:absolute;
        transform:rotateY(180deg);
        backface-visibility:hidden;
        transition:all .4s linear;
      }
      .login-html .sign-in,
      .login-html .sign-up,
      .login-form .group .check{
        display:none;
      }
      .login-html .tab,
      .login-form .group .label,
      .login-form .group .button{
        text-transform:uppercase;
      }
      .login-html .tab{
        font-size:22px;
        margin-right:15px;
        padding-bottom:5px;
        margin:0 15px 10px 0;
        display:inline-block;
        border-bottom:2px solid transparent;
      }
      .login-html .sign-in:checked + .tab,
      .login-html .sign-up:checked + .tab{
        color: #ffffff;
        border-color:#e9b327;
      }
      .login-form{
      
        position:relative;
        perspective:1000px;
        transform-style:preserve-3d;
      }
      .login-form .group{
        margin-bottom:15px;
      }
      .login-form .group .label,
      .login-form .group .input,
      .login-form .group .button{
        width:100%;
        color:#1b1c1d;
        display:block;
      }
      .login-form .group .input,
      .login-form .group .button{
        border:none;
        padding:15px 20px;
        border-radius:8px;
           background: #ffffff;
        color: #131212;
        
        
      }
      .login-form .group input[data-type="password"]{
        text-security:circle;
        -webkit-text-security:circle;
      }
      .login-form .group .label{
       color: #ffffff;
        font-size: 12px;
        margin: 10px 0px 10px 0px;
      }
      .login-form .group .button{
           background: linear-gradient(to right, #0069eb 10%, #00ebe2 100%);
    color: white;
    box-shadow: 0px 2px 1px 0px #42ffdc; 
      }
      .login-form .group label .icon{
        width:15px;
        height:15px;
        border-radius:2px;
        position:relative;
        display:inline-block;
      }
      }
      .login-form .group label .icon:before,
      .login-form .group label .icon:after{
        content:'';
        width:10px;
        height:2px;
        background:#fff;
        position:absolute;
        transition:all .2s ease-in-out 0s;
      }
      .login-form .group label .icon:before{
        left:3px;
        width:5px;
        bottom:6px;
        transform:scale(0) rotate(0);
      }
      .login-form .group label .icon:after{
        top:6px;
        right:0;
        transform:scale(0) rotate(0);
      }
      .login-form .group .check:checked + label{
        color: #1b1c1d;
      }
      .login-form .group .check:checked + label .icon{
        background:#1161ee;
      }
      .login-form .group .check:checked + label .icon:before{
        transform:scale(1) rotate(45deg);
      }
      .login-form .group .check:checked + label .icon:after{
        transform:scale(1) rotate(-45deg);
      }
      .login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm{
        transform:rotate(0);
      }
      .login-html .sign-up:checked + .tab + .login-form .sign-up-htm{
        transform:rotate(0);
      }
      .inp1 {
        border:none;
        padding: 15px 97px 15px 141px;
        border-radius:25px;
        background: #ffffff;
           color: black;
      }
      .hr{
        height: 2px;
        margin: 10px 0 10px 0;
        background: rgba(255,255,255,.2);
      }
      .foot-lnk{
        text-align:center;
      }
      .white{
        color:white;
      }
      .logo_style{
        width: 60%;
      }
      .logo{
        text-align: center;
        margin: 20px 0px;
      }
      .ab_se{
       position: absolute;
    padding: 15px 2px 5px 3px;
    top: 27px;
    font-size: 12px;
    border: transparent;
    color: #000000;
    background: color: #1b1c1d;
    border-radius: 33px;
    border-color: #ced4da;
    border-right: none;
      }
      .login-html .tab {
        font-size: 22px;
        margin-right: 15px;
        padding-bottom: 5px;
        margin: 0 15px 10px 0;
        display: inline-block;
        border-bottom: 2px solid #36216b;
      }
      
      
      .demo {
           position: absolute;
    
    width: 100%;
      }
      .re-la {  position: relative;}
  .add-padding-left{    padding-left: 110px !important;}
      
    </style>
  </head>
  <body class="wrap_add">
   
    <div class="container">
         <div class="demo">
    
      <div class="row">
        <div class="login-add">
          <div class="login-wrap">
            <div class="login-html">
              <div class="logo">
                <a href = "<?= base_url(); ?>"><img class="logo_style" src="<?= base_url('') ?>resource/front/images/logo.png"> </a>
              </div>
              <div class="sign-in-htm"> 
              </div>
              
              <div class="text-danger"><strong>
                  
              <?php echo $this->session->flashdata('message');   ?>                  
                  
              </strong></div>
              
              <input id="tab-2" type="radio" name="tab" class="sign-up" checked="">
              <label for="tab-2" class="tab" style="margin-left: 32%;">Sign Up
              </label>
              <div class="login-form">
                <div class="sign-up-htm">
                  <?php echo form_open_multipart('home/registration');?>
                  <div class="group">
                    <label for="user" class="label">First name
                    </label>
                    <input id="user" name="firstname" type="text" class="input" placeholder="First name" required="">
                  </div>
                  <div class="group">
                    <label for="user" class="label">last name
                    </label>
                    <input id="user" name="lastname" type="text" class="input" required="" placeholder="last name">
                  </div>
                  <div class="group">
                    <label for="pass" class="label">Email Address
                    </label>
                    <input id="pass" name="email" type="text" class="input" placeholder="Email Address" required>
                  </div>


                <div class="group re-la">

                        <select class="ab_se" id="" name="mobile_code">
                            <?php
                
                            foreach($country_code as $key => $value){
                            ?>
                            <option style= "color: black;"value="+<?php echo $value['phonecode']; ?>" <?php if($value['id']=='99'){ echo "selected"; } ?>><?php echo $value['iso3']; ?>(+<?php echo $value['phonecode']; ?>)</option>
                            <?php } ?>
                        </select>   

                        <label for="pass" class="label">Mobile Number </label>
                        
                        <input type="text" class="input add-padding-left"   placeholder="Mobile Number" name="mobile" maxlength="10" required="">
                    
                </div>

                    <?php 
                        if(!empty($this->uri->segment(3))){
                            $value = $this->uri->segment(3);
                            $readonly = 'readonly';
                        }
                        else{
                            $value = '';
                            $readonly ='';
                        }
                    ?>
                  
                  <div class="group">
                    <div class="position-relative has-icon-right">
                      <label for="exampleInputUsername" class="sr-only">Sponser Id
                      </label>
                      <input type="text" class="input" placeholder="Sponser Id" name="sponser" required="" value="<?= $value; ?>" <?= $readonly; ?>  >
                      <span class="error_msg sponser_error">
                      </span>
                      <div class="form-control-position">
                        <i class="icon-user">
                        </i>
                      </div>
                    </div>
                  </div>
                  
                  <div class="group">
                    <label for="pass" class="label">Position
                    </label>
                    <select class="input" placeholder="position" name="position" class="input" required="">
                      <option value="l" class="input">Select Postion
                      </option>
                      <option value="L" class="input">Left
                      </option>
                      <option value="R" class="input">Right
                      </option>
                    </select>
                  </div>
                  <div class="group">
                    <label for="pass" class="label">Activation Code
                    </label>
                    <input id="green_id" name="green_id" type="text" class="input" placeholder="Activation Code" required="">
                    <span class="sponser_error error_msg">
                    </span>
                  </div>
                  
                  <div class="group">
                    <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                  </div>
                  
<!-- <div class="group">
<label for="pass" class="label">Password</label>
<input id="signpass" name="password" type="password" class="input" data-type="password">
<span class="password_error error_msg"></span>
</div> -->
                  <!-- <div class="group">
<label for="pass" class="label">Repeat Password</label>
<input id="signcpass" name="cpassword" type="password" class="input" data-type="password">
<span class="cpassword_error"></span>
</div> -->
                  <!-- captcha -->
                  <!-- <div class="group">
<div class="position-relative has-icon-right">
<img src="" id='captchaimg'><a href="javascript:;" class="reload_captcha" onclick="refreshCaptcha()"><i class="fa fa-refresh"> refresh</i></a>
</div>
</div> -->
                  <!-- <div class="group">
<div class="position-relative has-icon-right">
<label for="pass" class="label">Enter Captcha</label>
<input type="text" name="captcha" id="captcha" class="input" placeholder="Enter Captcha here..." required autocomplete="off">
<input type="hidden" name="mycap" id="mycap" value="">
<span class="captcha_error error_msg"></span>
</div>
</div> -->
                  <!-- end -->
                  <div class="group">
                    <input type="submit" name="register" class="button" value="Sign Up">
                  </div>
                  <div class="hr">
                  </div>
                  <div class="foot-lnk">
                      Already Member? <a href="<?= base_url();?>home/login"> Login</a> 
                  </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>                 
      </div>
    </div>
    </div>
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="<?= base_url() ?>/resource/front/uat_public/js/jquery-starfield.js"> </script> 
    
    
    <script type="application/javascript"> 
      $('.demo').starfield({
        starDensity: 0.2, mouseScale: 0.2 
      }
                          );
    </script>


<script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key">
</script>
<script>
  grecaptcha.ready(function() {
    grecaptcha.execute('reCAPTCHA_site_key', {
      action: 'homepage'}
                      ).then(function(token) {
      ...
    }
      );
    }
                            );
</script>
    
    
    
    
    
    
    
    
  </body>
</html>



