<?php
//print_r($token_list);
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">BCO History</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>BCO History</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                                        
                        <thead>
                            <tr>
                                <th>Sno</th>
                                <th>From: Date </th>
                                <th>To: Date </th>
                                <th><?= COIN ?> Value($)</th>
                                <th>Status</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach($token_list as $val):?>
                            <tr>
                                <td><?= $i++;?></td>
                                <td><?= $val->from_date;?></td>
                                <td><?= $val->to_date;?></td>
                                <td><?= $val->value;?></td>
                                <td><?= $val->status;?></td>
                                
                            </tr>
                          
                        </tbody>
                        <?php endforeach;?>
                        <tfoot>
                            <tr>
                                <th>Sno</th>
                                <th>From Date:</th>
                                <th>To Date:</th>
                                <th><?= COIN ?> Value($)</th>
                                <th>Status</th>
                                
                            </tr>
                        </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
          
