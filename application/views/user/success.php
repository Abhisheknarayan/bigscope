<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bigscope.co
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/plugins.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/theme-styles.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/blocks.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/widgets.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/addstyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/login-style.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700i,900," rel="stylesheet">
    <style>
      .demo {
        position: absolute;
        height: 100%;
        width: 100%;
      }
    </style>
  </head>
  <body>
    <div class="demo">
    </div>
    <div class="container">
      <div class="materialContainer">
        <div class=" verify ">
          <div class="box">
            <div class="card-wrap">
              <div class="card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown">
                <div class="card-body ">
                  <div class="card-content p-2 ">
                    <div class="text-center">
                      <a href="<?= base_url()?>"><img src="<?= base_url('') ?>resource/front/images/logo2.png" style="height: 114px;"> </a>
                      
                    </div>
                    <div class="card-body">
                    <?php if(isset($_SESSION['user_id'])){ ?>  
                    
                      <div class="card-block">
                        <!--<p class="alertok">Congratulations</p>-->
                        <p class="sucess_title" style="
                                                       margin-bottom: 4px;
                                                       ">
                          
                        </p>
                        <p class="sucess_title sucess_title_plus ">Hi, 
                          <b style="color: #eee;" class="highlight_text"><?php echo $this->session->first_name.' '.$this->session->last_name; ?></b>
                            
                           
                        </p> 
                        <p class="success_msg text_white text-center">
                          <span style='color:white;'>You have successfully created a Bigscope account Please check your inbox for Email verfication & complete your registration
                          </span> 
                        </p>
                      </div>
                     
                      <!--<h4 class="text-uppercase text-bold-400 grey darken-1 text-center"><a href="login.php" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Sign in</a></h4>-->
                      <hr>               
                      <p class="sucess_content text_white"> (If not receive in inbox , Dont forget to check your Spam folder)</p> 
                      
                      
                    <?php }else{ ?>
                    
                      <div class="card-block">
                        <!--<p class="alertok">Congratulations</p>-->
                        <p class="sucess_title" style="margin-bottom: 4px;"></p>
                        <p class="sucess_title sucess_title_plus "> 
                          <b style="color: #eee;" class="highlight_text"><?php $this->session->flashdata('user_registered');  ?></b>
                            
                           
                        </p> 
                        
                      </div>                    
                    
                    
                    <?php }?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery.min.js">
    </script>
    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery-starfield.js">
    </script> 
    <script type="application/javascript"> 
      $('.demo').starfield({
        starDensity: 0.2, mouseScale: 0.2 
      }
                          );
    </script>
  </body>
</html>
