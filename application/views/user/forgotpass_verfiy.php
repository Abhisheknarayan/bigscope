<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bigscope.co</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/plugins.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/theme-styles.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/blocks.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/widgets.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/addstyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/login-style.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700i,900," rel="stylesheet">

    <style>
        .demo {
            position: absolute;
            height: 100%;
            width: 100%;
        }
    </style>

</head>

<body>

    <div class="demo"></div>
    <div class="container">

        <div class="materialContainer">

            <div class=" verify ">
                <div class="box">
                    <div class="card-wrap">
                        <div class="card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown">

                            <div class="card-body ">
                                <div class="card-content p-2 ">
                                    <div class="text-center">
                                        <img src="<?= base_url('') ?>resource/front/images/logo2.png" style="height: 114px;>
		 	</div>
		<div class=" card-body ">

                        <div class="card-block ">
						<p class="sucess_title ">Dear <b >, 
                         <?php 

						  	if(!empty($fname)){
						  		echo $fname.' '.$lname;

						  	}

						  	?>
						</b> </p> 
						<p class="success_msg text_white text-center "><span style='color:white;'>Your password has been sent to your registered Email ID Please check</span> </p>

                  </div>

              <hr>  
              <p class="success_msg text_white text-center "><span style='color:red;'> (If you do not receive the forgot mail with in few minutes, Please ckeck you spam folder.)</span> </p>
      <div class="button login ">
         <button class="form-btn "><span><a href="<?= base_url() ?>home/login">LOGIN</a>
                                        </span> <i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    </div>

    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery.min.js"></script>
    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery-starfield.js"></script>

    <script type="application/javascript">
        $('.demo').starfield({
            starDensity: 0.2,
            mouseScale: 0.2
        });
    </script>

</body>

</html>