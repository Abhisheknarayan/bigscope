<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">My BCO</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>My BCO</div>
            <div class="card-body">
              <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                      <thead>
                            <tr>
                                <th>SNo</th>
                                <th>User Id</th>
                                <th><?= COIN ?> Qty</th>
                                <th>Purchase Amt($)</th>
                                <th>Transaction Key</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                foreach($token_history as $row){
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $row->to_wallet_address ?></td>
                                <td><?= $row->value ?></td>
                                <td><?= $row->amount ?></td>
                                <td><?= $row->transaction_key ?></td>
                                <td><?= $row->status ?></td>
                                <td><?= $row->date_ ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
$(document).ready(function() {
    $('#example').DataTable( {  
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],   
        dom: 'Bfrtip',        
        buttons: [            
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'
            
        ]
    } );
} );
</script>
    
          
