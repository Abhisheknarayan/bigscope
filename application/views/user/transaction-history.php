<?php include('panel/header.php');?>

<?php 
$con=connectdb();
$data=mysqli_query($con,"select * from purchase_list where user_rf_id='".$_SESSION['userid']."' and remarks='Buy Game' order by id desc");
?>
<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
  <?php include("panel/left-sidebar.php"); ?>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<?php include("panel/top-header.php"); ?>
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Transaction History by Daily Game</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Transaction History  by Daily Game</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                      <thead>
                      <tr>
                        <th>SNo</th>
                        <th>Order Id</th>
                        <th>Purchase Amount($)</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                           <?php 
                            $i=1;
                            while($row=mysqli_fetch_array($data))
                                {
                            ?>
                               <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $row['ord_id'];?></td>
                                <td><?php echo $row['total_price'];?></td>
                               <?php 
                                if($row['status']=='pending')
                                {
                                    echo '<td><span class="badge badge-danger shadow-danger">Pending</span></td>';
                                    
                                }
                                else
                                {
                                   echo '<td><span class="badge badge-success shadow-success">Success</span></td>';  
                                }
                                
                               ?>
                               <td><?php if($row['pay_via_admin']==0){ echo "Online"; }else{ echo "By Admin"; } ?></td>
                                <td><?php echo $row['pay_datetime'];?></td>
                               </tr>
                            <?php
                                $i++;
                                }
                            ?>
                        </tbody>
                         
                     </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
<?php include('panel/footer.php');?>
     