 
 <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2019 Bigscope.co
        </div>
      </div>
    </footer>
	<!--End footer-->
   
  </div>
  <!--End wrapper-->

      <!-- Bootstrap core JavaScript-->
     
      <script src="<?=base_url('resource/user/')?>js/popper.min.js"></script>
      <script src="<?=base_url('resource/user/')?>js/bootstrap.min.js"></script>
	
      <!-- simplebar js -->
      <script src="<?=base_url('resource/user/')?>plugins/simplebar/js/simplebar.js"></script>
      <!-- waves effect js -->
      <script src="<?=base_url('resource/user/')?>js/waves.js"></script>
      <!-- sidebar-menu js -->
      <script src="<?=base_url('resource/user/')?>js/sidebar-menu.js"></script>
      <!-- Custom scripts -->
      <script src="<?=base_url('resource/user/')?>js/app-script.js"></script>
    
      <!--Data Tables js-->
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/jszip.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>
    
       <!-- Vector map JavaScript -->
      <script src="<?=base_url('resource/user/')?>plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
      <!-- Chart js -->
      <script src="<?=base_url('resource/user/')?>plugins/sparkline-charts/jquery.sparkline.min.js"></script>
      <script src="<?=base_url('resource/user/')?>plugins/Chart.js/Chart.min.js"></script>
      <!-- Index js -->
      <script src="<?=base_url('resource/user/')?>js/bootstrap-toggle.js" rel="stylesheet"></script>
      <!-- <script src="<?=base_url('resource/user/')?>js/index.js"></script> -->
      <script src="<?=base_url('resource/user/')?>js/index3.js"></script>
      <script src="<?=base_url('resource/user/')?>js/simplyCountdown.min.js"></script>
  
  
    <script src="<?= base_url() ?>resource/front/uat_public/js/jquery-starfield.js"></script> 
    
    <script type="application/javascript"> 
      $('.demo').starfield({
        starDensity: 0.2, mouseScale: 0.2 
      }
                          );
    </script>
  
  
  
  
  
    <script>
     
      var d = new Date();
        d.setDate(d.getDate() + 8);
        //jQuery example
        $('#simply-countdown-losange').simplyCountdown({
            year: d.getFullYear(),
            month: d.getMonth() + 1,
            day: d.getDate(),
            enableUtc: false
        });
        
       var m = new Date();
        m.setDate(m.getDate() + 29);
        //jQuery example
        $('#simply-countdown-losangem').simplyCountdown({
            year: m.getFullYear(),
            month: m.getMonth() + 1,
            day: m.getDate(),
            enableUtc: false
        });
        
    </script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       /*var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );*/
      
      
      
    $('#example').DataTable( {  
    "bDestroy": true,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],   
        dom: 'Bfrtip',        
        buttons: [            
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'
            
        ]
    } );      
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );


        if ($('#AreaChart').length) {
          var ctx = document.getElementById('AreaChart').getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: ['','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
              datasets: [{
                label: 'Daily ROI',
                data: [<?php echo (!empty($daily))? $daily : 100 ; ?>],
                backgroundColor: "rgba(70,182,187, 0.7)",
                borderColor: "#46b6bb",
                borderWidth: 1
                
              }, {
                label: 'Level Income',
                data: [<?php echo (!empty($level))? $level : 200 ; ?>],
                backgroundColor: "rgba(0, 140, 255, 0.8)",
                borderColor: "#098ffc",
                borderWidth: 1,
                borderDash: [4, 4]
               
              }, {
                label: 'Directs',
                data: [<?php echo (!empty($user_count))? $user_count : 300 ; ?>],
                backgroundColor: "rgba(121,40,240, 0.9)",
                borderColor: "#7928f0",
                borderWidth: 1,
                borderDash: [2, 2]

              }]
            }
          });
        }
      });
       
    </script>
    
    
    
    
           
   
    
  
</body>


</html>
