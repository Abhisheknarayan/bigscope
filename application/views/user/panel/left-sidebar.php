<style type="text/css">
    .sidebar-menu>li>a {
        padding: 13px 5px 13px 15px;
        display: block;
        border-left: 3px solid transparent;
        color: #343a40;
        font-size: 14px;
    }
    
    .sidebar-menu .sidebar-submenu>li>a {
        padding: 5px 5px 5px 15px;
        display: block;
        font-size: 14px;
        color: #343a40;
    }
</style>
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true" class=" bounceInDown">
    <div class="brand-logo">
        <a href="<?= base_url('user/dashboard')?>">
       <img src="<?= base_url('') ?>resource/front/images/logo2.png" class="logo-icon" alt="logo icon">

     </a>
    </div>
    <ul class="sidebar-menu do-nicescrol">

        <li>
            <a href="<?=base_url('user/dashboard')?>" class="waves-effect">
                <i class="icon-home"></i><span>Dashboard</span>
                <!-- <i class="fa fa-angle-left pull-right"></i> -->
            </a>
        </li>
        <li>
            <a href="<?=base_url('user/packages')?>" class="waves-effect">
                <i class="fa fa-tasks "></i><span>Packages</span>
                <!-- <i class="fa fa-angle-left pull-right"></i> -->
            </a>
        </li>
        <li>
            <a href="<?=base_url('user/purchase_history')?>" class="waves-effect">
                <i class="fa fa-tasks "></i><span>Purchase History</span>
                <!-- <i class="fa fa-angle-left pull-right"></i> -->
            </a>
        </li>

        <li>
            <a href="#" class="waves-effect">
                <i class="fa fa-sitemap"></i><span>Network</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <!-- <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li> -->
                <!--<li><a href="<?=base_url('user/direct_view')?>"><i class="fa fa-circle-o"></i>Directs</a></li>-->

                <li>
                    <a href="<?=base_url('binary/tree')?>">
                        <i class="fa fa-circle-o"></i><span>Tree View</span>
                    </a>
                </li>
                
                 <li>
                    <a href="<?=base_url('user/direct_view')?>">
                        <i class="fa fa-circle-o"></i><span>My Direct</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#" class="waves-effect">
                <i class="fa fa-usd"></i> <span>Income</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li> -->
                <li><a href="<?=base_url('user/daily_return')?>"><i class="fa fa-circle-o"></i>Daily Return</a></li>
                <li><a href="<?=base_url('user/direct_income')?>"><i class="fa fa-circle-o"></i>Direct Income</a></li>
                <li><a href="<?=base_url('user/binary_income')?>"><i class="fa fa-circle-o"></i>Binary Income</a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="waves-effect">
                <i class="fa fa-trophy"></i><span>BCO Wallet</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="<?=base_url('token/buy_token')?>"><i class="fa fa-circle-o"></i>Buy BCO</a></li>
                <li><a href="<?=base_url('token/my_tokens')?>"><i class="fa fa-circle-o"></i>My BCO List </a></li>
                <li><a href="<?=base_url('token/token_value_list')?>"><i class="fa fa-circle-o"></i>BCO History</a></li>
                <li><a href="<?=base_url('token/withdraw_token')?>"><i class="fa fa-circle-o"></i>Withdraw BCO</a></li>
                <!-- <li><a href="<?=base_url('user/tree_view')?>"><i class="fa fa-circle-o"></i>Daily lucky No list</a></li></li> -->
                <li></li>
            </ul>
            <li>
                <a href="#" class="waves-effect">
                    <i class="fa fa-trophy"></i><span>Activation Pin</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="<?=base_url('user/generate_new_activation')?>"><i class="fa fa-circle-o"></i>Genarate pin</a></li>
                    <li><a href="<?=base_url('user/activation_code_history')?>"><i class="fa fa-circle-o"></i>No of pins Generated</a></li>
                    <li><a href="<?=base_url('user/used_pin')?>"><i class="fa fa-circle-o"></i>Total pins used</a></li>
                    <li><a href="<?=base_url('user/unused_pin')?>"><i class="fa fa-circle-o"></i>Total pins Unused</a></li>
                </ul>
            </li>

            <li>

                <a href="#" class="waves-effect">
                    <i class="fa fa-tasks "></i><span>Withdraw</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="<?=base_url('user/withdraw_btc')?>"><i class="fa fa-circle-o"></i>Withdraw By BTC</a></li>
                    <!--<li><a href="<?=base_url('user/withdraw')?>"><i class="fa fa-circle-o"></i>Withdraw Simple</a></li>-->
                    <li><a href="<?=base_url('user/withdraw_history')?>"><i class="fa fa-circle-o"></i>Withdraw History</a></li>

                </ul>
            </li>

            <!-- <ul class="sidebar-submenu"> 
          <li><a href="<?=base_url('user/tree_view')?>"><i class="fa fa-circle-o"></i>My Tickets</a></li>
          <li><a href="<?=base_url('user/tree_view')?>"><i class="fa fa-circle-o"></i>Ticket History</a></li>
          <li><a href="<?=base_url('user/tree_view')?>"><i class="fa fa-circle-o"></i>Winners List</a></li>

        </ul>  -->
        </li>

        <li>
            <a href="<?=base_url('user/referal')?>" class="waves-effect">
                <i class="fa fa-reply"></i><span>Referral Link</span>
                <i class="fa fa-angle-left pull-right"></i></a>
        </li>

        <!-- <li>
            <a href="<?=base_url('user/tree_view')?>" class="waves-effect" >
            <i class="icon-map"></i><span>Business plan</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li> -->

        <li>
            <a href="#" class="waves-effect">
                <i class="fa fa-bell"></i> <span>Support</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="<?=base_url('user/support_ticket')?>"><i class="fa fa-circle-o"></i> Raise Support Ticket</a></li>
                <li><a href="<?=base_url('user/support_history')?>"><i class="fa fa-circle-o"></i> Support Ticket History</a></li>
            </ul>
        </li>
    </ul>
</div>