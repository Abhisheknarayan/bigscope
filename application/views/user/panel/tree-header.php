<header class="top-nav">
<div class="top-nav-inner">
<div class="nav-header">
<style>
	.navbar-toggle {
	position: relative;
    float: right;
    padding: 10px;
    margin-top: 8px;
    margin-right: 15px;
    margin-bottom: 8px;
    background-color: #e9cf58;
    background-image: none;
    border: 1px solid #c39736;
    border-radius: 4px;
    }
    </style>
						<button type="button" class="navbar-toggle pull-left sidebar-toggle" id="sidebarToggleSM">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
						<ul class="nav-notification pull-right">
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg"></i></a>
								<span class="badge badge-danger bounceIn">1</span>
								<ul class="dropdown-menu dropdown-sm pull-right user-dropdown">
									<li class="user-avatar">
										
								<div class="user-content">
								<h5 class="no-m-bottom"></h5>
								<div class="m-top-xs">
								<a href="https://www.ebitmoney.com/user/changePassword.php"><i class="fa fa-user"></i> Change Password</a>
                                <a href="updaterecievingaddress.php"><i class="fa fa-user"></i> Update Receiving  Address</a>
                                <a href="logout.php"><i class="fa fa-power-off fa-lg"></i> Sign out</a>
											</div>
										</div>
									</li>	  
									
									<li class="divider"></li>
									<li>
										<a href="#">Setting</a>
									</li>			  	  
								</ul>
							</li>
						</ul>
						
						<a href="index.php" class="brand">
						<img src="../images/logo/ebitlogo.png" alt="" > </a>
					</div>
					<div class="nav-container">
						<button type="button" class="navbar-toggle pull-left sidebar-toggle" id="sidebarToggleLG">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
						<div class="pull-right m-right-sm">

							<div class="user-block hidden-xs">
								<b style="color: #eab213a8">Welcome :</b> <strong style="color: #eab213a8"><?php echo $sql['user_name'];?> <?php echo $sql['last_name'];?></strong>
								<a href="#" id="userToggle" data-toggle="dropdown">
									<img src="images/profile/user.jpg" alt="" class="img-circle inline-block user-profile-pic">
									<div class="user-detail inline-block">
										<span></b> <i class=" icofont icofont-simple-down"></i></span>
										<i class="fa fa-angle-down"></i>
									</div>
								</a>
                    <div class="panel border dropdown-menu user-panel">
                        <div class="panel-body paddingTB-sm">
                            <ul>
                                <li><a href="changePassword.php"><i class="fa fa-user"></i> Change Password</a></li>
                                <li><a href="updateRecievingAddress.php"><i class="fa fa-user"></i> Update Receiving  Address</a></li>
                                <li><a href="logout.php"><i class="fa fa-power-off fa-lg"></i> Sign out</a></li>
                            </ul>
                        </div>
                    </div>
							</div>
							
						</div>
					</div>
				</div><!-- ./top-nav-inner -->	
			</header>