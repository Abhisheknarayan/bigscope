
			<aside class="sidebar-menu fixed <?php echo $_SESSION['point']; ?>" >
				<div class="sidebar-inner scrollable-sidebar">
                <div class="user-widget sidebar">
					<div class="user-widget-body bg-grey">
						<!--<img src="images/profile/profile1.jpg" alt="">-->
						<div class="user-detail">
							<div class="m-top-sm"></div>
								<div class="m-top-sm">
									<span class="userid"></span>
								</div>
						
							<div class="m-top-sm"></div>
						</div>
					</div>
					<div class="user-widget-statatistic">
						<ul class="clearfix">
							<li class="border-red">
								<div class="text-muted text-upper font-sm"><b> user id:</b> <?php echo  $_SESSION['user_id'];?></div>
								
							</li>
						</ul>
					</div>
				</div>
				<div class="main-menu">
						<ul class="accordion">
							<li class="menu-header">
								Main Menu
							</li>
							<li class="bg-palette1 active">
								<a href="index.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-home fa-lg"></i></span>
										<span class="text m-left-sm">Dashboard</span>
									</span>
									
								</a>
							</li>
							<li class="bg-palette1 active">
								<a href="kyc_update.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="fa fa-bank"></i></span>
										<span class="text m-left-sm">Update KYC</span>
									</span>
									
								</a>
							</li>
                            <li class="bg-palette3">
    							<a href="purchasePackageAgain.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
    									<span class="text m-left-sm">Buy Package</span>
    								</span>
    								
    							</a>
							</li>
							<li class="bg-palette3">
    							<a href="refund.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="fa fa-money" aria-hidden="true"></i></span>
    									<span class="text m-left-sm">Refund Status</span>
    								</span>
    								
    							</a>
							</li>
							
							
                        	<li class="bg-palette3">
    							<a href="purchaseHistory.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
    									<span class="text m-left-sm">Purchase History</span>
    								</span>
    								
    							</a>
							</li>
							
							<li class="bg-palette3">
    							<a href="lock_plan.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
    									<span class="text m-left-sm">Lock Plan History</span>
    								</span>
    								
    							</a>
							</li>
							
								<li class="bg-palette3">
    							<a href="directuserlist.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
    									<span class="text m-left-sm">Direct User List</span>
    								</span>
    								
    							</a>
							</li>
							
							<li class="bg-palette2">
								<a href="dailyReturn.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="fa fa-usd"></i></span>
										<span class="text m-left-sm">Daily Return </span>
									</span>
									
								</a>
							</li>

							<li class="bg-palette2">
								<a href="directIncome.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="fa fa-usd"></i></span>
										<span class="text m-left-sm">Direct Income </span>
									</span>
									
								</a>
							</li>
							
							<li class="bg-palette2">
    							<a href="binary_income_list.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
    									<span class="text m-left-sm">Binary Income </span>
    								</span>
    								
    							</a>
							</li>
                            
                            	<li class="bg-palette2">
    							<a href="binarytree.php">
    								<span class="menu-content block">
    									<span class="menu-icon"><i class="block fa fa-users fa-lg"></i></span>
    									<span class="text m-left-sm">Network Binary Tree </span>
    								</span>
    								
    							</a>
							</li>
							
						
						
							<li class="bg-palette3">
								<a href="withdraw.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
										<span class="text m-left-sm">Withdraw</span>
									</span>
									
								</a>
							</li>

							<li class="bg-palette3">
								<a href="withdrawStatus.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
										<span class="text m-left-sm">Withdraw Request Status</span>
									</span>
									
								</a>
							</li>
							
							<li class="bg-palette3">
								<a href="reinvestment.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-list fa-lg"></i></span>
										<span class="text m-left-sm">Reinvestment</span>
									</span>
									
								</a>
							</li>

							<li class="bg-palette3">
								<a href="sharedLink.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-external-link fa-lg" aria-hidden="true"></i></span>
										<span class="text m-left-sm">Shared link</span>
									</span>
								</a>
							</li>		<!-- <a href="https://www.idoc9.com/user/register.php?sponsor_id=<?php //echo $a;?>" style="margin-left:10px;"></a> -->
							
							<li class="bg-palette3">
								<a href="supportTicket.php">
									<span class="menu-content block">
										<span class="menu-icon"><i class="fa fa-support" aria-hidden="true"></i> </span>
										<span class="text m-left-sm">Support</span>
									</span>
									
								</a>
							</li>
							
						</ul>
					</div>	
                    
					<div class="sidebar-fix-bottom clearfix">
						
						<a href="logout.php" class="pull-right font-18"><i class="ion-log-out"></i></a>
					</div>
				</div><!-- sidebar-inner -->
			</aside>