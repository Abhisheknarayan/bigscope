<div id="wrapper" class="">

    <header class="topbar-nav">
        <nav class="navbar navbar-expand fixed-top  bounceInDown">

            <ul class="navbar-nav mr-auto align-items-center">
                <li class="nav-item">
                    <a class="nav-link toggle-menu" href="javascript:void();">
                        <i class="icon-menu menu-icon"></i>
                    </a>
                </li>
                <!-- <li class="nav-item"> -->
                <!-- <form class="search-bar"> -->
                <!-- <input type="text" class="form-control" placeholder="Enter keywords"> -->
                <!-- <a href="javascript:void();"><i class="icon-magnifier"></i></a> -->
                <!-- </form> -->
                <!-- </li> -->
            </ul>
            <marquee>
                <h3><?php echo $this->session->flashdata('user_registered'); ?></h3></marquee>
            <ul class="navbar-nav align-items-center right-nav-link">

                <li class="nav-item dropdown-lg">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                        <i class="icon-bell"></i><span class="badge badge-primary badge-up"></span></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                You have Notifications
                                <span class="badge badge-primary"></span>
                            </li>

                            <li class="list-group-item">
                                <a href="">
                                    <div class="media">
                                        <i class="icon-people fa-2x mr-3 text-info"></i>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title"></h6>
                                            <p class="msg-info">...</p>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li class="list-group-item"><a href="">See All Notifications</a></li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                        
                <?php 
                   $puchage_plane = $this->db->query("select * from users where id='".$this->session->userdata('logged_in')->id."' ");
                   $jprofi = $puchage_plane->result()[0]->profile_pic;
                   
                   if(!empty($jprofi) || $jprofi != null){
                       
                       $profil_pic = base_url('uploads/profile/').$jprofi;
                   }else{
                       $profil_pic = base_url('resource/front/uat_public/image/logo.png');
                   }
                   
                   
              
                
                ?> 
                
        <span class="user-profile"><img src="<?= $profil_pic?>" class="img-circle" alt="user avatar"></span>
      </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-item user-details">
                            <a href="javaScript:void();">
                                <div class="media">
                                    <div class="avatar"><img class="align-self-start mr-3" src="<?=base_url('resource/user/')?>images/0.png" alt="user avatar"></div>
                                    <div class="media-body">
                                        <h6 class="mt-2 user-title"></h6>
                                        <p class="user-subtitle"></p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item">
                            <a href=""></i>
                                <?= $this->session->userdata('logged_in')->first_name?>&nbsp
                                <?= $this->session->userdata('logged_in')->last_name ?>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item"><a href="<?=base_url('user/user_profile')?>"><i class="icon-envelope mr-2"></i> Profile</a></li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item"><a href="<?=base_url('home/logout')?>"><i class="icon-wallet mr-2"></i>  Sign out</a></li>
                        <li class="dropdown-divider"></li>

                    </ul>
                </li>
            </ul>
        </nav>
    </header>