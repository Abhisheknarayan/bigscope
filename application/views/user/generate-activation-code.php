
<body>
<!-- Start wrapper-->
 <div id="wrapper">
 

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

           <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Generate Activation Code</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Activation Code</div>
            <div class="card-body">
						 <div class="card-content p-2">
								
                        									
                        	<div class="form-group">
                            <form method="post" onsubmit="return validate()" action="<?= base_url('user/activation_code');?>">
                        	
                          <div class="position-relative has-icon-right">
                          <label for="timesheetinput1">Amount</label>
                          <input type="number" id="amount" class="form-control form-control-rounded" name="amount" value="<?= total_income($this->session->userdata('logged_in')->user_id)?>"   required="" disabled>
                          </div>
                          
                        	<div class="position-relative has-icon-right">
                        	<label for="timesheetinput1">Activation code</label>
                        	<input type="number" id="code" class="form-control form-control-rounded" name="code" placeholder="Enter the no to generate the actvation code" onsubmit="return validateForm()" required="">
                        	</div>
                        	<span class="error_msg amt_error"></span>
                        	</div>
                        	<button type="submit" name="withdraw" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light"onsubmit="return validateForm()">Generate code</button>
	
							</form>
						</div>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>
</div>
</div>
<script>
function validate(){
            //var amt=parseInt($(this).val());
            var amt=parseInt($("#amount").val());
            var amt1=parseInt($("#code").val());
          var avail_amt=parseInt($("#avail_amt").val());
          //alert(avail_amt);
          
          
          if(amt/20 >= amt1){
            $(".amt_error").html("Total no of pin you can generate");
            avail_amt=amt/20;
            $("#amount").focus();
            return true;
          }
          
          if(amt/20 < amt1){
              $(".amt_error").html("Exceed the account limit");
            $("#amount").focus();
            return false;
                  
          }         
        }
      </script> 


			 