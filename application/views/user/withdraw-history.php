<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Withdraw History</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Withdraw History</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>UserName</th>
                                                <th>Req Amt($)</th>
                                                <th>BTC Value</th>
                                                <th>Admin Chg</th>
                                                <th>Total Pay Amt</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>BTC Address</th>

                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php 
                                            $i=1;
                                            foreach ($withdrwal_history as $row){
                                                $total_pay_amt = ($row->btc_value * (100 - $row->admin_charge)/100) ;
                                                
                                            ?>
                                                 <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $row->user_id;?></td>
                                                   <td><?php echo $row->first_name;?></td>
                                                   <td ><?php echo $row->request_amount;?></td>
                                                   <td ><?php echo $row->btc_value;?></td>
                                                   <td ><?php echo $row->admin_btc_value;?></td>
                                                   <td ><?php echo $row->total_pay_amt;?></td>
                                                   <td>
                                                        <?php 
                                                        if(!empty($row->message)){
                                                            $msg= $row->message;
                                                        }else{
                                                            $msg= '';
                                                        }
                                                        
                                                        if ($row->status=='1') {
                                                           echo '<span class="badge badge-success shadow-success" title="'.$msg.'">Accept</span>';
                                                        }elseif($row->status=='0'){
                                                           echo '<span class="badge badge-danger shadow-danger" title="'.$msg.'">Decline</span>';  
                                                        }elseif($row->status=='2'){
                                                            echo '<span class="badge badge-danger shadow-danger" >New Request</span>';  
                                                        }else{
                                                            echo '<span class="badge badge-danger shadow-danger">Cancel</span>';
                                                        }
                                                        
                                                       ?>
                                                   </td>
                                                   
                                                   
                                                   
                                                   <td ><?php echo $row->request_date;?></td> 
                                                   <td><?php echo $row->btc_address;?></td>
                                                  </tr>
                                            <?php 
                                            
                                            $i++;
                                            }; 
                                            ?>
                                        </tbody>                                        

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>