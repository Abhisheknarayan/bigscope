<?php 

 /*$package = CI()->db->query("select * from purchase_package where user_id='".$this->session->userdata('logged_in')->user_id."' && status = 'success' ");
 
 foreach($package->result() as $row){
     
    packageValidorNot($row->id);
     //echo "<pre>"; print_r($row->id); 
     
 }
 
 
 
 die();*/

?>



<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">User Profile</h4>
                        <!-- <ol class="breadcrumb"> -->
                        <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                        <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Pages</a></li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page">User Profile</li> -->
                        <!-- </ol> -->
                    </div>
                </div>
                <!-- End Breadcrumb-->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="profile-card-3">
                            <div class="card">
                                <form id="myform" enctype="multipart/form-data">
                                    <div class="user-fullimage">
                                        <?php 
                                        if(!empty($user_data[0]->profile_pic) || $user_data[0]->profile_pic !== '' ){
                                            
                                            $profil_pic = base_url('uploads/profile/').$user_data[0]->profile_pic;
                                            
                                        }else{
                                            $profil_pic = base_url('resource/front/uat_public/image/logo.png');
                                        }
                                        ?>
                                        <img src="<?= $profil_pic?>" alt="user avatar" class="img-circle mCS_img_loaded img-responsive" id="show_img">
                                        <div class="image-upload">
                                            <label for="file-input">
                                                <i class="fa fa-camera"></i>
                                            </label>

                                            <input id="file-input" type="file" name="profile" />
                                        </div>
                                        <div class="details">
                                            <h5 class="mb-1 text-white ml-3"><?php echo $this->session->userdata('logged_in')->user_id; ?></h5>
                                            <h6 class="text-white ml-3"><?php echo $this->session->userdata('logged_in')->first_name; ?></h6>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified ">
                                    <li class="nav-item">
                                        <a href="javascript:void();" data-target="#profile" data-toggle="pill" class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">Profile</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void();" data-target="#messages" data-toggle="pill" class="nav-link"><i class="icon-envelope-open"></i> <span class="hidden-xs">Change Password</span></a>
                                    </li>

                                </ul>
                                <div class="tab-content p-3">
                                    <div class="tab-pane active" id="profile">

                                        <div class="row">
                                            <div class="col-lg-10 ">
                                                <div class="card">
                                                    
                                                  <div class="text-danger"><strong>
                                                      
                                                  <?php
                                                    if ( $this->session->flashdata('update_profile')) {
                                                        echo $this->session->flashdata('update_profile'); 
                                                
                                                    }else{
                                                        echo $this->session->flashdata('failed');  
                                                    }
                                                  ?>                  
                                                      
                                                  </strong></div>                                                    
                                                    
                                                    
                                                    
                                                    <div class="card-body">
                                                        <div class="card-title">Edit Profile</div>
                                                        <hr>
                                                       <?php echo form_open_multipart('user/user_profile_update');?>
                                                            <input type="hidden" id="id" class="form-control" name="id" value="<?php echo $user_data[0]->id ?>">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">FirstName</label>
                                                                <input type="text" id="timesheetinput1" class="form-control" name="fname" value="<?php echo $user_data[0]->first_name ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">LastName</label>
                                                                <input type="text" id="timesheetinput1" class="form-control" name="lname" value="<?php echo $user_data[0]->last_name ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="input-2">Email</label>
                                                                <input type="email" id="input-2" class="form-control" name="email" value="<?php echo $user_data[0]->user_email ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="input-3">Mobile Number</label>
                                                                <input type="text" id="input-3" class="form-control" name="mobile" value="<?php echo $user_data[0]->user_mobile ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="input-4">Bitcoin Wallet Address</label>
                                                                <input type="text" id="input-3" class="form-control" name="bitcoin" value="<?php echo $user_data[0]->bitcoin_wallet_address ?>" pattern="[a-zA-Z0-9]+" maxlength="34" >
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" name="edit_profile" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Update</button>
                                                            </div>
                                                       
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="tab-pane" id="messages">
                                        <div class="alert alert-info alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        </div>
                                        <div class="col-lg-10 ">
                                            <div class="card">
                                                
                                                                                                   
                                                <div class="card-body">
                                                <div class="text-danger"><strong>
                                                      
                                                  <?php
                                                    if ( $this->session->flashdata('change_password')) {
                                                        echo $this->session->flashdata('change_password'); 
                                                
                                                    }else{
                                                        echo $this->session->flashdata('failed');  
                                                    }
                                                  ?>                  
                                                      
                                                 </strong></div> 
                                                    <div class="card-title" >Change Password</div>
                                                    <hr>
                                                    <?php echo form_open_multipart('user/user_change_password');?>
                                                        <input type="hidden" id="id" class="form-control" name="id" value="<?php echo $user_data[0]->id ?>">
                                                        <div class="form-group">
                                                            <label for="input-1">Old Password</label>
                                                            <input type="password" id="timesheetinput1" class="form-control" name="oldpass">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="input-2">New Password</label>
                                                            <input type="password" id="timesheetinput2" class="form-control" name="newpass">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="input-3">Confirm Password</label>
                                                            <input type="password" id="timesheetinput3" class="form-control" name="confirmpass">
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" name="change_pass" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Change</button>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- End container-fluid-->
        </div>

        <!--footer-->

        <script type="text/javascript">
            $("#file-input").change(function() {
                
                if (this.files && this.files[0]) {
                    
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#show_img').attr('src', e.target.result);
                    }
                        // view uploaded file.
                    reader.readAsDataURL(this.files[0]);

                    $.ajax({
                        url: "<?= base_url('user/update_profile_img')?>",
                        type: "POST",
                        data: new FormData($("#myform")[0]),
                        contentType: false,
                        cache: false,
                        processData: false,

                        success: function(data) {
                            if (data == '0' || data == "") {
                                alert("invalid");
                            } else {
                                history.go();
                            }
                        }

                    });
                }

            });
        </script>