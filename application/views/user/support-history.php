<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Support Ticket History</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>
                    <!-- <div class="col-sm-3 pull-right text-right">
          <a href="add-locking-plan.php" class="btn btn-primary shadow-primary px-2" name="add">  Add New ROI Plan</a>
      </div> -->
                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Support Ticket History</div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Action</th>
                                                <th>User Id</th>
                                                <th>Message</th>
                                                <th>Subject</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            <?php 
                                            $i = 1;
                                            foreach ($ticket->result() as $row) :
                                              
                                            ?>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td class="center">
                                                    <?php echo $i;?>
                                                </td>
                                                <td class="menu-action">
                                                    <a href="<?=base_url('user/view_support_ticket/').$row->id?>" data-original-title="View" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-eye"></i> </a>
                                                </td>
                                                <td class="center">
                                                    <?php echo $row->user_id ;?>
                                                </td>
                                                <td class="center">
                                                    <?php echo $row->message ;?>
                                                </td>
                                                <td class="center">
                                                    <?php echo $row->subject ;?>
                                                </td>
                                                <td class="center">
                                                    
                                                 <?php 
                                                    if ($row->status=='1') {
                                                        echo '<span class="badge badge-danger shadow-danger">open</span>';  
                                                    }else{
                                                        echo '<span class="badge badge-success shadow-success">close</span>';
                                                       
                                                    }
                                                    
                                                 ?>                         
                                                </td>
                                                <td class="center">
                                                    <?php echo date('Y-m-d', strtotime($row->date)); ?>
                                                </td>

                                            </tr>

                                            <?php 
                                            $i++;
                                            endforeach ;
                                            ?>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>