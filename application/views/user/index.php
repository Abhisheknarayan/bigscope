<?php 

/*echo "<pre>";
print_r(tokentCurrentValue()); 
die();
*/
//print_r($records);
?>

<style>
    .circle-wrapper {
        position: relative;
        width: 170px;
        top: -40px;
        left: -48px;
        height: 142px;
        padding: 110px 0px 0px 0px;
    }
    
    .circle-zero:before {
        content: "";
        background: rgba(0, 0, 0, 0.07);
        position: absolute;
        width: 168px;
        height: 2px;
        left: -60%;
        top: 50%;
        transform: rotate(45deg);
        z-index: -2;
    }
    
    .circle-zero {
        background: #1c1e21;
        border-radius: 50%;
        box-shadow: 0px 3px 7px 0.7px rgba(0, 0, 0, .5);
        box-sizing: content-box;
        height: 68px;
        width: 68px;
        position: absolute;
        top: 35%;
        left: 35%;
        text-align: center;
    }
    
    .circle-one:before {
        content: "";
        background: rgba(21, 167, 235, .7);
        border-radius: 50%;
        position: absolute;
        width: 4px;
        height: 4px;
        right: -56%;
        top: 45%;
        animation: circle-move 10s ease infinite;
    }
    
    .circle-one {
        border: 7px solid;
        border-color: rgb(28, 30, 33) rgb(132, 171, 52) transparent transparent;
        border-radius: 50%;
        box-sizing: content-box;
        height: 90px;
        width: 92px;
        position: absolute;
        top: 27%;
        left: 25%;
        z-index: -1;
        animation: circle-move 10s ease infinite;
    }
    
    .circle-two {
        background: rgba(0, 0, 0, .017);
        border: 15px solid;
        border-color: rgb(243, 162, 77) transparent transparent;
        border-radius: 50%;
        box-shadow: 15px 0 25px -20px rgba(0, 0, 0, 0.65);
        box-sizing: content-box;
        height: 90px;
        width: 90px;
        position: absolute;
        top: 25%;
        left: 21%;
        z-index: -2;
        animation: circle-move 14s ease infinite;
    }
    
    .circle-three {
        background: rgba(0, 0, 0, .012);
        border: 2px solid;
        border-color: #CE93D8 transparent;
        border-radius: 50%;
        box-sizing: content-box;
        height: 168px;
        width: 168px;
        position: absolute;
        z-index: -3;
        top: 15%;
        left: 15%;
        animation: circle-move 15s ease infinite;
    }
    
    .circle-shadow {
        background: rgba(0, 0, 0, 0.2);
        border-radius: 50%;
        filter: blur(2px);
        height: 20px;
        width: 160px;
        position: absolute;
        top: 100%;
        left: 18%;
    }
    
    @keyframes circle-move {
        0% {
            transform: rotate(0deg);
        }
        70% {
            transform: rotate(180deg);
        }
        100% {
            transform: rotate(0deg);
        }
    }
    
    .circle-zero h1 {
        position: relative;
        top: 0px;
        font-size: 22px;
        color: #ffffff;
    }
    
    .circle-zero p {
        position: relative;
        top: -25px;
        font-size: 14px;
        color: white;
    }
    
    .min-height-add:hover .add-animatio_a {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_b {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_c {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio_d {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .add-animatio {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .min-height-add:hover .gradient-bloody {
        position: relative;
        top: -50px;
        left: 40px;
    }
    
    .extra_wrap .card-img-top {
        width: 20%;
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px);
        margin: 0 auto;
        padding: 10px 0px 0px 0px;
    }
    
    .Binary_bg {
        background-image: url(<?=base_url('resource/front/')?>images/gallery/12.jpg);
    }
    
    .Binary_text {
        padding: 0px 10px 20px 10px;
        text-align: center;
        font-size: 26px;
        /* float: left; */
        /* font-family: cursive; */
        /* display: inline-block; */
        color: #f77e03;
    }
    
    .sparkley {
        background: lighten( $buttonBackground, 10%);
        display: inline-block;
        border: none;
        padding: 16px 36px;
        font-weight: normal;
        border-bottom: 11px solid #87ad37;
        background: #151515;
        color: white;
        margin-bottom: 21px;
        margin: 0 auto;
        margin-bottom: 10px;
        border-radius: 3px;
        transition: all 0.25s ease;
        box-shadow: 0 38px 32px -23px black;
        &:hover {
            background: $buttonBackground;
            color: transparentize( $buttonColor, 0.8);
        }
    }
    
    .prize3 {
        margin: 56px 0px 0px 0px;
    }
    
    header a,
    a:visited {
        text-decoration: none;
        color: #fcfcfc;
    }
    
    @keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-webkit-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-moz-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @-ms-keyframes move-twink-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: -10000px 5000px;
        }
    }
    
    @keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-webkit-keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-moz-keyframes move-clouds-back {
        from {
            background-position: 0 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    @-ms-keyframes move-clouds-back {
        from {
            background-position: 0;
        }
        to {
            background-position: 10000px 0;
        }
    }
    
    .stars,
    .twinkling,
    .clouds {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        display: block;
    }
    
    .stars {
        background: #000 url(http://www.script-tutorials.com/demos/360/images/stars.png) repeat top center;
        z-index: 0;
    }
    
    .twinkling {
        z-index: 1;
        -moz-animation: move-twink-back 200s linear infinite;
        -ms-animation: move-twink-back 200s linear infinite;
        -o-animation: move-twink-back 200s linear infinite;
        -webkit-animation: move-twink-back 200s linear infinite;
        animation: move-twink-back 200s linear infinite;
    }
    
    .clouds {
        background: transparent url(http://www.script-tutorials.com/demos/360/images/clouds3.png) repeat top center;
        z-index: 3;
        -moz-animation: move-clouds-back 200s linear infinite;
        -ms-animation: move-clouds-back 200s linear infinite;
        -o-animation: move-clouds-back 200s linear infinite;
        -webkit-animation: move-clouds-back 200s linear infinite;
        animation: move-clouds-back 200s linear infinite;
    }
    
    .text_highlightani {
        font-size: 22px;
        font-weight: bold;
    }
    
    .hight_light_text_two {
        font-size: 26px;
        font-size: 26px;
        font-weight: bolder;
        color: black;
        padding: 0px 4px;
        background: linear-gradient(to right, #87ad37 20%, #929292 40%, #f5881f 60%, #d89328 80%);
        background-size: 200% auto;
        background-clip: text;
        text-fill-color: transparent;
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        animation: shine 1s linear infinite;
    }
    
    @keyframes shine {
        to {
            background-position: 200% center;
        }
    }
    
    #canvas {
        position: absolute;
        z-index: 1;
    }
    
    .overflow {
        overflow: hidden;
    }
    
    .prize1 {
        margin: 44px 0px;
    }
    
    .demo {
        position: absolute;
        height: 100%;
        width: 100%;
    }
</style>

<body>
    <!-- Start wrapper-->
    <canvas id="canvas"></canvas>
    <div id="wrapper">
        <div class="clearfix"></div>
        <div class="content-wrapper" style="min-height: 840px;">

            <div class="container-fluid  addcard-bg">

                <div class="demo">
                </div>
                <div class="row mt-3">

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-info border-left-sm animated bounceInDown min-height-add add gradient-scooter ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">

                                        <h4 class=" text_white"> $ <?= total_daily_return($this->session->userdata('logged_in')->user_id) ?> </h4> 
                                        <span class="text_white highlight_text">Total Daily Return</span>
                                        
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                                        <i class="icon-pie-chart text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-success border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white"> $ <?= total_direct_income($this->session->userdata('logged_in')->user_id)?></h4>
                                        <span class="text_white highlight_text">Total Direct Income</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                                        <i class="icon-wallet text-white"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-warning border-left-sm animated bounceInDown min-height-add  gradient-blooker ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ <?= total_binary_income($this->session->userdata('logged_in')->user_id)?></h4>
                                        <span class="text_white highlight_text">Total Binary Income</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card border-danger border-left-sm animated bounceInDown min-height-add gradient-bloody">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ <?= total_income($this->session->userdata('logged_in')->user_id)?></h4>
                                        <span class="text_white highlight_text">Total Income</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                                        <i class="fa fa-users text-white"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card border-success border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body  bg_gra_a ">
                                <div class="media">
                                    <div class="media-body text-left ">

                                        <h4 class="text_white"> <?= my_direct_users($logged_in->user_id) ?></h4>
                                        <span class="text_white highlight_text">My Direct  Referrals</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle add-animatio_a ">
                                        <i class="icon-user text-white"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card border-danger border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body    bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class="text_white">$ <?= my_package_amount($logged_in->user_id) ?></h4>
                                        <span class="text_white highlight_text">My Package Amount</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card border-warning border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">

                                        <h4 class="text-warning"></h4>
                                        <span class="text_white highlight_text "><?= COIN ?> -  </span><span class=" text_white"><?= availableToken()?></span></br>
                                        <span class="text_white highlight_text ">Current Value -   </span><span class="text_white">$<?= token_price() ?></span> </br>
                                        <h><span class= "text_white highlight_text ">Future Value -  </span> <span class="text_white">$<?=tokentFutureValue()?></span></h>
                                        
                                        </div>
                                        
                                        
                                   <!-- <div class="align-self-center w-circle-icon rounded-circle  add-animatio ">
                                        <i class="icon-user text-white"></i></div> -->
                                        
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card border-danger border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body    bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class="text_white">$ 0</h4>
                                        <span class="text_white highlight_text">Team Revenue Sharing Income</span>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle add-animatio_f">
                                        <i class="icon-wallet text-white"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-info border-left-sm animated bounceInDown min-height-add gradient-scooter ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ 0</h4>
                                        <b><span class="text_black highlight_text">Total Daily Cash-in by Gateway</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-success border-left-sm animated bounceInDown min-height-add  gradient-blooker ">
                            <div class="card-body bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ 0</h4>
                                        <b><span class="text_black highlight_text">Total Weekly Binary Income</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-success border-left-sm animated bounceInDown min-height-add gradient-quepal ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ 0</h4>
                                        <b><span class="text_black highlight_text">Total Daily Cash-in Income</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-warning border-left-sm animated bounceInDown min-height-add  gradient-blooker ">
                            <div class="card-body bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ </h4>
                                        <b><span class="text_black highlight_text">Available Total Income</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->
                <!--<div class="row mt-3">-->

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-info border-left-sm animated bounceInDown min-height-add gradient-scooter ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white">$ <?= total_withdraw($logged_in->user_id)->amount ?></h4>
                                        <b><span class="text_black highlight_text">Total Accept Withdraw</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-success border-left-sm animated bounceInDown min-height-add gradient-quepal">
                            <div class="card-body bg_gra_a">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h6 class=" text_white">Value in $ <?= total_withdraw_request($this->session->userdata('logged_in')->user_id)->amount?></h6>
                                        <h6 class=" text_white">Value in BTC <?= total_withdraw_request($this->session->userdata('logged_in')->user_id)->btcAmount?></h6>
                                        <b><span class="text_black highlight_text">Total Request Withdraw</span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3 ">
                        <div class="card border-warning border-left-sm animated bounceInDown min-height-add  gradient-blooker ">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body text-left">
                                        <h4 class=" text_white"> <?= total_purchase_coin($this->session->userdata('logged_in')->user_id)->coin ?></h4>
                                        <b><span class="text_black highlight_text">My BCO </span></b>
                                    </div>
                                    <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                                        <i class="fa fa-dollar text-white spinner"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                <!--</div>-->

                <div class="row mt-3">

                    <div class="col-12 col-lg-12 col-xl-12 col-sm-12">

                        <div class="row " style="display:none;">

                            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">

                                <div class="card" style="">
                                    <div class="card-header">Current Week Bank</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="card border-success border-left-sm animated bounceInDown ">

                                                    <div class="card-body">
                                                        <div class="row">

                                                            <div class="col-lg-12 col-xl-12">
                                                                <p class="Binary_text">Binary Countdown Timer</p>
                                                            </div>

                                                            <div class=" col-lg-12 col-xl-12">

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="days"></h1>
                                                                                <p>Days</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="hours"></h1>
                                                                                <p>Hours</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="minutes"></h1>
                                                                                <p>Min</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="seconds"></h1>
                                                                                <p>Sec</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-lg-12 col-xl-12 col-sm-12 overflow">
                                                <canvas id="canvas"></canvas>
                                                <div class="card" style="">

                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-xl-12">
                                                                <div class="card border-success border-left-sm animated bounceInDown ">

                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-xl-12 col-sm-12">
                                                                                <div class="media">

                                                                                </div>

                                                                                <div class="media">
                                                                                    <div class="media-body text-left">
                                                                                        <span class="blink_text"></span>

                                                                                        <h4 class=" blink card-text fo_si_40" id="total_amt"></h4>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-12 col-xl-12 col-sm-12">
                                                                                    <div class="media">
                                                                                        <div class="media-body text-center">
                                                                                            <span class="sparkley last">Weekly Win Prize</span>

                                                                                            <h4 class=" blink card-text fo_si_40 " id="total_amt"></h4>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-12 col-xl-12 col-sm-12">
                                                                                    <div class="media">
                                                                                        <div class="media-body text-center">
                                                                                            <img src="<?=base_url('resource/user/')?>images/prize1.gif" class="prize1">

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-6 col-sm-12">

                                <div class="card" style="">
                                    <div class="card-header">Daily Cash-in Game</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="card border-success border-left-sm animated bounceInDown ">

                                                    <div class="card-body">
                                                        <div class="row">

                                                            <div class="col-lg-12 col-xl-12">
                                                                <p class="Binary_text">Daily Cash-in Game Countdown Timer</p>
                                                            </div>

                                                            <div class=" col-lg-12 col-xl-12">
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="daysd">NaN</h1>
                                                                                <p>Days</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="hoursd">NaN</h1>
                                                                                <p>Hours</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="minutesd">NaN</h1>
                                                                                <p>Min</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="circle-wrapper">
                                                                            <div class="circle-zero">
                                                                                <h1 id="secondsd">NaN</h1>
                                                                                <p>Sec</p>
                                                                            </div>
                                                                            <div class="circle-one"></div>
                                                                            <div class="circle-two"></div>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-12 col-xl-12 col-sm-12 overflow">

                                                <div class="card" style="">

                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-xl-12">
                                                                <div class="card border-success border-left-sm animated bounceInDown ">

                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-xl-6 col-sm-6">
                                                                                <div class="media">
                                                                                    <div class="media-body text-left">
                                                                                        <!--      <span class="blink_text hight_light_text_two">Daily Turnover</span>-->

                                                                                        <!--<h4 class=" blink card-text fo_si_40 " id="total_amt">$-->

                                                                                    </div>
                                                                                    <div class="col-lg-12 col-xl-12 col-sm-12">
                                                                                        <div class="media">
                                                                                            <div class="media-body text-left">
                                                                                                <span class="blink_text hight_light_text_two">Daily Income</span>
                                                                                                <h4 class=" blink card-text fo_si_40" id="total_amt">$</h4>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-lg-6 col-xl-6 col-sm-6" style="text-align:center;">
                                                                                    <div>

                                                                                        <div>
                                                                                            <div class=" center">
                                                                                                <div class="ripple1"></div>
                                                                                                <div class="ripple2"></div>
                                                                                                <div class="ripple3"></div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="media sparkley last">
                                                                                            <div class="media-body text-left Winning">
                                                                                                <div class="stars"></div>
                                                                                                <div class="twinkling"></div>

                                                                                                <span class="blink_text "> <p class="">Winning Lucky No</p>   <h6 class=" blink card-text fo_si_40 text-info"></h6></span>
                                                                                                <h4 class=" blink card-text fo_si_40 " id="total_amt"></h4

                                          >

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-lg-12 col-xl-12 col-sm-12 text-center">
                                                                                    <img src="<?=base_url('resource/user/')?>images/prize3.gif" class="prize3">
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <!--<h4 class="modal-title">Modal Header</h4> -->
                                    </div>
                                    

                                    <!-- Modal body -->
                                    <div class="modal-body">

                                        <section id="pricing1" class="pricing pricing-1 bg-overlay bg-overlay-dark2 pt-110 pb-0 bg-section bg_image top-pad">
                                            <div class=""></div>
                

                                            <div class="container-fluid">
                                                <div class="row clearfix">
                                                    <div class="col-sm-12 col-md-12 col-lg-6 offset-md-3">
                                                        <div class="heading heading-1 text-center mb-40">
                                                            <!-- <p class="heading--subtitle">Get Started Now</p> -->
                                                            <h2 class="heading--title ">Join Bigscope</h2>
                                                            <!-- <p class="heading--desc heading--light mb-0">Get started now with us to earn every day and forever in your business. We accept Investment from all over the world.</p> -->
                                                        </div>
                                                    </div>
                                                    <!-- .col-md-8 end -->
                                                </div>
                                                <!-- .row end -->
                                            </div>
                                            <div class="container pt-40">
                                                <div class="row">

                                                    <?php
                                                        foreach($package as $row){
                                                    ?>
                                                    <!-- Pricing Packge #1============================================= -->
                                                    <div class="col-sm-12 col-md-3   col-lg-3 price-table">
                                                        <div class="pricing-panel gradient-scooter">
                                                            <!--  Pricing heading  -->
                                                            <div class="pricing--heading text--center">
                                                                <div class="pricing--icon">
                                                                    <img class="icon_size" src="<?= base_url('resource/front/images/logo2.png')?>">
                                                                </div>
                                                                <!--<h4>Starter Plan </h4>-->
                                                                <p><?= $row->name ?></p>
                                                                <div class="pricing--desc">
                                                                    <div class="start_style">
                                                                        <ul>
                                                                            <li>$<?= $row->amount ?></li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <a class="pop_up_button" target="_blank" href="<?= base_url("user/pay_now/$row->name/$row->amount/$row->percent/$row->id") ?>"> Join Now</a>
                                                            </div>
                                                            <!--  Pricing heading  -->
                                                        </div>
                                                    </div>
                                                    <?php } ?>
    
                                                </div>
                                                <!-- .row end -->
                                            </div>
                                            <!-- .container end -->
                                            <div class="section-divider"></div>
                                        </section>

                                        <!-- <div class="modal-footer">
                                            <i class="fa fa-times" data-dismiss="modal"></i>
                                        </div>-->

                                        <!--	 <div class="modal-footer">-->
                                        <!--  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                        <!--</div>-->

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="Binary_bg_two">
                            <div class="modal" id="dailycashid">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header btn-info">
                                            <h4 class="modal-title" style="margin: 0 auto; color: white;"> Daily Cash Game </h4>
                                            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                        </div>
                                        <!-- Modal body -->
                                        <!-- <form method="post" action="ajax1.php"> -->
                                        <div class="modal-body">
                                            <!-- <form method="post" action=""> -->
                                            <h4 class="text-info" style="text-align: center;">Select your a lucky number</h4>
                                            <input type="hidden" id="num_gui" value="" name="uid">

                                            <div class="Binary_bg_two">

                                                <div class="row">
                                                    <div class="col-12 col-lg-12 col-xl-12 col-sm-12 text-center">
                                                        <div id="chart"></div>
                                                    </div>
                                                    <div class="col-12 col-lg-12 col-xl-12 col-sm-12 text-center">
                                                        <input type="hidden" id="ques" name="ques">
                                                        <div id="question">
                                                            <h1></h1></div>
                                                        <div style="text-align: center;">
                                                            <h4 class="daily_rett"></h4>
                                                            <span class="daily_ret"></span>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <input type="button" id="btnGetValue" class="btn btn-danger" Value="BUY" />
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                            <!-- </form> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal hide fade" tabindex="-1" role="dialog" id="mynotimodal">
                            <div class="modal-dialog noti_modal">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 col-xl-12">
                                                <button type="button" class="close close_add spinner">&times;</button>
                                                <div class="prize_style">
                                                    <canvas id="canvas" class="prize_ani"></canvas>
                                                    <input type="hidden" id="win_jac" value="">
                                                    <div class="media">
                                                        <div class="media-body  prize_min_height text-center">
                                                            <h4 class="">Congratulations!!</h4>
                                                            <span class="win_text">You won a Binary Price</span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal hide fade" tabindex="-1" role="dialog" id="mynotimodal">
                            <div class="modal-dialog noti_modal">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 col-xl-12">
                                                <button type="button" class="close close_add spinner">&times;</button>
                                                <div class="prize_style">
                                                    <canvas id="canvas" class="prize_ani"></canvas>
                                                    <input type="hidden" id="win_jac" value="">
                                                    <div class="media">
                                                        <div class="media-body  prize_min_height text-center">
                                                            <h4 class="">Congratulations!!</h4>
                                                            <span class="win_text">You won a Binary Price</span>
                                                        </div>
                                                        <div class="align-self-center rounded-circle" style="width: 230px;">
                                                            <a href="javascript:;" onclick="openbox()">
				                                            <img src="<?=base_url('resource/user/')?>images/gift_shake.gif" class="card-img-absolute win_img img-responsive rel_img" style="margin-left: 0px;"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal hide fade" tabindex="-1" role="dialog" id="mymodal">
                            <div class="modal-dialog noti_modal">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close">&times;</button>
                                        <img src="<?=base_url('resource/user/')?>images/popup.gif">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                      
                      
                </div>  
            </div>         
        </div> 
            
            
            
    <script>
    
    
     $(window).load(function(){    
         //var rec = 1;//
         var rec = '<?= $records;?>';
         
         if(rec > 0){
            $('#myModal').modal('hide');
         }
         else{
            $('#myModal').modal('show'); 
         }
     }); 
           
    </script>
            

 <!--footer-->