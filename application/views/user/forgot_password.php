<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bigscope.co</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/plugins.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/theme-styles.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/blocks.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/widgets.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/addstyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resource/front/uat_public/') ?>css/login-style.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700i,900," rel="stylesheet">

    <style>
    
        .creat-account {
            font-size: 18px;
            text-transform: capitalize;
            font-weight: 600;
            /* background: linear-gradient(to right, #eeb828 10%, #ff572236 100%); */
            
            color: #ffffff;
            margin-top: 10px;
            display: block;
            padding: 0px 40px;
            color: #dfaa26;
        }    
    
        .hr {
            height: 2px;
            margin: 10px 0 10px 0;
            background: rgba(255, 255, 255, .2);
        }  
        
        .foot-lnk {
            text-align: center;
        }
        .white {
            color: white;
        }    
        .h2_style {
            font-size: 20px;
            margin: 0px 0px 20px 0px;
            color: #f3810f;
            text-align: center;
        }
        
        .demo {
            position: absolute;
            height: 100%;
            width: 100%;
        }
        
        .input-style {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border: none;
            padding: 5px 20px !important;
            border-radius: 25px;
            background: rgb(255, 255, 255);
        }
        
        .btn-forgot-password {
            background: linear-gradient(to right, #eeb828 10%, #ff572236 100%);
            color: white;
            border: none;
            padding: 15px 20px;
            display: block;
            border-radius: 25px;
            margin: 0 auto;
            width: 50%;
        }
    </style>

</head>

<body>

    <div class="demo"></div>
    <div class="container">

        <div class="materialContainer">

            <div class=" verify ">
                <div class="box">
                    <div class="card-wrap">
                        <div class="card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown">

                            <div class="card-body ">

                                <div class="card-content p-2 ">

                                    <div class="text-center">

                                        <?php
                                            if ( $this->session->flashdata('forgot_password')) {
                                                echo $this->session->flashdata('forgot_password'); 
                            
                                            }else{
                                                echo $this->session->flashdata('failed');  
                                            }
                                        ?>

                                    </div>
                                    <div class="text-center">
                                        <a href="<?= base_url();?>"><img src="<?= base_url('') ?>resource/front/images/logo2.png" style="height: 114px;"></a>
                                    </div>
                                    <div class="card-body">

                                        <!--<form class="form-signin" action="" method="post">-->
                                        <?php echo form_open_multipart('home/forgot_password_mail');?>

                                            <h2 class="h2_style">  Forgot Password </h2>

                                            <div class="input-group">
                                                <!-- <span class="input-group-addon"><i class="fa fa-user"></i></span>-->
                                                <input class="form-control input-style" id="name" name="userid" type="text" placeholder="User id" required="">
                                            </div>

                                            <div class="input-group">
                                                <!--  <span class="input-group-addon"><i class="fa fa-user"></i></span>-->
                                                <input class="form-control input-style" id="name" name="email_id" type="text" placeholder="Email id" required="">
                                            </div>

                                            <button type="submit" name="submit" value="Sign In" class="btn-color btn-forgot-password"><b> Retrieve Password</b></button>
                                            
                                            <?php echo form_close(); ?>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery.min.js"></script>
    <script src="<?= base_url('resource/front/uat_public/') ?>js/jquery-starfield.js"></script>

    <script type="application/javascript">
        $('.demo').starfield({
            starDensity: 0.2,
            mouseScale: 0.2
        });
    </script>

</body>

</html>