<body>
<div id="wrapper">
<div class="clearfix"></div>
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Package list</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Package list</div>
            <div class="card-body">
                
              <?= $this->session->flashdata('purchage_ticket')?>
              <div class="table-responsive">
                    <table id="example" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>SNo</th>
                                <th>Name</th>
                                <th>Amount($)</th>
                                <th>Percent</th>
                                <th>Days</th>
                                <th>Action</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i=1;
                                foreach ($packages->result() as $row) : ?>
                                    <tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $row->name;?></td>
                                        <td><?php echo $row->amount;?></td>
                                        <td><?php echo $row->percent;?></td>
                                        <td><?php echo $row->days;?></td>
                                        <td >
                                            <a  href="<?=base_url('user/pay_now')?>/<?php echo $row->name;?>/<?php echo $row->amount;?>/<?php echo $row->percent;?>/<?= $row->id ?>" >
                                            <button class="btn btn-info" type="button" name="buy" >buy</button></a>
                                        </td> 
                                    </tr>
                            <?php 
                                $i++;
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
    <script>

          
