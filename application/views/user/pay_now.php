
<body>
<div id="wrapper">
<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Pay Now</h4>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Pay Now</div>
            <div class="card-body">
                    <div class="container">
                      <!-- jk -->
                      <?= $this->session->flashdata('message') ?>
                       <form method="post" action="<?= base_url('user/createInvoice') ?>">
                            <input type="hidden" name="item_name" value="<?php echo $this->uri->segment(3); ?>">
                            <input type="hidden" name="item_description" value="<?= $this->session->userdata('processOrder')  ?>">
                            <input type="hidden" name="order_id"  value="<?= $this->session->userdata('processOrder')  ?>">
                            <input type="hidden" name="invoice_currency" value="usd">
                            <input type="hidden" name="package_id" value="<?php echo $this->uri->segment(6); ?>" data-type="number">
                            <input type="hidden" name="language" value="en">
                            <input type="hidden" name="success_url" value="<?=base_url('user/payment_success')?>">
                            <input type="hidden" name="failed_url" value="<?=base_url('user/payment_failure')?>">
                            <input type="submit" value="Pay With Cryptocurrency"  class="btn btn-info text-white" name="submit">
                        </form> 
                        
                        <p class="success_msg">  <h4>Note -</h4>
                        <p class="p_stylee"> Click on Pay with Cryptocurrency and After payment Please Do Not Close the Page. Page Automatically refresh and send you on your Dashboard.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
