<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->
        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-12 text-center">
                        <h4 class="page-title">Support Ticket</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i> Raise Ticket</div>
                            <div class="card-body">

                                <div class="text-danger"><strong>
                                      
                                  <?php
                                    if ( $this->session->flashdata('support_tickect')) {
                                        echo $this->session->flashdata('support_tickect'); 
                                
                                    }else{
                                        echo $this->session->flashdata('failed');  
                                    }
                                  ?>                  
                                      
                                 </strong></div>  
                                 
                                <div class="card-content p-2">
                                    <?php echo form_open_multipart('user/insert_support_ticket'); ?>
                                    <input type="hidden" id="id" class="form-control" name="id" value="<?php echo $this->session->userdata('logged_in')->id ;?>">
                                        <div class="form-group">
                                            <div class="position-relative has-icon-right">
                                                <label for="timesheetinput1">Subject</label>
                                                <input type="text" class="form-control form-control-rounded" name="subject" placeholder ="Enter your Subject" required>
                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="position-relative has-icon-right">
                                                <label for="timesheetinput1">Message</label>
                                                <textarea id="message" class="form-control form-control-rounded" name="message"  placeholder ="Enter your message" required=""></textarea>
                                                <span class="text-danger"><?php echo form_error('message'); ?></span>
                                            </div>
                                            <span class="error_msg amt_error"></span>
                                        </div>
                                        <div class="form-group">
                                            <div class="position-relative has-icon-right">
                                                <label for="timesheetinput1">Attach your Files</label>
                                                <!--<input type="text" class="form-control form-control-rounded" name="subject" placeholder ="Enter your Subject" required>-->
                                                <input type="file" class="form-control form-control-rounded" name="files" required>
                                                <span class="text-danger"><?php echo form_error('files'); ?></span>
                                            </div>
                                        </div>    
                                        <button type="submit" name="" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                                            Send
                                        </button>
                                        <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>    
            </div>
            
        </div>

<!--footer-->

