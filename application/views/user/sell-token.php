<body>
    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-12">
                        <h4 class="page-title text-center"><?= COIN ?></h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Buy <?= COIN ?></div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message') ?>
                                <div class="card-content p-2">
                                    
                                    <?php echo form_open_multipart('token/buy_token_submit',array("onsubmit" => "return validate()"));?>
                                        <div class="form-group">
                                                
                                                <div class="position-relative has-icon-right">
                                                    <label for="timesheetinput1">Total income</label>
                                                    <input type="number" id="balance" class="form-control form-control-rounded" name="balance" placeholder="0.00" value="<?= total_income($logged_in->user_id) ?>" readonly >
                                                </div>
                                                
                                                <div class="position-relative has-icon-right">
                                                    <label for="timesheetinput1">Amount</label>
                                                    <input type="number" id="amount" class="form-control form-control-rounded" name="amount" placeholder="0.00" value="0" autocomplete="off" required>
                                                    <span class="error_msg amt_error"></span>
                                                </div>
                                                
                                                <div class="position-relative has-icon-right">
                                                    <label for="timesheetinput1"><?= COIN ?> Qty</label>
                                                    <input type="number" id="coin_qty" class="form-control form-control-rounded" name="coin_qty" placeholder="0.00" value="" readonly >
                                                </div>
                                                
                                                
                                        </div>
                                        <button type="submit" name="buy_token" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Buy Now</button>
                                    <?php echo form_close();?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>

        <!--footer-->
        <script>
            
            $(document).on('input','#amount',function(){
                var amount=$(this).val();
                
                
                var amt=parseInt($("#amount").val());
				var balance=parseInt($("#balance").val());
				//alert(balance);
				
				if(amt>balance){
					$(".amt_error").html("Exceed the Available Total balance");
					$("#amount").focus();
					return false;
				}else{
				    $(".amt_error").html("");
					$("#amount").focus();
				}
                
                if(typeof(amount)=='undefined' || amount.length==''){
                    amount=0;
                }
                
                
                $.get("<?= base_url('token/tokenPrice/') ?>"+amount, function(data){
                    $('input[name="coin_qty"]').val(data);
                });
                
                
                var coin_qty=amount*tokenprice;
                
                $('input[name="coin_qty"]').val(coin_qty);
                
            });
            
        	function validate(){
			    var amt=parseInt($("#amount").val());
				var balance=parseInt($("#balance").val());
				//alert(balance);
				
				if(amt>balance){
					$(".amt_error").html("Exceed the Available Total balance");
					$("#amount").focus();
					return false;
				}
									
			}
			
			
			
			
				
            
            
        </script>