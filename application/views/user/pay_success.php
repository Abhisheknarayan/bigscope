
<style>
#success .card .card-header {
   padding: 0px 0px;
   border-bottom: none;
   background-color: transparent;
}

#success  .card .card-block {
   padding: 0px 0px;
}

.success_msg{ 
  font-size:12px !important;
   /*color: #28d094 !important;*/
   font-size: 18px !important; 
}

#success  p {       
  font-weight: bold;
   color: #000000c9;
   font-size: 14px;
   text-align: center;
}

#success  .card .card-header {
   padding: 0px 0px;
   border-bottom: none;
   background-color: transparent;
}

#success   .card .card-block {
   padding: 0px 0px;
}

#success  form .form-group {
   margin-bottom: 8px;
}

#success  .gradient-aqua-marine {
color: #fff;
background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
background-size: 400% 400%;
   height: 100vh;
-webkit-animation: Gradient 15s ease infinite;
-moz-animation: Gradient 15s ease infinite;
animation: Gradient 15s ease infinite;
}

@-webkit-keyframes Gradient {
0% {
background-position: 0% 50%
}
50% {
background-position: 100% 50%
}
100% {
background-position: 0% 50%
}
}

@-moz-keyframes Gradient {
0% {
background-position: 0% 50%
}
50% {
background-position: 100% 50%
}
100% {
background-position: 0% 50%
}
}

@keyframes Gradient {
0% {
background-position: 0% 50%
}
50% {
background-position: 100% 50%
}
100% {
background-position: 0% 50%
}
}

</style>
 <body>

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
 
<!--End topbar header-->

<div class="clearfix"></div>
  
  <div class="content-wrapper" style="min-height: 840px;">
    <div class="container-fluid">

    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Payment Success</h4>
            <ol class="breadcrumb">
                <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
             </ol>
        </div>
     
    </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Payment Success</div>
                    <div class="card-body">
                        <div class="card-block">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>
                 </div>
                </div>
              </div>
        </div>
    </div>
      
   
         
        
