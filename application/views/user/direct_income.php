<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->

        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper" style="min-height: 840px;">
            <div class="container-fluid">

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">Direct Referral Income</h4>
                        <ol class="breadcrumb">
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Rocker</a></li> -->
                            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
                            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                        </ol>
                    </div>

                </div>
                <!-- End Breadcrumb-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-table"></i>Direct Referral Income</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Direct user</th>
                                                <th>Package Amount($)</th>
                                                <th>Percent(%)</th>
                                                <th>Direct Income</th>
                                                <th>Status</th>
                                                <th>Date</th>

                                            </tr>
                                            <?php $i=1;?>
                                            <?php foreach ($data as $row) : ?>

                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td>
                                                    <?php  echo $i++; ?>
                                                </td>

                                                <td>
                                                    <?php echo $row->user_id;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->direct_user;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->package_amount;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->percent;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->direct_amount;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->status;?>
                                                </td>
                                                <td>
                                                    <?php echo $row->date;?>
                                                </td>

                                            </tr>
                                            <?php  endforeach; ?>
                                        </tbody>
                                        
                                        <tfoot>
                                            <tr>
                                                <th>SNo</th>
                                                <th>User Id</th>
                                                <th>Direct user</th>
                                                <th>Package Amount($)</th>
                                                <th>Percent(%)</th>
                                                <th>Direct Income</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print'

                    ]
                });
            });
        </script>