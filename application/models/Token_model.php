<?php 

defined('BASEPATH') OR exit('no direct script access allowed');

class Token_model extends CI_Model
{
    
	function __construct()
	{
		parent::__construct();
	}
	
	function token_list(){
	    $query=$this->db->query("SELECT * from token_history order by id desc");
	    if($query->num_rows()>0){
	        return $query->result();
	    }
	    else{
	        return array();
	    }
	}
	
	
	function token_transation($data){
	    $query=$this->db->insert('token_transaction_history',$data);
	    if($query){
	        return true;
	    }
	    else{
	        return false;
	    }
	}
	
	function get_token_transation($user_id=null){
	    
	    if(empty($user_id) || $user_id==''){
	        
	        $query=$this->db->query("SELECT * FROM `token_transaction_history` order by id desc");
	        
	    }
	    else{
	        $query=$this->db->query("SELECT * FROM `token_transaction_history` where to_wallet_address='$user_id'");
	    }
	    
	    if($query->num_rows()>0){
	        return $query->result();
	    }
	    else{
	        return array();
	    }
	}
	
	public function user_get_token_value_list()
	{
	    $query = $this->db->get('token_history');
	    if($query->num_rows() > 0){
	       return $query->result();
	       
	        
	    }
	    else{
	        return array();
	    }
	}
	
}