<?php 
defined('BASEPATH') OR exit('no direct script access allowed');

class User_model extends CI_Model
{
	
	function __construct()
	{
			parent::__construct();
	}

    public function get_package_records($uid)
    {
       $query = $this->db->where(array('user_id'=> $uid,'status' => 'success'))->get('purchase_package');
       if($query->num_rows() > 0){
           //$bb = $query->num_rows()
           return  $query->num_rows();
           
       }
       else{
           return '0';
       }
    }
    
	public function get_packages()
	{
		$query = $this->db->get('packages');  
        return $query; 
     }


     public function purchase_amount($id)
     {
     	$id=$this->db->select('amount')->where('amount',$id)->get('packages')->num_rows();

			if($id>0){
			return true;
			}
			else
			{
			return false;
			}

     }
     public function get_purchase_history()
	    {
		    $query = $this->db->get('purchase_package');  
            return $query; 
        }
     

    
   
   public function insert_activation_code($new_array)
     {
        
            if ($this->db->insert("activation_code",$new_array)) 
            {
            return true;
            
            }

     }
    public function get_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE green_id_type='user'");
            return $query;
    }

    public function used_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE status ='1' And green_id_type='user'");
            return $query;
    }
    public function unused_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE status ='0' And green_id_type='user'");
            return $query;
    }
    
    /*jai code start*/
    
    public function get_user_by_id($id) {
	
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	} 
	
	
	public function upadte_user_profie($data,$id) {
	
		$this->db->where('id', $id);
		if( $this->db->update('users',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	} 
	
	
	
	public function change_password($data,$id) {
	
		$this->db->where('id', $id);
		if( $this->db->update('users',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	} 	
	
	public function insert_withdrawal_btc($data) {
	
		if ($this->db->insert("withdrawal_request",$data)){
            
            return true;
        }else{
            return false;
        }
		
	} 
	

    public function get_withdrawal_history($id) {
    
	  $query = $this->db->query("SELECT * FROM `withdrawal_request` INNER JOIN `users` ON `withdrawal_request`.`user_id`=`users`.`user_id` WHERE `withdrawal_request`.`user_id`='".$id."'");
	  return $query->result();	  
    	
    }
    
    
    public function get_daily_roi($id) {
    		
	  $query = $this->db->query("SELECT tbl_extra_roi.*,packages.name,packages.amount,users.first_name,users.user_email FROM `tbl_extra_roi` INNER JOIN `users` ON `tbl_extra_roi`.`register_id`=`users`.`user_id` INNER JOIN `packages` ON `packages`.`id`=`tbl_extra_roi`.`plan_id` WHERE `tbl_extra_roi`.`register_id`='$id'");
	  return $query->result();	  
    	
    }   
    
    public function get_daily_roi_by_id($id,$pid,$plan_id) {
	  
	  $query = $this->db->query("SELECT purchase_package.name,purchase_package.amount,tbl_extra_roi.* FROM purchase_package INNER JOIN tbl_extra_roi ON tbl_extra_roi.register_id = purchase_package.user_id where purchase_package.id = '$pid' && status = 'success' && purchase_package.user_id = '$id' && tbl_extra_roi.plan_id ='$plan_id' ");
	  
	  return $query->result();	  
    	
    }      
    
    public function update_pofi_img($data,$id) {
        
        $this->db->where('id', $id);
		if( $this->db->update('users',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
        
    }
    
    public function insert_ticket($data){
         
     	if ($this->db->insert('support_ticket',$data)) 
     	{
     		return true;
     		
     	}
    } 
    
    public function get_support_ticket($id){
        
     	$query=$this->db->query("SELECT * FROM `support_ticket` WHERE user_id='$id'");
     	return $query;
    }    
    
    

    public function get_tickect_log($id){
        
     	$query=$this->db->query("SELECT * FROM `support_ticket_log` WHERE support_id='$id'");
     	return $query->result();
    } 
    
    
    public function insert_purchase_history($data){
         
        if ($this->db->insert("purchase_package",$data)){
            
            return true;
        } 


    }  
    
    public function get_packages_list(){
        
       $query = $this->db->get('packages');
       if($query->num_rows() > 0){
           return $row = $query->result();
           
       }
       else{
           return array();
       }
    }    
    
   
    public function get_package_details($id)
    {
    	$query = $this->db->where('id', $id)->get('packages')->row();

    	return $query;
    }
    
	
    /*jai code end*/
    
    
    
    /* jagdeep*/
   public function directIncome($userId){
       
                $query=$this->db->where('user_id',$userId)->get('direct_income_user_list');
                
                if($query->num_rows()>0){
                    return $query->result();
                }
                else{
                    return array();
                }
                
        }

    public function update_purchase_history($data)
        {
            if ($this->db->where('order_id',$data['order_id'])->update("purchase_package",$data)) 
                {
                  return true;
                } 
            else{
                return false;
            }
            
         }

    public function findDirectIncome($userId='BI48052',$purchaseamount='',$order_id){
                
                $incomeAmount=($purchaseamount*directPercent())/100;
                
                $data = array(
                        'order_id' => $order_id,
                        'user_id' => sponsor($userId),
                        'direct_user' =>$userId,
                        'package_amount'=>$purchaseamount,
                        'percent'=>directPercent(),
                        'direct_amount' =>$incomeAmount,
                        'status'=>'success',
                 );

                $directIncomeInsert = $this->db->insert('direct_income_user_list', $data);

                if($directIncomeInsert){
                        return true;
                    }
                else
                    {
                        return false;
                    }   
    }


    // public function get_direct_income(){
    //             $query=$this->db->get('users');
    //             return $query;
    //  }

    public function get_direct_income($user_id)
	{
	    $query = $this->db->where('user_id',$user_id)->get('direct_income_user_list');
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}

     public function check_exists($order_id){
            $query = $this->db->query("SELECT * FROM `direct_income_user_list` where order_id='$order_id'");

            if($query->num_rows() >  0){
                return true;
            }
            else{
                return false;
            }
     }
     
     public function get_purchase_history1($userid)
	    {
		    
            $query=$this->db->select('purchase_package.*,packages.name')->join('packages','packages.id=purchase_package.package_id')->where('user_id',$userid)->get('purchase_package');
		    return $query->result_array();
        }
    public function get_direct_user($id)
        {
            $query=$this->db->query("SELECT * FROM `users` WHERE sponser_id='$id'");
            return $query->result();

        }
        public function get_binary_income($userID)
        {
            $query=$this->db->query("SELECT * FROM `my_income1` WHERE usid='$userID'");
            return $query->result();

        }
        
     
    


 }
 ?>
