<?php 
defined('BASEPATH') OR exit('no direct script access allowed');

class Home_model extends CI_Model
{
	
	function __construct()
	{
			parent::__construct();
	}

	public function insert($data){

		if ($this->db->insert("users",$data)){
		  return $insert_id = $this->db->insert_id();
		}
	}

	public function checkGreenid($id)
	{

			$id=$this->db->select('*')->where('greenid',$id)->where('status','1')->get('activation_code')->num_rows();

			if($id>0){
			return true;
			}
			else
			{
			return false;
			}

	}

    public function checkSponser($id){
        $id=$this->db->select('*')->where('user_id',$id)->where('user_status','active')->get('users')->num_rows();

		if($id>0){
	    	return true;
		}
		else
		{
		    return false;
		}
    }
    
    public function adminid(){
        $query=$this->db->select('user_id')->where('sponser_id','admin')->where('user_status','active')->get('users');

		if($query->num_rows()>0){
	    	return $query->row()->user_id;
		}
		else
		{
		    return false;
		}
    }

	public function login($data) 
	{
	   		$condition = "user_id =" . "'" . $data['user_id'] . "' AND " . "password =" . "'" . $data['password'] . "' AND user_status = 'active' ";
	    	$this->db->select('*');
	    	$this->db->from('users');
	    	$this->db->where($condition);
	    	$this->db->limit(1);
	    	$query = $this->db->get();
			if ($query->num_rows() == 1) 
	    	{

	        return $query->row();
	        
	    	} 
	    	else 
	    	{
	        return false;
	    	}
	}
	public function admin_login($data)
	{
			$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
	    	$this->db->select('*');
	    	$this->db->from('admin');
	    	$this->db->where($condition);
	    	$this->db->limit(1);
	    	// $this->db->set(array('last_login_date' => date('Y-m-d h:i:s')));
	    	$query = $this->db->get();

	    	if ($query->num_rows() == 1) 
	    	{

	            return $query->row();
	    	} 
	    	else 
	    	{
	            return false;
	    	}
	}

  

    public function update_user($data, $user){
    	
		$this->db->where('id', $user);
		if( $this->db->update('users',$data)) {
			return true;
		} else {
			return false;
		}		
	}



	public function get_user($id) {
	
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return null;
		}
	}

	

	public function get_country_code() {
	
	  $query = $this->db->query("select * from country");
  	  return $query->result_array();
	}
	
	
	
	public function update_forgot_pass($data_hai,$idd)
	{
	    
		$this->db->where('id', $idd);
		if( $this->db->update('users',$data_hai)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	}
	
	public function getLastChildOfLR($parentUserName="",$position='')
        { 

            $query = $this->db->query("SELECT user_id FROM users WHERE user_id='$parentUserName'");

            $parentid = $query->row_array();
            
            $childid = $this->getTreeChildId($parentid['user_id'], $position); 
            
            if($childid!="-1"){
                $mid=$childid;
              } 
            else
              {
                $mid=$parentid['user_id'];
              }
              
            $flag=0;
            
            //while($mid!="" || $mid!="0")
            while($mid!="" || $mid!="0")
            {
                if($this->isMemberExists($mid))
                {   
                    $nextchildid=$this->getTreeChildId($mid,$position);

                    if($nextchildid=="-1")
                    {
                        $flag=1;
                        break;
                    } 
                    else 
                    {
                        $mid = $nextchildid;
                    }
                }//if
                else
                    break;
                    
            }//while
            
            return $mid;    
        }

    public function isMemberExists($mid='0')
	    {
		    // $count = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM users WHERE user_id='".$mid."'"));
		    // if ($count[0] == 1){
		    //      return true;
		    //  }else{
		    //     return false;       
		    // }

		    $query=$this->db->query("SELECT * FROM users WHERE user_id = '".$mid."'");

		    if($query->num_rows()>0){
		    	return true;
		    }
		    else{
		    	return false;
		    }
	   }  

	public function getTreeChildId($parentid="",$position="")
	    {
	        // $cou = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user WHERE posid='".$parentid."' AND position='".$position."'"));

	        // $cid = mysqli_fetch_array(mysqli_query($con,"SELECT user_id FROM user WHERE posid='".$parentid."' AND position='".$position."'"));

	        $query=$this->db->query("SELECT `user_id` FROM users WHERE user_posid='$parentid' AND user_position='$position' order by id desc");

	        if($query->num_rows() == 1)
	        {
	            $result = $query->row_array();
	            return $result['user_id'];
	        }else
	        {
	            return -1;       
	        }

	    }
	    public function check_postion_id()
	    {
	    	 $query=$this->db->query("SELECT * FROM `users` ORDER BY id ASC LIMIT 1");
	    if($query->num_rows()>0)
	    {
		      $result = $query->row_array();
	            return $result['user_id'];
		}
		else
		{
		    	return 'admin';
		}
	 	
	 	}
	 	
	 	public function insert_enquiry($data){
	 		if ($this->db->insert('enquiry',$data)) {
	 			return true;
	 		}
	 		else{
	 			return false;
	 		}


	 	}
	 	 public function insert_user_ip($data){
	 	        
	 	       	if ($this->db->insert('cookie_info',$data)) {
	 			return true;
	 		}
	 		else{
	 			return false;
	 		}
	        
	    }
	    public function check_user_ip($id)
	{

			$id=$this->db->select('*')->where('ip_address',$id)->get('cookie_info')->num_rows();

			if($id>0){
			return true;
			}
			else
			{
			return false;
			}

	}

	
	        
	    
	
	
	
	




	





















}