<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shailu_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		
	}
	
	public function get_constant_list($income)
	{
	   $query = $this->db->where('type',$income)->get('constant');
	   if($query->num_rows()>0){
	       return $query->result();
	   }
	   else{
	       return array();
	   }
	}
	public function constant_list_edit($id)
	{
	    $query = $this->db->where('id',$id)->get('constant');
	    if($query->num_rows() > 0){
	       return $query->result(); 
	    }
	    else{
	       return array();
	    }
	}
	
	public function edit_constant_row($id,$data)
	{
	    $query = $this->db->where('id', $id)->update('constant', $data);
	    if($query){
	        return $query;
	    }
	    else{
	        return false;
	    }
	}
	
	public function get_daily_return()
	{
	    $query = $this->db->get('tbl_extra_roi');
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}
	
    public function get_direct_income()
	{
	    $query = $this->db->get('direct_income_user_list');
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}
	
	public function get_user_direct_income($session_id)
	{
	    $query = $this->db->where('user_id', $session_id)->get('direct_income_user_list');
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}
	
	public function launch_token($data)
	{
	    $query = $this->db->insert('launch_token', $data);
	    if($query){
	        return $query;
	    }
	    else{
	        return false;
	    }
	}
	
	public function get_token_value_list()
	{
	    $query = $this->db->get('token_history');
	    if($query->num_rows() > 0){
	        return $query->result();
	    }
	    else{
	        return array();
	    }
	}
	
	public function value_token($data)
	{
	    $query = $this->db->insert('token_history', $data);
	    if($query){
	        return $query;
	    }
	    else{
	        return false;
	    }
	}
	
	
	
}	
	
?>