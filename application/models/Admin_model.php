<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_model extends CI_Model{
    
	
	function __construct(){
	    
		parent::__construct();
	}
	
	
	
	/*jai code edit start here*/
	
	public function get_user_list(){
		$query=$this->db->get('users');
		return $query;
	}
	
    /*shailu code edit start here*/
public function get_paid_list()
	{
		//SELECT SUM(amount) FROM `purchase_package` WHERE user_id='BI98352' AND   FIND_IN_SET(name,'PRIMARY,GOLD,SILVER,PLATINUM,DIAMOND,STAR') AND status='success'
	    $query=$this->db->query("SELECT *, GROUP_CONCAT(CONCAT_WS(',', NAME, name) SEPARATOR ',') as result,SUM(amount) as totalamount FROM purchase_package where status='success' GROUP BY user_id");
        return $query;
    }
	  
	
	public function get_unpaid_list()
	{
	    $query=$this->db->query("SELECT *, GROUP_CONCAT(CONCAT_WS(',', NAME, name) SEPARATOR ',') as result,SUM(amount) as totalamount FROM purchase_package where status='pending' GROUP BY user_id");
            return $query;
	}
	
	/*end shailu code edit start here*/
	
	public function get_edit_list($id)
	{
	    $query = $this->db->where('user_id',$id)->get('users');
	    if($query->num_rows() > 0){
	        return $query->result();
	    }
	    else{
	        return array();
	    }
	}
	
	public function user_profile_edit($id,$data)
	{
	    $query = $this->db->where('user_id', $id)->update('users',$data);
	    if($query){
	        return $query;
	    }
	    else{
	        return false;
	    }
	}

	
	public function support_ticket_history(){
	    
		$query=$this->db->get('support_ticket');
		return $query;
	}
	
	
	public function update_tickect_status($data,$id){
	    
        $this->db->where('id', $id);
		if( $this->db->update('support_ticket',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}
	
	
	public function delete_ticket($id){
	    
        $query = $this->db->where('id',$id)->delete('support_ticket');
		if($query) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}	
	
  	public function inset_suport_replay($data){
  	    
        if ($this->db->insert("support_ticket_log",$data)){
            
            return true;
        }else{
            return false;
        } 
    }
    
    public function get_all_withrawal(){
  	   
        $query = $this->db->query("SELECT withdrawal_request.*,users.first_name,users.last_name,users.user_email FROM `withdrawal_request` INNER JOIN `users` ON `withdrawal_request`.`user_id`=`users`.`user_id` ");
	    return $query->result_array();	  
    }
   
	public function withdrowal_cancel($data,$id){
	    
        $this->db->where('id', $id);
		if( $this->db->update('withdrawal_request',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}   
	
	public function withdrowal_accept($data,$id){
	    
        $this->db->where('id', $id);
		if( $this->db->update('withdrawal_request',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}	
      
	
	
	
	/*jai code edit end here*/
	
	
	public function get_packages()
	{
		$query=$this->db->get('packages');
		return $query;
	}
	
	public function get_transaction_history()
	{
		$query=$this->db->select('purchase_package.*,packages.name')->join('packages','packages.id=purchase_package.package_id')->get('purchase_package');
		return $query;
	}
	
	public function insert_purchase_history($data)
     {
         if ($this->db->insert("purchase_package",$data)) 
         {
              return true;
          } 
     }
     
    public function insert_activation_code($new_array)
     {
        
            if ($this->db->insert("activation_code",$new_array)) 
            {
            return true;
            
            }

     }
   public function get_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE green_id_type='admin'");
            return $query;
    }

    public function used_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE status ='0' And green_id_type='admin'");
            return $query;
    }
    public function unused_activation_code()
    {
            $query=$this->db->query("SELECT * FROM `activation_code` WHERE status ='1' And green_id_type='admin'");
            return $query;
    }
    
    public function get_package_details($id)
    {
    	$query = $this->db->where('id', $id)->get('packages')->result_array();

    	return $query;
    }
    public function update_package($data, $id)
    {
    	//print_r($id);
    	return $query = $this->db->where('id', $id)->update('packages', $data);
    }
    
    public function package_add($data)
    {
        
        if($query = $this->db->insert('packages',$data))
        {
            return $query;
        }
        else
        {
            return $query;
        }
    }
    
    
    public function get_daily_return()
	{
	    $query = $query = $this->db->query("SELECT * FROM `tbl_extra_roi` INNER JOIN `users` ON `tbl_extra_roi`.`register_id`=`users`.`user_id` INNER JOIN `packages` ON `packages`.`id`=`tbl_extra_roi`.`plan_id`");
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}
    
    public function delete_package($id)
    {
        return $query = $this->db->where('id',$id)->delete('packages');
    }
    
    public function get_direct_percent()
    {
        return $query = $this->db->get('direct_percent')->result_array();
       
         
    }
    
    public function get_direct_income()
	{
	    $query = $this->db->get('direct_income_user_list');
	    if($query->num_rows() > 0){
	       return $query->result();
	    }
	    else{
	       return array();
	    }
	}
    
    public function update_direct_percent($id)
    {
        return $query = $this->db->where('id', $id)->get('direct_percent')->result_array();
        
    }
    public function percent_update($data)
    {
       return $query = $this->db->update('direct_percent',$data);
    }
    public function percent_delete($id)
    {
      return $query = $this->db->where('id', $id)->delete('direct_percent'); 
    }
    
   public function get_activated_user_pin()
    {
        $query = $this->db->where('status','0')->get('activation_code');
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return array();
        }
    	
    }
    
    public function check_userid($id)
	{

			$id=$this->db->select('user_id')->where('user_id',$id)->where('user_status','active')->get('users')->num_rows();

			if($id>0){
			return true;
			}
			else
			{
			return false;
			}

	}
	
	public function update_enquiry_status($data,$id){
	    
        $this->db->where('id', $id);
		if( $this->db->update('enquiry',$data)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}
	
	
	public function delete_enquiry($id){
	    
        $query = $this->db->where('id',$id)->delete('enquiry');
		if($query) 
		{
			return true;
		} 
		else 
		{
			return false;
		}		
	}
	public function get_binary_capping1(){
		$query=$this->db->get('binary_capping');
		return $query;
	}
	public function get_binary_capping($id)
    {
    	$query = $this->db->where('id', $id)->get('binary_capping')->result_array();

    	return $query;
    }
    public function update_capping($data, $id)
    {
    	//print_r($id);
    	return $query = $this->db->where('id', $id)->update('binary_capping', $data);
    }
    
    
    public function get_token_transaction_history(){

    	$query=$this->db->get('token_transaction_history');
    	return $query;
    }
    public function get_binary_income()
        {
            $query=$this->db->query("SELECT * FROM `my_income1`");
            return $query->result();

        }
	
	

     

}