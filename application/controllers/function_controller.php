<?php

defined('BASEPATH')OR exit('no direct script access allowed');


class Function extends CI_Controller
{
  
  function __construct(argument)
  {
    parent::__construct();
  }




function connectdb()
{
    
function member_down($user_refid,$level){
    global $qry,$i;
    $con=connectdb();
    $tuser_qry=mysqli_query($con,"select * from user where under_userid='".$user_refid."'");
    
    if(mysqli_num_rows($tuser_qry)>0){
        
        while($tuser_row=mysqli_fetch_array($tuser_qry)){
            $qry[$i]=$tuser_row;
            $qry[$i]['level']=$level;
            $i++;
            
            member_down($tuser_row['user_id'],$level+1);
        }
        
    }
    return $qry;
                                        
}

$ans="";
function check_left($id){
  $con=connectdb();
  global $ans;
  $ans=$id;
  $res=mysqli_query($con,"select * from tree where user_id='$id' and left_child!=''");
  if(mysqli_num_rows($res)>0){
    $row=mysqli_fetch_array($res);
    $ans=$row['left_child'];
    check_left($row['left_child']);
  }
  return $ans;
  
}
$ans1="";
function check_right($id){
  $con=connectdb();
  global $ans1;
  $ans1=$id;
  $res=mysqli_query($con,"select * from tree where user_id='$id' and right_child!=''");
  if(mysqli_num_rows($res)>0){
    $row=mysqli_fetch_array($res);
    $ans1=$row['right_child'];
    check_right($row['right_child']);
  }
  
    return $ans1;
  
  
}

function get_join_detail($user_refid){
    $con=connectdb();
    $tuser_qry=mysqli_query($con,"select * from user a JOIN purchase_list b ON b.user_id=a.user_id where a.user_id='".$user_refid."'");
    if(mysqli_num_rows($tuser_qry)>0){
        $tuser_row=mysqli_fetch_array($tuser_qry);
            
        return $tuser_row;                
    }
    else{                                  
        return $tuser_row=array('total_price'=>'0','joindate'=>'00-00-0000');
    }
}

$pattern="";
function get_left_child($id){
    $con=connectdb();
    //echo $id;
    $qry=mysqli_query($con,"select * from tree where user_id='$id' and left_child!=''");
    global $pattern;
    if(mysqli_num_rows($qry)){
        $row=mysqli_fetch_array($qry);
        $qry_tree=mysqli_query($con,"select * from user where user_id='".$row['left_child']."'");
        $row_tree=mysqli_fetch_array($qry_tree);
        $join_row=get_join_detail($row['left_child']);
        if($row_tree['profile_img']==""){
          $src="assets/images/avatars/25.png";
        }
        else{
          $src="assets/images/profile/".$row_tree['profile_img'];
        }
        $pattern.='
                <div class="hv-item-child toolti_plus">
                  <div class="person">
                      <img src="'. $src.'" alt="">
                      <p class="name">
                        '.$row_tree['fname'].' <b>/ '.$row_tree['user_id'].'</b>
                      </p>

                    <span class="tooltitext">
                      Joining Amount: '.$join_row['total_price'].'<br>
                      Joining Date: '.date("d-m-Y",strtotime($join_row['joindate'])).'
                    </span>
                  </div>';


 $pattern.='<div class="hv-item-children-add">';
 echo get_left_child($row['left_child']);
 echo get_right_child($row['left_child']);
 $pattern.=' </div>';
                 
               $pattern.=' </div>';
    }
    return $pattern;
}

$pattern1="";
function get_right_child($id){
    $con=connectdb();
    $qry=mysqli_query($con,"select * from tree where user_id='$id' and right_child!=''");
    global $pattern1;
    if(mysqli_num_rows($qry)){
        $row=mysqli_fetch_array($qry);
        $qry_tree=mysqli_query($con,"select * from user where user_id='".$row['right_child']."'");
        $row_tree=mysqli_fetch_array($qry_tree);
        $join_row=get_join_detail($row['right_child']);
        if($row_tree['profile_img']==""){
          $src="assets/images/avatars/25.png";
        }
        else{
          $src="assets/images/profile/".$row_tree['profile_img'];
        }
        $pattern1.='
                <div class="hv-item-child toolti_plus">
                  <div class="person">
                      <img src="'. $src.'" alt="">
                      <p class="name">
                        '.$row_tree['fname'].' <b>/ '.$row_tree['user_id'].'</b>
                      </p>

                    <span class="tooltitext">
                      Joining Amount: '.$join_row['total_price'].'<br>
                      Joining Date: '.date("d-m-Y",strtotime($join_row['joindate'])).'
                    </span>
                  </div>';


 $pattern1.='<div class="hv-item-children-add">';
 echo get_left_child($row['right_child']);
 echo get_right_child($row['right_child']);
 $pattern1.=' </div>';
                 
               $pattern1.=' </div>';
    }
    return $pattern1;
}


function get_parent($id){
  $con=connectdb();
  $qry=mysqli_query($con,"select * from user where user_id='".$id."'");
  $row=mysqli_fetch_array($qry);

  return $row['under_userid'];
  
}
$count=0;
function get_left_count($id){
  $con=connectdb();
  global $count;
  $c=0;
  $qry1=mysqli_query($con,"select * from tree where user_id='".$id."' and left_child!=''");
  if(mysqli_num_rows($qry1)){

    $row=mysqli_fetch_array($qry1);
    $count+=(int)$row['leftcount'];
  if($row['left_child']!='')
    $count+=get_lchild($row['left_child']);
  }
 
    return $count;
  
}

$count1=0;
function get_right_count($id){
  $con=connectdb();
  global $count1;

  $qry1=mysqli_query($con,"select * from tree where user_id='".$id."' and right_child!=''");
  if(mysqli_num_rows($qry1)){

    
    $row=mysqli_fetch_array($qry1);
    $count1+=(int)$row['rightcount'];
    if($row['right_child']!='')
      $count1+=get_rchild($row['right_child']);
  }
  
    return $count1;
  
}
$lc=0;
function get_lchild($id){
  $con=connectdb();
  global $lc;

  $qry1=mysqli_query($con,"select * from tree where user_id='".$id."'");
  if(mysqli_num_rows($qry1)){

    $row1=mysqli_fetch_array($qry1);
    
    if($row1['left_child']!=''){
      $lc++;
      get_lchild($row1['left_child']);
    }
    if($row1['right_child']!=''){
      $lc++;
      get_lchild($row1['right_child']);
    }
  }
  
    return $lc;
  
}
$rc=0;
function get_rchild($id){
  $con=connectdb();
  global $rc;

  $qry1=mysqli_query($con,"select * from tree where user_id='".$id."'");
  if(mysqli_num_rows($qry1)){

    $row2=mysqli_fetch_array($qry1);
    if($row2['left_child']!=''){
      $rc++;
      get_rchild($row2['left_child']);
    }
    if($row2['right_child']!=''){
      $rc++;
      get_rchild($row2['right_child']);
    }
  }

    return $rc;
  
}

$child=array();
function left_child($id){
   $con=connectdb();
    global $child;
   $qry1=mysqli_query($con,"select * from tree where user_id='".$id."' and left_child!=''");
  if(mysqli_num_rows($qry1)){
    $row=mysqli_fetch_array($qry1);
    array_push($child,$row['left_child']);
    both_lchild($row['left_child']);
  }
  
    return $child;
  
  //print_r($child);
}

$child1=array();
function right_child($id){
   $con=connectdb();
   global $child1;
   $qry1=mysqli_query($con,"select * from tree where user_id='".$id."' and right_child!=''");
  if(mysqli_num_rows($qry1)){
    $row=mysqli_fetch_array($qry1);
    array_push($child1, $row['right_child']);
    both_rchild($row['right_child']);
  }
 
    return $child1;
  
}

function both_lchild($id){
   $con=connectdb();
    global $child;
   $qry1=mysqli_query($con,"select * from tree where user_id='".$id."'");
  if(mysqli_num_rows($qry1)){
    $row=mysqli_fetch_array($qry1);
    array_push($child,$row['left_child']);
    both_lchild($row['left_child']);

    array_push($child,$row['right_child']);
    both_lchild($row['right_child']);

  }
}
function both_rchild($id){
   $con=connectdb();
    global $child1;
   $qry1=mysqli_query($con,"select * from tree where user_id='".$id."'");
  if(mysqli_num_rows($qry1)){
    $row=mysqli_fetch_array($qry1);
    array_push($child1,$row['left_child']);
    both_rchild($row['left_child']);

    array_push($child1,$row['right_child']);
    both_rchild($row['right_child']);
  }
}
$count=0;
function total_down($user_refid){
    global $count;
    $con=connectdb();
    $tuser_qry=mysqli_query($con,"select * from user where under_userid='".$user_refid."'");
    if(mysqli_num_rows($tuser_qry)>0){
        while($tuser_row=mysqli_fetch_array($tuser_qry)){
            $count++;
            total_down($tuser_row['user_id']);
            
        }
                                            //echo $count;
    }
                                        
    return $count;
                                        
}
function get_amount($l,$r){
  $con=connectdb();
  $left=$l;
  $right=$r;

  $left_amount=0;
  $right_amount=0;
  $larg=0;

  for($i=0;$i<count($left);$i++){
    $order_rec_qry = mysqli_query($con, "SELECT * FROM `purchase_list` WHERE user_id='".$left[$i]."' and status='success'");
    if(mysqli_num_rows($order_rec_qry)){
      $row=mysqli_fetch_array($order_rec_qry);
      $left_amount+=$row['total_price'];
    }
  }

  for($i=0;$i<count($right);$i++){
    $order_rec_qry = mysqli_query($con, "SELECT * FROM `purchase_list` WHERE user_id='".$right[$i]."' and status='success'");
    if(mysqli_num_rows($order_rec_qry)){
      $row=mysqli_fetch_array($order_rec_qry);
      $right_amount+=$row['total_price'];
    }
  }

    if($left_amount>=$right_amount){
      $larg=$right_amount;
    }
    else{
      $larg=$left_amount;
    }

    return $larg;
  }

// matrix level income cal==========================================>>>>>
function matrix_level($id){
  $date=date("Y-m-d");
  $con=connectdb(); 
     $leveli1= 0;
    // $array_level2 = "";
     $leveli2 = 0;
    // $array_level3 = "";
     $leveli3 = 0;
    // $array_level4 = "";
     $leveli4 = 0;
    // $array_level5 = "";
     $leveli5 = 0;
    // $array_level6 = "";
     $leveli6 = 0;
    // $array_level7 = "";
     $leveli7 = 0;
    // $array_level8 = "";
     $leveli8 = 0;

    $level_user1 = 0;
    $level_user2 = 0;
    $level_user3 = 0;
    $level_user4 = 0;
    $level_user5 = 0;
    $level_user6 = 0;
    $level_user7 = 0;
    $level_user8 = 0;

    //echo"select * from user where under_userid='".$id."'";
      $sqry_level1 = mysqli_query($con,"select * from user where under_userid='".$id."'");
    if(mysqli_num_rows($sqry_level1) > 0){
       while($display_level1 = mysqli_fetch_assoc($sqry_level1)){
       // echo $display_level1['user_id'];
      $array_level1[$leveli1] = $display_level1['user_id'];
     
      $sqry_level2 = mysqli_query($con, "select * from user where under_userid='".$display_level1['user_id']."'");
    if(mysqli_num_rows($sqry_level2) > 0){
      while($display_level2 = mysqli_fetch_array($sqry_level2)){
      $display_level2['user_id'];
      $array_level2[$leveli2] = $display_level2['user_id'];

      $select_level3 = mysqli_query($con, "select * from user where under_userid='".$display_level2['user_id']."'");
    if(mysqli_num_rows($select_level3) > 0){
      while ($display_level3=mysqli_fetch_array($select_level3)) {
      $array_level3[$leveli3]  = $display_level3['user_id'];

      $select_level4 = mysqli_query($con, "select * from user where under_userid='".$display_level3['user_id']."'");
    if(mysqli_num_rows($select_level4)>0){
      while($display_level4=mysqli_fetch_array($select_level4)){
      $array_level4[$leveli4] = $display_level4['user_id'];

      $select_level5 = mysqli_query($con, "select * from user where under_userid='".$display_level4['user_id']."'");
    if(mysqli_num_rows($select_level5)>0){
      while($display_level5 = mysqli_fetch_array($select_level5)){
      $array_level5[$leveli5] = $display_level5['user_id'];

      $select_level6 = mysqli_query($con, "select * from user where under_userid='".$display_level5['user_id']."'");
    if(mysqli_num_rows($select_level6)>0){
      while($display_level6 = mysqli_fetch_array($select_level6)){
      $array_level6[$leveli6] = $display_level6['user_id'];

      $select_level7 = mysqli_query($con, "select * from user where under_userid='".$display_level6['user_id']."'");
    if(mysqli_num_rows($select_level7)>0){
      while($display_level7 = mysqli_fetch_array($select_level7)){
      $array_level7[$leveli7] = $display_level7['user_id'];

      $select_level8 = mysqli_query($con, "select * from user where under_userid='".$display_level7['user_id']."'");
    if(mysqli_num_rows($select_level8)>0){
      while($srow8 = mysqli_fetch_array($select_level8)){
      $array_level8 = $srow8['user_id'];
      $leveli8++;

   }  
      } 
      $leveli7++; 
    }
       }
      $leveli6++;

    }
      }
      $leveli5++;  
    }  
      }
      $leveli4++;
   }  
      }
      $leveli3++;
    }
      }
      $leveli2++;
    }

      }
    
      $leveli1++;

    }
      }
        

//print_r($array_level1);
   $total_plan_sum_lev1=0;
    if($array_level1){
     
      for($i=0; $i < count($array_level1); $i++){
        $select_user_match_lev1 = mysqli_query($con, "select sum(total_price) as total_price,user_id from purchase_list where user_id='".$array_level1[$i]."' && status='success'");
        $display_users_rows_lev1 = mysqli_fetch_array($select_user_match_lev1);
        $display_users_rows_lev1['user_id'];
        $total_plan_sum_lev1 = $display_users_rows_lev1['total_price'];

      if($display_users_rows_lev1 > 0){
        $array_level1[$i];
        $level_user1 = $level_user1 + 1; 

        $level_user1;
  
      if($level_user1 >=4){
       }  
        $comission_per_leve1 = mysqli_fetch_assoc(mysqli_query($con,"select * from level_commission_plan where level='1'"));
        $lev_per1 = $comission_per_leve1['percentage'];
        $total_plan_sum_lev1 = $display_users_rows_lev1['total_price'];
        $total_sum1 = $total_plan_sum_lev1;
        $total_income_lev1 = $total_sum1 * $lev_per1/100;
      
        $insert_level1_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev1['user_id']."','".$total_plan_sum_lev1."','".$total_income_lev1."','$lev_per1','1','complete','complete','".$date."')");

        //echo "insert".'<br>';
      }
      }
    }
     $total_plan_sum_lev1; 
 


//print_r($array_level2);    
    
  if($array_level2){

    for($i = 0; $i < count($array_level2); $i++){

        $select_user_match_lev2 = mysqli_query($con, "select sum(total_price) as total_price,user_id from purchase_list where user_id='".$array_level2[$i]."'  && status='success'");
        $display_users_rows_lev2 = mysqli_fetch_array($select_user_match_lev2);
        $display_users_rows_lev2['user_id'];
        $total_plan_sum_lev2 = $display_users_rows_lev2['total_price'];

      if($display_users_rows_lev2 > 0){
        $array_level2[$i];
        $level_user2 = $level_user2 + 1; 
        $level_user2.'<br>';
         
      if($level_user2 >=4){
        } 
        $comission_per_leve2 = mysqli_fetch_assoc(mysqli_query($con,"select * from level_commission_plan where level='2'"));
        $lev_per2 = $comission_per_leve2['percentage'];
        $total_plan_sum_lev2 = $display_users_rows_lev2['total_price'];
        $total_sum2 = $total_plan_sum_lev2;
        $total_income_lev2 = $total_sum2 * $lev_per2/100;


         $insert_level2_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev2['user_id']."','".$total_plan_sum_lev2."','".$total_income_lev2."','$lev_per2','2','complete','complete','".$date."')");
  }          
 }
     
 }
  

 //print_r($array_level3);   
    $total_plan_sum_lev3=0;  
    if($array_level3){
      for($i = 0; $i < count($array_level3); $i++){
          $select_user_match_lev3 = mysqli_query($con, "select sum(total_price) as total_price, user_id from purchase_list where user_id='".$array_level3[$i]."'  && status='success'");
          $display_users_rows_lev3 = mysqli_fetch_array($select_user_match_lev3);
          $display_users_rows_lev3['user_id'];
          $total_plan_sum_lev3 = $display_users_rows_lev3['total_price'];

        if($display_users_rows_lev3 > 0){
          $array_level3[$i];
          $level_user3 = $level_user3 + 1; 
        }
        if($level_user3 >=4){
         
          $comission_per_leve3 = mysqli_fetch_assoc(mysqli_query($con,"select * from level_commission_plan where level='3'"));
          $lev_per3 = $comission_per_leve3['percentage'];
          $total_sum3 = $total_plan_sum_lev3 ;
          $total_income_lev3 = $total_sum3 * $lev_per3/100;

          $insert_level3_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev3['user_id']."','".$total_plan_sum_lev3."',".$total_income_lev3."','".$comission_per_leve3['percentage']."','3','complete','complete','".$date."')");
        }
     }
   }
    
//print_r($array_level4)
    $total_plan_sum_lev4=0; 
    if($array_level4){
      for($i = 0; $i < count($array_level4); $i++){
        $select_user_match_lev4 = mysqli_query($con, "select sum(total_price) as total_price ,user_id from purchase_list where user_id='".$array_level4[$i]."'  && status='success' ");
        $display_users_rows_lev4 = mysqli_fetch_array($select_user_match_lev4);
        $display_users_rows_lev4['user_id'];
        $total_plan_sum_lev4 = $display_users_rows_lev4['total_price'];

      if($display_users_rows_lev4 >0){
        $array_level4[$i]. "<br>";                //total users list of sponsers
        $level_user4 = $level_user4+ 1;

      if($level_user4 >=4){
        $comission_level_per4 = mysqli_fetch_assoc(mysqli_query($con, "select * from level_commission_plan where level='4'"));

        $lev_per4 = $comission_per_leve4['percentage'];
        $total_sum4 = $total_plan_sum_lev4;
        $total_income_lev4 = $total_sum4 * $lev_per4/100;


        $insert_level4_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev4['user_id']."','".$total_plan_sum_lev4."',".$total_income_lev4."','".$comission_per_leve4['percentage']."','4','complete','complete','".$date."')");
        }

        }
      
      }
    }
//echo $total_plan_sum_lev4;

//print_r($array_level5);
$total_plan_sum_lev5;
    if($array_level5){
     for($i = 0; $i < count($array_level5); $i++){
        $select_user_match_lev5 = mysqli_query($con, "select sum(total_price) as total_price,user_id from purchase_list where user_id='".$array_level5[$i]."'");
        $display_users_rows_lev5 = mysqli_fetch_array($select_user_match_lev5);
        $total_plan_sum_lev5 = $display_users_rows_lev5['total_price'];

      if($display_users_rows_lev5 >0){
        $array_level5[$i]. "<br>";                //total users list of sponsers
        $level_user5 = $level_user5+ 1;

      if($level_user5 >=4){
        $comission_level_per5 = mysqli_fetch_assoc(mysqli_query($con, "select * from level_commission_plan where level='5'"));

        $lev_per5 = $comission_per_leve5['percentage'];
        $total_sum5 = $total_plan_sum_lev5;
        $total_income_lev5 = $total_sum5 * $lev_per5/100;


        $insert_level5_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev5."','".$total_plan_sum_lev5."',".$total_income_lev5."','".$comission_per_leve5['percentage']."','5','complete','complete','".$date."')");
        }

        }
      
      }
    }

//print_r($array_level6);
$total_plan_sum_lev6;    
    if($array_level6){
      for($i = 0; $i < count($array_level6); $i++){
        $select_user_match_lev6 = mysqli_query($con, "select sum(total_price) as total_price,user from purchase_list where user_id='".$array_level6[$i]."'");
        $display_users_rows_lev6 = mysqli_fetch_array($select_user_match_lev6);
        $total_plan_sum_lev6 = $display_users_rows_lev6['total_price'];

      if($display_users_rows_lev6 >0){
        $array_level6[$i]. "<br>";                //total users list of sponsers
        $level_user6 = $level_user6+ 1;

      if($level_user6 >=4){
        $comission_level_per6 = mysqli_fetch_assoc(mysqli_query($con, "select * from level_commission_plan where level='6'"));

        $lev_per6 = $comission_per_leve6['percentage'];
        $total_sum6 = $total_plan_sum_lev6;
        $total_income_lev6 = $total_sum6 * $lev_per6/100; 

        $insert_level6_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev6['user_id']."','".$display_users_rows_lev6['total_price']."',".$total_income_lev6."','".$comission_per_leve6['percentage']."','6','complete','complete','".$date."')");

      }
     }
    }
  }

//print_r($array_level7);
$total_plan_sum_lev7;
    if($array_level7){
      for($i = 0; $i < count($array_level7); $i++){
        $select_user_match_lev7 = mysqli_query($con, "select sum(total_price) as total_price,user_id from purchase_list where user_id='".$array_level7[$i]."'");
        $display_users_rows_lev7 = mysqli_fetch_array($select_user_match_lev7);
        $total_plan_sum_lev7 = $display_users_rows_lev7['total_price'];

      if($display_users_rows_lev7 >0){
        $array_level7[$i]. "<br>";                //total users list of sponsers
        $level_user7 = $level_user7+ 1;

      if($level_user7 >=4){
        $comission_level_per7 = mysqli_fetch_assoc(mysqli_query($con, "select * from level_commission_plan where level='7'"));

        $lev_per7 = $comission_per_leve7['percentage'];
        $total_sum7 = $total_plan_sum_lev7;
        $total_income_lev7 = $total_sum7 * $lev_per7/100; 

        $insert_level7_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev7['user_id']."','".$total_sum7."',".$total_income_lev7."','".$comission_per_leve7['percentage']."','7','complete','complete','".$date."')");

      }
    }
  }
}

//print_r($array_level8);
$total_plan_sum_lev8;
    if($array_level8){
      for($i = 0; $i < count($array_level8); $i++){
        $select_user_match_lev8 = mysqli_query($con, "select sum(total_price) as total_price,user_id from purchase_list where user_id='".$array_level8[$i]."'");
        $display_users_rows_lev8 = mysqli_fetch_array($select_user_match_lev8);
        $total_plan_sum_lev8 = $display_users_rows_lev8['total_price'];

      if($display_users_rows_lev8 >0){
        $array_level8[$i]. "<br>";                //total users list of sponsers
        $level_user8 = $level_user8+ 1;

      if($level_user8 >=4){
        $comission_level_per8 = mysqli_fetch_assoc(mysqli_query($con, "select * from level_commission_plan where level='8'"));

        $lev_per8 = $comission_per_leve8['percentage'];
        $total_sum8 = $total_plan_sum_lev8;
        $total_income_lev8 = $total_sum8 * $lev_per8/100; 

        $insert_level8_income = mysqli_query($con, "INSERT INTO `matrix_level_income`(`mid`, `ref_id`, `total_amt`, `ref_amt`, `percent_amt`, `income_level`, `ref_status`, `pay_status`, `add_date`) VALUES ('".$id."','".$display_users_rows_lev8['user_id']."','',".$total_income_lev8."','".$comission_per_leve8['percentage']."','8','complete','complete','".$date."')");

      }
    }
  }
}


}
  
  

// binary fuction or their working starts from here:==========================================>

// function set_binary_income($user,$level){
//     global $count,$count1,$lc,$rc;
//     $con=connectdb();
//     $date=date("Y-m-d");
//     echo "parent=".$parent=get_parent($user);
//   if($parent!=""){
//     echo "lc=".$left_count=get_left_count($parent);
//     echo "rc=".$right_count=get_right_count($parent);
//     $count=0;
//     $count1=0;
//     $lc=0;
//     $rc=0;
// // for 1:1 condition
//   if($left_count==$right_count)
//   {
//     $left=left_child($parent);
//     $right=right_child($parent);
//     echo "left=";
//     print_r($left);
//     echo "right=";
//     print_r($right);
//     echo "income=".$plan_amount=get_amount($left,$right);
//     $income1=$plan_amount*5/100;
//   if($income1>0)   
//      $insert_1 = mysqli_query($con, "insert into level__income (mid, ref_id, ref_amt, percent_amt, income_level, ref_status, pay_status, add_date, total_amt) values ('".$parent."','".$_SESSION['userid']."','".$income1."','5','".$level."','complete','complete','".$date."','".$plan_amount."') ");
//     }
//   elseif($left_count==(2*$right_count))
//     {
//     $left=left_child($parent);
//     $right=right_child($parent);
//     echo "income=".$plan_amount=get_amount($left,$right);
    
//     $income1=$plan_amount*10/100;
//     if($income1>0)
//      $insert_1 = mysqli_query($con, "insert into level__income (mid, ref_id, ref_amt, percent_amt, income_level, ref_status, pay_status, add_date, total_amt) values ('".$parent."','".$_SESSION['userid']."','".$income1."','10','".$level."','complete','complete','".$date."','".$plan_amount."') ");
//     }

//     set_binary_income($parent,$level+1);
//   }
//   else{
//     return 0;
//   }
// }


session_start();

$baseurl = 'http://localhost/jagdeep/milkyway_mlm_demo/';

 function isMemberExists($mid='0',$con)
    {
    $count = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user WHERE user_id='".$mid."'"));

    if ($count[0] == 1){
         return true;
     }else{
        return false;       
    }

   }  

 function getParentId($mid ="",$con)
    {
    $count = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user WHERE user_id='".$mid."'"));
    $posid = mysqli_fetch_array(mysqli_query($con,"SELECT posid FROM user WHERE user_id='".$mid."'"));
 if ($count[0] == 1){
        return $posid[0];
     }else{
        return 0;       
    }


        
    } 
function getPositionParent($mid ="",$con)
    {
    $count = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user WHERE user_id='".$mid."'"));
    $position = mysqli_fetch_array(mysqli_query($con,"SELECT position FROM user WHERE user_id='".$mid."'"));
 if ($count[0] == 1){
         return $position[0];
     }else{
        return 0;       
    }

        
    }  



############################# LAST CHILD
  
    function getLastChildOfLR($parentUserName="",$position='',$con)
        { 
            $parentid= mysqli_fetch_array(mysqli_query($con,"SELECT user_id FROM user WHERE user_id='".$parentUserName."'"));
            $childid= getTreeChildId($parentid[0], $position,$con); 
            
            if($childid!="-1"){
                 $mid=$childid;
              } 
            else
              {
                 $mid=$parentid[0];
              }
              
            $flag=0;
            
            while($mid!=""||$mid!="0")
            {
                if(isMemberExists($mid,$con))
                {   
                    $nextchildid= getTreeChildId($mid, $position,$con);
                    if($nextchildid=="-1")
                    {
                        $flag=1;
                        break;
                    } 
                    else 
                    {
                        $mid = $nextchildid;
                    }
                }//if
                else
                    break;
                    
            }//while
            return $mid;    
        }  


    function getTreeChildId($parentid="",$position="",$con)
    {
        $cou = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user WHERE posid='".$parentid."' AND position='".$position."' "));
        $cid = mysqli_fetch_array(mysqli_query($con,"SELECT user_id FROM user WHERE posid='".$parentid."' AND position='".$position."'"));
        if ($cou[0] == 1){
            return $cid[0];
        }else{
            return -1;       
        }
    }  



############################# LAST CHILD


///////////////////////// UPDATE BELOW MEMBER

///////////////////////// TREE AUTH

    function treeeee($mid='', $uid='',$con)
    {
    $formid=$mid;
     while($mid!=""||$mid!="0")
        {
            if(isMemberExists($mid,$con))
            {
                $posid=getParentId($mid,$con);
                if($posid=="0")
                break;
                $position=getPositionParent($mid,$con);   
                if($posid==$uid){
                    return true;
                }
              $mid =$posid;
                
            } else {
                break;
            }
                
        }//while       
        return 0;   
        
    }  
    
    
        function updateMemberBelow($mid='', $type='',$con)
    {
   
//$con= connectdb();
 $formid=$mid;
      
      
  while($mid!=""||$mid!="0")
        {
            if(isMemberExists($mid,$con))
            {
                $posid=getParentId($mid,$con);
                if($posid=="0")
                break;
                
                $position=getPositionParent($mid,$con);   

$currentCount = mysqli_fetch_array(mysqli_query($con,"SELECT lpaid, rpaid, lfree, rfree FROM member_below WHERE mid='".$posid."'"));


$new_lpaid = $currentCount[0];
$new_rpaid = $currentCount[1];
$new_lfree = $currentCount[2];
$new_rfree = $currentCount[3];

                        
                if($position=="l")
                {

                    if($type=='FREE'){
                            $new_lfree = $new_lfree+1;
                    }else{
                            $new_lpaid = $new_lpaid+1;
                    }

                }
                else
                {

                    if($type=='FREE'){
                            $new_rfree = $new_rfree+1;
                    }else{
                            $new_rpaid = $new_rpaid+1;
                    }
                }   
                


mysqli_query($con,"UPDATE member_below SET lpaid='".$new_lpaid."', rpaid='".$new_rpaid."', lfree='".$new_lfree."', rfree='".$new_rfree."' WHERE mid='".$posid."'");





                $mid =$posid;
                
            } else {
                break;
            }
                
        }//while       
        return 0;   
        
    } 


  function updateDepositBV($mid='', $deposit_amount=0,$con)
    {
   
//$con= connectdb();
   $formid=$mid;
      
      
  while($mid!=""||$mid!="0")
        {
            if(isMemberExists($mid,$con))
            {
                $posid=getParentId($mid,$con);
                if($posid=="0")
                break;
                
                $position=getPositionParent($mid,$con);   

$currentBV = mysqli_fetch_array(mysqli_query($con,"SELECT lbv, rbv FROM member_bv WHERE mid='".$posid."'"));
                   
//echo "$posid - $position ----<br/> ";
                        
                if($position=="l")
                {
                    $new_lbv=$currentBV[0]+$deposit_amount; 
                    $new_rbv=$currentBV[1]; 
                }
                else
                {
                    $new_lbv=$currentBV[0]; 
                    $new_rbv=$currentBV[1]+$deposit_amount;
                }   
                


mysqli_query($con,"UPDATE member_bv SET lbv='".$new_lbv."', rbv='".$new_rbv."' WHERE mid='".$posid."'");
  $mid =$posid;
                
            } else {
                break;
            }
                
        }//while       
        return 0;   
        
    } 

  function updatePaid($mid,$con)
    {
   $formid=$mid;
      
      
  while($mid!=""||$mid!="0")
        {
            if(isMemberExists($mid,$con))
            {
                $posid=getParentId($mid,$con);
                if($posid=="0")
                break;
                
                $position=getPositionParent($mid,$con);   

$currentCount = mysqli_fetch_array(mysqli_query($con,"SELECT lpaid, rpaid, lfree, rfree FROM member_below WHERE mid='".$posid."'"));


$new_lpaid = $currentCount[0];
$new_rpaid = $currentCount[1];
$new_lfree = $currentCount[2];
$new_rfree = $currentCount[3];

                        
                if($position=="L")
                {

                            $new_lfree = $new_lfree-1;
                            $new_lpaid = $new_lpaid+1;

                }
                else
                {

                            $new_rfree = $new_rfree-1;
                            $new_rpaid = $new_rpaid+1;
                }   
                


mysqli_query($con,"UPDATE member_below SET lpaid='".$new_lpaid."', rpaid='".$new_rpaid."', lfree='".$new_lfree."', rfree='".$new_rfree."' WHERE mid='".$posid."'");





                $mid =$posid;
                
            } else {
                break;
            }
                
        }//while       
        return 0;   
        
    }  
function printBV($con,$mid)
{
echo  '<li class="clear_all"><p class = "lef"> Userid </p> <p class="lef extra_add">'.$mid.'</p> </li>';
//   echo  '<li class="clear_all"><p class = "lef"> Group </p> <p class="lef extra_add">L- '.$cbv[0].' | R-'.$cbv[1].'</p> </li>';
}
function printpackage($con,$mid)
{
$ddata=mysqli_query($con,"SELECT * FROM `purchase_package` where user_id='".$mid."' and status='success' order by id asc  ");
$data=mysqli_fetch_array($ddata);
if($data['date']!=''){
echo  '<li class="clear_all"><p class = "lef"> Active Since </p> <p class="lef extra_add">'.date('Y-m-d',strtotime($data['date'])).' </p> </li>';
}
else
{
echo  '<li class="clear_all"><p class = "lef"> Active Since </p> <p class="lef extra_add">0000-00-00 </p> </li>';
}
}
function printinvest($con,$mid)
{
   // $con= connectdb();
    $bmbr = mysqli_fetch_array(mysqli_query($con,"SELECT * FROM user WHERE user_id='".$mid."'"));
    $data=mysqli_fetch_array(mysqli_query($con,"SELECT sum(amount) as invest FROM `purchase_package` where user_id='".$bmbr['user_id']."' and status='success'"));
    if($data['invest']!='')
    {
    echo  '<li class="clear_all"><p class = "lef"> Invested Amount </p> <p class="lef extra_add">$ '.$data['invest'].'</p> </li>';
    }
    else
    {
      echo  '<li class="clear_all"><p class = "lef"> Invested Amount </p> <p class="lef extra_add">$ 0</p> </li>';  
    }
}

function printBelowMember($con,$mid)
{
    $bmbr = mysqli_fetch_array(mysqli_query($con,"SELECT lpaid, rpaid, lfree, rfree FROM member_below WHERE mid='".$mid."'"));
    echo  '<li class="clear_all"><p class = "lef"> Active User </p> <p class="lef extra_add"> L-'.$bmbr[0].' | R-'.$bmbr[1].' </p> </li>';
    echo  '<li class="clear_all"><p class = "lef"> InActive User</p> <p class="lef extra_add">L-'.$bmbr[2].' | R-'.$bmbr[3].'</p> </li>';
}
function dailyReturn($con){
    $date = date('Y-m-d h:i:s');
    
  $sql = mysqli_query($con,"SELECT * FROM `lock_daily_return` WHERE roi_start_date<='".$date."' and roi_expiry_date>='".$date."'  && status='success'");
  
  $arrdata = array();
  
  while($sql_row = mysqli_fetch_array($sql)){
  
          $peramt = $sql_row['amount']*$sql_row['percent']/100;
    
            $obj = new stdClass();
            
            $obj->user_id = $sql_row['user_id'];
            $obj->package_amount = $sql_row['amount'];
            $obj->percent = $sql_row['percent'];
            $obj->daily_return =  $sql_row['amount']*$sql_row['percent']/100;
            $obj->expiry_date = $sql_row['roi_expiry_date'];
            $obj->lock_date = $sql_row['roi_lock_date'];
            
            array_push($arrdata,$obj);  
         
  }
  
  foreach($arrdata as $sql_row){
       echo $sql_row->user_id;
       $sql = mysqli_query($con,"INSERT INTO `daily_return_user_list`(`user_id`, `package_id`, `package_amount`, `percent`, `daily_return`, `expire_date`,`date`)
       VALUES ('".$sql_row->user_id."','','".$sql_row->package_amount."','".$sql_row->percent."','".$sql_row->daily_return."','".$sql_row->expiry_date."','".date('Y-m-d h:i:s')."')");
  }
  
}
/* direct income  */
function directIncome($con,$packageId,$amount,$order_id,$userid,$status){

  $qry = mysqli_fetch_array(mysqli_query($con,"SELECT * FROM `direct_percent`"));

  $directIncomeAmount = $amount * $qry['percent'] / 100;

  $query = mysqli_fetch_array(mysqli_query($con,"SELECT * FROM `user` WHERE `user_id`='$userid'"));
if($query['sponsor_id']!='')
{
   // echo "INSERT INTO `direct_income_user_list`(`order_id`, `user_id`, `sponsor_id`, `package_amount`, `percent`, `direct_amount`, `status`) VALUES ('".$order_id."','$userid','".$query['sponsor_id']."','".$amount."','".$qry['percent']."','".$directIncomeAmount."','".$status."')";die();
  $directIncomeInsert = mysqli_query($con,"INSERT INTO `direct_income_user_list`(`order_id`, `user_id`, `sponsor_id`, `package_amount`, `percent`, `direct_amount`, `status`) VALUES ('".$order_id."','$userid','".$query['sponsor_id']."','".$amount."','".$qry['percent']."','".$directIncomeAmount."','".$status."')");

  if($directIncomeInsert){
    echo "<script>window.location.href='index.php'</script>";
    }

  else{
    echo "<script>alert('Some Issue Occured !');</script>";
  }
 }
  
}





?>
}



 ?>