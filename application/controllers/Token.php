<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
class Token extends CI_Controller
{
    
	private $logged_in;
	
	public function __construct()
		{
		    parent::__construct();
		    
		    
		    $this->load->library('encryption');
		    
		    if(!$this->session->userdata('logged_in'))
			{
				redirect('home');
			}
			else
			{
				$this->logged_in=$this->session->userdata('logged_in');
			}
		    
		    $this->load->model('token_model');
		
	}
		
	public function tokenPrice($amt=1){
	    echo token_price($amt);   
	}
	
	
	public function btc_price($amt){
	    echo usd_to_btc($amt);
	}
	
	public function buy_token(){
	    $data['logged_in']=$this->logged_in;
	    
        $this->load->view('user/panel/top-header',$data);
        $this->load->view('user/panel/header',$data);
        $this->load->view('user/panel/left-sidebar',$data);
        $this->load->view('user/buy_token');
        $this->load->view('user/panel/footer',$data);
	}
	
// 	public function token_value()
// 		{
// 			$data['logged_in']=$this->logged_in;
// 			$this->load->view('user/panel/top-header',$data);
// 			$this->load->view('user/panel/header',$data);
// 			$this->load->view('user/panel/left-sidebar',$data);
// 			$this->load->view('user/token-value');
// 			$this->load->view('user/panel/footer',$data);
// 	    }
	
	public function my_tokens()
		{
				$data['logged_in']=$this->logged_in;
				
				$data['token_history']=$this->token_model->get_token_transation($this->logged_in->user_id);
				
				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/my-tokens');
				$this->load->view('user/panel/footer',$data);
	    }
	    
	
	public function buy_token_submit(){
	    
	   $key=strtoupper(hash("sha256",date('Ymdhis')));
	    
	   if($this->input->post('amount')){
	       
	        $status='Success'; 
	       
            $data = array(
                    'transaction_key'=>$key,
                    'to_wallet_address'=>$this->logged_in->user_id,
                    'value'=>$this->input->post('coin_qty'),
                    'amount'=>$this->input->post('amount'),
                    'date_'=>date('Y-m-d H:i:s'),
                    'from_wallet_address'=>'admin',
                    'transaction_type'=>'2', // 2= purchase
                    'status'=>$status,
                );
                
            if($this->token_model->token_transation($data)){
                $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success !</strong></div>');
            }
            else{
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Failed !</strong></div>');
            }
	   }
	   
	   redirect('token/buy_token');
	   
	}
	
		public function token_value_list()
		{
             
		    $data['logged_in'] = $this->logged_in;
	        $data['token_list'] = $this->token_model->user_get_token_value_list();
	        
            $this->load->view('user/panel/top-header',$data);
            $this->load->view('user/panel/header',$data);
            $this->load->view('user/panel/left-sidebar',$data);
            $this->load->view('user/token-value',$data);
            $this->load->view('user/panel/footer',$data);
		}
		public function token_sold_history(){

  				$data['logged_in']=$this->logged_in;
  				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/token-sell-history');
				$this->load->view('user/panel/footer',$data);
  				}
		public function withdraw_token(){

  					$data['logged_in']=$this->logged_in;
  					$this->load->view('user/panel/top-header',$data);
					$this->load->view('user/panel/header',$data);
					$this->load->view('user/panel/left-sidebar',$data);
					$this->load->view('user/withdraw-BCO',$data);
					$this->load->view('user/panel/footer',$data);
  				}



  		public function withdraw_token_submit(){
	    
		   	$key=strtoupper(hash("sha256",date('Ymdhis')));
		    $todate=date('Y-m-d H:i:s', mktime(date('h'),date('i'),date('s'),date('m'),date('d')+45,date('Y')));
		   	if($this->input->post('amount')){
		       
		        $status='Success'; 
		       
	            $data = array(
                    'transaction_key'=>$key,
                    'to_wallet_address'=>'admin',
                    'value'=>$this->input->post('coin_qty'),
                    'amount'=>$this->input->post('amount'),
                    'date_'=>date('Y-m-d H:i:s'),
                    'to_date'=>$todate,
                    'from_wallet_address'=>$this->logged_in->user_id,
                    'transaction_type'=>'1', // 2= purchase
                    'status'=>$status,
                );
                
            if($this->token_model->token_transation($data)){
                $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success !</strong></div>');
            }
            else{
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Failed !</strong></div>');
            }
	   }
	   
	   redirect('token/buy_token');
	   
	}
	
}

