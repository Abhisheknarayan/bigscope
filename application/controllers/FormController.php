<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class FormController extends CI_Controller {
 
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->helper('url');
       $this->load->library('session');
    }
 
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index()
    {
        $this->load->view('formPost');
    }
 
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function formPost()
    {
        
        
        $data = array(
            'captch' => $this->input->post('g-recaptcha-response'),
            'userid' => $this->input->post('exampleInputEmail1'),
            'password' => $this->input->post('exampleInputPassword1'),
            'ipaddress' => $this->input->ip_address(),
            'secret' => $this->config->item('google_secret')
        );
            
        
        
        $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
 
        $userIp=$this->input->ip_address();
     
        $secret = $this->config->item('google_secret');
   
        $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
         
        $status= json_decode($output, true);
 
        if ($status['success']) {
            echo "<pre>"; print_r('hhiii'); die(); 
            print_r('Google Recaptcha Successful');
            exit;
        }else{
            $this->session->set_flashdata('flashError', 'Sorry Google Recaptcha Unsuccessful!!');
        }
 
        redirect('form', 'refresh');
    }   
}