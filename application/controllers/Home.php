<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    function _construct(){
        parent::_construct();
        $this->load->model('Home_model');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('income');
        $this->load->database();
    }

    public function index(){
       /*  $ip= $_SERVER['REMOTE_ADDR'];
         

// Use JSON encoded string and converts 
// it into a PHP variable 
$ipdat = @json_decode(file_get_contents( 
	"http://www.geoplugin.net/json.gp?ip=" . $ip)); 
	$date=date('Y-m-d h:i:s');

 'Country Name: ' . $ipdat->geoplugin_countryName . "\n"; 
 'City Name: ' . $ipdat->geoplugin_city . "\n"; 
 'Continent Name: ' . $ipdat->geoplugin_continentName . "\n"; 
 'Latitude: ' . $ipdat->geoplugin_latitude . "\n"; 
 'Longitude: ' . $ipdat->geoplugin_longitude . "\n"; 
'Currency Symbol: ' . $ipdat->geoplugin_currencySymbol . "\n"; 
 'Currency Code: ' . $ipdat->geoplugin_currencyCode . "\n"; 
'Timezone: ' . $ipdat->geoplugin_timezone; 
 'browser:' .$_SERVER['HTTP_USER_AGENT'];
 'User IP: '.$ip;
 $user_ip=$this->Home_model->check_user_ip($ip);
 if($user_ip){
   
 }
 else{
 $data=array(
     'ip_address'=>$ip,
     'country '=>$ipdat->geoplugin_countryName,
     'city'=>$ipdat->geoplugin_city,
     'continent_name'=> $ipdat->geoplugin_continentName,
     'latitude'=>$ipdat->geoplugin_latitude ,
     'longitude'=>$ipdat->geoplugin_longitude ,
     'currency_symbol'=> $ipdat->geoplugin_currencySymbol,
     'currency_code'=>$ipdat->geoplugin_currencyCode,
     'timezone'=>$ipdat->geoplugin_timezone,
     'browser'=>$_SERVER['HTTP_USER_AGENT'],
     'created_date'=>$date,
     'updated_date'=>$date,
     
     );
    $this->Home_model->insert_user_ip($data);
}*/

        $this->load->view('index');
    
    }
    
    public function privacy_policy(){
        
        $this->load->view('privacy_policy');
    }
    public function login(){
        
        $this->load->view('user/login');
    }
    public function admin(){
        
        $this->load->view('admin/login');
    }
        
    public function registration()
        {
            
            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
            
            $userIp=$this->input->ip_address();
            
            $secret = $this->config->item('google_secret');
            
            $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
            
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);      
             
            $status= json_decode($output, true);
            
            if($status['success']) 
                {  
                    $date=date('Y-m-d h:i:s');
                    $UserId = 'BI'.mt_rand(10000,99999); //random user id generation
                    $str = "avBwPxCyZbfgDhiSjkPlmZnrNsFtu";
                    $str=substr(str_shuffle($str),0,4);//for unique password
                    $str1='@';
                    $Password = $str.$str1.mt_rand(100,999);
                    $user_type='user';
                    $sponserid=$this->input->post('sponser');
                    
                    if($sponserid==''){
                        $sponserid=$this->Home_model->adminid();
                    }
                    else if($sponserid!==''){
                        $validateSponser=$this->Home_model->checkSponser($sponserid);
                        if(!$validateSponser){
                            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Registration failed ! </strong>Sponser id is incorrect !</div>');
                            redirect('home/register');
                        }
                    }
                    else{
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Registration failed ! </strong>Something went wrong!</div>');
                        redirect('home/register');
                    }
                    
                        $greenid=$this->Home_model->checkGreenid($this->input->post('green_id'));
                
                        if($greenid){
                                $merge_mobile = $this->input->post('mobile_code').''.$this->input->post('mobile');
                                $mdpassword = md5($Password);
                                $data = array(
                                    'user_id'=>$UserId,
                                    'user_position'=>$this->input->post('position'),
                                    'user_posid'=>$this->Home_model->getLastChildOfLR($sponserid,$this->input->post('position')),
                                    'sponser_id'=>$sponserid,
                                    'user_type'=>$user_type,
                                    'first_name' =>$this->input->post('firstname'),
                                    'last_name'=>$this->input->post('lastname'),
                                    'user_email'=>$this->input->post('email'),
                                    'user_mobile'=>$merge_mobile,
                                    'password'=>$mdpassword,
                                    'confirm_password'=>$mdpassword,
                                    'green_pin'=>$this->input->post('green_id'),
                                    // 'user_status'=>$this->input->post('user_status'),
                                    'joining_date'=>$date,
                                    // 'last_login_date'=>$this->input->post('login_date'),
                                );
                                
                                $this->session->set_userdata($data);
                                $uniqiq_id = $this->Home_model->insert($data);
                                $this->session->set_flashdata('user_login','please check your mail to login');
                                $data=array(
                                    'username'=>$this->input->post('firstname'),
                                    'last_name'=>$this->input->post('lastname'),
                                    'password'=>$Password,
                                    'candidate_id'=>$UserId,
                                    'base_url'=>'http://bigscope.co/bigscope/home/login',
                                    'confirm_id'=>$UserId,
                                    'register_date'=>$date,
                                    'email'=>$this->input->post('email'),
                                    'uniqiq_id'=>$uniqiq_id
                                );
                                $this->db->where('greenid',$this->input->post('green_id'))->update('activation_code',array('status'=>'0'));
                                $this->load->view('mail_function',$data,true);
                                redirect('home/success');           
                        }
                    else
                        {
                            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Registration failed ! </strong> Wrong activation code Please Check your activation code and try again</div>');
                            redirect('home/register');
                        }
                }
            else{
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Failed</strong>Sorry Google Recaptcha Unsuccessfull!</div>');
                    redirect('home/register');
                }
        }   
        
        public function referal(){
            $currentURL = current_url();
            $this->load->view('user/register');
        }

        public function signin(){
            
            if(!empty($this->input->post('userid')) || !empty($this->input->post('password')) ){  
                
                $data = array(
                    'user_id' =>$this->input->post('userid') ,
                    'password'=>md5($this->input->post('password')),
                );
                $result = $this->Home_model->login($data);
                //echo "<pre>"; print_r($result); die();
    
                if ($result){
                    
                    // Add user data in session
                    $this->session->set_userdata('logged_in',$result);
                    $this->session->set_flashdata('user_registered', 'Welcome To! Dashboard');
                    redirect('user/dashboard');
                    
                }else{
                       
                    $this->session->set_flashdata('user_registered', 'user id and password is wrong please check.');
                
                    redirect('home/login');
                    
                }
                
            }else{
                
                $this->session->set_flashdata('user_registered', 'please enter you user id and password.');
                redirect('home/login');             
                
            }
        }
        
        public function admin_signin(){
            
            $data = array(
                'username' =>$this->input->post('username'),
                'password'=>$this->input->post('password'),
            );

            $result = $this->Home_model->admin_login($data);

            if ($result){
                
                // Add user data in session
                $this->session->set_userdata('admin_logged_in',$result);
                $this->session->set_flashdata('user_registered', 'Welcome To! Dashboard');
                redirect('admin/dashboard');
                
            }else{
                if($data['username']==''){
                    $this->session->set_flashdata('user_registered', 'Login failed! Please check your details.');
                }else{

                    $this->session->set_flashdata('user_registered', 'Login failed! Please check your details.');
                    redirect('home/admin');
                }
            }
        }
        

        public function logout(){
            
            session_destroy();
            redirect('home');
        }
        public function success(){
            
            $this->load->view('user/success');
        }
        public function register(){
            
            $data['country_code'] = $this->Home_model->get_country_code();
            $this->load->view('user/register',$data);
        }

        

        public function email_verfiy($id){

            $user=base64_decode($id);
            $user_data = $this->Home_model->get_user($user);    

            $dataa = array(
                'user_status' => 'active'
            );

            $data['fname'] = $user_data[0]->first_name;
            $data['lname'] = $user_data[0]->last_name;

            $result = $this->Home_model->update_user($dataa,$user);     
            
            if ($result == TRUE) {
                $this->load->view('user/email_verfiy', $data);
            } else {
                redirect('');
            }


        }
        
        
        
        public function forgot_password(){

            $this->load->view('user/forgot_password');


        }
        
        
        public function forgot_password_mail(){
            
            //echo "<pre>"; print_r($user_id); die();
            
            
            if($this->input->post('userid') =='' || $this->input->post('email_id') ==''){
                
                $this->session->set_flashdata('forgot_password', 'please field userid and email Id');
                
                redirect('home/forgot_password');
                
            }
            else{
                $user_id = $this->input->post('userid');
                $user_email = $this->input->post('email_id');
                
                
                $result = $this->db->query("select * from users where user_id='$user_id' and `user_email`='$user_email' and user_status='active' ");    
                
                
                if($result->num_rows() > 0){
                    
                    $date=date('Y-m-d h:i:s');
                    $UserId = 'BI'.mt_rand(10000,99999); //random user id generation
                    $str = "avBwPxCyZbfgDhiSjkPlmZnrNsFtu";
                    $str=substr(str_shuffle($str),0,4);//for unique password
                    $str1='@';
                    $Password = $str.$str1.mt_rand(100,999);
                    
                    $data_hai = array(
                        'password' => md5($Password),
                        'confirm_password' => md5($Password)
                    ); 
                    
                    $data=array(
                        'username'=>$result->result_array()[0]['first_name'],
                        'last_name'=>$result->result_array()[0]['last_name'],
                        'password'=>$Password,
                        'candidate_id'=>$result->result_array()[0]['user_id'],
                        'base_url'=>'http://bigscope.co/bigscope/home/login',
                        'confirm_id'=>$result->result_array()[0]['user_id'],
                        'register_date'=>$date,
                        'email'=>$result->result_array()[0]['user_email'],
                        'uniqiq_id'=>$result->result_array()[0]['id']
                    ); 
                    
                    $idd = $result->result_array()[0]['id'];
                    
                    $result_ok = $this->Home_model->update_forgot_pass($data_hai,$idd); 
                    
                    if($result_ok == TRUE){
                        
                        $this->load->view('user/forgot_mail',$data,true);
                        
                        $this->session->set_flashdata('forgot_password', 'Your password is send to your registered Email ID Please check');
                        redirect("home/forgotpass_vereify/$idd");                       
                        
                    }else{
                        $this->session->set_flashdata('forgot_password', 'mail no sent ');
                        redirect('home/forgot_password');                       
                        
                    }
                    
                }else{
                   
                    $this->session->set_flashdata('forgot_password', 'user userid or email id are wrong!');
                    redirect('home/forgot_password'); 
                }
                  
            }
                
        }
        
        
        public function forgotpass_vereify($id = null){
            
            
            if(!empty($id) || $id !== null ){
                $user_data = $this->Home_model->get_user($id);
                $data['fname'] = $user_data[0]->first_name;
                $data['lname'] = $user_data[0]->last_name;                 
                
            }else{
                $data['fname'] = '';
                $data['lname'] = ''; 
                
            }
            
            $this->load->view('user/forgotpass_verfiy',$data);
            
        }
            
        function get_daily_return(){
            $result=$this->db->get('users')->result();
            foreach($result as $row){
                daily_return($row->user_id);
            }
        } 
        
        public function send_enquiry(){
             $date=date('Y-m-d h:i:s');
            $data = array(
                'name' =>$this->input->post('name'),
                'email'=>$this->input->post('email'),
                'comment'=>$this->input->post('comment'),

               
                'date'=> $date
                 );
            $result=$this->Home_model->insert_enquiry($data);
            if ($result==true) {
                $this->session->set_flashdata('enquiry','thank you for your interest,your message submited successfully we will reach you soon');  
                $this->load->view('admin/panel/mail_function',$data,true);
                redirect('Home');
            }
            else
            {
                $this->session->set_flashdata('enquiry','Error! your message not sent please try again'); 
                redirect('Home'); 
            }
        }
            
}