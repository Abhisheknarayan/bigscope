<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Binary extends CI_Controller {
		
		private $logged_in;

		public function __construct()
		{
			// auth
			parent::__construct();
			$this->load->model('binary_model');
			$this->load->library('business_value');
			if(!$this->session->userdata('logged_in'))
			{
				//redirect('home');
			}
			else
			{
				$this->logged_in=$this->session->userdata('logged_in');
			}
		}
		
		public function tree(){
		    if(!$this->session->userdata('logged_in'))
			{
				redirect('home');
			}
            $data['ci']=$this;
            $data['user_id']=$this->logged_in->user_id;
            $this->load->view('user/panel/top-header',$data);
            $this->load->view('user/panel/header',$data);
            $this->load->view('user/panel/left-sidebar',$data);
            $this->load->view('user/binary-tree',$data);
            $this->load->view('user/panel/footer',$data);
		}
		
	    public function binary(){
	        //$this->db->query("DELETE FROM income_status");
	        //$this->db->query("DELETE FROM `my_income1`");
	        //$users=array('BI48052');
	        $this->load->library('binary_income');
	        $users=$this->db->select("user_id")->get('users')->result();
	        foreach($users as $row){
    	          $data= $this->binary_income->findLatestUser($row->user_id);
    	          $this->binary_income->generateBinary();
	        }
	    }   
		
	}