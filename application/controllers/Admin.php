<?php 

defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller
{
	private $admin_logged_in;
	
		public function __construct()
		{
			// auth
			parent::__construct();
			$this->load->model('admin_model');
			$this->load->model('user_model');
			$this->load->library('session');

			if(!$this->session->userdata('admin_logged_in'))
			{
				 redirect('home');	
			}
			else
			{
				$this->admin_logged_in=$this->session->userdata('admin_logged_in');
			}
		}
	
    	public function index()
    	{
    	    $this->load->view('admin/login');
    	}
    
    	
    
    	public function dashboard(){
        	 $data['admin_logged_in']=$this->admin_logged_in;
        	 $this->load->view('admin/panel/top-header',$data);
        	 $this->load->view('admin/panel/header',$data);
        	 $this->load->view('admin/panel/left-sidebar',$data);
        	 $this->load->view('admin/index',$data);
        	 $this->load->view('admin/panel/footer',$data);
    	}
    	
    	/*jai code edit start here*/
    	
    	
    	public function support_ticket_history(){
    	    
    		$data['admin_logged_in']=$this->admin_logged_in;
    		$data['ticket_his']=$this->admin_model->support_ticket_history();
    		//echo "<pre>"; print_r($datajd->result()); die();
    		
            $this->load->view('admin/panel/top-header',$data);
    		$this->load->view('admin/panel/header',$data);
    		$this->load->view('admin/panel/left-sidebar',$data);
    		$this->load->view('admin/support-history',$data);
    		$this->load->view('admin/panel/footer',$data);
    			
    	}
    
    	public function change_tick_status(){
           
            if(!empty($_POST['support']) || !empty($_POST['action']) ){
                
             	$id=$_POST['support'];
             	$action=$_POST['action'];
            
             	$data = array('status' => $action);
             	$update = $this->admin_model->update_tickect_status($data,$id);
            
             	if($update){
             		echo '1';
             	}
             	else{
             		echo '0';
             	}            
              	
            }		
    	
    	}
    	
    	
    	public function delete_ticket(){
           
            if(!empty($_POST['support']) ){
                
             	$id=$_POST['support'];
             	$delete = $this->admin_model->delete_ticket($id);
            
             	if($delete){
             		echo '1';
             	}
             	else{
             		echo '0';
             	}            
              	
            }		
    	
    	}	
    	
    	public function admin_tickect_replay(){
    	    
		    if(!empty($this->input->post('support_id')) || !empty($this->input->post('message'))){
		       
                $data=array(
                    'support_id'=>$this->input->post('support_id'),
                    'reply_by '=>1,
                    'message '=>$this->input->post('message')
                ); 
                $result = $this->admin_model->inset_suport_replay($data);
                if($result == TRUE){
                    
                    $this->session->set_flashdata('inset_message', 'Sent message successfully');
                    redirect('admin/support_ticket_history');                       
                    
                }else{
                    $this->session->set_flashdata('inset_message', 'Somthing went wrong ');
                    redirect('admin/support_ticket_history');                       
                    
                } 
                
            }else{
                
                $this->session->set_flashdata('inset_message', 'please field mendatroy field ! ');
                redirect('admin/support_ticket_history');
                
            }             
 
    	} 
    	
    	
    	public function withdraw_request(){
    	    
    	    $data['all_withrowal'] = $this->admin_model->get_all_withrawal();
    	    //$dahhh = $this->admin_model->get_all_withrawal();
    	    //echo "<pre>"; print_r($dahhh); die();
			$data['admin_logged_in']=$this->admin_logged_in;
	        $this->load->view('admin/panel/top-header',$data);
			$this->load->view('admin/panel/header',$data);
			$this->load->view('admin/panel/left-sidebar',$data);
			$this->load->view('admin/withdraw-request',$data);
			$this->load->view('admin/panel/footer',$data);
    			
    	}    
    	
    	
    	public function cancel_withdrawal(){
    	    
		    if(!empty($this->input->post('withd_id'))){
		        $id = $this->input->post('withd_id');
                $data=array(
                    'message'=>$this->input->post('message'),
                    'status '=>0,
                ); 
                $result = $this->admin_model->withdrowal_cancel($data,$id);
                if($result == TRUE){
                    
                    $this->session->set_flashdata('withdwal_message', 'Cancel withdrawal successfully');
                    redirect('admin/withdraw_request');                       
                    
                }else{
                    $this->session->set_flashdata('withdwal_message', 'Somthing went wrong ');
                    redirect('admin/withdraw_request');                       
                    
                } 
                
            }else{
                
                $this->session->set_flashdata('withdwal_message', 'please field mendatroy field ! ');
                redirect('admin/withdraw_request');
                
            }             
 
    	}   
    	
    	
        public function accept_withdrawal(){
    	    
    	    
		    if(!empty($this->input->post('withd_id'))){
		        $id = $this->input->post('withd_id');
                $data=array(
                    'message'=>$this->input->post('message'),
                    'status '=>1,
                ); 
                $result = $this->admin_model->withdrowal_accept($data,$id);
                if($result == TRUE){
                    
                    $this->session->set_flashdata('withdwal_message', 'Cancel withdrawal successfully');
                    redirect('admin/withdraw_request');                       
                    
                }else{
                    $this->session->set_flashdata('withdwal_message', 'Somthing went wrong ');
                    redirect('admin/withdraw_request');                       
                    
                } 
                
            }else{
                
                $this->session->set_flashdata('withdwal_message', 'please field mendatroy field ! ');
                redirect('admin/withdraw_request');
                
            }             
 
    	}  	
    	
    	
    	
    	/*jai code edit end here*/
    	
    	 public function users()
    	 {
			$data['admin_logged_in']=$this->admin_logged_in;
			$data['user']=$this->admin_model->get_user_list();
			$this->load->view('admin/panel/top-header',$data);
			$this->load->view('admin/panel/header',$data);
 			$this->load->view('admin/panel/left-sidebar',$data);
			$this->load->view('admin/user-list',$data);
			$this->load->view('admin/panel/footer',$data);
    			
    	}
    	public function paid_users()
    	{
    	    $data['admin_logged_in']=$this->admin_logged_in;
			$data['paidusers'] = $this->admin_model->get_paid_list();
			
			$this->load->view('admin/panel/top-header');
			$this->load->view('admin/panel/header');
 			$this->load->view('admin/panel/left-sidebar');
			$this->load->view('admin/paid-users',$data);
			$this->load->view('admin/panel/footer');
    	}
    	
    	public function unpaid_users()
    	{
    	    $data['admin_logged_in']=$this->admin_logged_in;
			$data['unpaidusers'] = $this->admin_model->get_unpaid_list();
			
			$this->load->view('admin/panel/top-header');
			$this->load->view('admin/panel/header');
 			$this->load->view('admin/panel/left-sidebar');
			$this->load->view('admin/unpaid-users',$data);
			$this->load->view('admin/panel/footer'); 
    	}
    	public function profile()
    	 {
 			$data['admin_logged_in']=$this->admin_logged_in;
			$this->load->view('admin/panel/top-header',$data);
 			$this->load->view('admin/panel/header',$data);
 			$this->load->view('admin/panel/left-sidebar',$data);
 			$this->load->view('admin/profile',$data);
			$this->load->view('admin/panel/footer',$data);
    			
    	 }
    	 public function view_user_profile($userid = null)
    	 {
    	    
    	    
    	    $data['list'] = $this->admin_model->get_edit_list($userid);
			$this->load->view('admin/panel/top-header');
 			$this->load->view('admin/panel/header');
 			$this->load->view('admin/panel/left-sidebar');
 			$this->load->view('admin/edit-user-profile',$data);
			$this->load->view('admin/panel/footer');
    	 }
    	 public function update_user_profile($userid)
    	 {
    	     $data = array(
    	            "first_name" => $this->input->post('fname'),
    	            "last_name" => $this->input->post('lname'),
    	            "user_email" => $this->input->post('email'),
    	            "user_mobile" => $this->input->post('mobile'),
    	            "password" => $this->input->post('pass'),
    	            "bitcoin_wallet_address" => $this->input->post('btcadd'),
    	            "user_status"=>$this->input->post('status'),
    	         );
    	          
    	     $result = $this->admin_model->user_profile_edit($userid,$data);
    	     
    	     if($result){
    	         $this->session->set_flashdata('update_user','<div class="alert alert-success">Update success!</div>');
    	        redirect("admin/users/$userid");
    	         
    	     }
    	     else{
    	        
    	         return false;
    	     }
    	 }
    	 
    	 public function transaction_history()
    	 {
    	 			$data['admin_logged_in']=$this->admin_logged_in;
    	 			$data['p_history']=$this->admin_model->get_transaction_history();
     	        	$this->load->view('admin/panel/top-header',$data);
    	 			$this->load->view('admin/panel/header',$data);
    	 			$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/transaction-history',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}
    	
    // 	public function daily_return()
    // 	{
				// $data['admin_logged_in']=$this->admin_logged_in;
		  //      $this->load->view('admin/panel/top-header',$data);
				// $this->load->view('admin/panel/header',$data);
				// $this->load->view('admin/panel/left-sidebar',$data);
				// $this->load->view('admin/daily-return',$data);
				// $this->load->view('admin/panel/footer',$data);
    			
    // 	}
    	
    	
    	public function daily_return()
    		{
    		     $result['data'] = $this->admin_model->get_daily_return();
    			 $this->load->view('admin/daily-return',$result);
    			 $this->load->view('admin/panel/header');
    			 $this->load->view('admin/panel/top-header');
    			 $this->load->view('admin/panel/left-sidebar');
    			 $this->load->view('admin/panel/footer'); 
    		}
    	
    	public function direct_income()
    	{
    	    $data['logged_in']=$this->admin_logged_in;
    		$data['data'] = $this->admin_model->get_direct_income();
			$data['admin_logged_in']=$this->admin_logged_in;
	        $this->load->view('admin/panel/top-header',$data);
			$this->load->view('admin/panel/header',$data);
			$this->load->view('admin/panel/left-sidebar',$data);
			$this->load->view('admin/direct_income',$data);
			$this->load->view('admin/panel/footer',$data);
    			
    	}
    	
    	public function daily_level_income()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/level_daily_income',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}
    	public function binary_income()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    				$data['binary']=$this->admin_model->get_binary_income();
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/binary-income',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}

    	public function daily_joining_income1()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/level_daily_income',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}
    	public function daily_referal_income()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/level_daily_income',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}

	    public function send_notifications()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/send-notification',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}
	    public function notification_history()
    	{
    				$data['admin_logged_in']=$this->admin_logged_in;
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/notification-history',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}
	
	    public function pay_by_admin()
    	{
    		$data['admin_logged_in']=$this->admin_logged_in;
    		$data['package']=$this->admin_model->get_packages();
            $this->load->view('admin/panel/top-header',$data);
    		$this->load->view('admin/panel/header',$data);
    		$this->load->view('admin/panel/left-sidebar',$data);
    		$this->load->view('admin/make-payment',$data);
    		$this->load->view('admin/panel/footer',$data);
    			
    	}
    	
        public function paid_by_admin(){
            
    		$data['admin_logged_in']=$this->admin_logged_in;
    		$data['package']=$this->admin_model->get_packages();
    		$date=date('Y-m-d h:i:s');
    		$order_id=date('Ymd').rand('0','9999');
            $id=$this->input->post('userid');
            $pac_id = $this->input->post('planid');
            
            $check = $this->db->query("SELECT * FROM `purchase_package` WHERE `user_id` = '$id' && `package_id` = '$pac_id' && `status` = 'success'");
            
            if($check->num_rows() >0){
			    
			    $this->session->set_flashdata('purchage_ticket', "<script>alert('User have already purchase this package ')</script>");
			    return redirect('admin/pay_by_admin');
			    
			}else{
			    
			    $check_upper = $this->db->query("SELECT MAX(package_id) as max_pac_id FROM `purchase_package` WHERE `user_id` = '$id' && `status` = 'success'");
			    
			    $max_pack_id = $check_upper->row()->max_pac_id;
			    
			    $check_pur_or_not = $this->db->query("SELECT * FROM `packages` WHERE id > '$max_pack_id' && '$max_pack_id' < '$pac_id' ");
			    
			    
			    if($check_pur_or_not->num_rows() > 0){
    			    
    			    
                    $data = array(
                        'user_id'=> $this->input->post('userid'),
                        'order_id' =>$order_id,
                        'package_id'=>$pac_id,
                        'name'=>$this->input->post('plan_name'),
                        'amount'=>$this->input->post('amount'),
                        'percent'=>daily_return_percent(),
                        'type'=>'Paid-by-admin',
                        'status'=>'success',
                        'date'=>$date,
                    ); 
              
                    $this->session->set_flashdata('user','Payment Success to the selected user ');
                    $this->user_model->findDirectIncome($this->input->post('userid'),$this->input->post('amount'),$order_id);
                    $this->admin_model->insert_purchase_history($data);
                    redirect('admin/transaction_history');                            			    
    			    
			    }else{
			        
    			    $this->session->set_flashdata('purchage_ticket', "<script>alert('User can not purchase lower pakage')</script>");
    			    return redirect('admin/pay_by_admin');         			        
			       
    		    }
    		    
			}	
          
        }
        
      
    	
    	public function packages()
    	{           
    	            $this->session->flashdata('success');
    				$data['admin_logged_in']=$this->admin_logged_in;
    				$data['package']=$this->admin_model->get_packages();
    		        $this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/packages',$data);
    				$this->load->view('admin/panel/footer',$data);
    			
    	}

    	public function logout()
        	{
        		echo '<pre>';
        		// print_r($this->session->userdata());
        		// if($this->session->session_destroy()){
        		// 		
        
        		// }
        		
        		session_destroy();
        
        		redirect('admin/login');
        	}
	
            public function tokens(){
  				$data['admin_logged_in']=$this->admin_logged_in;
  				//$data['code']=$this->admin_model->get_token();
				$this->load->view('admin/panel/top-header',$data);
				$this->load->view('admin/panel/header',$data);
				$this->load->view('admin/panel/left-sidebar',$data);
				$this->load->view('admin/token');
				$this->load->view('admin/panel/footer',$data);
  			}	
  				
  				
  		    public function constant($id = null)
  		    {
  		        
      			$result['id'] = $id;
      			$data['admin_logged_in'] = $this->admin_logged_in;
      			$result['data'] = $this->admin_model->get_package_details($id);
      			$this->load->view('admin/constant',$result);
      			$this->load->view('admin/panel/header');
      			$this->load->view('admin/panel/top-header');
      			$this->load->view('admin/panel/left-sidebar');
      			$this->load->view('admin/panel/footer');
  			
  			
  		    }	

      		public function packages_update($id)
      		{
    
      			$data = array(
      				'name' =>$this->input->post('name'),
      				'amount' =>$this->input->post('amount'),
      				'percent' =>$this->input->post('percent'),
      				'days' =>$this->input->post('days'),
      				'withdraw_limit' =>$this->input->post('limit')
    
      			);
      			//print_r($data);
      			$result = $this->admin_model->update_package($data,$id);
      			if($result)
      			{
      				
      				$this->session->set_flashdata('success', "SUCCESSFULLY UPDATED!"); 
      				redirect('admin/packages');
      			}
      			else
      			{
      				$this->session->set_flashdata('error', "ERROR_MESSAGE_HERE");
      				return false;
      			}
     
      		}
      		public function package_delete($id)
      		{
      		   
      		    $result = $this->admin_model->delete_package($id);
      		   if($result)
      		   {
      		        $this->session->set_flashdata('message', 'Deleted !');
                    redirect('admin/packages');
      		   }
      		   else
      		   {
      		      	$this->session->set_flashdata('error', "ERROR_MESSAGE_HERE");
      				return false; 
      		   }
      		}
      		
      		public function add_package()
      		{
      		    $data = array(
      		            "name" => $this->input->post('newname'),
      		            "amount" => $this->input->post('newamount'),
      		            "percent" => $this->input->post('newpercent'),
      		            "days" => $this->input->post('newdays'),
      		            "withdraw_limit" => $this->input->post('newwithdrawlimit')
      		        );
      		    $result = $this->admin_model->package_add($data);
      		    if($result)
      			{
      			 
                    $this->session->set_flashdata('message', 'INSERTED !');
                    redirect("admin/packages");
                    
      			}
      			else
      			{
      				$this->session->set_flashdata('error', "ERROR_MESSAGE_HERE");
      				return false;
      			}
      		    
      		}
      		
      		public function direct_percent_change()
      		{   
      		   
      		    $data['admin_logged_in']=$this->admin_logged_in;
      		    $result['data'] = $this->admin_model->get_direct_percent();
      		    
      		    
      		    $this->load->view('admin/direct_percent', $result);
      		    $this->load->view('admin/panel/header');
      			$this->load->view('admin/panel/top-header');
      			$this->load->view('admin/panel/left-sidebar');
      			$this->load->view('admin/panel/footer');
      			
      		}
      		
      		public function update_percent_change($id)
      		{   
      		  
      		  
      		    $data['admin_logged_in'] = $this->admin_logged_in;
      		    $result['data'] = $this->admin_model->update_direct_percent($id);
      		    
      		    
      		    $this->load->view('admin/update_percent',$result);
      		    $this->load->view('admin/panel/header');
      			$this->load->view('admin/panel/top-header');
      			$this->load->view('admin/panel/left-sidebar');
      			$this->load->view('admin/panel/footer');
      			
      		}
      		
      		public function update_percent()
      		{
      		    $data = array(
      		        "level" => $this->input->post('level'),
      		        "percent" => $this->input->post('percent')
      		        );
      		    $result = $this->admin_model->percent_update($data);
      		    if($result)
      		    {
      		        redirect('admin/direct_percent_change');
      		    }
      		}
      		public function delete_percent($id)
      		{
      		   $result = $this->admin_model->percent_delete($id);
      		   if($result)
      		   {
      		        $this->session->set_flashdata('message', 'Deleted !');
                    redirect('admin/direct_percent_change');
      		   }
      		   else
      		   {
      		      	$this->session->set_flashdata('error', "ERROR_MESSAGE_HERE");
      				return false; 
      		   }
      		}
      		
      		
      	public function activation_code()
          {
            $data['admin_logged_in']=$this->admin_logged_in;
           
            $date=date('Y-m-d h:i:s');
            
              $n = $this->input->post('amount');
            $i=1;
            $new_array = array();
            $a=implode($new_array);
            while( $i <= $n)
            {
            $a = mt_rand(10000000, 99999999);
            
    
              $data=
               array
               (
                'greenid' =>$a,
                'user_id'=>'admin',
                'green_id_type'=>'admin',
                'sponser_id'=>'admin',
                'amount'=>'20',
                'history'=>'no',
                'status'=>'1',
                'created_at'=>$date,
                'updated_at'=>$date,
    
                 );
              $this->admin_model->insert_activation_code($data);
              
    
              $i++;
              
    
            
            }
            redirect('admin/activation_code_history');
            
            
          
            
              }
  				public function generate_new_activation()
  				{
  					$data['admin_logged_in']=$this->admin_logged_in;
  					$this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    
      				$this->load->view('admin/generate-activation-code');
      				$this->load->view('admin/panel/footer',$data);
  				
  				}

  				public function activation_code_history()
  				{
  					$data['admin_logged_in']=$this->admin_logged_in;
  					$data['code']=$this->admin_model->get_activation_code();
    				$this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/activation-code-history');
    				$this->load->view('admin/panel/footer',$data);
  				}

  				public function used_pin()
  				{

  					$data['admin_logged_in']=$this->admin_logged_in;
  					$data['used_pin']=$this->admin_model->get_activated_user_pin();
  				
    				$this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/used-activation-code',$data);
    				$this->load->view('admin/panel/footer',$data);


  				}
  				public function unused_pin()
  				{

  					$data['admin_logged_in']=$this->admin_logged_in;
  					$data['code']=$this->admin_model->unused_activation_code();
    				$this->load->view('admin/panel/top-header',$data);
    				$this->load->view('admin/panel/header',$data);
    				$this->load->view('admin/panel/left-sidebar',$data);
    				$this->load->view('admin/unused-activation-code');
    				$this->load->view('admin/panel/footer',$data);


  				}
  				public function all_enquiry()
  				{
                    $data['admin_logged_in']=$this->admin_logged_in;
                    $this->load->view('admin/panel/top-header',$data);
                    $this->load->view('admin/panel/header',$data);
                    $this->load->view('admin/panel/left-sidebar',$data);
                    $this->load->view('admin/enquiry');
                    $this->load->view('admin/panel/footer',$data);
                }


              public function change_enquiry_status()
              {
               
                if(!empty($_POST['support']) || !empty($_POST['action']) ){
                    
                  $id=$_POST['support'];
                  $action=$_POST['action'];
                
                  $data = array('status' => $action);
                  $update = $this->admin_model->update_enquiry_status($data,$id);
                
                  if($update){
                    echo '1';
                  }
                  else{
                    echo '0';
                  }            
                    
                }   
          
            }
      
      
          public function delete_enquiry()
          {
               
                if(!empty($_POST['support']) ){
                    
                  $id=$_POST['support'];
                  $delete = $this->admin_model->delete_enquiry($id);
                
                  if($delete){
                    echo '1';
                  }
                  else{
                    echo '0';
                  }            
                    
                }   
          
          } 
       public function binary_capping()
          {

            $data['admin_logged_in']=$this->admin_logged_in;
            $data['code']=$this->admin_model->get_binary_capping1();
            $this->load->view('admin/panel/top-header',$data);
            $this->load->view('admin/panel/header',$data);
            $this->load->view('admin/panel/left-sidebar',$data);
            $this->load->view('admin/binary-capping');
            $this->load->view('admin/panel/footer',$data); 
          	

  		}

          

          public function edit_binary_capping($id = null)
          {
              
            $result['id'] = $id;
            $data['admin_logged_in'] = $this->admin_logged_in;
            $result['data'] = $this->admin_model->get_binary_capping($id);
            
            $this->load->view('admin/panel/header');
            $this->load->view('admin/panel/top-header');
            $this->load->view('admin/panel/left-sidebar');
            $this->load->view('admin/edit-binary-capping',$result);
            $this->load->view('admin/panel/footer');
        
        
          } 
          public function binary_capping_update($id)
          {
           $date= date('Y-m-d h:i:s');
    
            $data = array(
              
              'amount' =>$this->input->post('amount'),
              'capping_percent' =>$this->input->post('capping'),
              'capping_amount' =>$this->input->post('percent'),
              
              'update_date'=>$date
    
            );
            //print_r($data);
            $result = $this->admin_model->update_capping($data,$id);
            if($result)
            {
              
              $this->session->set_flashdata('success', "SUCCESSFULLY UPDATED!"); 
              redirect('admin/binary_capping');
            }
            else
            {
              $this->session->set_flashdata('error', "ERROR_MESSAGE_HERE");
              return false;
            }
     
          }
          
          
          public function token_transaction_history(){


           $data['admin_logged_in'] = $this->admin_logged_in;
            $result['data'] = $this->admin_model->get_token_transaction_history();
           
         
            
             $this->load->view('admin/token-transaction-history',$result);

            $this->load->view('admin/panel/header');
            $this->load->view('admin/panel/top-header');
            $this->load->view('admin/panel/left-sidebar');
            $this->load->view('admin/panel/footer');
        }

	
  	}
