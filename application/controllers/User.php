<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends CI_Controller {
		
		private $logged_in;
		private $get_packages;

		public function __construct()
		{
			// auth
			parent::__construct();
			$this->load->model('user_model');
			$this->load->library('session');
            $this->load->helper('form');
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->library('merchant_api',array('b1cfb79e86edd94a17642f6c4a534508','e70b865dd90ab67491cef6eb47d82ec6'));
            /*custome helper*/
            //$this->load->helper('income');
			if(!$this->session->userdata('logged_in'))
			{
				redirect('home');
			}
			else
			{
				$this->logged_in=$this->session->userdata('logged_in');
			}
		}
		
		public function dashboard(){
			$data['logged_in']=$this->logged_in;
			$id = $this->session->userdata('logged_in')->id;
			$uid = $this->session->userdata('logged_in')->user_id;
			$data['user_data'] = $this->user_model->get_user_by_id($id);
            $data['package']=$this->user_model->get_packages_list();
            $data['records'] = $this->user_model->get_package_records($uid);
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/index',$data);
			$this->load->view('user/panel/footer',$data);
	    }

	    
	    /*jai code start here*/
	    
	    
	    public function user_profile(){
	        $data['logged_in']=$this->logged_in;
	        $id = $this->session->userdata('logged_in')->id;
	        $data['user_data'] = $this->user_model->get_user_by_id($id);
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/profile',$data);
			$this->load->view('user/panel/footer',$data);
	    }
	    
		public function user_profile_update(){
		    
		    $id = $this->input->post('id');
		   
		    if(!empty($this->input->post('email')) || !empty($this->input->post('mobile'))){
		        
                $data=array(
                    'first_name'=>$this->input->post('fname'),
                    'last_name'=>$this->input->post('lname'),
                    'user_email'=>$this->input->post('email'),
                    'user_mobile'=>$this->input->post('mobile'),
                    'bitcoin_wallet_address'=> $this->input->post('bitcoin')
                );
                
                
                $result = $this->user_model->upadte_user_profie($data,$id);
                  
                if($result == TRUE){
                    
                    $this->session->set_flashdata('update_profile', 'User profile updated successfully');
                    redirect('user/user_profile');                       
                    
                }else{
                    $this->session->set_flashdata('update_profile', 'Somthing went wrong ');
                    redirect('user/user_profile');                       
                    
                } 
                
            }else{
                
                $this->session->set_flashdata('update_profile', 'please field mendatroy field ! ');
                redirect('user/user_profile');
               
                
            } 
		    
           
		}
		
		
	    public function user_change_password(){
		    $id = $this->input->post('id');
		    if(empty($this->input->post('oldpass')) || empty($this->input->post('newpass')) || empty($this->input->post('confirmpass'))){
                $this->session->set_flashdata('change_password', 'please field mendatroy field ! ');
                redirect('user/user_profile');
                
            }else{
                $oldpass = md5($this->input->post('oldpass'));
                $newpass = $this->input->post('newpass');
                $confirmpass = $this->input->post('confirmpass');
                
                if($newpass ==  $confirmpass){
                    $check_password = $this->user_model->get_user_by_id($id);
                    
                    if($check_password[0]->password == $oldpass ){
                        $data=array(
                            'password'=>md5($this->input->post('newpass')),
                            'confirm_password'=>md5($this->input->post('newpass')),
                        );    
                        $result = $this->user_model->change_password($data,$id);
                        if($result == TRUE){
                            $this->session->set_flashdata('change_password', 'your password has been changed');
                            redirect('user/user_profile');                       
                            
                        }else{
                            $this->session->set_flashdata('change_password', 'Somthing went wrong ');
                            redirect('user/user_profile');                       
                        }     
                    }else{
                        $this->session->set_flashdata('change_password', 'your current password is wrong  ');
                        redirect('user/user_profile');                          
                    }
                }else{
                    $this->session->set_flashdata('change_password', 'new password or conform password does not match  ');
                    redirect('user/user_profile');                    
                }
            } 
		}
		
	    public function withdraw(){  
		   	$id = $this->session->userdata('logged_in')->id;
	        $data['user_data'] = $this->user_model->get_user_by_id($id); 
			$data['logged_in']=$this->logged_in;
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/withdraw',$data);
			$this->load->view('user/panel/footer',$data);
		}	
		 
		public function withdraw_btc(){  
		    
		   	$id = $this->session->userdata('logged_in')->id;
	        $data['user_data'] = $this->user_model->get_user_by_id($id); 
			$data['logged_in']=$this->logged_in;
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/withdraw_btc',$data);
			$this->load->view('user/panel/footer',$data);
		}
		
        public function withdrwal_request(){
            
            $id = $this->input->post('id');
            $date = $date=date("Y-m-d");
            $admin_chage_per = withraw_admin_charge();
            
            
            if(!empty($this->input->post('balamount')) || !empty($this->input->post('btc_value')) || !empty($this->input->post('user_id')) || !empty($this->input->post('amount')) || !empty($this->input->post('btc_address')) || !empty($this->input->post('admin_btc_value'))){
               
               
               $total_pay_amt = ($this->input->post('btc_value') * (100 - $admin_chage_per)/100) ;
               $data = array(
                    'user_id' => $this->input->post('user_id'),
                    'request_amount'=>$this->input->post('amount'),
                    'btc_value'=>$this->input->post('btc_value'),
                    'btc_address'=>$this->input->post('btc_address'),
                    'admin_btc_value'=>$this->input->post('admin_btc_value'),
                    'total_pay_amt'=> $total_pay_amt,
                    'admin_charge' => $admin_chage_per,
                    'request_date' => $date
                ); 
                
                //echo "<pre>"; print_r($data); die();
                $result = $this->user_model->insert_withdrawal_btc($data);
                
                if($result == TRUE){
                    
                    $this->session->set_flashdata('wihdrawal_btc', 'withdrawal request has been submitted successfully');
                    redirect('user/withdraw_btc');                       
                    
                }else{
                    $this->session->set_flashdata('wihdrawal_btc', 'Somthing went wrong ');
                    redirect('user/withdraw_btc');                       
                    
                }                      
                
            }else{
                
                $this->session->set_flashdata('wihdrawal_btc', 'please field mendatroy field ! ');
                redirect('user/withdraw_btc');    
                
                
            }
        }
        
    	public function withdraw_history(){
    	    
    	    $id = $this->session->userdata('logged_in')->user_id;
			$data['logged_in']=$this->logged_in;
			$data['withdrwal_history'] =$this->user_model->get_withdrawal_history($id);
			//echo "<pre>"; print_r($dataokk); die();
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/withdraw-history',$data);
			$this->load->view('user/panel/footer',$data);
		} 
		
		public function daily_return($pid = null,$plan_id = null){
		    
    		    $id = $this->session->userdata('logged_in')->user_id;
    			$data['logged_in']=$this->logged_in;
    			if(empty($pid) || $pid==null ){
    			    $data['dailyreturn_roi'] = $this->user_model->get_daily_roi($id);
    			}else{
    			    $data['dailyreturn_roi'] = $this->user_model->get_daily_roi_by_id($id,$pid,$plan_id);
    			}
    			
				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/daily-return',$data);
				$this->load->view('user/panel/footer',$data);
		}
		
		
		
		public function support_ticket(){
		    
			$data['logged_in']=$this->logged_in;
			$id=$this->logged_in->user_id;
			$date=date('Y-m-d h:i:s');
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/raise-ticket',$data);
			$this->load->view('user/panel/footer',$data);
			
		}	
		
		
		public function insert_support_ticket(){

			$this->form_validation->set_rules('subject','Subject field','trim|required');
		    $this->form_validation->set_rules('message','Message field','trim|required');
		    $this->form_validation->set_rules('files','Files field','trim|required');

		    if(!empty($this->input->post('subject')) || !empty($this->input->post('message')) || !empty($this->input->post('files')) ){
                
                $id = $this->input->post('id');
		        $subject = $this->input->post('subject');
		        $message = $this->input->post('message');
		        $user_id = $this->session->userdata('logged_in')->user_id;
                
	            if (isset($_FILES["files"]) && !empty($_FILES['files']['name'])) {
	            	$rand = rand(0000,9999);
	                $fileInfo = pathinfo($_FILES["files"]["name"]);
	                $img_name = $id.$rand. '.' . $fileInfo['extension'];
	                $uplodad = move_uploaded_file($_FILES["files"]["tmp_name"], "./uploads/supporttikts/" . $img_name);
	                if($uplodad){

	                	$data = array(
	                		'user_id' =>$user_id ,
	                		'message' =>$message ,
	                		'subject' =>$subject ,
	                		'img_path' =>$img_name 

	                	);

	                	$insert = $this->user_model->insert_ticket($data);
	                	if($insert){

	                		$this->session->set_flashdata('support_tickect', 'Tickect Updated Successfully');
	                        redirect('user/support_ticket'); 	

	                	}else{
		                	$this->session->set_flashdata('support_tickect', 'Somthing went wrong !');
	                        redirect('user/support_ticket'); 
	                	}

	                }else{

		                $this->session->set_flashdata('support_tickect', 'Somthing went wrong in file upload');
                        redirect('user/support_ticket'); 	
	                }
	            }
		    }
		}
		
		public function support_history(){
		    
			$data['logged_in']=$this->logged_in;
			$id=$this->logged_in->user_id;
			
			//print_r($id); die();
			$data['ticket']=$this->user_model->get_support_ticket($id);
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/support-history');
			$this->load->view('user/panel/footer',$data);
		}	
		
		public function update_profile_img(){
		    
	        $id = $this->session->userdata('logged_in')->id;
           
            if(!empty($_FILES['profile'])){
                
            	$pimg=$_FILES['profile']['name'];
            	if(move_uploaded_file($_FILES['profile']['tmp_name'], "./uploads/profile/".$pimg)){
            	   
            	    $data = array('profile_pic' => $pimg);
            	    
            		//$update = mysqli_query($con,"UPDATE users SET profile_img='".$pimg."' WHERE refid='".$_SESSION['userid']."'");
            	    $update = $this->user_model->update_pofi_img($data,$id);
            	   	
            		
            		if ($update){
            			echo '1';
            		}
            		else{
            			echo '0';
            		}
            	}
            }		
		
		}
		
		
		public function view_support_ticket($id = null){
		    
		    $data['logged_in']=$this->logged_in;
		    $data['tckects_history_log'] =$this->user_model->get_tickect_log($id);
		    
	        $data['logged_in']=$this->logged_in;
			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/view-support-ticket',$data);
			$this->load->view('user/panel/footer',$data);

		}
		
		
		public function payment_success()
			{
				$data['logged_in']=$this->logged_in;
				
				// if($orderid != ''){
				
                $orderDetails=$this->db->select('*')->where('user_id',$this->logged_in->user_id)->where('status','pending')->order_by('id','desc')->get('purchase_package')->row();
                $invoiceStatus=$this->merchant_api->getInvoice($orderDetails->invoice_id);  
			        
				// }
				// else{
				//     $this->session->set_flashdata('message','<div class="alert alert-warning"> <strong>Notice !</strong> Incase your payment is deduct form your account please contact support team! </div>'.__LINE__);
				//     redirect('user/payment_success_page');
				// }
				
			    if(empty($invoiceStatus)){
			        $this->session->set_flashdata('message','<div class="alert alert-warning"> <strong>Notice !</strong> Incase your payment is deduct form your account please contact support team! </div>'.__LINE__);
				    redirect('user/payment_success_page');
			    }
			    
			    if($invoiceStatus['status']=='paid'){
			        $status='success';
			    }
			    else{
			        $status='pending';
			    }
			    
				$data = array(
					'order_id'=>$orderDetails->order_id,
					'user_id'=> $this->logged_in->user_id,
					'status'=>$status
				);
				
				$this->user_model->findDirectIncome($this->logged_in->user_id,$orderDetails->amount,$orderDetails->order_id);
					
				$updatePayment=$this->user_model->update_purchase_history($data);
				
				if($updatePayment==true && $invoiceStatus['status']=='paid'){
				    $this->session->set_flashdata('message','<div class="alert alert-success"> <strong>Payment Success !</strong> Payment successfully recieved !</div>');
				}
				else{
				    $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Payment Failed !</strong> Something went wrong please try again !</div>');
				}
				
				redirect('user/payment_success_page');
			}
			
		public function payment_success_page(){
			    $data=array();
			    $this->session->unset_userdata('processOrder');
				$this->session->unset_userdata('invoice_id');
			    $this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/pay_success',$data);
				$this->load->view('user/panel/footer',$data);
			}
		
		
        public function pay_now(){
            
			$data['logged_in']=$this->logged_in;
			$date=date('Y-m-d h:i:s');
			$order_id=date('Ymd').rand('0','9999');
			$amount=$this->uri->segment(4);
			$name=$this->uri->segment(3);
			$percent=$this->uri->segment(5);
			$user_id = $this->logged_in->user_id;
			$pac_id = $this->uri->segment(6);
			
			$this->session->unset_userdata('processOrder');
		    $this->session->unset_userdata('invoice_id');
			
			$check = $this->db->query("SELECT * FROM `purchase_package` WHERE `user_id` = '$user_id' && `package_id` = '$pac_id' && `status` = 'success'");
			
			if($check->num_rows() >0){
			    
			    $this->session->set_flashdata('purchage_ticket', "<script>alert('You have already purchase this package ')</script>");
			    return redirect('user/packages');
			    
			}
			else{
			    
			    $check_upper = $this->db->query("SELECT MAX(package_id) as max_pac_id FROM `purchase_package` WHERE `user_id` = '$user_id' && `status` = 'success'");
			    
			    $max_pack_id = $check_upper->row()->max_pac_id;
			    
			    $check_pur_or_not = $this->db->query("SELECT * FROM `packages` WHERE id > '$max_pack_id' && '$max_pack_id' < '$pac_id' ");
			    
			    /*   echo "<pre>"; print_r($check_pur_or_not->num_rows()); 
			    die();*/
			    
			    if($check_pur_or_not->num_rows() > 0){
    			    
                    $data = array(
        				'order_id' =>$order_id,
        				'user_id'=> $this->logged_in->user_id,
        				'package_id'=>$this->uri->segment(6),
        				'name'=>$name,
        				'amount'=>$amount,
        				'percent'=>$percent,
        				'type'=>'User',
        				'status'=>'pending',
        				'date'=>$date,
        			);
        				
        			//$this->user_model->insert_purchase_history($data);
            		$this->load->view('user/panel/top-header',$data);
        			$this->load->view('user/panel/header',$data);
        			$this->load->view('user/panel/left-sidebar',$data);
        			$this->load->view('user/pay_now',$data);            			    
    			    
			    }else{
			        
    			    $this->session->set_flashdata('purchage_ticket', "<script>alert('you can not purchase lower pakage')</script>");
    			    return redirect('user/packages');         			        
			       
    		    }
    	
			}
	
    	}
    	
    	public function createInvoice(){
    	    
    	    $packages=$this->user_model->get_package_details($this->input->post('package_id'));
    	    $success = base_url('user/payment_success');
    	    $failed = base_url('user/payment_failure');
    	    $order_id=date('Ymd').rand('0','9999');
    	    
    	    $options = array(
                'item_name'             => $packages->name,
                'order_id'              => $order_id,
                'item_description'      => $packages->name,
                'checkout_currency'     => 'bitcoin',
                'invoice_amount'        => $packages->amount,
                'invoice_currency'      => 'usd',
                'success_url'           => $success,
                'failed_url'            => $failed
            );
    	   
    	    $this->session->set_userdata('processOrder',$order_id);
    	    
    	    $setOrderid=$this->session->userdata('processOrder');
    	    
    	    if($setOrderid!==''){
    	        
        	    $invoicedata=(object) $this->merchant_api->createInvoice($options);
        	   
        	    if(!empty($invoicedata->invoice_id)){
        	        $this->session->set_userdata('invoice_id',$invoicedata->invoice_id);
        	        $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'projectbigscope@gmail.com',
                        'smtp_pass' => 'support@123',
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    
                    $this->email->from('projectbigscope@gmail.com', 'Bigscope');
                    $this->email->to($this->logged_in->user_email); 
                    $this->email->subject('Bigscope purchase package');
                    $this->email->message($order_id);  
                    $result = $this->email->send();
        	        
        	        if(true){
        	            $data = array(
                				'order_id' =>$order_id,
                				'user_id'=> $this->logged_in->user_id,
                				'package_id'=>$this->input->post('package_id'),
                				'name'=>$packages->name,
                				'amount'=>$packages->amount,
                				'percent'=>$packages->percent,
                				'invoice_id'=>$invoicedata->invoice_id,
                				'type'=>'User',
                				'status'=>'pending',
                				'date'=>date('Y-m-d H:i:s'),
            			);
            			
            			$this->user_model->insert_purchase_history($data);
            			
            	        //$this->db->where('order_id',$this->input->post('order_id'))->update('purchase_package',array('invoice_id'=>$invoicedata->invoice_id));
            	    }
            	    
            	    echo 'Processing...';
            	    
            	    echo "<script>window.location.href='https://www.cryptonator.com/merchant/invoice/$invoicedata->invoice_id'</script>";   
        	    }
        	    else{
        	        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Failed !</strong> Something went wrong please try again !</div>');
        	        redirect($_SERVER['HTTP_REFERER']);
        	    }
    	   }
    	   else{
    	        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Failed !</strong> Something went wrong please try again !</div>');
                redirect($_SERVER['HTTP_REFERER']);
    	   }
    	}
    	
    	public function packages(){
    	    
			$data['logged_in']=$this->logged_in;
			$data['packages']=$this->user_model->get_packages();
			$this->session->userdata('get_packages');
			
			//echo "<pre>"; print_r($data); die();

			$this->load->view('user/panel/top-header',$data);
			$this->load->view('user/panel/header',$data);
			$this->load->view('user/panel/left-sidebar',$data);
			$this->load->view('user/package',$data);
			$this->load->view('user/panel/footer',$data);
				
		}	
    				
		
		/*jai code end here*/
		

        // 	public function direct_income()
        // 		{
        // 			$data['logged_in']=$this->logged_in;
        			
        //             $data['direct_income']=$this->user_model->directIncome($this->logged_in->user_id);
                    
        // 			$this->load->view('user/panel/top-header',$data);
        // 			$this->load->view('user/panel/header',$data);
        // 			$this->load->view('user/panel/left-sidebar',$data);
        // 			$this->load->view('user/direct_income',$data);
        // 			$this->load->view('user/panel/footer',$data);
        // 		}
        

        public function direct_income()
    		{
    		     $data['logged_in']=$this->logged_in;
    		     $result['data'] = $this->user_model->get_direct_income($this->logged_in->user_id);
    			 $this->load->view('user/direct_income',$result);
    			 $this->load->view('user/panel/header');
    			 $this->load->view('user/panel/top-header');
    			 $this->load->view('user/panel/left-sidebar');
    			 $this->load->view('user/panel/footer'); 
    		}
    	

	    public function binary_income()
    		{
    				$data['logged_in']=$this->logged_in;
    				
    				$data['binary']=$this->user_model->get_binary_income($this->logged_in->user_id);
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/binary-income');
    				$this->load->view('user/panel/footer',$data);
    		}

	    public function direct_view()
    		{
    				$data['logged_in']=$this->logged_in;
    				
    
    				$data['direct']=$this->user_model->get_direct_user($this->logged_in->user_id);
    				  // print_r($data);die();
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/direct-view',$data);
    				$this->load->view('user/panel/footer',$data);
    		}

		public function level_view()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/direct-view');
    				$this->load->view('user/panel/footer',$data);
    		}

		public function token_history()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/token-history');
    				$this->load->view('user/panel/footer',$data);
    		}


		public function tree_view()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/binary-tree');
    				$this->load->view('user/panel/footer',$data);
    		}
		
		public function my_tokens()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/my-tokens');
    				$this->load->view('user/panel/footer',$data);
    		}
		
		public function token_value()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/token-value');
    				$this->load->view('user/panel/footer',$data);
    		}


		public function referal()
    		{
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/referral-link');
    				$this->load->view('user/panel/footer',$data);
    		}

		public function buy_now()
    		{
    
    				$data['logged_in']=$this->logged_in;
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/buy');
    				$this->load->view('user/panel/footer',$data);
    		}
		
		public function pay_buy()
    		{
    				$data['logged_in']=$this->logged_in; 
    				$this->session->set_userdata($data);
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/paybuy',$data);
    				$this->load->view('user/panel/footer',$data);
    		}
		
		public function purchase_history()
    		{
    				$data['logged_in']=$this->logged_in; 
    				$userid=$this->logged_in->user_id;
    				$this->session->userdata('get_packages');
    				$data['packages_history']=$this->user_model->get_purchase_history1($userid);
    				$this->load->view('user/panel/top-header',$data);
    				$this->load->view('user/panel/header',$data);
    				$this->load->view('user/panel/left-sidebar',$data);
    				$this->load->view('user/purchase_history');
    				$this->load->view('user/panel/footer',$data);
    		}
		
		public function payment_failure()
			{
				$data['logged_in']=$this->logged_in;
				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/pay_failure',$data);
				$this->load->view('user/panel/footer',$data);
			}
			
		public function activation_code()
  			{
                $data['logged_in']=$this->logged_in;
                $userid=$this->logged_in->user_id;
                $date=date('Y-m-d h:i:s');
                
                $n = $this->input->post('code');
                $i=1;
                
                $new_array = array();
                $a=implode($new_array);
                
                
                while( $i <= $n)
                {
                    $a = mt_rand(10000000, 99999999);
                    $data=array(
                    'greenid' =>$a,
                    'user_id'=>$userid,
                    'green_id_type'=>'user',
                    'sponser_id'=>'user',
                    'amount'=>'20',
                    'history'=>'no',
                    'status'=>'0',
                    'created_at'=>$date,
                    'updated_at'=>$date,
                    );
                    $this->user_model->insert_activation_code($data);
                    $total_amount=$total_amount-20*$i;
                    
                    $i++;
                }
                redirect('user/activation_code_history');
		  }
		  
  		public function generate_new_activation()
  			{
  					$data['logged_in']=$this->logged_in;
  					$this->load->view('user/panel/top-header',$data);
				    $this->load->view('user/panel/header',$data);
				    $this->load->view('user/panel/left-sidebar',$data);
  					$this->load->view('user/generate-activation-code');
  					$this->load->view('user/panel/footer',$data);
  			}

  		public function activation_code_history()
  			{
  				$data['logged_in']=$this->logged_in;
  				$data['code']=$this->user_model->get_activation_code();
				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/activation-code-history');
				$this->load->view('user/panel/footer',$data);
  			}

  		public function used_pin()
  			{
  				$data['logged_in']=$this->logged_in;
  				$data['code']=$this->user_model->used_activation_code();
				$this->load->view('user/panel/top-header',$data);
				$this->load->view('user/panel/header',$data);
				$this->load->view('user/panel/left-sidebar',$data);
				$this->load->view('user/used-activation-code');
				$this->load->view('user/panel/footer',$data);
  			}
  			
  		public function unused_pin()
  			{
                $data['logged_in']=$this->logged_in;
                $data['code']=$this->user_model->unused_activation_code();
                $this->load->view('user/panel/top-header',$data);
                $this->load->view('user/panel/header',$data);
                $this->load->view('user/panel/left-sidebar',$data);
                $this->load->view('user/unused-activation-code');
                $this->load->view('user/panel/footer',$data);
  			}
 
	}
?>