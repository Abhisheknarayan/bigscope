<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//time 10:21  , date-11-09-2019


// work for home Controller

class Jagdeep_controller extends CI_Controller {
    
    function _construct(){
        parent::_construct();
        $this->load->model('home_model');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->database();
    }
    
    
     public function registration()
    {
        
            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
     
            $userIp=$this->input->ip_address();
         
            $secret = $this->config->item('google_secret');
       
            $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
     
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);      
             
            $status= json_decode($output, true);
    if($status['success']) 
    {
                
             
         $date=date('Y-m-d h:i:s');
            $UserId = 'BI'.mt_rand(10000,99999); //random user id generation
            $str = "avBwPxCyZbfgDhiSjkPlmZnrNsFtu";
            $str=substr(str_shuffle($str),0,4);//for unique password
            $str1='@';
            $Password = $str.$str1.mt_rand(100,999);
            $user_type='user';
            $sponserid=$this->input->post('sponser');
            if($sponserid=='') 
			{
				$getsponserid = $this->Home_model->check_postion_id();
				if ($getsponserid >0) 
				{
					$sponserid=$getsponserid;
				}
				else
			{
				$sponserid='admin';
			}
						     	
			}

            if ($sponserid=='admin')
             {
                $greenid=$this->Home_model->checkActivationPin($this->input->post('green_id'));
        
            if($greenid == true)
            {

                $merge_mobile = $this->input->post('mobile_code').''.$this->input->post('mobile');
                 
                $data = array(
                'user_id'=>$UserId,
                'user_position'=>$this->input->post('position'),
                 'user_posid'=>$sponserid,
                'sponser_id'=>$sponserid,
                'user_type'=>$user_type,
                'first_name' =>$this->input->post('firstname'),
                'last_name'=>$this->input->post('lastname'),
                'user_email'=>$this->input->post('email'),
                'user_mobile'=>$merge_mobile,
                'password'=>$Password,
                'confirm_password'=>$Password,
                'green_pin'=>$this->input->post('green_id'),
                // 'user_status'=>$this->input->post('user_status'),
                'joining_date'=>$date,
                // 'last_login_date'=>$this->input->post('login_date'),
                );
                $this->session->set_userdata($data);
                $uniqiq_id = $this->Home_model->insert($data);
                $query=$this->db->get("users");
                $data['records']=$query->result();
                $this->session->set_flashdata('user_login','please check your mail to login');

                $data=array(
                    'username'=>$this->input->post('firstname'),
                    'last_name'=>$this->input->post('lastname'),
                    'password'=>$Password,
                    'candidate_id'=>$UserId,
                    'base_url'=>'http://bigscope.co/home/login',
                    'confirm_id'=>$UserId,
                    'register_date'=>$date,
                    'email'=>$this->input->post('email'),
                    'uniqiq_id'=>$uniqiq_id
                );

                $this->load->view('mail_function',$data,true);
                
                redirect('home/success');           
            }
            else
            {
                $this->session->set_flashdata('failed','Registration failed ! Wrong activation code Please Check your activation code and try again');
                    
                redirect('home/register');
                    
            }



        }



            else
            {
                $greenid=$this->Home_model->checkGreenid($this->input->post('green_id'));
        
            if($greenid == true){

                $merge_mobile = $this->input->post('mobile_code').''.$this->input->post('mobile');
                $mdpassword = md5($Password);
                $data = array(
                'user_id'=>$UserId,
                'user_position'=>$this->input->post('position'),
                 'user_posid'=>$this->Home_model->getLastChildOfLR($sponserid,$this->input->post('position')),
                'sponser_id'=>$sponserid,
                'user_type'=>$user_type,
                'first_name' =>$this->input->post('firstname'),
                'last_name'=>$this->input->post('lastname'),
                'user_email'=>$this->input->post('email'),
                'user_mobile'=>$merge_mobile,
                'password'=>$mdpassword,
                'confirm_password'=>$mdpassword,
                'green_pin'=>$this->input->post('green_id'),
                // 'user_status'=>$this->input->post('user_status'),
                'joining_date'=>$date,
                // 'last_login_date'=>$this->input->post('login_date'),
                );
                $this->session->set_userdata($data);
                $uniqiq_id = $this->Home_model->insert($data);
                $query=$this->db->get("users");
                $data['records']=$query->result();
                $this->session->set_flashdata('user_login','please check your mail to login');

                $data=array(
                    'username'=>$this->input->post('firstname'),
                    'last_name'=>$this->input->post('lastname'),
                    'password'=>$Password,
                    'candidate_id'=>$UserId,
                    'base_url'=>'http://bigscope.co/bigscope/home/login',
                    'confirm_id'=>$UserId,
                    'register_date'=>$date,
                    'email'=>$this->input->post('email'),
                    'uniqiq_id'=>$uniqiq_id
                );

                $this->load->view('mail_function',$data,true);
                
                redirect('home/success');           
            }
            else
            {
                $this->session->set_flashdata('failed','Registration failed ! Wrong activation code Please Check your activation code and try again');
                    
                redirect('home/register');
                    
            }


            }



    }



     else
            {
                
                $this->session->set_flashdata('user_login', 'Sorry Google Recaptcha Unsuccessful!!');
                redirect('home/register');
            }
}
          
       
       
         
          #work for admin controller
          
        public function activation_code()
              {
                $data['admin_logged_in']=$this->admin_logged_in;
               
                $date=date('Y-m-d h:i:s');
                $n = $this->input->post('amount');
                $i=1;
                $new_array = array();
                $a=implode($new_array);
                while( $i <= $n)
                {
                    
                    $a = mt_rand(10000000, 99999999);
                
        
                    $data=array(
                        'greenid' =>$a,
                        'user_id'=>'admin',
                        'green_id_type'=>'admin',
                        'sponser_id'=>'admin',
                        'amount'=>'admin',
                        'history'=>'no',
                        'status'=>'0',
                        'created_at'=>$date,
                        'updated_at'=>$date,
            
                     );
                     
                  $this->admin_model->insert_activation_code($data);
                  $i++;
                }
                
                redirect('admin/activation_code_history');
                
            }
}