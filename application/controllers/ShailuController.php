<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ShailuController extends CI_Controller {
    
    private $admin_logged_in;
    private $logged_in;
	private $get_packages;
	
    public function __construct()
		{
			// auth
				parent::__construct();
				$this->load->model('Shailu_model');
				$this->load->library('session');

				if(!$this->session->userdata('admin_logged_in'))
				{
					 redirect('home');	
				}
				else
				{
					$this->admin_logged_in=$this->session->userdata('admin_logged_in');
				}
		}
		
		public function income_constant_list()
		{
		     $result['data'] = $this->Shailu_model->get_constant_list('income');
			 $this->load->view('admin/income_constant',$result);
			 $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/panel/footer');
		}
		
		public function edit_constant_list($id)
		{
		     $result['data'] = $this->Shailu_model->constant_list_edit($id);
			 $this->load->view('admin/edit_income_constant',$result);
			 $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/panel/footer');
			 
		}
		
		public function edit_constant($id)
		{
		     $data = array(
		            "name" => $this->input->post('name'),
		            "type" => $this->input->post('type'),
		            "value" => $this->input->post('value'),
		            "symbol" => $this->input->post('symbol')
		            
		        );
		        
		     $result = $this->Shailu_model->edit_constant_row($id,$data);
		     
		     $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success!</strong></div>');
		     
		     if($result){
		         redirect('ShailuController/income_constant_list');
		     }
		     else{
		         return false;
		     }
		}
		
		public function daily_return()
		{
		     $result['data'] = $this->Shailu_model->get_daily_return();
			 $this->load->view('admin/daily-return',$result);
			 $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/panel/footer'); 
		}
        
    
		public function user_direct_income()
		{   
		     $data['logged_in'] = $this->logged_in;
	         $id = $this->session->userdata('logged_in')->user_id;
	         $result['data'] = $this->Shailu_model->get_user_direct_income($id);
			 $this->load->view('user/direct_income',$result);
			 $this->load->view('user/panel/header');
			 $this->load->view('user/panel/top-header');
			 $this->load->view('user/panel/left-sidebar');
			 $this->load->view('user/panel/footer'); 
		}
		public function token_launch()
		{   
			 $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/token-launch');
			 $this->load->view('admin/panel/footer');  
		}
		
		public function token_quantity()
		{
		    $data = array(
		            "qty" => $this->input->post('qty')
		        );
		    $result = $this->Shailu_model->launch_token($data);  
		    $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success!</strong></div>');
		    if($result){
		         redirect('ShailuController/token_launch');
		     }
		     else{
		         return false;
		     }
		}
		
		public function token_value_list()
		{
             $result['data'] = $this->Shailu_model->get_token_value_list();    		    
		     $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/token-value-list',$result);
			 $this->load->view('admin/panel/footer'); 
		}
		
		
		
		public function token_value()
		{
		     $this->load->view('admin/panel/header');
			 $this->load->view('admin/panel/top-header');
			 $this->load->view('admin/panel/left-sidebar');
			 $this->load->view('admin/token-value');
			 $this->load->view('admin/panel/footer'); 
		}
		
		public function tokenvalue()
		{
		    $data = array(
		            "future_value"=>$this->input->post('fut_val'),
		            "value" => $this->input->post('val'),
		            "from_date" => $this->input->post('from'),
		            "to_date" => $this->input->post('to')
		        );
		        
		    $result = $this->Shailu_model->value_token($data);  
		    $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success!</strong></div>');
		    if($result){
		         redirect('ShailuController/token_value');
		     }
		     else{
		         return false;
		     }
		}
		

		
}

?>