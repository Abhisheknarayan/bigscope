-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2019 at 04:22 AM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bigscope_mlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_code`
--

CREATE TABLE `activation_code` (
  `id` int(10) NOT NULL,
  `greenid` varchar(12) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `green_id_type` varchar(50) NOT NULL,
  `sponser_id` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `history` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0= Deactive, 1= active',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activation_code`
--

INSERT INTO `activation_code` (`id`, `greenid`, `user_id`, `green_id_type`, `sponser_id`, `amount`, `history`, `status`, `created_at`, `updated_at`) VALUES
(1, '33945896', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(2, '53545916', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(3, '32060264', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(4, '27765117', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(5, '63964650', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(6, '45527733', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(7, '68832195', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(8, '61042788', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(9, '72558801', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(10, '13063819', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(11, '61413151', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(12, '26547671', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(13, '60283438', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(14, '73841376', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(15, '76876900', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(16, '89694940', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(17, '70681948', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(18, '65066282', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(19, '60670051', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(20, '75755200', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(21, '39662118', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(22, '91225007', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(23, '25651329', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(24, '66817074', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(25, '90722324', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(26, '74009012', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(27, '29885225', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(28, '18475150', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(29, '40867358', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(30, '66605597', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(31, '70315952', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(32, '46278166', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(33, '88910568', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(34, '81770401', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(35, '75705432', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(36, '92745320', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(37, '80920185', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(38, '70275665', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(39, '76256397', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(40, '43017924', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(41, '96181580', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(42, '78509829', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(43, '63840616', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(44, '65923512', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(45, '24504381', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(46, '95201554', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(47, '94660432', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(48, '53341093', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(49, '46041875', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(50, '69313471', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 03:34:12', '2019-09-11 03:34:12'),
(51, '80329640', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(52, '58094532', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(53, '98611109', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(54, '63543238', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(55, '78732174', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(56, '45918266', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(57, '21183420', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(58, '14568335', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(59, '64314928', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(60, '77630556', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(61, '10427899', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:05', '2019-09-11 04:12:05'),
(62, '88088509', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:58', '2019-09-11 04:12:58'),
(63, '57588631', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:58', '2019-09-11 04:12:58'),
(64, '99963794', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:58', '2019-09-11 04:12:58'),
(65, '67424643', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:12:58', '2019-09-11 04:12:58'),
(66, '36685716', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 04:13:34', '2019-09-11 04:13:34'),
(67, '67808335', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 05:43:53', '2019-09-11 05:43:53'),
(68, '42806387', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-11 05:43:53', '2019-09-11 05:43:53'),
(69, '93746214', 'BI48052', 'user', 'user', '20', 'no', '1', '2019-09-11 05:54:25', '2019-09-11 05:54:25'),
(70, '70040714', 'BI48052', 'user', 'user', '20', 'no', '1', '2019-09-11 05:54:25', '2019-09-11 05:54:25'),
(71, '71436434', 'BI98352', 'user', 'user', '20', 'no', '1', '2019-09-12 09:40:43', '2019-09-12 09:40:43'),
(72, '89829522', 'BI98352', 'user', 'user', '20', 'no', '1', '2019-09-12 09:40:43', '2019-09-12 09:40:43'),
(73, '48302131', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 10:44:11', '2019-09-12 10:44:11'),
(74, '17016424', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 10:44:11', '2019-09-12 10:44:11'),
(75, '57135636', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 10:44:11', '2019-09-12 10:44:11'),
(76, '47244684', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 11:24:28', '2019-09-12 11:24:28'),
(77, '26491433', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 11:24:28', '2019-09-12 11:24:28'),
(78, '29976028', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 11:24:28', '2019-09-12 11:24:28'),
(79, '68741798', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 11:24:28', '2019-09-12 11:24:28'),
(80, '28688238', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 11:24:28', '2019-09-12 11:24:28'),
(81, '65179109', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(82, '98297806', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(83, '39407333', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(84, '70942808', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(85, '31720886', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(86, '60076422', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(87, '19962466', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(88, '24461722', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(89, '23415157', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(90, '63044963', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-12 01:02:30', '2019-09-12 01:02:30'),
(91, '69318058', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-12 02:36:40', '2019-09-12 02:36:40'),
(92, '11591363', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-12 02:36:40', '2019-09-12 02:36:40'),
(93, '79991567', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-12 02:36:40', '2019-09-12 02:36:40'),
(94, '94817815', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-12 02:36:40', '2019-09-12 02:36:40'),
(95, '75741010', 'admin', 'admin', 'admin', '20', 'no', '1', '2019-09-12 02:36:40', '2019-09-12 02:36:40'),
(96, '40013351', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(97, '14665748', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(98, '39437214', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(99, '94435245', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(100, '20133096', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(101, '69866125', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(102, '83258812', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(103, '86673750', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(104, '64011474', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(105, '26966411', 'BI98352', 'user', 'user', '20', 'no', '0', '2019-09-12 06:20:44', '2019-09-12 06:20:44'),
(106, '28957882', 'admin', 'admin', 'admin', '20', 'no', '0', '2019-09-16 03:13:41', '2019-09-16 03:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(16) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `cpass` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `pwr` varchar(255) NOT NULL,
  `hide` varchar(255) NOT NULL DEFAULT '0',
  `sid` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `fullname`, `password`, `cpass`, `email`, `phone`, `pwr`, `hide`, `sid`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 'admin@gmail.com', '7878454454', '', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `binary_capping`
--

CREATE TABLE `binary_capping` (
  `id` int(10) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `max_amount` varchar(10) NOT NULL,
  `capping_percent` varchar(3) NOT NULL,
  `capping_amount` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `binary_capping`
--

INSERT INTO `binary_capping` (`id`, `amount`, `max_amount`, `capping_percent`, `capping_amount`, `status`, `create_date`, `update_date`) VALUES
(1, '50', '99', '50', '25', '', '2019-09-13 11:39:55', '2019-09-13 11:39:55'),
(2, '100', '199', '50', '50', '', '2019-09-13 11:40:24', '2019-09-13 11:40:24'),
(3, '200', '399', '50', '100', '', '2019-09-13 17:14:02', '2019-09-13 17:14:02'),
(4, '400', '799', '50', '200', '', '2019-09-13 17:14:02', '2019-09-13 17:14:02'),
(5, '800', '1599', '50', '400', '', '2019-09-13 17:14:05', '2019-09-13 17:14:05'),
(6, '1600', '3199', '50', '800', '', '2019-09-13 17:14:05', '2019-09-13 17:14:05'),
(7, '3200', '6399', '50', '1600', '', '2019-09-13 17:15:26', '2019-09-13 17:15:26'),
(8, '6400', '100000', '50', '3200', '', '2019-09-13 17:15:26', '2019-09-13 06:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `constant`
--

CREATE TABLE `constant` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `value` varchar(10) NOT NULL,
  `symbol` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constant`
--

INSERT INTO `constant` (`id`, `name`, `type`, `value`, `symbol`) VALUES
(1, 'coin', '', '1', '$'),
(2, 'Daily Income', 'income ', '1', '%'),
(3, 'Direct Income', 'income ', '5', '%'),
(4, 'Binary Income', 'income', '10', '%');

-- --------------------------------------------------------

--
-- Table structure for table `cookie_info`
--

CREATE TABLE `cookie_info` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `continent_name` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `currency_symbol` varchar(100) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `browser` varchar(100) NOT NULL,
  `created_date` varchar(50) NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cookie_info`
--

INSERT INTO `cookie_info` (`id`, `ip_address`, `country`, `city`, `continent_name`, `latitude`, `longitude`, `currency_symbol`, `currency_code`, `timezone`, `browser`, `created_date`, `updated_date`) VALUES
(1, '122.177.47.99', 'India', 'Ghaziabad', 'Asia', '28.6667', '77.4333', '?', 'INR', 'Asia/Kolkata', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.13', '2019-09-19', '2019-09-19'),
(2, '45.115.105.1', 'India', 'Delhi', 'Asia', '28.6667', '77.2167', '?', 'INR', 'Asia/Kolkata', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132', '2019-09-19', '2019-09-19'),
(3, '34.69.201.210', 'United States', '', 'North America', '37.751', '-97.822', '$', 'USD', '', 'python-requests/2.22.0', '2019-09-20', '2019-09-20'),
(4, '171.48.32.228', 'India', 'Ghaziabad', 'Asia', '28.6667', '77.4333', '?', 'INR', 'Asia/Kolkata', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.13', '2019-09-20', '2019-09-20'),
(5, '34.217.123.209', 'United States', 'Boardman', 'North America', '45.8491', '-119.7143', '$', 'USD', 'America/Los_Angeles', 'Go-http-client/2.0', '2019-09-20', '2019-09-20'),
(6, '52.38.168.35', 'United States', 'Boardman', 'North America', '45.8491', '-119.7143', '$', 'USD', 'America/Los_Angeles', 'Go-http-client/1.1', '2019-09-20', '2019-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, 0, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `daily_return_income`
--

CREATE TABLE `daily_return_income` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `green_pin` varchar(255) NOT NULL,
  `user_status` enum('pending','success') NOT NULL DEFAULT 'success',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `direct_income_user_list`
--

CREATE TABLE `direct_income_user_list` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `direct_user` varchar(255) DEFAULT NULL,
  `package_amount` varchar(255) NOT NULL,
  `percent` varchar(255) DEFAULT NULL,
  `direct_amount` varchar(255) NOT NULL,
  `status` enum('pending','success') NOT NULL DEFAULT 'pending',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `direct_income_user_list`
--

INSERT INTO `direct_income_user_list` (`id`, `order_id`, `user_id`, `direct_user`, `package_amount`, `percent`, `direct_amount`, `status`, `date`) VALUES
(33, '201909113494', 'BI78126', 'BI98352', '50', '5', '2.5', 'success', '2019-09-11 11:56:01'),
(34, '20190911140', 'BI78126', 'BI98352', '50', '5', '2.5', 'success', '2019-09-11 11:56:29'),
(35, '201909114543', 'BI78126', 'BI98352', '100', '5', '5', 'success', '2019-09-11 12:23:04'),
(36, '201909117005', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 12:24:04'),
(37, '201909112190', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 12:24:28'),
(38, '20190911310', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 12:26:38'),
(39, '201909115133', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 12:44:50'),
(40, '201909117206', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 12:53:44'),
(41, '201909115924', 'BI78126', 'BI98352', '400', '5', '20', 'success', '2019-09-11 12:55:46'),
(42, '201909117684', 'BI78126', 'BI98352', '3200', '5', '160', 'success', '2019-09-11 12:57:11'),
(43, '201909118040', 'BI78126', 'BI98352', '200', '5', '10', 'success', '2019-09-11 12:58:52'),
(44, '201909114003', 'BI78126', 'BI98352', '200', '5', '10', 'success', '2019-09-11 12:59:24'),
(45, '201909118932', 'BI78126', 'BI98352', '6400', '5', '320', 'success', '2019-09-11 13:00:22'),
(46, '201909119981', 'BI78126', 'BI98352', '1600', '5', '80', 'success', '2019-09-11 13:04:43'),
(47, '201909111824', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 13:06:53'),
(48, '201909116824', 'BI78126', 'BI98352', '400', '5', '20', 'success', '2019-09-11 13:10:28'),
(49, '201909111507', 'BI78126', 'BI98352', '400', '5', '20', 'success', '2019-09-11 13:11:45'),
(50, '201909115132', 'BI78126', 'BI98352', '800', '5', '40', 'success', '2019-09-11 13:12:42'),
(51, '201909117209', 'BI78126', 'BI98352', '6400', '5', '320', 'success', '2019-09-11 13:13:42'),
(52, '201909127276', 'BI98352', 'BI13057', '200', '5', '10', 'success', '2019-09-12 04:27:00'),
(53, '20190912825', 'BI13057', 'BI23412', '100', '5', '5', 'success', '2019-09-12 04:33:48'),
(54, '201909127925', '', 'fthhrj', '50', '5', '2.5', 'success', '2019-09-12 06:24:05'),
(55, '201909124183', '', 'dfhtnht', '3200', '5', '160', 'success', '2019-09-12 06:48:11'),
(56, '201909129536', 'BI13057', 'BI94613', '1', '5', '0.05', 'success', '2019-09-12 10:50:41');

-- --------------------------------------------------------

--
-- Table structure for table `direct_percent`
--

CREATE TABLE `direct_percent` (
  `id` int(11) NOT NULL,
  `sno` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `percent` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `direct_percent`
--

INSERT INTO `direct_percent` (`id`, `sno`, `level`, `percent`, `date`) VALUES
(1, '1', '1', '10', '2019-09-10 04:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `green_master`
--

CREATE TABLE `green_master` (
  `id` int(10) NOT NULL,
  `greenid` varchar(12) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0= Deactive, 1= active',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `green_master`
--

INSERT INTO `green_master` (`id`, `greenid`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'GR100131', '1', '1', '2019-08-22 09:58:20', '2019-08-22 09:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `income_status`
--

CREATE TABLE `income_status` (
  `id` int(10) NOT NULL,
  `member_id` varchar(20) NOT NULL,
  `name` text NOT NULL,
  `posid` varchar(10) NOT NULL,
  `carry_forward` float NOT NULL,
  `total_amount` varchar(10) NOT NULL,
  `income_type` varchar(5) NOT NULL DEFAULT '1:1',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `date_` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income_status`
--

INSERT INTO `income_status` (`id`, `member_id`, `name`, `posid`, `carry_forward`, `total_amount`, `income_type`, `status`, `date_`) VALUES
(99, 'BI48052', 'BI13057,BI94613,BI23412', 'L', 0, '', '2:1', '0', '2019-09-18 16:28:50'),
(100, 'BI13057', 'BI94613,BI60522,BI48419', 'L', 0, '', '2:1', '0', '2019-09-18 16:28:50'),
(101, 'BI48052', 'BI48419,BI66610,BI19992', 'L', 0, '', '2:1', '0', '2019-09-18 16:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `launch_token`
--

CREATE TABLE `launch_token` (
  `id` int(10) NOT NULL,
  `qty` varchar(20) NOT NULL,
  `sold_token` varchar(10) NOT NULL,
  `launch_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `launch_token`
--

INSERT INTO `launch_token` (`id`, `qty`, `sold_token`, `launch_date`) VALUES
(1, '10000000', '500000', '2019-09-07 17:02:05');

-- --------------------------------------------------------

--
-- Table structure for table `my_income1`
--

CREATE TABLE `my_income1` (
  `id` int(11) NOT NULL,
  `usid` varchar(255) NOT NULL,
  `capping_amt` varchar(40) NOT NULL,
  `matching_amt` varchar(40) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `leftmatching` int(11) NOT NULL,
  `rightmatching` int(11) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `tm` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `income_type` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_income1`
--

INSERT INTO `my_income1` (`id`, `usid`, `capping_amt`, `matching_amt`, `amount`, `leftmatching`, `rightmatching`, `percentage`, `tm`, `status`, `income_type`) VALUES
(63, 'BI48052', '200', '6450', '50', 3851, 0, '25', '2019-09-18 16:28:50', 1, '1:2'),
(64, 'BI13057', '50', '50', '12.5', 301, 0, '25', '2019-09-18 16:28:50', 0, '2:1'),
(65, 'BI48052', '100', '100', '25', 3751, 0, '25', '2019-09-18 16:29:25', 0, '2:1');

-- --------------------------------------------------------

--
-- Table structure for table `notification_msg`
--

CREATE TABLE `notification_msg` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `message` text NOT NULL,
  `subject` text NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `msg_view_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `percent` varchar(255) NOT NULL,
  `days` int(11) NOT NULL,
  `withdraw_limit` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `amount`, `percent`, `days`, `withdraw_limit`, `date`) VALUES
(1, 'PRIMARY', '50', '1', 200, '20', '2019-09-13 07:51:55'),
(2, 'SILVER', '100', '1', 200, '20', '2019-09-13 07:52:14'),
(3, 'GOLD', '200', '1', 200, '20', '2019-09-13 07:52:31'),
(4, 'PLATINUM', '400', '1', 200, '20', '2019-09-13 07:52:50'),
(5, 'DIAMOND', '800', '1', 200, '20', '2019-09-13 07:55:43'),
(6, 'CROWN', '1600', '1', 200, '20', '2019-09-13 07:53:37'),
(7, 'RUBY', '3200', '1', 200, '20', '2019-09-13 07:53:51'),
(8, 'STAR', '6400', '1', 200, '20', '2019-09-13 07:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_package`
--

CREATE TABLE `purchase_package` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `package_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `days` varchar(50) NOT NULL,
  `percent` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` enum('pending','success') NOT NULL DEFAULT 'pending',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `withdraw_limit` int(11) NOT NULL,
  `expire_status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1 = active && 0 = expire'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_package`
--

INSERT INTO `purchase_package` (`id`, `order_id`, `user_id`, `package_id`, `name`, `amount`, `days`, `percent`, `type`, `status`, `date`, `withdraw_limit`, `expire_status`) VALUES
(140, '201909116266', 'BI98352', '1', 'PRIMARY', '50', '', '1.5', 'User', 'pending', '2019-09-11 05:25:43', 0, '1'),
(141, '201909113494', 'BI98352', '1', 'PRIMARY', '50', '', '1.5', 'User', 'success', '2019-09-11 11:56:01', 0, '1'),
(142, '20190911374', 'BI98352', '1', 'PRIMARY', '50', '', '1.5', 'User', 'pending', '2019-09-11 05:26:11', 0, '1'),
(143, '20190911457', 'BI98352', '1', 'PRIMARY', '50', '', '1.5', 'User', 'pending', '2019-09-11 05:26:15', 0, '1'),
(144, '20190911140', 'BI98352', '1', 'PRIMARY', '50', '', '1.5', 'User', 'success', '2019-09-11 11:56:29', 0, '1'),
(152, '201909116902', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'pending', '2019-09-11 05:49:59', 0, '1'),
(153, '201909112419', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'pending', '2019-09-11 05:50:07', 0, '1'),
(154, '201909111595', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'pending', '2019-09-11 05:50:18', 0, '1'),
(155, '201909114543', 'BI98352', '2', 'SILVER', '100', '', '1.5', 'User', 'success', '2019-09-11 12:23:04', 0, '1'),
(156, '201909117005', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 12:24:04', 0, '1'),
(157, '201909112190', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 12:24:28', 0, '1'),
(158, '201909118299', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 05:55:34', 0, '1'),
(159, '201909117499', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 05:56:03', 0, '1'),
(160, '20190911310', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 12:26:38', 0, '1'),
(161, '201909119784', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 06:01:49', 0, '1'),
(164, '201909111031', 'BI98352', '3', 'GOLD', '200', '', '1.5', 'User', 'pending', '2019-09-11 06:10:48', 0, '1'),
(165, '201909115133', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 12:44:50', 0, '1'),
(166, '201909115779', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 06:16:17', 0, '1'),
(167, '201909117206', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 12:53:44', 0, '1'),
(168, '201909117631', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 06:24:39', 0, '1'),
(169, '201909115924', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'success', '2019-09-11 12:55:46', 0, '1'),
(170, '201909117684', 'BI98352', '7', 'RUBY', '3200', '', '1.5', 'User', 'success', '2019-09-11 12:57:11', 0, '1'),
(171, '201909118040', 'BI98352', '3', 'GOLD', '200', '', '1.5', 'User', 'success', '2019-09-11 12:58:52', 0, '1'),
(172, '201909114003', 'BI98352', '3', 'GOLD', '200', '', '1.5', 'User', 'success', '2019-09-11 12:59:24', 0, '1'),
(173, '201909118932', 'BI98352', '8', 'STAR', '6400', '', '1.5', 'User', 'success', '2019-09-11 13:00:22', 0, '1'),
(174, '201909112617', 'BI98352', '8', 'STAR', '6400', '', '1.5', 'User', 'pending', '2019-09-11 06:30:26', 0, '1'),
(175, '201909119981', 'BI98352', '6', 'CROWN', '1600', '', '1.5', 'User', 'success', '2019-09-11 13:04:43', 0, '1'),
(176, '201909114565', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'pending', '2019-09-11 06:36:31', 0, '1'),
(177, '201909111824', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 13:06:53', 0, '1'),
(178, '201909116824', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'success', '2019-09-11 13:10:28', 0, '1'),
(179, '201909111507', 'BI98352', '4', 'PLATINUM', '400', '', '1.5', 'User', 'success', '2019-09-11 13:11:45', 0, '1'),
(180, '201909115132', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'User', 'success', '2019-09-11 13:12:42', 0, '1'),
(181, '201909117209', 'BI98352', '8', 'STAR', '6400', '', '1.5', 'User', 'success', '2019-09-11 13:13:42', 0, '1'),
(183, '201909127276', 'BI13057', '3', 'GOLD', '200', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:36:53', 0, '1'),
(184, '20190912825', 'BI23412', '2', 'SILVER', '100', '', '1.5', 'User', 'success', '2019-09-12 04:33:48', 0, '1'),
(190, '20190912307', 'BI78126', '3', 'GOLD', '200', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:36:40', 0, '1'),
(191, '201909124921', 'BI98352', '5', 'DIAMOND', '800', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:37:19', 0, '1'),
(192, '201909126668', 'BI48052', '1', 'PRIMARY', '50', '', '1.5', 'User', 'pending', '2019-09-12 12:43:14', 0, '1'),
(193, '201909127783', 'BI98352', '6', 'CROWN', '1600', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:37:33', 0, '1'),
(194, '201909121224', 'BI13057', '4', 'PLATINUM', '400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:29:26', 0, '1'),
(195, '201909126393', 'BI48052', '8', 'STAR', '6400', '', '1.5', 'User', 'pending', '2019-09-12 01:22:56', 0, '1'),
(197, '201909129536', 'BI94613', '1', 'PRIMARY', '1', '', '1', 'User', 'success', '2019-09-12 10:50:41', 0, '1'),
(199, '201909126228', 'BI48052', '1', 'PRIMARY', '1', '', '1', 'User', 'pending', '2019-09-12 05:33:29', 0, '1'),
(200, '201909124044', 'BI48052', '1', 'PRIMARY', '1', '', '1', 'User', 'pending', '2019-09-12 05:33:45', 0, '1'),
(201, '201909124475', 'BI48052', '8', 'STAR', '6400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:29:12', 0, '1'),
(206, '201909138409', 'BI13057', '7', 'RUBY', '3200', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 09:25:32', 0, '1'),
(209, '201909139999', 'BI19992', '2', 'planid1', '100', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 04:25:02', 0, '1'),
(210, '201909133022', 'BI13057', '2', 'planid1', '100', '', '1.5', 'Paid-by-admin', 'success', '2019-09-13 04:50:53', 0, '1'),
(213, '201909152118', 'BI48419', '1', 'STAR', '50', '', '1.5', 'Paid-by-admin', 'success', '2019-09-15 11:48:07', 0, '1'),
(223, '201909164170', 'BI13057', '5', 'DIAMOND', '800', '', '1', 'User', 'pending', '2019-09-16 05:34:08', 0, '1'),
(224, '201909163442', 'BI13057', '6', 'CROWN', '1600', '', '1', 'User', 'pending', '2019-09-16 05:49:01', 0, '1'),
(225, '201909164900', 'BI13057', '6', 'CROWN', '1600', '', '1', 'User', 'pending', '2019-09-16 06:22:00', 0, '1'),
(226, '201909161947', 'BI13057', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-16 06:22:06', 0, '1'),
(227, '201909164283', 'BI13057', '8', 'STAR', '6400', '', '1', 'User', 'pending', '2019-09-16 06:22:20', 0, '1'),
(228, '201909178544', 'BI13057', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-17 10:06:44', 0, '1'),
(229, '201909179011', 'BI13057', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-17 10:28:19', 0, '1'),
(230, '201909176686', 'BI13057', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-17 10:28:25', 0, '1'),
(231, '201909172158', 'BI13057', '8', 'STAR', '6400', '', '1', 'User', 'pending', '2019-09-17 10:28:29', 0, '1'),
(232, '201909171130', 'BI13057', '6', 'CROWN', '1600', '', '1', 'User', 'pending', '2019-09-17 10:28:32', 0, '1'),
(233, '201909177687', 'BI13057', '8', 'STAR', '6400', '', '1', 'User', 'pending', '2019-09-17 10:47:27', 0, '1'),
(234, '201909176313', 'BI66610', '1', 'STAR', '50', '', '1.5', 'Paid-by-admin', 'success', '2019-09-17 12:12:46', 0, '1'),
(235, '201909177765', 'BI60522', '4', 'STAR', '400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-17 12:31:41', 0, '1'),
(236, '201909181188', 'BI13057', '8', 'STAR', '6400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-18 10:04:18', 0, '1'),
(237, '201909188669', 'BI23412', '8', 'STAR', '6400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-18 10:05:45', 0, '1'),
(238, '201909188197', 'BI23412', '8', 'STAR', '6400', '', '1.5', 'Paid-by-admin', 'success', '2019-09-18 10:09:04', 0, '1'),
(239, '201909191837', 'BI15834', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-19 10:21:20', 0, '1'),
(240, '201909199129', 'BI15834', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-19 10:48:34', 0, '1'),
(241, '201909193670', 'BI15834', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-19 10:52:20', 0, '1'),
(242, '201909196865', 'BI15834', '1', 'PRIMARY', '50', '', '1', 'User', 'pending', '2019-09-19 12:24:03', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket`
--

CREATE TABLE `support_ticket` (
  `id` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `subject` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=close, 1=open',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `img_path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_ticket`
--

INSERT INTO `support_ticket` (`id`, `user_id`, `message`, `subject`, `status`, `date`, `img_path`) VALUES
(6, 'BI98352', 'gbuihdig', 'jaiid', '0', '2019-09-09 12:29:20', '10.jpg'),
(7, 'BI98352', 'dfhodjhiofg', 'xfgjdfh', '0', '2019-09-09 12:30:08', '10.png'),
(11, 'BI53647', 'vere mess', 'Veer', '0', '2019-09-10 06:58:51', '20241.jpg'),
(12, 'BI39930', 'Test issue', 'test', '0', '2019-09-10 07:50:27', '191247.png'),
(16, 'BI98352', 'sdgsfgsdh', 'dgsgdf', '0', '2019-09-12 12:42:34', '107690.png');

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket_log`
--

CREATE TABLE `support_ticket_log` (
  `id` int(11) NOT NULL,
  `support_id` int(11) NOT NULL,
  `reply_by` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=admin, 2=user',
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_ticket_log`
--

INSERT INTO `support_ticket_log` (`id`, `support_id`, `reply_by`, `message`, `date`) VALUES
(1, 17, '1', 'hi aji', '0000-00-00 00:00:00'),
(2, 11, '1', 'bdfjbdyudvyudbj', '2019-09-11 07:44:47'),
(3, 3, '1', 'etepjriohr', '2019-09-11 07:45:45'),
(4, 1, '1', 'efjsbxbjksdbfg\r\nsgsdhdt', '2019-09-11 07:45:54'),
(5, 12, '1', 'qfodsfhibyui dfgfgyud', '2019-09-11 07:46:02'),
(6, 16, '1', 'afuzgyusgd7fsd eet4eyrey', '2019-09-11 07:46:11'),
(7, 17, '1', 'dfugd djh', '2019-09-11 07:46:17'),
(8, 17, '1', 'egiuyrgyudg rgreye', '2019-09-11 07:46:22'),
(9, 19, '1', 'ok jaiav', '2019-09-11 08:59:51'),
(10, 19, '1', 'hiisfvs syfgudbf', '2019-09-11 08:59:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_extra_roi`
--

CREATE TABLE `tbl_extra_roi` (
  `id` int(11) NOT NULL,
  `register_id` text NOT NULL,
  `plan_id` int(11) NOT NULL,
  `roi_amount` varchar(20) NOT NULL,
  `roi_date` datetime NOT NULL,
  `percentage` text NOT NULL,
  `admin_status` int(11) NOT NULL,
  `package_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_extra_roi`
--

INSERT INTO `tbl_extra_roi` (`id`, `register_id`, `plan_id`, `roi_amount`, `roi_date`, `percentage`, `admin_status`, `package_id`) VALUES
(10, 'BI98352', 1, '0.015', '2019-09-12 09:59:45', '1.5', 1, 0),
(11, 'BI98352', 2, '1.5', '2019-09-12 09:59:45', '1.5', 1, 0),
(12, 'BI98352', 5, '12', '2019-09-12 09:59:45', '1.5', 1, 0),
(13, 'BI98352', 4, '6', '2019-09-12 09:59:45', '1.5', 1, 0),
(14, 'BI98352', 7, '48', '2019-09-12 09:59:45', '1.5', 1, 0),
(15, 'BI98352', 3, '3', '2019-09-12 09:59:45', '1.5', 1, 0),
(16, 'BI98352', 8, '96', '2019-09-12 09:59:45', '1.5', 1, 0),
(17, 'BI98352', 6, '24', '2019-09-12 09:59:45', '1.5', 1, 0),
(18, 'BI13057', 3, '3', '2019-09-12 09:59:45', '1.5', 1, 0),
(19, 'BI48052', 8, '64', '2019-09-13 00:07:03', '1', 1, 0),
(20, 'BI78126', 3, '2', '2019-09-13 00:07:03', '1', 1, 0),
(21, 'BI98352', 1, '0.5', '2019-09-13 00:07:03', '1', 1, 0),
(22, 'BI98352', 2, '1', '2019-09-13 00:07:03', '1', 1, 0),
(23, 'BI98352', 5, '8', '2019-09-13 00:07:03', '1', 1, 0),
(24, 'BI98352', 4, '4', '2019-09-13 00:07:03', '1', 1, 0),
(25, 'BI98352', 7, '32', '2019-09-13 00:07:03', '1', 1, 0),
(26, 'BI98352', 3, '2', '2019-09-13 00:07:03', '1', 1, 0),
(27, 'BI98352', 8, '64', '2019-09-13 00:07:03', '1', 1, 0),
(28, 'BI98352', 6, '16', '2019-09-13 00:07:03', '1', 1, 0),
(29, 'BI13057', 3, '2', '2019-09-13 00:07:03', '1', 1, 0),
(30, 'BI13057', 4, '4', '2019-09-13 00:07:03', '1', 1, 0),
(31, 'BI23412', 2, '1', '2019-09-13 00:07:03', '1', 1, 0),
(32, 'BI94613', 1, '0.5', '2019-09-13 00:07:03', '1', 1, 0),
(33, 'BI48052', 8, '64', '2019-09-16 15:18:26', '1', 1, 201),
(34, 'BI13057', 3, '2', '2019-09-16 15:18:26', '1', 1, 183),
(35, 'BI13057', 4, '4', '2019-09-16 15:18:26', '1', 1, 194),
(36, 'BI13057', 7, '32', '2019-09-16 15:18:26', '1', 1, 206),
(37, 'BI13057', 2, '1', '2019-09-16 15:18:26', '1', 1, 210),
(38, 'BI23412', 2, '1', '2019-09-16 15:18:26', '1', 1, 184),
(39, 'BI19992', 2, '1', '2019-09-16 15:18:26', '1', 1, 209),
(40, 'BI19992', 6, '16', '2019-09-16 15:18:26', '1', 1, 211),
(41, 'BI94613', 1, '0.5', '2019-09-16 15:18:26', '1', 1, 197),
(42, 'BI48419', 1, '0.5', '2019-09-16 15:18:26', '1', 1, 213),
(43, 'BI48052', 8, '64', '2019-09-18 16:26:08', '1', 1, 201),
(44, 'BI13057', 3, '2', '2019-09-18 16:26:08', '1', 1, 183),
(45, 'BI13057', 4, '4', '2019-09-18 16:26:08', '1', 1, 194),
(46, 'BI13057', 7, '32', '2019-09-18 16:26:08', '1', 1, 206),
(47, 'BI13057', 2, '1', '2019-09-18 16:26:08', '1', 1, 210),
(48, 'BI13057', 8, '64', '2019-09-18 16:26:08', '1', 1, 236),
(49, 'BI23412', 2, '1', '2019-09-18 16:26:08', '1', 1, 184),
(50, 'BI23412', 8, '64', '2019-09-18 16:26:08', '1', 1, 237),
(51, 'BI19992', 2, '1', '2019-09-18 16:26:08', '1', 1, 209),
(52, 'BI94613', 1, '0.5', '2019-09-18 16:26:08', '1', 1, 197),
(53, 'BI66610', 1, '0.5', '2019-09-18 16:26:08', '1', 1, 234),
(54, 'BI48419', 1, '0.5', '2019-09-18 16:26:08', '1', 1, 213),
(55, 'BI60522', 4, '4', '2019-09-18 16:26:08', '1', 1, 235),
(56, 'BI48052', 8, '64', '2019-09-20 09:48:03', '1', 1, 201),
(57, 'BI13057', 3, '2', '2019-09-20 09:48:03', '1', 1, 183),
(58, 'BI13057', 4, '4', '2019-09-20 09:48:03', '1', 1, 194),
(59, 'BI13057', 7, '32', '2019-09-20 09:48:03', '1', 1, 206),
(60, 'BI13057', 2, '1', '2019-09-20 09:48:03', '1', 1, 210),
(61, 'BI13057', 8, '64', '2019-09-20 09:48:03', '1', 1, 236),
(62, 'BI23412', 2, '1', '2019-09-20 09:48:03', '1', 1, 184),
(63, 'BI23412', 8, '64', '2019-09-20 09:48:03', '1', 1, 237),
(64, 'BI19992', 2, '1', '2019-09-20 09:48:03', '1', 1, 209),
(65, 'BI94613', 1, '0.5', '2019-09-20 09:48:03', '1', 1, 197),
(66, 'BI66610', 1, '0.5', '2019-09-20 09:48:03', '1', 1, 234),
(67, 'BI48419', 1, '0.5', '2019-09-20 09:48:03', '1', 1, 213),
(68, 'BI60522', 4, '4', '2019-09-20 09:48:03', '1', 1, 235);

-- --------------------------------------------------------

--
-- Table structure for table `token_history`
--

CREATE TABLE `token_history` (
  `id` int(10) NOT NULL,
  `value` varchar(5) NOT NULL,
  `future_value` varchar(50) NOT NULL,
  `from_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL COMMENT '0=''Deactive'',''1''=Active'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token_history`
--

INSERT INTO `token_history` (`id`, `value`, `future_value`, `from_date`, `to_date`, `status`) VALUES
(1, '1', '2', '2019-09-19 00:00:00', '2020-09-19 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `token_transaction_history`
--

CREATE TABLE `token_transaction_history` (
  `id` int(11) NOT NULL,
  `transaction_key` varchar(255) NOT NULL,
  `to_wallet_address` varchar(255) NOT NULL COMMENT 'transfering user',
  `value` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `date_` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Success','Pending','Failed') NOT NULL,
  `from_wallet_address` varchar(255) NOT NULL,
  `transaction_type` enum('1','2') NOT NULL COMMENT '1=sales,2=purchase'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token_transaction_history`
--

INSERT INTO `token_transaction_history` (`id`, `transaction_key`, `to_wallet_address`, `value`, `amount`, `date_`, `status`, `from_wallet_address`, `transaction_type`) VALUES
(4, '4F44EF7C273C730157B449D0B88914AC7069E9774580F8F8CD8F0EBFFF07E6AB', 'BI98352', '330', '330', '2019-09-13 00:16:39', 'Success', 'admin', '2'),
(5, '0BAE968BE2ED532DBC5604F1EE0A68F8DB1B890871931C2858CAB9062F0E4DD9', 'BI48052', '70', '70', '2019-09-13 13:12:35', 'Success', 'admin', '2'),
(13, 'CBC80ADD81D167033161D3625275B81E43FFC39C738F1F3D0FCC1A3224D499EE', 'BI13057', '1', '1', '2019-09-13 17:54:22', 'Success', 'admin', '2'),
(12, '8E7CAE01B5091AF65163DC68D88EAA30262D05E64DE1DF6D419B4C0A94833EA7', 'BI13057', '2', '2', '2019-09-13 17:51:12', 'Success', 'admin', '2'),
(11, '6304A3025DD224F5AED9B1CDE441535EBC45DCB6AC52261C87AF6560FDF34181', 'BI13057', '2', '2', '2019-09-13 17:44:31', 'Success', 'admin', '2');

-- --------------------------------------------------------

--
-- Table structure for table `upgradeplan_user`
--

CREATE TABLE `upgradeplan_user` (
  `id` int(11) NOT NULL,
  `plan_id` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `package_id` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_position` varchar(50) NOT NULL,
  `user_posid` varchar(255) DEFAULT NULL,
  `sponser_id` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_mobile` varchar(50) NOT NULL,
  `green_pin` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirm_password` varchar(255) NOT NULL,
  `profile_pic` text NOT NULL,
  `bitcoin_wallet_address` varchar(100) DEFAULT NULL,
  `user_status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `binary_status` enum('0','1') NOT NULL,
  `joining_date` varchar(255) NOT NULL,
  `last_login_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_position`, `user_posid`, `sponser_id`, `user_type`, `first_name`, `last_name`, `user_email`, `user_mobile`, `green_pin`, `password`, `confirm_password`, `profile_pic`, `bitcoin_wallet_address`, `user_status`, `binary_status`, `joining_date`, `last_login_date`) VALUES
(1, 'BI48052', 'R', 'admin', 'admin', 'user', 'Admin', '', '', '', 'GR100131', 'e10adc3949ba59abbe56e057f20f883e', 'tSPC@354', '', NULL, 'active', '0', '2019-09-06 10:29:26', ''),
(27, 'BI13057', 'L', 'BI48052', 'BI48052', 'user', 'lalit', 'rawat', 'lalit22890@gmail.com', '+919899786326', '53545916', 'e6e061838856bf47e1de730719fb2609', 'e6e061838856bf47e1de730719fb2609', 'banner-design.png', '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', 'active', '0', '2019-09-11 04:00:26', ''),
(28, 'BI23412', 'R', 'BI48052', 'BI48052', 'user', 'song', 'world', 'songworld799@gmail.com', '+919899786326', '88088509', 'a7a6909bccea0de39788c9ca29d90740', 'a7a6909bccea0de39788c9ca29d90740', '', '', 'active', '0', '2019-09-11 04:14:27', ''),
(29, 'BI25561', 'R', 'BI50332', 'BI98352', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com1', '8375011900', '67424643', '4ef51acb7b9a4f8063812cbd3510c19c', '4ef51acb7b9a4f8063812cbd3510c19c', '', '', 'active', '0', '2019-09-11 04:39:13', ''),
(31, 'BI19992', 'L', 'BI23412', 'BI23412', 'user', 'milkyway', 'multidimensional', 'veditamilkyway@gmail.com', '7532021982', '99963794', '74521694c27e8d07e295baa4d118fc08', '74521694c27e8d07e295baa4d118fc08', '', NULL, 'active', '0', '2019-09-11 06:31:23', ''),
(32, 'BI45843', 'R', 'BI25561', 'BI98352', 'user', 'Abhishek', 'narayan', 'abhisekh.milkyway@gmail.com', '8375011800', '33945896', 'cbffa4d80e1e74e805cd24f3f001745e', 'cbffa4d80e1e74e805cd24f3f001745e', '', NULL, 'active', '0', '2019-09-12 09:52:46', ''),
(33, 'BI15834', 'L', 'BI13057', 'BI98352', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com2', '+918375011800', '53545916', 'e10adc3949ba59abbe56e057f20f883e', '6e4e168d82f215ed5fb329c013cf01bb', '', NULL, 'active', '0', '2019-09-12 10:34:29', ''),
(34, 'BI47642', 'L', 'BI45843', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com3', '+918375011800', '32060264', '6454dc22dad43ee1b775f119ccb66dd1', '6454dc22dad43ee1b775f119ccb66dd1', '', NULL, 'inactive', '0', '2019-09-12 10:46:26', ''),
(35, 'BI25672', 'L', 'BI19992', 'BI23412', 'user', 'Jaiveer', 'Gupta', 'phpdevloperjaiveer@gmail.com', '+917400232198', '43017924', '5016d5b0cb4f6ff4a624d76b6f481c22', '5016d5b0cb4f6ff4a624d76b6f481c22', '', NULL, 'inactive', '0', '2019-09-12 10:46:52', ''),
(36, 'BI42964', 'L', 'BI47642', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com4', '+918375011800', '27765117', '4b1dedea4e5333d689ff7a3ca0d94254', '4b1dedea4e5333d689ff7a3ca0d94254', '', NULL, 'inactive', '0', '2019-09-12 10:52:01', ''),
(37, 'BI85978', 'L', 'BI42964', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com5', '+918375011800', '63964650', 'ae3444118fba69c7b71a9b37aa56886a', 'ae3444118fba69c7b71a9b37aa56886a', '', NULL, 'inactive', '0', '2019-09-12 11:14:43', ''),
(38, 'BI18816', 'L', 'BI85978', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com6', '+918375011800', '45527733', '9bfec837370f8076667fa5d726876b04', '9bfec837370f8076667fa5d726876b04', '', NULL, 'inactive', '0', '2019-09-12 11:18:08', ''),
(39, 'BI93122', 'L', 'BI18816', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com7', '+918375011800', '68832195', '97928aa58df83f2a30459aa7ba0ff6f6', '97928aa58df83f2a30459aa7ba0ff6f6', '', NULL, 'inactive', '0', '2019-09-12 11:26:26', ''),
(40, 'BI83164', 'L', 'BI93122', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheshnarayan@gmail.com', '+918375011800', '61042788', '6a8aad087a6e714b99207f2cd994ccbe', '6a8aad087a6e714b99207f2cd994ccbe', '', NULL, 'inactive', '0', '2019-09-12 11:36:55', ''),
(41, 'BI68163', 'L', 'BI83164', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheknarayan2017@gmail.com1', '+918375011800', '72558801', '302947609e3e73e2793601da03502dbf', '302947609e3e73e2793601da03502dbf', '', NULL, 'inactive', '0', '2019-09-12 11:39:41', ''),
(42, 'BI99159', 'L', 'BI68163', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheknarayan2017@gmail.com2', '+918375011800', '13063819', 'a0296380f853f5b3673bf22b3e75d58a', 'a0296380f853f5b3673bf22b3e75d58a', '', NULL, 'inactive', '0', '2019-09-12 11:44:33', ''),
(43, 'BI27030', 'L', 'BI99159', 'BI45843', 'user', 'Abhishek', 'narayan', 'abhisheknarayan2017@gmail.com', '+918375011800', '61413151', 'e8cc6243716f7a8ff8944250057029bb', 'e8cc6243716f7a8ff8944250057029bb', '', NULL, 'inactive', '0', '2019-09-12 11:52:50', ''),
(44, 'BI10634', 'R', 'BI23412', 'BI13057', 'user', 'ankit', 'sinha', 'ankit.sinha25@gmail.com1', '+918607248802', '26547671', '48721689f2f8b1045e8360d5e0bf0f22', '48721689f2f8b1045e8360d5e0bf0f22', '', NULL, 'active', '0', '2019-09-12 11:57:55', ''),
(45, 'BI80090', 'L', 'BI27030', 'BI45843', 'user', 'Abhishek', 'narayan', 'ankit.sinha25@gmail.com', '+918375011800', '73841376', 'ff35dd70662dd7f5fb1a54bde46172ce', 'ff35dd70662dd7f5fb1a54bde46172ce', '', NULL, 'inactive', '0', '2019-09-12 12:02:55', ''),
(46, 'BI94613', 'L', 'BI15834', 'BI13057', 'user', 'milkyway', 'multidimensional', 'veditamilkyway@gmail.com', '+917532021982', '60283438', '0751df37f27f780dfab51ed32d5adba6', '0751df37f27f780dfab51ed32d5adba6', '', NULL, 'active', '0', '2019-09-12 03:54:45', ''),
(47, 'BI66610', 'R', 'BI13057', 'BI13057', 'user', 'sudhanshu', 'baluni', 'Sudhanshu.milkyway@gmail.com', '+915785544245', '76876900', 'c20b493085b91dc45e426051bdcf7187', 'c20b493085b91dc45e426051bdcf7187', '', NULL, 'active', '0', '2019-09-13 01:26:34', ''),
(48, 'BI57964', 'R', 'BI10634', 'BI23412', 'user', 'Rajesh', 'Rana', 'phpdevloperjaiveer@gmail.com', '+917400232198', '89694940', 'e6e061838856bf47e1de730719fb2609', 'e6e061838856bf47e1de730719fb2609', '', NULL, 'active', '0', '2019-09-13 03:37:25', ''),
(49, 'BI48419', 'R', 'BI66610', 'BI13057', 'user', 'jenny', 'fire', 'songworld799@gmail.com', '+919899786326', '70681948', '7de7debb86c622b7af5dde994c0ea9d4', '7de7debb86c622b7af5dde994c0ea9d4', '', NULL, 'active', '0', '2019-09-15 11:41:20', ''),
(51, 'BI60522', 'l', 'BI94613', 'BI13057', 'user', 'deepak', 'ranga', 'deepak@milkywayservices.com', '+917878888888', '65066282', '1fc060dc72a23e0911ce62a2207b16f5', '1fc060dc72a23e0911ce62a2207b16f5', '', NULL, 'active', '0', '2019-09-17 11:58:44', '');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_request`
--

CREATE TABLE `withdrawal_request` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `request_amount` bigint(20) NOT NULL,
  `btc_address` text NOT NULL,
  `request_date` date NOT NULL,
  `btc_value` decimal(30,8) NOT NULL,
  `admin_charge` int(11) NOT NULL COMMENT 'admin charge in %',
  `admin_btc_value` decimal(30,8) NOT NULL COMMENT 'admin charge',
  `total_pay_amt` decimal(30,8) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2' COMMENT '2=new,1=accept, 0=decline',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdrawal_request`
--

INSERT INTO `withdrawal_request` (`id`, `user_id`, `request_amount`, `btc_address`, `request_date`, `btc_value`, `admin_charge`, `admin_btc_value`, `total_pay_amt`, `message`, `status`, `update_at`) VALUES
(1, 'BI13057', 2, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-12', '0.00019904', 10, '0.00001990', '0.00000000', 'test', 0, '2019-09-12 10:02:23'),
(2, 'BI13057', 3, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-12', '0.00029627', 10, '0.00002963', '0.00000000', '', 2, '2019-09-12 10:02:42'),
(3, 'BI13057', 1, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-12', '0.00009876', 10, '0.00000988', '0.00008888', 'accept', 1, '2019-09-12 10:12:01'),
(4, 'BI13057', 2, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-12', '0.00019752', 10, '0.00001975', '0.00017777', 'accept', 1, '2019-09-12 10:12:05'),
(5, 'BI13057', 3, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-12', '0.00029857', 10, '0.00002986', '0.00026871', 'cnaccel', 0, '2019-09-12 10:12:09'),
(6, 'BI13057', 1, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-13', '0.00009684', 10, '0.00000968', '0.00008716', 'cavvg', 0, '2019-09-13 09:42:17'),
(7, 'BI13057', 1, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-13', '0.00009681', 10, '0.00000968', '0.00008713', 'cancel', 0, '2019-09-13 09:55:55'),
(8, 'BI13057', 20, '35oW8xJsSTNkgqeRDDyG5XEq34skd3jrQy', '2019-09-17', '0.00196627', 10, '0.00019663', '0.00176964', 'test', 0, '2019-09-17 12:03:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activation_code`
--
ALTER TABLE `activation_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `binary_capping`
--
ALTER TABLE `binary_capping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constant`
--
ALTER TABLE `constant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cookie_info`
--
ALTER TABLE `cookie_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_return_income`
--
ALTER TABLE `daily_return_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_income_user_list`
--
ALTER TABLE `direct_income_user_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_percent`
--
ALTER TABLE `direct_percent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `green_master`
--
ALTER TABLE `green_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income_status`
--
ALTER TABLE `income_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `launch_token`
--
ALTER TABLE `launch_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_income1`
--
ALTER TABLE `my_income1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_msg`
--
ALTER TABLE `notification_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_package`
--
ALTER TABLE `purchase_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_ticket`
--
ALTER TABLE `support_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_ticket_log`
--
ALTER TABLE `support_ticket_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_extra_roi`
--
ALTER TABLE `tbl_extra_roi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token_history`
--
ALTER TABLE `token_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token_transaction_history`
--
ALTER TABLE `token_transaction_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upgradeplan_user`
--
ALTER TABLE `upgradeplan_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawal_request`
--
ALTER TABLE `withdrawal_request`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activation_code`
--
ALTER TABLE `activation_code`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `binary_capping`
--
ALTER TABLE `binary_capping`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `constant`
--
ALTER TABLE `constant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cookie_info`
--
ALTER TABLE `cookie_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `daily_return_income`
--
ALTER TABLE `daily_return_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direct_income_user_list`
--
ALTER TABLE `direct_income_user_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `direct_percent`
--
ALTER TABLE `direct_percent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `green_master`
--
ALTER TABLE `green_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `income_status`
--
ALTER TABLE `income_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `launch_token`
--
ALTER TABLE `launch_token`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `my_income1`
--
ALTER TABLE `my_income1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `notification_msg`
--
ALTER TABLE `notification_msg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchase_package`
--
ALTER TABLE `purchase_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `support_ticket`
--
ALTER TABLE `support_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `support_ticket_log`
--
ALTER TABLE `support_ticket_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_extra_roi`
--
ALTER TABLE `tbl_extra_roi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `token_history`
--
ALTER TABLE `token_history`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `token_transaction_history`
--
ALTER TABLE `token_transaction_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `upgradeplan_user`
--
ALTER TABLE `upgradeplan_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `withdrawal_request`
--
ALTER TABLE `withdrawal_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
