$(function() {

// user filter -----------------------------------------
	$('#userid').keyup(function(){
		filter_user();
	});
	$('#planid').change(function(){
		filter_user();
	});
	$('#date_from').change(function(){
		filter_user();
	});
	$('#date_to').change(function(){
		filter_user();
	});

	function filter_user(){
		var user=$("#userid").val();
		var planid=$("#planid").val();
		var date_from=$("#date_from").val();
		var date_to=$("#date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&action=user_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}

//transaction filter ------------------------------------------------
	$('#trans_userid').keyup(function(){
		trans_filter_user();
	});
	$('#trans_planid').change(function(){
		trans_filter_user();
	});
	$('#trans_date_from').change(function(){
		trans_filter_user();
	});
	$('#trans_date_to').change(function(){
		trans_filter_user();
	});

	function trans_filter_user(){
		var user=$("#trans_userid").val();
		var planid=$("#trans_planid").val();
		var date_from=$("#trans_date_from").val();
		var date_to=$("#trans_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&action=trans_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}

// roi filter--------------------------------------------------------
	$('#roi_userid').keyup(function(){
		roi_filter_user();
	});
	$('#roi_planid').change(function(){
		roi_filter_user();
	});
	$('#roi_date_from').change(function(){
		roi_filter_user();
	});
	$('#roi_date_to').change(function(){
		roi_filter_user();
	});

	function roi_filter_user(){
		var user=$("#roi_userid").val();
		var planid=$("#roi_planid").val();
		var date_from=$("#roi_date_from").val();
		var date_to=$("#roi_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&action=roi_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}

// level filter-------------------------------------------------
	$('#level_userid').keyup(function(){
		level_filter_user();
	});
	$('#level_planid').keyup(function(){
		level_filter_user();
	});
	$('#level_level').keyup(function(){
		level_filter_user();
	});
	$('#level_roiper').keyup(function(){
		level_filter_user();
	});
	$('#level_level').change(function(){
		level_filter_user();
	});
	$('#level_roiper').change(function(){
		level_filter_user();
	});
	$('#level_date_from').change(function(){
		level_filter_user();
	});
	$('#level_date_to').change(function(){
		level_filter_user();
	});

	function level_filter_user(){
		var user=$("#level_userid").val();
		var planid=$("#level_planid").val();
		var level=$("#level_level").val();
		var roiper=$("#level_roiper").val();
		var date_from=$("#level_date_from").val();
		var date_to=$("#level_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&level='+level+'&roiper='+roiper+'&action=level_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}


// withdraw filter -----------------------------------------
	$('#withdraw_userid').keyup(function(){
		withdraw_filter_user();
	});
	$('#withdraw_planid').change(function(){
		withdraw_filter_user();
	});
	$('#withdraw_date_from').change(function(){
		withdraw_filter_user();
	});
	$('#withdraw_date_to').change(function(){
		withdraw_filter_user();
	});

	function withdraw_filter_user(){
		var user=$("#withdraw_userid").val();
		var planid=$("#withdraw_planid").val();
		var date_from=$("#withdraw_date_from").val();
		var date_to=$("#withdraw_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&status='+planid+'&from='+date_from+'&to='+date_to+'&action=withdraw_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}


	// jackpot willing filter -----------------------------------------
	$('#willing_userid').keyup(function(){
		willing_filter_user();
	});
	$('#willing_planid').change(function(){
		willing_filter_user();
	});
	$('#willing_jackpot').change(function(){
		willing_filter_user();
	});
	$('#willing_date_from').change(function(){
		willing_filter_user();
	});
	$('#willing_date_to').change(function(){
		willing_filter_user();
	});

	function willing_filter_user(){
		var user=$("#willing_userid").val();
		var planid=$("#willing_planid").val();
		var jackpot=$("#willing_jackpot").val();
		var date_from=$("#willing_date_from").val();
		var date_to=$("#willing_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&jackpot='+jackpot+'&action=willing_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}


	// jackpot willing filter -----------------------------------------
	$('#jackpot_userid').keyup(function(){
		jackpot_filter_user();
	});
	$('#jackpot_planid').change(function(){
		jackpot_filter_user();
	});
	$('#jackpot_jackpot').change(function(){
		jackpot_filter_user();
	});
	$('#jackpot_date_from').change(function(){
		jackpot_filter_user();
	});
	$('#jackpot_date_to').change(function(){
		jackpot_filter_user();
	});

	function jackpot_filter_user(){
		var user=$("#jackpot_userid").val();
		var planid=$("#jackpot_planid").val();
		var jackpot=$("#jackpot_jackpot").val();
		var date_from=$("#jackpot_date_from").val();
		var date_to=$("#jackpot_date_to").val();

		$.ajax({
			type:'post',
			url:'ajax.php',
			data:'userid='+user+'&planid='+planid+'&from='+date_from+'&to='+date_to+'&jackpot='+jackpot+'&action=jackpot_filter',
			
			success:function(data){
				$(".actual_data").css('display','none');
				$(".filter_data").css('display','block');
				$(".filter_data").html(data);
			}
		});
	}
});