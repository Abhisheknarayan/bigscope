$(function() {
    "use strict";

	// chart 1
	
	$('.dashboard3-chart-1').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#008cff'
        });
		
	// chart 2
	
		$('.dashboard3-chart-2').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#fd3550'
        });
		
	// chart 3	
		
		$('.dashboard3-chart-3').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#15ca20'
        });
		
	// chart 4	
		
		$('.dashboard3-chart-4').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#ff9700'
        });
		
	// chart 5	
		
		$('.dashboard3-chart-5').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#0dceec'
        });
		
	// chart 6	
		
		$('.dashboard3-chart-6').sparkline([5,8,7,10,9,10,8,6,4,6,8,7,6,8,9,10,8,6,4,5,8,7,10,9,5,8,7,9,5,4], {
            type: 'bar',
            height: '25',
            barWidth: '2',
            resize: true,
            barSpacing: '2',
            barColor: '#223035'
        });
	
	
	// chart 7
 
 

	  
	  
// worl map

jQuery('#dashboard-map').vectorMap(
{
    map: 'world_mill_en',
    backgroundColor: 'transparent',
    borderColor: '#818181',
    borderOpacity: 0.25,
    borderWidth: 1,
    zoomOnScroll: false,
    color: '#009efb',
    regionStyle : {
        initial : {
          fill : '#ff9700'
        }
      },
    markerStyle: {
      initial: {
                    r: 9,
                    'fill': '#fff',
                    'fill-opacity':1,
                    'stroke': '#000',
                    'stroke-width' : 5,
                    'stroke-opacity': 0.4
                },
                },
    enableZoom: true,
    hoverColor: '#009efb',
    markers : [{
        latLng : [21.00, 78.00],
        name : 'Lorem Ipsum Dollar'
      
      }],
    hoverOpacity: null,
    normalizeFunction: 'linear',
    scaleColors: ['#b6d6ff', '#005ace'],
    selectedColor: '#c9dfaf',
    selectedRegions: [],
    showTooltip: true,
});	  
	  

// chart 8

  $("#dashboard3-chart-8").sparkline([3,5,3,7,5,10,3,6,5,0], {
            type: 'line',
            width: '100',
            height: '40',
            lineWidth: '2',
            lineColor: '#0dceec',
            fillColor: 'rgba(13, 206, 236, 0.2)',
            spotColor: '#0dceec',
    }); 

  
// chart 9    

  $("#dashboard3-chart-9").sparkline([3,5,3,7,5,10,3,6,5,0], {
            type: 'line',
            width: '100',
            height: '40',
            lineWidth: '2',
            lineColor: '#ff9700',
            fillColor: 'rgba(255, 151, 0, 0.2)',
            spotColor: '#ff9700',
    }); 

// chart 10    

  $("#dashboard3-chart-10").sparkline([3,5,3,7,5,10,3,6,5,0], {
            type: 'line',
            width: '100',
            height: '40',
            lineWidth: '2',
            lineColor: '#15ca20',
            fillColor: 'rgba(21, 202, 32, 0.2)',
            spotColor: '#15ca20',
    }); 

 // chart 11 
	
	
	
	
	  

});
      
	  